#!/bin/sh
#
# ups.sh This shell script takes care of that starting or stoping ups daemon
#
clear_debug_file()
{
	local filesize=`/usr/bin/du -k /tmp/usbdebug|/bin/awk '{printf $1}'`
	if [ $filesize -gt 10 ]; then
		/bin/mv /tmp/usbdebug /tmp/usbdebug.1
	fi
}
DAEMON_TYPE=`/sbin/getcfg "UPS" "Daemon Type"`
UPS_CONF_DIR=/etc/config/ups
UPS_CONF_DEFAULT=/etc/default_config/ups
UPPID=`/bin/grep PPid: /proc/$$/status|/bin/awk -F ' ' '{printf $2}'`

check_upsd_conf()
{
	[ -f $UPS_CONF_DIR/upsd.conf ] || return 0
	/bin/grep "^LISTEN" $UPS_CONF_DIR/upsd.conf > /dev/null 2>&1
	[ $? = 0 ] || echo "LISTEN 0.0.0.0" >> $UPS_CONF_DIR/upsd.conf
}

try_ups_drivers()
{
	[ -d /usr/local/ups/bin ] || return 1
	if [ "x$1" = "x" ] && [ "x$2" = "x" ]; then
		return 1
	fi
	if [ "x$1" = "x0x" ] || [ "x$2" = "x0x" ]; then
		return 1
	fi
	local cwd=$PWD
	local drv
	echo "$UPPID::Try [$1:$2]" >> /tmp/usbdebug
	cd /usr/local/ups/bin
	for drv in *; do
		/sbin/setcfg qnapups driver $drv -f /etc/config/ups/ups.conf
		vendorid=`printf "%.4x" $1`
		productid=`printf "%.4x" $2`
		echo "$UPPID::/usr/local/ups/bin/$drv -a qnapups -u admin -x vendorid=$vendorid -x productid=$productid" >> /tmp/usbdebug
		/usr/local/ups/bin/$drv -a qnapups -u admin -x vendorid=$vendorid -x productid=$productid
		if [ $? = 0 ]; then
			UPS_DRV=$drv
			printf "%s,%s,%s\n" $1 $2 $drv >> /etc/config/ups/upsdrv.map
			return 0
		fi
	done
	UPS_DRV=
	printf "%s,%s,NOT_UPS\n" $1 $2 >> /etc/config/ups/upsdrv.map
	return 1
}
clear_debug_file
case "$1" in
  start)
	UPS_ENABLE=`/sbin/getcfg UPS Enable -u -d FALSE`

	if [ "$2" != "" ]; then
		USB_UPS_LCK=/var/lock/usb_ups_$2.lck
	elif [ "$3" != "" ]; then
		if [ "$4" != "" ]; then
			USB_UPS_LCK=/var/lock/usb_ups_$3_$4.lck
		else
			USB_UPS_LCK=/var/lock/usb_ups_$3.lck
		fi
	else
		USB_UPS_LCK=/var/lock/usb_ups.lck
	fi
	echo "$UPPID::USB_UPS_LCK of [$@] is $USB_UPS_LCK" >> /tmp/usbdebug

	if [ ! -d ${UPS_CONF_DIR} ]; then
                [ ! -f ${UPS_CONF_DIR} ] || /bin/rm ${UPS_CONF_DIR}
                /bin/mkdir ${UPS_CONF_DIR}
                [ -d ${UPS_CONF_DEFAULT} ] && /bin/cp ${UPS_CONF_DEFAULT}/* ${UPS_CONF_DIR}
        fi

        # Start daemons.
	echo -n "Starting UPS monitoring:" 
	if [ -f ${USB_UPS_LCK} ]; then
		echo $0 is running
		exit 0
	fi
	/bin/touch ${USB_UPS_LCK}
	echo "$UPPID::ups start....." >> /tmp/usbdebug
	#wait for kernel driver
	sleep 3
	if [ -f /var/run/upsisrunning  ]; then

                echo $UPPID::ups driver is running.

		if [ "$UPS_ENABLE" = "FALSE" ]
		then
                	echo disabled
			/bin/rm ${USB_UPS_LCK}
            	    	exit 0
	        fi

		if [ $DAEMON_TYPE = "3" ]
	       	then
			/bin/kill -USR1 `/sbin/pidof upsutil`
	       	fi

		/bin/rm ${USB_UPS_LCK}
                exit 0
        fi
	LOAD_DRV_RET="1"
	UPS_DRV=$2
	if [ "$UPS_DRV" != "" ]; then
		if [ ! -x /usr/local/ups/bin/$UPS_DRV ]; then
			/bin/rm ${USB_UPS_LCK}
			exit 0
		fi
		/sbin/setcfg qnapups driver $UPS_DRV -f /etc/config/ups/ups.conf
		echo "$UPPID::FIX::/usr/local/ups/bin/$UPS_DRV -a qnapups -u admin" >> /tmp/usbdebug
		/usr/local/ups/bin/$UPS_DRV -a qnapups -u admin
		LOAD_DRV_RET=$?
	fi
#	while [ 1 ]; do
#		/usr/bin/upsdrvctl -u admin start
#		echo "upsdrvctl start....." >> /tmp/usbdebug
#		if [ $? = 0 ]; then
#			echo "success...." >> /tmp/usbdebug
#			break;
#		fi
#		/bin/sleep 1
#	done
	
	echo "$UPPID::LOAD_DRV_RET $LOAD_DRV_RET" >> /tmp/usbdebug
	if [ "$LOAD_DRV_RET" != "0" ]; then
		UPS_DRV=""
		if [ "$3" != "" ] && [ "$4" != "" ]; then
			try_ups_drivers "$3" "$4"
		fi
	fi
	#check ups driver is dead or not
	/bin/sleep 3
	ups_drv_pid=
	ups_drv_pid=`/bin/pidof $UPS_DRV`
	echo "$UPPID::ups_drv_pid is $ups_drv_pid" >> /tmp/usbdebug
	if [ "$ups_drv_pid" = "" ]; then
		try_ups_drivers
#		/usr/bin/upsdrvctl -u admin start
		ups_drv_pid=`/bin/pidof $UPS_DRV`
		echo "$UPPID::upsdrvctl start after dead.....$ups_drv_pid" >> /tmp/usbdebug
	else
		/sbin/setcfg UPS "Server Mode" TRUE
		/sbin/setcfg UPS "Daemon Type" 3
		/bin/kill -USR1 `/sbin/pidof upsutil`
	fi
	echo $ups_drv_pid > /var/run/upsdrv.pid

	if [ "x$ups_drv_pid" = "x" ]; then
		if [ "$UPS_ENABLE" != "FALSE" ]; then
			[ $DAEMON_TYPE = "3" ] && /bin/kill -USR1 `/sbin/pidof upsutil`
		fi
		/bin/rm ${USB_UPS_LCK}
		exit 0
	fi
	check_upsd_conf
	/sbin/daemon_mgr upsd start "/usr/sbin/upsd -u admin" 2>/dev/null
	/bin/touch /var/run/upsisrunning

	if [ "$UPS_ENABLE" = "FALSE" ]
	then
                echo disabled
		/bin/rm ${USB_UPS_LCK}
                exit 0
        fi
	
	if [ $DAEMON_TYPE = "3" ]
        then
		/bin/kill -USR1 `/sbin/pidof upsutil`
	fi

	/bin/rm ${USB_UPS_LCK}
        ;;
  stop)
        # Stop daemons.
	echo -n "Shutting down UPS monitoring:"

	#/sbin/daemon_mgr upsmon stop "/usr/sbin/upsmon" 2>/dev/null 1>/dev/null	

	/usr/bin/upsdrvctl stop

	/sbin/daemon_mgr upsd stop "/usr/sbin/upsd" 2> /dev/null 1>/dev/null

	/bin/kill `pidof poweroff` 2> /dev/null

	/bin/kill -USR2 `/sbin/pidof upsutil`
	ups_drv_pid=`/bin/cat /var/run/upsdrv.pid` 2>/dev/null
	/bin/rm /var/run/upsdrv.pid
	/bin/kill -9 $ups_drv_pid

	/sbin/setcfg UPS "Server Mode" FALSE
	
	/bin/rm -f /var/run/upsisrunning
        ;;
  restart)
	$0 stop
	$0 start
	;;
  *)
        echo "Usage: $0 {start|stop|restart}"
        exit 1
esac


