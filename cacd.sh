#!/bin/sh
# For Administration

	[ -f /usr/bin/cacd ] || exit 0

case "$1" in
	"start")
		echo -n "Start cacd:"
		/sbin/daemon_mgr cacd start "/usr/bin/cacd &" 2>/dev/null > /dev/null
		echo -n " cacd"
		echo "."
		;;

	"stop")
		echo -n "Shutting down cacd:"
		/sbin/daemon_mgr cacd stop /usr/bin/cacd 2>/dev/null > /dev/null
		echo -n " cacd"
		echo "."
		;;

	"restart")
		"$0" stop
		"$0" start
		;;

	*)
        	echo "Usage: /etc/init.d/cacd.sh {start|stop|restart}"
        	exit 1
esac

exit 0
