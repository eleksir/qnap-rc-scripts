#!/bin/sh
RSYNCD_CONF="/etc/config/rsyncd.conf"
#RSYNC_SCHEDULE_CONF="/etc/config/rsync_schedule.conf"
rsyncd_port=`/sbin/getcfg "" "port" -f "${RSYNCD_CONF}" -d "873"`
rsyncd_user=`/sbin/getcfg "" "rsync user" -f "${RSYNCD_CONF}" -d "rsync"`
rsyncd_pswd=`/sbin/getcfg "" "rsync pswd" -f "${RSYNCD_CONF}" -d ""`
rsyncd_slimitrate=`/sbin/getcfg "" "SLimitRate" -f "${RSYNCD_CONF}" -d ""`
rsyncd_rlimitrate=`/sbin/getcfg "" "RLimitRate" -f "${RSYNCD_CONF}" -d ""`

echo "uid = admin" > ${RSYNCD_CONF}
echo "gid = administrators" >> ${RSYNCD_CONF}
#echo "log file = /var/log/rsyncd.log" >> ${RSYNCD_CONF}
echo "pid file = /var/run/rsyncd.pid" >> ${RSYNCD_CONF}
echo "read only = false" >> ${RSYNCD_CONF}
echo "hosts allow = *" >> ${RSYNCD_CONF}
echo "port = ${rsyncd_port}" >> ${RSYNCD_CONF}
echo "rsync user = ${rsyncd_user}" >> ${RSYNCD_CONF}
echo "rsync pswd = ${rsyncd_pswd}" >> ${RSYNCD_CONF}
echo "SLimitRate = ${rsyncd_slimitrate}" >> ${RSYNCD_CONF}
echo "RLimitRate = ${rsyncd_rlimitrate}" >> ${RSYNCD_CONF}
echo >> ${RSYNCD_CONF}

tmp_smb_conf="/tmp/smb.conf.`date +%s`"
/bin/cp /etc/smb.conf ${tmp_smb_conf}
share_list="/tmp/share_list.`date +%s`"
/bin/sed -n 's/^\[\(.*\)\]/\1/p' ${tmp_smb_conf} | /bin/grep global -v | /bin/grep printer -v > ${share_list}

while read LINE
do
	share_path=`/sbin/getcfg "${LINE}" "path" -f ${tmp_smb_conf} -d ""`
	[ "x${share_path}" = "x" ] || /sbin/setcfg "${LINE}" "path" "${share_path}" -f ${RSYNCD_CONF}
done < ${share_list}

/bin/rm -f ${tmp_smb_conf} ${share_list}
