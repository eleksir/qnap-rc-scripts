#!/bin/sh
#
# initialise network card
#

MOUNTPOINT="/tmp/config"
SATA="/dev/sda"
ESATA="/dev/sdf"
USB1="/dev/sdi"
USB2="/dev/sdo"
USB3="/dev/sdu"

hdperm_test()
{
	# test SATA device
	if [ ! -f $MOUNTPOINT/hdparm.sata ]; then
		/bin/dd if=$SATA of=/tmp/pftest bs=1 count=1 2> /dev/null 1> /dev/null
		if [ $? = 0 ]; then
			/sbin/hdparm -t $SATA > $MOUNTPOINT/hdparm.sata
		fi
	fi
	# test ESATA device
	if [ ! -f $MOUNTPOINT/hdparm.esata ]; then
		/bin/dd if=$ESATA of=/tmp/pftest bs=1 count=1 2> /dev/null 1> /dev/null
		if [ $? = 0 ]; then
			/sbin/hdparm -t $ESATA > $MOUNTPOINT/hdparm.esata
		fi
	fi
	# test USB1 device
	if [ ! -f $MOUNTPOINT/hdparm.usb1 ]; then
		/bin/dd if=$USB1 of=/tmp/pftest bs=1 count=1 2> /dev/null 1> /dev/null
		if [ $? = 0 ]; then
			/sbin/hdparm -t $USB1 > $MOUNTPOINT/hdparm.usb1
		fi
	fi
	# test USB2 device
	if [ ! -f $MOUNTPOINT/hdparm.usb2 ]; then
		/bin/dd if=$USB2 of=/tmp/pftest bs=1 count=1 2> /dev/null 1> /dev/null
		if [ $? = 0 ]; then
			/sbin/hdparm -t /dev/sdk > $MOUNTPOINT/hdparm.usb2
		fi
	fi
	# test USB3 device
	if [ ! -f $MOUNTPOINT/hdparm.usb3 ]; then
		/bin/dd if=$USB3 of=/tmp/pftest bs=1 count=1 2> /dev/null 1> /dev/null
		if [ $? = 0 ]; then
			/sbin/hdparm -t /dev/sdr > $MOUNTPOINT/hdparm.usb3
		fi
	fi
}

#advanced bonding network rescan in booting
if [ `/sbin/getcfg "Network" "Advanced Bonding Support" -u -d "FALSE"` = "TRUE" ]; then
	if [ -f /sbin/network_boot_rescan ]; then
		/sbin/network_boot_rescan
	fi
fi
