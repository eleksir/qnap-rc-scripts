#!/bin/sh
RETVAL=0
STUNNEL_KEY=/etc/config/stunnel/backup.key
STUNNEL_CERT=/etc/config/stunnel/backup.cert
STUNNEL_KEY_DEFAULT=/etc/default_config/stunnel/backup.key
STUNNEL_CERT_DEFAULT=/etc/default_config/stunnel/backup.cert
prepare_cert_key()
{
	if [ ! -f $STUNNEL_CERT ] && [ -f $STUNNEL_CERT_DEFAULT ]; then
		/bin/cp $STUNNEL_CERT_DEFAULT $STUNNEL_CERT 2>>/dev/null
	fi
	
	if [ ! -f $STUNNEL_KEY ] && [ -f $STUNNEL_KEY_DEFAULT ]; then
		/bin/cp $STUNNEL_KEY_DEFAULT $STUNNEL_KEY 2>>/dev/null
	fi
}

case "$1" in
  start)
        echo -n "Starting FTP services: "
	prepare_cert_key
	if [ -f /etc/init.d/rcv_port.sh ]; then
		/etc/init.d/rcv_port.sh start
	fi
	if [ -f /usr/local/sbin/proftpd ] && [ `/sbin/getcfg FTP Enable -u -d FALSE` = TRUE ]; then
		/sbin/daemon_mgr proftpd start "TZ=/etc/localtime /usr/local/sbin/proftpd -n > /dev/null 2>&1 &"
#	        /sbin/daemon_mgr proftpd start "/usr/local/sbin/proftpd"
	fi
        RETVAL=$?
	echo "proftpd."
        ;;
  stop)
        echo "Shutting down FTP services: "
	/sbin/daemon_mgr proftpd stop "/usr/local/sbin/proftpd"

	/bin/kill -INT `pidof proftpd` 1>/dev/null 2>/dev/null
	/usr/bin/killall proftpd 1>/dev/null 2>/dev/null
	RETVAL=$?
	count=5
	while [ $count -gt 0 ]; do
		/bin/echo -n "."
		/bin/pidof proftpd >/dev/null 2>&1
		if [ $? -ne 0 ]; then
			#no any proftpd process.
			#count=0
			break
		else
			/bin/sleep 1
		fi
		count=$(($count-1));
	done
	if [ $count == 0 ]; then
		/bin/kill -9 `/bin/pidof proftpd`
		RETVAL=$?
	fi
	/bin/echo
        ;;
  restart)
        $0 stop
        $0 start
        RETVAL=$?
	echo "stopped."
        ;;
  reconfig)
	echo "Reconfigure FTP services: "
	pid=`/bin/pidof proftpd`
	if [ "$pid" != "" ]; then
		echo "reconfig ftp success"
		/bin/kill -HUP $pid
	fi
	;;
  *)
        echo "Usage: $0 {start|stop|restart|reconfig}"
        exit 1
esac

exit $RETVAL

