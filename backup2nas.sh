#!/bin/sh

RETVAL=0

case "$1" in
	"start")
		echo "Starting backup2nas services: "
		/sbin/daemon_mgr backup2nas start "/sbin/backup2nas -D"
		RETVAL=$?
		;;

	"stop")
		echo "Shutting down backup2nas services: "
		/sbin/daemon_mgr backup2nas stop "/sbin/backup2nas -D"
		RETVAL=$?
		;;

	"restart")
		"$0" stop
		"$0" start
		RETVAL=$?
		;;

	*)
		echo "Usage: $0 {start|stop|restart}"
		exit 1
esac

exit $RETVAL
