#!/bin/sh
#
# Copyright @ 2005 QNAP Systems Inc.
#
# network       This script start/stop/restart network.
#
# Version       1.0.0
#
# Author        Ricky Chen

LPD_LOCK="/var/spool/lpd"
LPD_SPOOL="/var/spool/smb"
LPD_SPOOL2="/var/spool/smb/2"
LPD_SPOOL3="/var/spool/smb/3"
LPD_SPOOL4="/var/spool/smb/4"
LPD_SPOOL5="/var/spool/smb/5"
LPD_SPOOL6="/var/spool/smb/6"
LPD_SPOOL7="/var/spool/smb/7"

start(){
	[ -d ${LPD_LOCK} ] || mkdir -p ${LPD_LOCK}
	[ -d ${LPD_SPOOL} ] || mkdir -p ${LPD_SPOOL}
	[ -d ${LPD_SPOOL2} ] || mkdir -p ${LPD_SPOOL2}
	[ -d ${LPD_SPOOL3} ] || mkdir -p ${LPD_SPOOL3}
	[ -d ${LPD_SPOOL4} ] || mkdir -p ${LPD_SPOOL4}
	[ -d ${LPD_SPOOL5} ] || mkdir -p ${LPD_SPOOL5}
	[ -d ${LPD_SPOOL6} ] || mkdir -p ${LPD_SPOOL6}
	[ -d ${LPD_SPOOL7} ] || mkdir -p ${LPD_SPOOL7}
	chmod 777 ${LPD_SPOOL}
	chmod 777 ${LPD_SPOOL2}
	chmod 777 ${LPD_SPOOL3}
	chmod 777 ${LPD_SPOOL4}
	chmod 777 ${LPD_SPOOL5}
	chmod 777 ${LPD_SPOOL6}
	chmod 777 ${LPD_SPOOL7}
}

case "$1" in
        start)
		start
                ;;

        stop)
                ;;

        restart)
		start
                ;;

        *)
                echo $"Usage: $0 {start|stop|restart}"
                exit 1
esac

exit 0

