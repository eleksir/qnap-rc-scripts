#!/bin/sh
#
# chkconfig: 345 13 89
# description: Starts and stops the iSCSI initiator
#
# processname: /sbin/iscsid
# pidfile: /var/run/sbin/iscsid.pid
# config:  /etc/config/iscsi/sbin/iscsid.conf

# Source function library.

PATH=/sbin:/bin:/usr/sbin:/usr/bin
INITNAME_FILE=/etc/iscsi/initiatorname.iscsi
CONF_FILE=/etc/config/iscsi/sbin/iscsid.conf
MAPPING_FILE=/etc/config/virtual_share_mapping

RETVAL=0

mkinitname()
{
	[ -d /etc/iscsi ] || /bin/mkdir -p /etc/iscsi
	MODEL=`/sbin/getcfg System Model`
	SERVER_NAME=`/sbin/getcfg System "Server Name"`
	project_name=`/sbin/getcfg Project Name -f /var/default`
	if [ "x$project_name" = "xAthens" ]; then
		/bin/echo "InitiatorName=iqn.1987-05.com.cisco:${MODEL}.${SERVER_NAME}" > $INITNAME_FILE
	else
		if [ -f /etc/IS_G ]; then
			/bin/echo "InitiatorName=iqn.2004-04.com.NAS:Initiator.${SERVER_NAME}" > $INITNAME_FILE
		else
			/bin/echo "InitiatorName=iqn.2004-04.com.qnap:${MODEL}.${SERVER_NAME}" > $INITNAME_FILE
		fi
	fi
}
start()
{
	INSTALL_FLAG_FILE=/tmp/check_install.flag
#	echo Test $INSTALL_FLAG_FILE >> /tmp/inst.log
	if [ -f $INSTALL_FLAG_FILE ] ; then
#		/bin/cat $INSTALL_FLAG_FILE >> /tmp/inst.log
		INSTALLING=`/bin/cat $INSTALL_FLAG_FILE`
		[ $INSTALLING = "1" ] && exit 0
	fi

	Q_CONF_FILE=/etc/config/iscsi-init.conf
	/bin/echo $"Starting iSCSI initiator service: "
	[ -f $Q_CONF_FILE ] || /bin/echo "#QNAP iSCSI initiator configure file" > $Q_CONF_FILE
	[ -f $MAPPING_FILE ] || echo "[VirtualDisk1]" > $MAPPING_FILE
	/sbin/insmod /lib/modules/misc/scsi_transport_iscsi.ko
	/sbin/insmod /lib/modules/misc/libiscsi.ko
	[ -f /lib/modules/misc/libiscsi_tcp.ko ] && /sbin/insmod /lib/modules/misc/libiscsi_tcp.ko
	/sbin/insmod /lib/modules/misc/iscsi_tcp.ko
	mkinitname
	/sbin/iscsid --config=${CONF_FILE} --initiatorname=${INITNAME_FILE}
	RETVAL=$?
	[ $RETVAL -eq 0 ] || return

	/bin/touch /var/lock/subsys/open-iscsi
	#VDD enhancement version uses vdd_control daemon to replace vdd_control+vdd_scan_target
	[ -f /sbin/vdd_scan_target ] || /sbin/daemon_mgr vdd_control start "/sbin/vdd_control -d >/dev/null 2>&1 &"

	[ -f /etc/config/virtual_share_mapping ] || echo "" >> /etc/config/virtual_share_mapping
	if [ -f /sbin/storage_boot_init ]; then
		/sbin/storage_boot_init 8
	fi



#	/bin/echo -n $"Setting up iSCSI targets: "
#	/sbin/iscsiadm -m node --loginall=automatic
#	success
#	/bin/echo
}

stop()
{
	[ -f /sbin/umount_all_vdd ] && /sbin/umount_all_vdd
	/bin/echo -n $"Stopping iSCSI initiator service: "
	/bin/sync
	/sbin/iscsiadm -m node --logoutall=all
	/usr/bin/killall iscsid
	/bin/rm -f /var/run/sbin/iscsid.pid
	[ $RETVAL -eq 0 ] && /bin/rm -f /var/lock/subsys/open-iscsi
	status=0
	/sbin/rmmod /lib/modules/misc/iscsi_tcp.ko
	if [ "$?" -ne "0" -a "$?" -ne "1" ]; then
		status=1
	fi
	/sbin/rmmod /lib/modules/misc/libiscsi_tcp.ko
	if [ "$?" -ne "0" -a "$?" -ne "1" ]; then
		status=1
	fi
	/sbin/rmmod /lib/modules/misc/libiscsi.ko
	if [ "$?" -ne "0" -a "$?" -ne "1" ]; then
		status=1
	fi
	/sbin/rmmod /lib/modules/misc/scsi_transport_iscsi.ko
	if [ "$?" -ne "0" -a "$?" -ne "1" ]; then
		status=1
	fi
}

restart()
{
	stop
	/bin/sleep 1
	start

}

ISCSI_INIT_KO=/lib/modules/misc/iscsi_tcp.ko

[ -f $ISCSI_INIT_KO ] || exit 0

case "$1" in
	start)
			start
			;;
	stop)
			stop
			;;
	restart)
			stop
			start
			;;
	*)
			/bin/echo $"Usage: $0 {start|stop|restart}"
			exit 1
esac

exit $RETVAL
