#!/bin/sh
PATH=/bin:/sbin:/usr/bin:/usr/sbin

UPNPD="/sbin/upnpd"
BOND_SUP=`/sbin/getcfg "Network" "BONDING Support" -d "FALSE"`
ADV_BOND_SUP=`/sbin/getcfg "Network" "Advanced Bonding Support" -d "FALSE"`
ETH0STATUS=`/bin/cat /sys/class/net/eth0/operstate 2>>/dev/null`
ETH1STATUS=`/bin/cat /sys/class/net/eth1/operstate 2>>/dev/null`
EXTIFACE="eth0"
INTIFACE="eth0"
ALLOW_MULTICAST="FALSE"
RETVAL=0
IS_GATEWAY_EXIT=0

if [ "$BOND_SUP" = "TRUE" ]; then
  EXTIFACE="bond0"
  INTIFACE="bond0"
else 
  if [ "$ETH0STATUS" = "up" ]; then
    EXTIFACE="eth0"
    INTIFACE="eth0"
  else 
    if [ "$ETH1STATUS" = "up" ]; then
      EXTIFACE="eth1"
      INTIFACE="eth1"
    fi
  fi
fi

if [ "x$ADV_BOND_SUP" = "xTRUE" ]; then
  EXTIFACE=`/sbin/getcfg "Network" "Default GW Device" -d "eth0"`
  INTIFACE=$EXTIFACE
fi

ckeck_gateway_is_exit()
{
  /bin/ip route | /bin/grep "^default" > /tmp/nasroute.org
  
  while read LINE
  do
    interface=`/bin/echo $LINE | /bin/cut -d ' ' -f 5`
    gateway=`/bin/echo $LINE | /bin/cut -d ' ' -f 3`
    if [ x"${interface}" = x"${INTIFACE}" ] ; then
      /bin/echo " get $interface gateway = $gateway"
      /bin/ping -q -c 3 $gateway 1>>/dev/null 2>>/dev/null
      [ $? = 0 ] && IS_GATEWAY_EXIT=1
      break;
    fi
  done < /tmp/nasroute.org
  /bin/rm -f /tmp/nasroute.org 2>>/dev/null
}

case "$1" in
  start)
  [ `/sbin/getcfg "UPnP Service" "Enable" -d TRUE` = "FALSE" ] && exit 1
  [ -f "$UPNPD" ] || exit 1
  if [ -f /var/lock/subsys/upnpd ]; then
    /bin/echo "upnpd service seems already be running"
    exit 1
  fi
  echo -n "Starting upnpd:" 
  ckeck_gateway_is_exit
  if [ $IS_GATEWAY_EXIT = 0 ];then
    /bin/echo "Gateway failed. UPnP Discovery Service stopped."
    #/sbin/write_log "[Network Service Discovery] Gateway failed. UPnP Discovery Service stopped." 2
    exit 1
  fi
  [ "$ALLOW_MULTICAST" = "TRUE" ] && route add -net 239.0.0.0 netmask 255.0.0.0 $INTIFACE
  /sbin/gen_upnp_desc
  [ $? != 0 ] && exit 1
  /sbin/daemon_mgr upnpd start "$UPNPD $EXTIFACE $INTIFACE"
  RETVAL=$?
  [ $RETVAL -eq 0 ] && touch /var/lock/subsys/upnpd
  echo "."
  ;;
  stop)
  echo -n "Shutting down upnpd services:" 
  /sbin/daemon_mgr upnpd stop "$UPNPD $EXTIFACE $INTIFACE"
  /bin/sleep 5
  /sbin/pidof upnpd
  [ $? = 0 ] && /bin/kill `/sbin/pidof upnpd`
  /bin/rm -f /var/lock/subsys/upnpd
  echo "."
  ;;
  restart)
  $0 stop
  /bin/sleep 5
  $0 start
  ;;
  *)
  echo "Usage: /etc/init.d/upnpd.sh {start|stop|restart}"
  exit 1
esac

exit $RETVAL
