#!/bin/sh

# check if the HAL subsystem exist
if [ -x /sbin/hal_app ]; then
    BOOT_DEV=$(/sbin/hal_app --get_boot_pd port_id=0)
else
    BOOT_DEV="/dev/sdx"
fi
FLASH_RFS1=${BOOT_DEV}2
FLASH_RFS2=${BOOT_DEV}3
FLASH_TMP="/flashfs_tmp"
FLASH_TMP1="/flashfs_tmp1"
BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf 2>/dev/null`
CMS_ENABLE=`/sbin/getcfg CMS Enable -d FALSE -f /etc/config/uLinux.conf`

check_sum()
{
	if [ ! -f $1 ] || [ ! -f $2 ];then
		echo "${1} or ${2} not found."
		return 1
	fi
	
	local file_cksum=`/sbin/cksum $1`
	local file_crc=`echo $file_cksum | cut -d ' ' -f 1`
	local file_size=`echo $file_cksum | cut -d ' ' -f 2`

	local cksum_cksum=`cat $2`
	local cksum_crc=`echo $cksum_cksum | cut -d ' ' -f 1`
	local cksum_size=`echo $cksum_cksum | cut -d ' ' -f 2`

	if [ $file_crc = $cksum_crc ] && [ $file_size = $cksum_size ]; then
		return 0
	fi
	return 1
}

check_flash_files()
{
	local ckflag=0
	check_sum "${1}/boot/bzImage" "${1}/boot/bzImage.cksum"
	[ $? = 0 ] || ckflag=1
	check_sum "${1}/boot/initrd.boot" "${1}/boot/initrd.boot.cksum"
	[ $? = 0 ] || ckflag=1
	check_sum "${1}/boot/rootfs2.bz" "${1}/boot/rootfs2.bz.cksum"
	[ $? = 0 ] || ckflag=1
	check_sum "${1}/boot/rootfs_ext.tgz" "${1}/boot/rootfs_ext.tgz.cksum"
	[ $? = 0 ] || ckflag=1
	
	if [ $ckflag = 1 ]; then
		[ x"${1}" = x"${FLASH_TMP}" ] && /sbin/write_log "The first boot area on the flash is corrupted." 1
		[ x"${1}" = x"${FLASH_TMP1}" ] && /sbin/write_log "The second boot area on the flash is corrupted." 1
		return 1
	fi
	
	return 0
}

check_flash()
{
	[ -d $FLASH_TMP ] || /bin/mkdir $FLASH_TMP
	/bin/mount ${FLASH_RFS1} $FLASH_TMP -o ro
	if [ $? != 0 ]; then
		/sbin/write_log "Flash read error." 1
	else
		check_flash_files $FLASH_TMP
	fi
	/bin/umount $FLASH_TMP
	/bin/sync
	/bin/mount ${FLASH_RFS2} $FLASH_TMP -o ro
	if [ $? != 0 ]; then
		/sbin/write_log "Flash read error." 1
	else
		check_flash_files $FLASH_TMP
	fi
	/bin/umount $FLASH_TMP
	/bin/sync	
	
	return 0
}

check_restore_flash()
{
	local status1=0
	local status2=0
	
	/bin/echo "Check/Restore Flash..."
	[ -d $FLASH_TMP ] || /bin/mkdir $FLASH_TMP
	[ -d $FLASH_TMP1 ] || /bin/mkdir $FLASH_TMP1
	/bin/mount ${FLASH_RFS1} $FLASH_TMP -o ro
	if [ $? != 0 ]; then
		/bin/umount ${FLASH_RFS1} 1>>/dev/null 2>>/dev/null
		/sbin/write_log "The first boot area on the flash is corrupted." 1
		status1=1
	else
		check_flash_files $FLASH_TMP
		[ $? = 0 ] || status1=1
	fi
	/bin/umount $FLASH_RFS1
	
	/bin/mount ${FLASH_RFS2} $FLASH_TMP1 -o ro
	if [ $? != 0 ]; then
		/bin/umount ${FLASH_RFS2} 1>>/dev/null 2>>/dev/null
		/sbin/write_log "The second boot area on the flash is corrupted." 1
		status2=1
	else
		check_flash_files $FLASH_TMP1
		[ $? = 0 ] || status2=1
	fi
	/bin/umount $FLASH_RFS2
	
	if [ $status1 = 0 ] && [ $status2 = 1 ]; then
		/bin/mount ${FLASH_RFS1} ${FLASH_TMP} -o ro
		/sbin/mke2fs -m0 -F -b 1024 -I 128 ${FLASH_RFS2}
		/bin/sync
		sleep 1
		/bin/mount ${FLASH_RFS2} ${FLASH_TMP1}
		if [ $? = 0 ]; then
			/bin/cp -a ${FLASH_TMP}/. ${FLASH_TMP1}
			[ $? = 0 ] && /sbin/write_log "The second boot area on the flash has been recovered." 2
		fi
		/bin/umount ${FLASH_RFS2}
		/bin/umount ${FLASH_RFS1}
	elif [ $status1 = 1 ] && [ $status2 = 0 ]; then
		/bin/mount ${FLASH_RFS2} ${FLASH_TMP1} -o ro
		/sbin/mke2fs -m0 -F -b 1024 -I 128 ${FLASH_RFS1}
		/bin/sync
		sleep 1
		/bin/mount ${FLASH_RFS1} ${FLASH_TMP}
		if [ $? = 0 ]; then
			/bin/cp -a ${FLASH_TMP1}/. ${FLASH_TMP}
			[ $? = 0 ] && /sbin/write_log "The first boot area on the flash has been recovered." 2
		fi
		/bin/umount ${FLASH_RFS1}
		/bin/umount ${FLASH_RFS2}
	fi

	[ ! -d $FLASH_TMP1 ] || /bin/rmdir $FLASH_TMP1
	
	return 0
}


check_ver_build()
{
	# check if version is the same between flash/disk
	FLASH_BUILD_NUM=`/sbin/getcfg System "Build Number" -d 0000 -f /etc/default_config/uLinux.conf`
	FLASH_VERSION=`/sbin/getcfg System "Version" -d 1.0.0 -f /etc/default_config/uLinux.conf`
	DISK_BUILD_NUM=`/sbin/getcfg System "Build Number" -d 0000 -f /etc/config/uLinux.conf`
	DISK_VERSION=`/sbin/getcfg System "Version" -d 1.0.0 -f /etc/config/uLinux.conf`
	if [ $FLASH_BUILD_NUM != $DISK_BUILD_NUM ]; then
		/sbin/write_log "The firmware versions of the system built-in flash ($FLASH_VERSION Build $FLASH_BUILD_NUM) and the hard drive ($DISK_VERSION Build $DISK_BUILD_NUM) are not consistent. It is recommended to update the firmware again for higher system stability." 2
	fi
}

write_dhcp_tftp_logs()
{
	# check if version is the same between flash/disk
	RET_UPDATECONFIG=`/sbin/getcfg "" "ret_updateconfig" -d "2" -f "/tmp/dhcp_tftp.log"`
	RET_TFTPSRVADDR=`/sbin/getcfg "" "ret_tftpsrvaddr" -d "2" -f "/tmp/dhcp_tftp.log"`
	RET_TFTPSRVNAME=`/sbin/getcfg "" "ret_tftpsrvname" -d "2" -f "/tmp/dhcp_tftp.log"`
	RET_TFTPBROADCAST=`/sbin/getcfg "" "ret_tftpbroadcast" -d "2" -f "/tmp/dhcp_tftp.log"`
	RET_BACKUPDRV=`/sbin/getcfg "" "ret_backupdrv" -d "2" -f "/tmp/dhcp_tftp.log"`
	
	if [ $RET_TFTPSRVNAME = 0 ] || [ $RET_TFTPSRVADDR = 0 ] || [ $RET_TFTPBROADCAST = 0 ] || [ $RET_BACKUPDRV = 0 ]; then
		/sbin/write_log "Download system configuration from TFTP server succeeded." 4
	else
		/sbin/write_log "Download system configuration from TFTP server failed." 2
	fi
	if [ $RET_UPDATECONFIG = 0 ]; then
		/sbin/write_log "Update system configuration succeeded." 4
	else
		/sbin/write_log "Update system configuration failed." 2
	fi
}

case "$1" in
    start)
	STARTED=`/sbin/getcfg Misc "System Started" -u -d FALSE`
	if [ $STARTED = TRUE ]; then
		echo "Last system shutdown process is incomplete."
		/sbin/log_tool -t 1 -a "The system was not shut down properly last time."
		/sbin/daemon_mgr
		/sbin/daemon_mgr.nvr
		/bin/touch /tmp/send_alert_mail
	else
		/sbin/setcfg Misc "System Started" TRUE
	fi
	/sbin/log_tool -t 0 -a "System started."
    RESULT=$?
    if [ "x${BOOT_CONF}" = "xTS-NASPPC" ]; then
        retry=0;
        while [ $RESULT -ne 0 -a $retry -le 10 ]; do
            retry=`expr $retry + 1`
            sleep 5
            /sbin/log_tool -t 0 -a "System started."
            RESULT=$?
        done
    fi
	check_ver_build
	[ "x${BOOT_CONF}" = "xTS-NASX86" ] && check_restore_flash
	[ -f /tmp/dhcp_tftp.log ] && write_dhcp_tftp_logs
    if [ -x /sbin/hal_event ]; then
        /sbin/hal_event --retrieve_booting_event
    else
        /sbin/kerrd
    fi
    ;;
    stop)
	/sbin/setcfg Misc "System Started" FALSE
	/sbin/log_tool -t 0 -a "System was shut down on `/bin/date`."
#wokes set rtc alarm    
	/sbin/gen_next_alarm 2
	cat /proc/acpi/alarm >  /etc/config/rtc_last_save
	cat /proc/driver/rtc >> /etc/config/rtc_last_save
#####   
	[ -f /sbin/picd ] && /sbin/daemon_mgr picd stop "/sbin/picd  1>/dev/null 2>/dev/null &"
	[ -f /sbin/gpiod ] && /sbin/daemon_mgr gpiod stop "/sbin/gpiod &"
	[ -f /sbin/hwmond ] && /sbin/daemon_mgr hwmond stop "/sbin/hwmond &"
	[ -f /sbin/acpid ] && /sbin/daemon_mgr acpid stop "/sbin/acpid"
	[ -f /sbin/gen_bandwidth ] && /sbin/daemon_mgr gen_bandwidth stop "/sbin/gen_bandwidth &"
	/etc/init.d/wireless.sh stop

	/bin/kill -INT `pidof qLogEngined`
	/sbin/daemon_mgr qLogEngined stop /sbin/qLogEngined
	[ "x${CMS_ENABLE}" != xTRUE ] && /bin/kill -INT `pidof qsyslogd`
	[ "x${CMS_ENABLE}" != xTRUE ] && /sbin/daemon_mgr qsyslogd stop /sbin/qsyslogd
	/bin/kill -INT `pidof qShield`
	/sbin/daemon_mgr qShield stop /sbin/qShield
	Internal_Model=`/sbin/getcfg System "Internal Model"`
	if [ "x$Internal_Model" = "xTS-119" ] || [ "x$Internal_Model" = "xTS-219" ] || [ "x$Internal_Model" = "xTS-419" ]; then
        if [ -x /sbin/hal_app ]; then
            if [ `/sbin/getcfg "Misc" "Wake On Lan" -u -d TRUE` = TRUE ]; then
                /usr/sbin/ethtool -s eth0 wol d
                /usr/sbin/ethtool -s eth0 wol g
            fi
        else
            CPU_MODEL=""
            [ ! -f /proc/tsinfo/cpu_model ] || CPU_MODEL=`/bin/cat /proc/tsinfo/cpu_model | cut -d ' ' -f 1 2>/dev/null`
            if [ "x${CPU_MODEL}" = "x88F6282" ]; then
                if [ `/sbin/getcfg "Misc" "Wake On Lan" -u -d TRUE` = TRUE ]; then
                    /usr/sbin/ethtool -s eth0 wol d
                    /usr/sbin/ethtool -s eth0 wol g
                fi
            fi
        fi            
	fi
    ;;
    restart)
    $0 stop
    $0 start
    ;;
    *)
	echo "Usage: /etc/init.d/shutdown_check.sh {start|stop|restart}"
	exit 1
esac

