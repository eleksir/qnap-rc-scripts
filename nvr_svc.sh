#!/bin/sh

. /etc/init.d/functions
FindDefVol
_ret=$?
if [ $_ret != 0 ]; then
 echo "Default volume not found."
 exit 1
fi
#echo "${DEF_VOLMP}"

INSTALL_FLAG_FILE=/tmp/check_install.flag
if [ -f $INSTALL_FLAG_FILE ] ; then
	INSTALLING=`/bin/cat $INSTALL_FLAG_FILE`
	[ x"$INSTALLING" != x"1" ] || exit 0
fi
NSS_ENABLED=`/sbin/getcfg NVR Enable -d FALSE`

NVR_CLASS=`/sbin/getcfg System "NVR Class" -d NSS -f /etc/default_config/uLinux.conf`
NVR_ROOTFS=
export NVR_ROOTFS
RecordingsShare=`/sbin/getcfg SHARE_DEF defRecordings -d Qrecordings -f /etc/config/def_share.info`
WGET=/usr/bin/wget-s
[ -x ${WGET} ] || WGET=/usr/bin/wget

if [ "$NVR_CLASS" == "NVR" ]; then
	NVR_DATA_ROOT=/data
else
	NVR_DATA_ROOT=/share/$RecordingsShare
fi

# used only for NAS
MNT_POINT="/mnt/ext"
NVR_SOURCE="${MNT_POINT}/opt/source/nvr_app2.tar.gz"
NVR_DIR="/"
ROOT_PART="/mnt/HDA_ROOT"
UPDATEPKG_DIR="${ROOT_PART}/update_pkg"
if [ -f "${NVR_SOURCE}" ] && [ x`/sbin/getcfg "NVR" "NSS Enabled" -d no` = xno ]; then
/sbin/setcfg NVR "NSS Enabled" yes
fi
check_nvr_bin()
{
	if [ ! -f /usr/bin/nvrd ] || [ ! -f /usr/bin/evtLogd ] || [ ! -f /usr/bin/evtd ]; then
		echo "Surveillance Station not available."
		return 1
	fi
	return 0
}

install_nvr_svc()
{
	if [ "$NVR_CLASS" == "NVR" ]; then
		return
	fi
	if [ ! -f /usr/bin/nvrd ]; then
		_nvr_create_link=0
		if [ -f ${UPDATEPKG_DIR}/nvr_app2.tar.gz ]; then
			_nvr_target=${DEF_VOLMP}/.nvr
			cd /
			/bin/tar zxf ${UPDATEPKG_DIR}/nvr_app2.tar.gz --exclude *.cab --exclude msb --exclude ajax_obj_v2 --exclude libtranscoder.so.0.0
			[ -d ${_nvr_target} ] || /bin/mkdir -p ${_nvr_target}
			/bin/tar zxvf ${UPDATEPKG_DIR}/nvr_app2.tar.gz -C ${_nvr_target} --wildcards *.cab usr/msb home/httpd/ajax_obj_v2 usr/lib/libtranscoder.so.0.0
			_nvr_create_link=1
		elif [ -f ${NVR_SOURCE} ]; then
			_nvr_target=/mnt/ext/opt/nvr
			cd /
			/bin/tar zxf ${NVR_SOURCE} --exclude *.cab --exclude msb --exclude ajax_obj_v2 --exclude libtranscoder.so.0.0
			[ -d /mnt/ext/opt/nvr ] || /bin/mkdir -p ${_nvr_target}
			/bin/tar zxvf ${NVR_SOURCE} -C /mnt/ext/opt/nvr --wildcards *.cab usr/msb home/httpd/ajax_obj_v2 usr/lib/libtranscoder.so.0.0
			_nvr_create_link=1
		else
			echo "Surveillance Station not available."
			/sbin/setcfg NVR Enable FALSE
			exit 1
		fi
		if [ "x${_nvr_create_link}" = "x1" ]; then
			/bin/rm -f /home/httpd/cgi-bin/NNVRVMon.cab 2>/dev/null
			/bin/rm -f /home/httpd/cgi-bin/X86VMon.cab 2>/dev/null
			/bin/rm -f /home/httpd/cgi-bin/Playback.cab 2>/dev/null
			/bin/rm -f /home/httpd/cgi-bin/QNAP*.cab 2>/dev/null
			/bin/ln -sf ${_nvr_target}/home/httpd/cgi-bin/X86VMon.cab /home/httpd/cgi-bin/X86VMon.cab
			/bin/ln -sf ${_nvr_target}/home/httpd/cgi-bin/Playback.cab /home/httpd/cgi-bin/Playback.cab
			/bin/ln -sf ${_nvr_target}/home/httpd/cgi-bin/QNAPG726.cab /home/httpd/cgi-bin/QNAPG726.cab
			/bin/ln -sf ${_nvr_target}/home/httpd/cgi-bin/QNAPQ264.cab /home/httpd/cgi-bin/QNAPQ264.cab
			/bin/ln -sf ${_nvr_target}/home/httpd/cgi-bin/QNAPQIVG.cab /home/httpd/cgi-bin/QNAPQIVG.cab
			/bin/ln -sf ${_nvr_target}/home/httpd/cgi-bin/QNAPQMP4.cab /home/httpd/cgi-bin/QNAPQMP4.cab
			/bin/ln -sf ${_nvr_target}/home/httpd/cgi-bin/QNAPQVivoTek.cab /home/httpd/cgi-bin/QNAPQVivoTek.cab
			/bin/rm -rf /usr/msb 2>/dev/null
			/bin/ln -sf ${_nvr_target}/usr/msb /usr/msb
			/bin/rm -f /home/httpd/ajax_obj_v2 2>/dev/null
			/bin/ln -sf ${_nvr_target}/home/httpd/ajax_obj_v2 /home/httpd/ajax_obj_v2
			/bin/rm -f /usr/lib/libtranscoder.so.0.0 2>/dev/null
			/bin/ln -sf ${_nvr_target}/usr/lib/libtranscoder.so.0.0 /usr/lib/libtranscoder.so.0.0
		fi
		[ ! -f /home/httpd/ajax_obj/js/qnapui.js_bak ] || /bin/cp /home/httpd/ajax_obj/js/qnapui.js_bak /home/httpd/ajax_obj/js/qnapui.js
		[ ! -f /home/httpd/cgi-bin/html/login.html_bak ] || /bin/cp /home/httpd/cgi-bin/html/login.html_bak /home/httpd/cgi-bin/html/login.html
		[ ! -f /home/httpd/cgi-bin/html/login_2.html_bak ] || /bin/cp /home/httpd/cgi-bin/html/login_2.html_bak /home/httpd/cgi-bin/html/login_2.html
	fi
	if [ -f /home/httpd/ajax_obj/js/qnapui.js ] && [ -f /home/httpd/cgi-bin/html/login.html ]; then
		[ -f /home/httpd/ajax_obj/js/qnapui.js_bak ] || /bin/cp /home/httpd/ajax_obj/js/qnapui.js /home/httpd/ajax_obj/js/qnapui.js_bak
		[ -f /home/httpd/cgi-bin/html/login.html_bak ] || /bin/cp /home/httpd/cgi-bin/html/login.html /home/httpd/cgi-bin/html/login.html_bak
		[ -f /home/httpd/cgi-bin/html/login_2.html_bak ] || /bin/cp /home/httpd/cgi-bin/html/login_2.html /home/httpd/cgi-bin/html/login_2.html_bak
		/bin/sed -i 's/\/cgi-bin\/camera_view.cgi/\/nssindex.html/g' /home/httpd/ajax_obj/js/qnapui.js 2>/dev/null
		/bin/sed -i 's/\/cgi-bin\/camera_view.cgi/\/nssindex.html/g' /home/httpd/cgi-bin/html/login.html 2>/dev/null
		/bin/sed -i 's/\/cgi-bin\/camera_view.cgi/\/nssindex.html/g' /home/httpd/cgi-bin/html/login_2.html 2>/dev/null
	fi
	check_nvr_bin
	if [ $? != 0 ]; then
		/sbin/setcfg NVR Enable FALSE
		exit 1
	fi
}

create_nvr_cfg_files()
{
	if [ ! -e /etc/config/nvrd.xml ]; then
		/bin/cp -a /etc/default_config/nvrd.xml /etc/config/
	fi

	if [ ! -e /etc/config/nvrstat.xml ]; then
		/bin/cp -a /etc/default_config/nvrstat.xml /etc/config/
	fi
	if [ ! -e /var/.nvr_quota_info ]; then
		/bin/mkdir /var/.nvr_quota_info
	fi
	if [ ! -f  /etc/config/cact.xml ]; then
		cp /etc/default_config/cact.xml /etc/config/cact.xml
	fi
	if [ ! -f  /etc/config/nvrd2.xml ]; then
		cp /etc/default_config/nvrd2.xml /etc/config/nvrd2.xml
	fi

	if [ ! -f  /etc/config/emapinfo.xml ]; then
        	cp /etc/default_config/emapinfo.xml /etc/config/
		if [ ! -e /etc/config/emap ]; then 
			mkdir /etc/config/emap
			cp /etc/default_config/emap/emap.jpg /etc/config/emap/
		fi
	fi

	evt_xml_file_list="evt act evt_act_list nvr_evt preset_tab sms_provider email_last_change cms_evt";

	for f in $evt_xml_file_list; do
		if [ ! -e /etc/config/$f.xml ]; then
			/bin/cp /etc/default_config/$f.xml /etc/config/
			if [ "$f" == "evt" ]; then
				qcmd ms_evt
			fi
			if [ "$f" == "cms_evt" ]; then
				qcmd cms_evt
			fi
		else
			if [ "$f" == "evt" ]; then
				qcmd ms_evt_upd
			fi
			if [ "$f" == "cms_evt" ]; then
				qcmd cms_evt_upd
			fi
		fi
	done

	if [ ! -e /etc/config/evt_sch0.xml ]; then
		cp /etc/default_config/evt_sch*.xml /etc/config/ ;
	fi
	if [  -e /etc/default_config/sys_sch.xml ]; then
		cp /etc/default_config/sys_sch.xml /etc/
	fi
	
	if [ -f /usr/nvr/model.xml ]; then
		max_ch=`/sbin/getcfg NVR NSS_MAX_CH_NUM -d 16 -f /etc/default_config/uLinux.conf`
		/bin/grep "<max_channel_num>$max_ch</max_channel_num>" /usr/nvr/model.xml 1>/dev/null 2>/dev/null
		[ $? = 0 ] || /bin/sed -i 's/^.*<max_channel_num>.*/'"<max_channel_num>$max_ch<\/max_channel_num>"'/g' /usr/nvr/model.xml
	fi
}

create_nvr_links()
{
	touch /home/httpd/cgi-bin/getstream.cgi
	chmod +x /home/httpd/cgi-bin/getstream.cgi
	touch /home/httpd/cgi-bin/ptz.cgi
	chmod +x /home/httpd/cgi-bin/ptz.cgi
	touch /home/httpd/cgi-bin/http_stream_start.cgi
	touch /home/httpd/cgi-bin/http_stream_stop.cgi
	chmod +x /home/httpd/cgi-bin/http_stream_start.cgi
	chmod +x /home/httpd/cgi-bin/http_stream_stop.cgi

	#Hugo add. correct link path for Marvell
	[ -e /usr/bin/nvrvideo_backup2usb.sh ] || ln -s /etc/init.d/nvrvideo_backup2usb.sh /usr/bin/nvrvideo_backup2usb.sh 2>/dev/null

	if [ ! -e /usr/bin/msfinder0 ]; then
		ln -s /usr/bin/msfinder /usr/bin/msfinder0
	fi
	if [ ! -e /usr/bin/msfinder1 ]; then
		ln -s /usr/bin/msfinder /usr/bin/msfinder1
	fi
	if [ ! -e /play ]; then
		mkdir /play
	fi
	for i in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
	do
		if [ ! -e /play/ch$i ]; then
			mkdir /play/ch$i
		fi
		if [ ! -e /play/ch$i/record_nvr ]; then
			mkdir /play/ch$i/record_nvr
		fi
		if [ ! -e /play/ch$i/record_nvr/channel$i ]; then
			ln -s $NVR_DATA_ROOT/record_nvr/channel$i /play/ch$i/record_nvr/channel$i
		fi
		if [ ! -e  /play/ch$i/.record_nvr_unk ]; then
			mkdir /play/ch$i/.record_nvr_unk
		fi
		if [ ! -e /play/ch$i/.record_nvr_unk/channel$i ]; then
			ln -s $NVR_DATA_ROOT/.record_nvr_unk/channel$i /play/ch$i/.record_nvr_unk/channel$i
		fi
		if [ ! -e /play/ch$i/record_nvr_alarm ]; then
			mkdir /play/ch$i/record_nvr_alarm
		fi
		if [ ! -e /play/ch$i/record_nvr_alarm/channel$i ]; then
			ln -s $NVR_DATA_ROOT/record_nvr_alarm/channel$i /play/ch$i/record_nvr_alarm/channel$i
		fi
	done

	if [ -e /home/httpd/cgi-bin/record ]; then
		/bin/rm /home/httpd/cgi-bin/record
	fi
	/bin/ln -s $NVR_DATA_ROOT /home/httpd/cgi-bin/record

	if [ -e /home/httpd/record ]; then
		/bin/rm /home/httpd/record
	fi
	# remove unused data folders
	/bin/ln -s $NVR_DATA_ROOT /home/httpd/record

	if [ -e $NVR_DATA_ROOT/record_nvr_tmp ]; then
    		/bin/rm -rf $NVR_DATA_ROOT/record_nvr_tmp
	fi
	if [ -e $NVR_DATA_ROOT/record_nvr_unk ]; then
    		/bin/rm -rf $NVR_DATA_ROOT/record_nvr_unk
	fi

	if [ -e $NVR_DATA_ROOT/record_nvr_pre ]; then
    		/bin/rm -rf $NVR_DATA_ROOT/record_nvr_pre
	fi
}

remove_nvr_links()
{
	for i in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
	do
		rm /play/ch$i/record_nvr/channel$i
		rm /play/ch$i/record_nvr_alarm/channel$i
		rm /play/ch$i/.record_nvr_unk/channel$i
	done
}

ROOT_PART="/mnt/HDA_ROOT"
HIDDEN_LOG_DIR="${ROOT_PART}/.logs"
HIDDEN_CONF_FILE=/mnt/HDA_ROOT/.conf
TS_LOG_DIR=/etc/logs

create_nvr_log_database()
{
	if [ -f $HIDDEN_CONF_FILE ] && [ -d $HIDDEN_LOG_DIR ]; then
		if [ -e $TS_LOG_DIR ] && [ ! -f $TS_LOG_DIR/nvrlog.log ]; then
			/sbin/nvr_log_tool -v -c
			/sbin/nvr_log_tool -v -b 10000
		fi
	else
		echo log hidden config directory not exist, initial 10K surv log file... [OK]
		if [ -e $TS_LOG_DIR ] && [ ! -f $TS_LOG_DIR/nvrlog.log ]; then
			/sbin/nvr_log_tool -v -c
			/sbin/nvr_log_tool -v -b 50
		fi
	fi
}

init_nvr_svc()
{
        if [ `/sbin/getcfg NVR Enable -u -d FALSE` != TRUE ]; then
		return 0;
	fi

	if [ -e /var/.nvr_init_ok ]; then
		check_nvr_bin
		[ $? != 0 ] || return 0
		/bin/rm -f /var/.nvr_init_ok 2>/dev/null
	fi
	/bin/touch /var/.nvr_init_ok

	install_nvr_svc

	create_nvr_cfg_files
	create_nvr_links
	create_nvr_log_database

	/usr/bin/qcmd XMLCR
	/usr/bin/sysadm init
	/usr/bin/sysadm start

	/usr/bin/qcmd nvrd_xml
	#update cron table for NVR here...
	# start http streaming here
	count=30;
	while [ $count -gt 0 ]; do
		${WGET} -4 -N http://127.0.0.1:`/sbin/getcfg System "Web Access Port" -d 8080`/cgi-bin/http_stream_start.cgi >& /dev/null
		if [ -e /home/httpd/cgi-bin/getstream.cgi ]; then
			count=0
		else
  		/bin/sleep 1
			count=$(($count-1));
		fi
	done
	return 1

}

start_nvrd()
{
	check_nvr_bin
	[ $? = 0 ] || exit 1
	# dont't do reentry control here!, do it in nvrd
	/sbin/daemon_mgr.nvr evtLogd start "/usr/bin/evtLogd"
	if [ -e $NVR_DATA_ROOT/.mxpg_raw ]; then /bin/rm -rf $NVR_DATA_ROOT/.mxpg_raw/*; fi
	if [ ! -e $NVR_DATA_ROOT/.mxpg_raw ]; then /bin/mkdir $NVR_DATA_ROOT/.mxpg_raw; fi
	echo -n "Starting nvrd services:" 
	/bin/touch /var/.nvrd_alive
	/sbin/daemon_mgr.nvr nvrd start "/usr/bin/nvrd  2>/dev/null"
	echo -n " nvrd"
	echo -n "Starting evtd services:" 
	/sbin/daemon_mgr.nvr evtd start "/usr/bin/evtd  2>/dev/null"
	echo -n " evtd"
	if [ -f /usr/bin/snapshotd ]; then
	    echo -n "Starting snapshotd services:" 
	    /sbin/daemon_mgr.nvr snapshotd start "/usr/bin/snapshotd  2>/dev/null"
	    echo -n " snapshotd"
	fi

	/usr/bin/qcmd adv_user_check
	echo "."
}

stop_nvrd()
{
	if [ -f /usr/bin/snapshotd ]; then
	    echo -n "Shutting down snapshotd services:" 
	    /sbin/daemon_mgr.nvr snapshotd stop /usr/bin/snapshotd
	    echo -n " snapshotd"
	fi
	echo -n "Shutting down evtd services:" 
	/sbin/daemon_mgr.nvr evtd stop /usr/bin/evtd
	echo -n " evtd"
	echo -n "Shutting down nvrd services:" 
	/sbin/daemon_mgr.nvr nvrd stop /usr/bin/nvrd
	echo -n " nvrd"
	/sbin/daemon_mgr.nvr evtLogd stop /usr/bin/evtLogd
	echo "."
}

_start_nvr_svc()
{
	if [ ! -e /var/.nvr_init_ok ]; then
		return 0
	fi
	# remove cache
	qcmd rmcache > /dev/null 2> /dev/null

	start_nvrd
	/etc/init.d/qplayd.sh start
	/etc/init.d/cacd.sh start
	# start HTTP Live Streaming here...
	# start NVR-RR here...
	if [ "$NVR_CLASS" == "NVR" ]; then
		# Check last time RR is finished
		if [ `/sbin/getcfg "ScheduleStatus" "Enable" -d False -f /etc/config/rsync_schedule.conf` = "True" ]; then
			if [ `/sbin/getcfg "ScheduleStatus" "isRRFinish" -d True -f /etc/config/rsync_schedule.conf` = "False" ]; then
				echo "start dataBackup that last time not finish"
				/usr/bin/dataBackup &
			fi
		fi
	fi
	return 1
}

start_nvr_svc()
{
	if [ ! -e /var/.nvr_init_ok ]; then
		return 0
	fi
	APP_RUNTIME_CONF="/var/.application.conf"
	NSS_Hide=`/sbin/getcfg DISABLE survielance -d 0 -f $APP_RUNTIME_CONF`
	if [ "x$NSS_Hide" = "x1" ] && [ x"$NSS_ENABLED" = x"TRUE" ]; then
		/sbin/setcfg NVR Enable FALSE
		NSS_ENABLED="FALSE"
	fi
	if [ "$NVR_CLASS" != "NSS" ] || [ "$NSS_ENABLED" != "TRUE" ]; then
		return 0
	fi
	_start_nvr_svc
}


_stop_nvr_svc()
{
	if [ ! -e /var/.nvr_init_ok ]; then
		return 0
	fi
	# stop HTTP Live Streaming here...
	/etc/init.d/cacd.sh stop
	/etc/init.d/qplayd.sh stop
	stop_nvrd
	return 1
}

stop_nvr_svc()
{
	if [ "$NVR_CLASS" != "NSS" ] || [ "$NSS_ENABLED" != "TRUE" ]; then
		return 0
	fi
	_stop_nvr_svc
}

nvr_svc_started()
{
        if [ ! -e /var/.nvr_start_ok ]; then
                echo 0
        else
                echo 1
        fi
}

uninit_nvr_svc()
{
	if [ ! -e /var/.nvr_init_ok ]; then
		return 0
	fi
	/bin/rm /var/.nvr_init_ok
	remove_nvr_links
	file_list="/usr/bin/msfinder0 /usr/bin/msfinder1  /home/httpd/cgi-bin/ptz.cgi /home/httpd/cgi-bin/getstream.cgi";
	for f in $file_list; do
		if [ ! -e $f ]; then
			/bin/rm $f
		fi
	done
	/usr/bin/sysadm stop
	/usr/bin/sysadm uninit
	/usr/bin/qcmd XMLRM
	# stop http streaming here
	count=30;
	while [ $count -gt 0 ]; do
		${WGET} -4 -N http://127.0.0.1:`/sbin/getcfg System "Web Access Port" -d 8080`/cgi-bin/http_stream_stop.cgi >& /dev/null
		if [ -e /home/httpd/cgi-bin/getstream.cgi ]; then
        		/bin/sleep 1
			count=$(($count-1));
		else
			count=0
		fi
	done
	return 1
}

# _init, _uninit, _start, _stop used by NVR
# start, stop used by NSS
case "$1" in
    start)
        init_nvr_svc
        start_nvr_svc
	;;
    stop)
        stop_nvr_svc
        uninit_nvr_svc
	;;
    restart)
	$0 stop
	$0 start
	;;	
    started)
        nvr_svc_started
        ;;
    _init)
        init_nvr_svc
	;;
    _unint)
        uninit_nvr_svc
	;;
    _start)
        _start_nvr_svc
	;;
    _stop)
        _stop_nvr_svc
	;;
    _restart)
	$0 _stop
	$0 _start
	;;	
    *)
        echo "Usage: /etc/init.d/nvr_svc {start|stop|restart}"
        exit 1
esac

exit 0
