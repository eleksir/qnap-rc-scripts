#!/bin/sh
# For radius

[ ! -f "/etc/config/radius_global.conf" ] && /bin/touch "/etc/config/radius_global.conf"

init_conf()
{
	if [ ! -f /sbin/radius_util ]; then
		/bin/echo "/sbin/radius_util is not exit."
		exit 1
	fi
	
	if [ -d /etc/raddb/log ]; then
		/bin/rm -rf /etc/raddb/log/*
		/bin/touch /etc/raddb/log/radutmp
	else
		/bin/mkdir -p /etc/raddb/log
		/bin/touch /etc/raddb/log/radutmp
	fi
	
	/sbin/radius_util init
}

start()
{
	if [ ! -f /usr/local/sbin/radiusd ]; then
		/bin/echo "radiusd is not exit."
		exit 1
	fi
	
	if [ -f /var/run/radiusd.pid ]; then
		/bin/echo "radiusd is aleady run."
		exit 1
	fi
	 
	if [ `/sbin/getcfg "Global" "Enable" -u -d "FALSE" -f "/etc/config/radius_global.conf"` = "TRUE" ]; then
		/bin/echo "radiusd start."
		init_conf
#		/usr/local/sbin/radiusd -d /etc/raddb
		/sbin/daemon_mgr radiusd start "/usr/local/sbin/radiusd -d /etc/raddb"
	else
		/bin/echo "radiusd is disabled." 
	fi
	
}

stop()
{
	/bin/echo "radiusd stop."
	/sbin/daemon_mgr radiusd stop "/usr/local/sbin/radiusd -d /etc/raddb"
	/bin/kill -9 `/bin/cat /var/run/radiusd.pid` 1>>/dev/null 2>&1
	/bin/rm -f /var/run/radiusd.pid 1>>/dev/null 2>&1
}

reload()
{
	/bin/echo "radiusd reload."
	init_conf
	if [ -f /var/run/radiusd.pid ]; then
		/bin/kill -HUP `/bin/cat /var/run/radiusd.pid`
	else
		/bin/kill -HUP `/bin/pidof radiusd`
	fi
}

case "$1" in
    start)
	start
	;;
    stop)
	stop
	;;
    restart)
	stop
	/bin/sleep 3
	start
	;;
    reload)
	reload
	;;
    *)
        echo "Usage: /etc/init.d/$0 {start|stop|restart|reload}"
        exit 1
esac

exit 0
