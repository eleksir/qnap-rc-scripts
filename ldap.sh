#!/bin/sh
#
NSS_CONFIG=/etc/nsswitch.conf

# Check that smb.conf exists.
[ -f $NSS_CONFIG ] || exit 0

start() {
	echo -n "Starting ldap services:"
	if [ `/sbin/getcfg LDAP Enable -u -d FALSE` = FALSE ]; then
		echo " disabled."
		exit 0
	fi
	ldap_host=`/sbin/getcfg LDAP host -d ""`
	lret=0
	if [ "x$ldap_host" != "x" ]; then
		if [ -f /sbin/dns_resolve ] && [ ! -f /tmp/.boot_done ]; then
			/sbin/dns_resolve "$ldap_host" 1>/dev/null 2>&1
			lret=$?
		fi
		if [ $lret = 0 ]; then
			/bin/sed "/compat winbind/d" ${NSS_CONFIG} > ${NSS_CONFIG}.tmp
			/bin/sed "21i\\passwd:     compat winbind ldap" ${NSS_CONFIG}.tmp > ${NSS_CONFIG}.tmp1
			/bin/sed "23i\\group:      compat winbind ldap" ${NSS_CONFIG}.tmp1 > ${NSS_CONFIG}
			/bin/rm -f ${NSS_CONFIG}.tmp ${NSS_CONFIG}.tmp1
		else
			if [ ! -f /tmp/.boot_done ]; then
				/sbin/setcfg LDAP Enable FALSE
			fi
		fi
	fi
	echo " done."
}

stop() {
	echo -n "Shutting down ldap services:"
	/bin/sed "/compat winbind/d" ${NSS_CONFIG} > ${NSS_CONFIG}.tmp
	/bin/sed "21i\\passwd:     compat winbind" ${NSS_CONFIG}.tmp > ${NSS_CONFIG}.tmp1
	/bin/sed "23i\\group:      compat winbind" ${NSS_CONFIG}.tmp1 > ${NSS_CONFIG}
	/bin/rm -f ${NSS_CONFIG}.tmp ${NSS_CONFIG}.tmp1
	echo " done."
}

restart() {
	stop
	start
}

case "$1" in
  start)
  	start
	;;
  stop)
  	stop
	;;
  restart)
  	restart
	;;
  *)
	echo "Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $?
