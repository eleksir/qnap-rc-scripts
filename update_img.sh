#!/bin/sh
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

PIC_RAW="/sbin/pic_raw"
TMP_UPDATE_FLAG="/tmp/update_flag"
UPDATE_PROCESS="/tmp/update_process"
UPDATE_FW_PROCESS="/tmp/update_fw_process"
UPDATE_FOLDER="/mnt/update"
BLOCK_SIZE=512
# check if the HAL subsystem exist
if [ -x /sbin/hal_app ]; then
    BOOT_DEV=$(/sbin/hal_app --get_boot_pd port_id=0)
    EXT_ROOT="/dev/md13"
else
    BOOT_DEV="/dev/sdx"
    EXT_ROOT="/dev/sda4"
fi
PURE_BOOT_DEV=$(/bin/echo $BOOT_DEV | /bin/cut -f3 -d/)
FLASH_BOOT=${BOOT_DEV}1
FLASH_RFS1=${BOOT_DEV}2
FLASH_RFS2=${BOOT_DEV}3
FLASH_BOOT_MP="/root/FLASH_BOOT"
FLASH_RFS1_MP="/root/FLASH_RFS1"
FLASH_RFS2_MP="/root/FLASH_RFS2"
FLASH_RFS1_SIZE=52560
#FLASH_SIZE_ARM=mtd1+mtd2+mtd3
FLASH_SIZE_ARM=14336
FLASH_SIZE_PPC=14336
FLASH_RFS1_SIZE_X59=230188
FLASH_RFS1_BLOCK_X59=484608
FLASH_RFS1_SIZE_X59H=110000
FLASH_RFS1_BLOCK_X59H=235008
FLASH_RFS_ERR=0
DEFAULT_CFG_FILE="/etc/config/uLinux.conf"
IMG_FILE_LIST="bzImage initrd.boot rootfs2.bz rootfs_ext.tgz"
MNT_ROOTFS2_TMP="/rootfs2_tmp"
FLASH_KERNEL="/dev/mtdblock1"
FLASH_ROOTFS_BASIC="/dev/mtdblock2"
FLASH_ROOTFS2="/dev/mtdblock3"
FLASH_FDT="/dev/mtdblock6"
IMG_FILE_LIST_ARM="uImage initrd.boot rootfs2.img"
IMG_FILE_LIST_PPC="uImage initrd.uboot rootfs2.bz canyonlands_qnap.dts"
BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf 2>/dev/null`
FLASH_RFS1_BLOCK=`/bin/cat /sys/block/${PURE_BOOT_DEV}/${PURE_BOOT_DEV}2/size 2>/dev/null`
Project_Name=`/sbin/getcfg Project Name -d null -f /var/default`
TMP_CONFIG="/tmp/nasconfig_tmp"
MIN_FSIZE=200 #minimum config file size required

do_beep()
{

	if [ x`/sbin/getcfg Misc "Buzzer Quiet Enable"` != "xTRUE" ]; then
# check if the HAL subsystem exist
        if [ -x /sbin/hal_app ]; then
    		/sbin/hal_app --se_buzzer enc_id=0,mode=1
            sleep 1
    		/sbin/hal_app --se_buzzer enc_id=0,mode=1
            sleep 1
    		/sbin/hal_app --se_buzzer enc_id=0,mode=1
        else
    		${PIC_RAW} 81; sleep 1; ${PIC_RAW} 81; sleep 1; ${PIC_RAW} 81
        fi    		
	fi
}


check_tune_config_partition()
{
echo check_tune_config_partition $1 $2 >> /tmp/check.log
NAS_CONFIG_BAK=/tmp/nas_config.tgz
NAS_CONFIG_IMG=/tmp/nas_config.img
TMP_MP=/tmp/nasconfig_mp
NAS_CONFIG=$1
NAS_CONFIG_SIZE=$2
[ -d $TMP_MP ] || /bin/mkdir $TMP_MP
/bin/mount $NAS_CONFIG $TMP_MP -o ro
CFG_SIZE=`/bin/df -k | /bin/grep "$NAS_CONFIG" | /bin/awk '{print $2}'`
echo Current NAS config formatted size is $CFG_SIZE >> /tmp/check.log
if [ $CFG_SIZE -ge $NAS_CONFIG_SIZE ]; then
	#Backup /tmp/mp6
	cd $TMP_MP
	/bin/tar czf $NAS_CONFIG_BAK *
	RET=$?
	cd /
	/bin/umount $TMP_MP
	echo Backup config_partition return $RET >> /tmp/check.log
	if [ $RET = 0 ]; then
		if [ -f /opt/nasconfig_fs.img.tgz ]; then
			echo Use /opt/nasconfig_fs.img.tgz >> /tmp/check.log
			/bin/tar xzf /opt/nasconfig_fs.img.tgz -O > $NAS_CONFIG
			RET=$?
		else
			echo Create a image with size $NAS_CONFIG_SIZE >> /tmp/check.log
			/bin/dd if=/dev/zero of=$NAS_CONFIG_IMG bs=1k count=$NAS_CONFIG_SIZE
			RET=$?
			if [ $RET = 0 ]; then
				echo Fine tune config partition >> /tmp/check.log
				/sbin/mke2fs -F -v -m0 -b 1024 -I 128 $NAS_CONFIG_IMG
				/bin/dd if=$NAS_CONFIG_IMG of=$NAS_CONFIG bs=1k count=$NAS_CONFIG_SIZE
			fi
			/bin/rm $NAS_CONFIG_IMG
		fi
		echo Dump config_partition image return $RET >> /tmp/check.log
		if [ $RET = 0 ]; then
			/bin/mount $NAS_CONFIG $TMP_MP
			/bin/tar xzf $NAS_CONFIG_BAK -C $TMP_MP
			/bin/umount $TMP_MP
		fi
	fi
	/bin/rm $NAS_CONFIG_BAK
fi
/bin/umount $TMP_MP
/sbin/tune2fs -i 0 -c 0 ${NAS_CONFIG}
/bin/sync
}

remove_update()
{
	for i in $IMG_FILE_LIST
	do
		if [ -f "$UPDATE_FOLDER/$i" ]; then
			/bin/rm -f ${UPDATE_FOLDER}/${i}* 2>/dev/null
		fi	
	done	
	/bin/rm -f ${path_name}* 2>>/dev/null
	if [ $1 = 1 ]; then
		/bin/rm -rf ${UPDATE_FOLDER}/*
	fi
	[ -d "/mnt/HDA_ROOT/update_pkg" ] && /bin/rm -f /mnt/HDA_ROOT/update_pkg/*.qfix 2>>/dev/null
	[ ! -d "/mnt/HDA_ROOT/update_pkg/.qfix" ] || /bin/rm -rf /mnt/HDA_ROOT/update_pkg/.qfix 2>>/dev/null
	[ ! -f /etc/config/qfix.conf ] || /bin/rm -f /etc/config/qfix.conf 2>>/dev/null
	/bin/grep "/dev/ram2" /proc/mounts | /bin/grep "${UPDATE_FOLDER} " 2>/dev/null 1>/dev/null
	if [ $? = 0 ]; then
		/bin/umount /dev/ram2 2>/dev/null
		readlink ${UPDATE_FOLDER} 1>/dev/null 2>/dev/null
		[ $? != 0 ] || /bin/rm -rf ${UPDATE_FOLDER}
	fi
}

get_new_ver()
{
	local filename=$path_name
	set -- `ls -l "$path_name"`
	local pos=`expr $5 - 48`
	/bin/dd if=$filename of=${UPDATE_FOLDER}/newver skip=$pos bs=1 count=16
	[ $? = 0 ] && cat ${UPDATE_FOLDER}/newver || echo ""
}

do_exit()
{
	local ret=0
	local old_ver=`/sbin/getcfg System Version`
	local new_ver=`/bin/cat ${UPDATE_FOLDER}/newver`

	case $1 in
		0)#normal exit
			ret=0
			[ ! -z $new_ver ]	\
			&& /sbin/write_log "[Firmware Upgrade] System updated successfully from $old_ver to $new_ver." 4			\
			|| /sbin/write_log "[Firmware Upgrade] System updated successfully." 4
			;;
		2)#size check failed
			echo -2 > ${UPDATE_PROCESS}
			echo -2 > ${UPDATE_FW_PROCESS}
			ret=1
			[ ! -z $new_ver ]	\
			&& /sbin/write_log "[Firmware Upgrade] System update failed from $old_ver to $new_ver due to update image size error." 1				\
			|| /sbin/write_log "[Firmware Upgrade] System update failed." 1
			;;
		3)#CRC check failed
			echo -3 > ${UPDATE_PROCESS}
			echo -3 > ${UPDATE_FW_PROCESS}
			ret=1
			[ ! -z $new_ver ]	\
			&& /sbin/write_log "[Firmware Upgrade] System update failed from $old_ver to $new_ver." 1				\
			|| /sbin/write_log "[Firmware Upgrade] System update failed." 1
			;;
		4)#Flash failed
			echo -4 > ${UPDATE_PROCESS}
			echo -4 > ${UPDATE_FW_PROCESS}
			ret=1
			[ ! -z $new_ver ]	\
			&& /sbin/write_log "[Firmware Upgrade] System update failed from $old_ver to $new_ver due to flash read/write error." 1		\
			|| /sbin/write_log "[Firmware Upgrade] System update failed due to flash read/write error." 1
			;;
		7)
			echo -7 > ${UPDATE_PROCESS}
			echo -7 > ${UPDATE_FW_PROCESS}
			ret=1
			/sbin/write_log "[Firmware Upgrade] System update failed. The system is being shut down. Please try again after the system started." 1
			;;
		*)
			echo -5 > ${UPDATE_PROCESS}
			echo -5 > ${UPDATE_FW_PROCESS}
			ret=1
			[ ! -z $new_ver ]	\
			&& /sbin/write_log "[Firmware Upgrade] System update failed from $old_ver to $new_ver." 1				\
			|| /sbin/write_log "[Firmware Upgrade] System update failed." 1
			;;
	esac

	if [ $ret = 0 ]; then
		remove_update 2
	else
		remove_update 1
	fi
	/bin/mv /usr/local/sbin/blkid_ /usr/local/sbin/blkid 2>/dev/null
	/sbin/setcfg -e System "QPKG CENTER XML"
	exit $ret
}

check_sum()
{
	if [ ! -f $1 ] || [ ! -f $2 ];then
		echo "${1} or ${2} not found."
		return 1
	fi
	local file_cksum=`/sbin/cksum $1`
	local file_crc=`echo $file_cksum | cut -d ' ' -f 1`
	local file_size=`echo $file_cksum | cut -d ' ' -f 2`

	local cksum_cksum=`cat $2`
	local cksum_crc=`echo $cksum_cksum | cut -d ' ' -f 1`
	local cksum_size=`echo $cksum_cksum | cut -d ' ' -f 2`

	if [ $file_crc = $cksum_crc ] && [ $file_size = $cksum_size ]; then
		return 0
	fi
	return 1
}

size_check()
{
	local sizetotal=0
	local sizekb=0
	for i in $IMG_FILE_LIST
	do
		if [ -f "$UPDATE_FOLDER/$i" ]; then
			sizekb=`/usr/bin/du -k "$UPDATE_FOLDER/$i" | /bin/cut -f 1 -d '/'`
			sizetotal=`/usr/bin/expr $sizetotal + $sizekb`
		fi	
	done
	[ $sizetotal -le $FLASH_RFS1_SIZE ] || do_exit 2
	return 0
}

size_check_arm()
{
	local mtd1_size=2097152
	local mtd2_size=9437184
	local mtd3_size=3145728

	for i in $IMG_FILE_LIST
	do
		if [ -f "$UPDATE_FOLDER/$i" ] && [ -f "$UPDATE_FOLDER/${i}.cksum" ]; then
			if [ ${i} = uImage ]; then
				mtdsize=$mtd1_size
			elif [ ${i} = initrd.boot ]; then
				mtdsize=$mtd2_size
			elif [ ${i} = rootfs2.img ]; then
				mtdsize=$mtd3_size
			else
				break
			fi
			/bin/echo -n "Check $i size ... "
			tmpsize=`/bin/cat "$UPDATE_FOLDER/${i}.cksum" | /bin/cut -d ' ' -f 2`
			if [ -z "$tmpsize" ] || [ $tmpsize -gt $mtdsize ]; then
				/bin/echo Error
				do_exit 2
			fi
			/bin/echo Pass
		fi
	done
	return 0
}

CRC_check()
{
	for i in $IMG_FILE_LIST rootfs_ext.tgz qpatch.tgz
	do
		if [ -f "$UPDATE_FOLDER/$i" ]; then
			check_sum "$UPDATE_FOLDER/$i" "$UPDATE_FOLDER/$i.cksum"
			[ $? = 0 ] || do_exit 3
		fi
	done
	return 0
}

DOM_CRC_check()
{
	for i in $IMG_FILE_LIST
	do
		/bin/echo -n "Checking ${i} ... "
		check_sum "${1}/boot/$i" "$UPDATE_FOLDER/$i.cksum"
		if [ $? != 0 ]; then
			/bin/umount "${1}" 1>>/dev/null 2>>/dev/null
			/bin/echo "Failed."
			do_exit 4
		fi
		/bin/echo "ok"
	done
	return 0
}

set_status_flag()
{
	/bin/mount | /bin/grep $EXT_ROOT 1>>/dev/null 2>>/dev/null
	if [ $? = 0 ]; then
		echo $1 > $TMP_UPDATE_FLAG
		# 402653200 = 384M + 16 bytes = 384 * 1024 * 1024 + 16
		/bin/dd if=$TMP_UPDATE_FLAG of=$EXT_ROOT seek=402653200 bs=1 count=1
		/bin/rm -f $TMP_UPDATE_FLAG
	fi
}
refresh_flash_config_file()
{
	[ -d $TMP_CONFIG ] || /bin/mkdir $TMP_CONFIG
	
	if [ "x${BOOT_CONF}" = "xTS-NASARM" ]; then
		/bin/mount /dev/mtdblock5 $TMP_CONFIG
	elif [ "x${BOOT_CONF}" = "xTS-NASPPC" ]; then
		/bin/mount /dev/mtdblock5 $TMP_CONFIG
	else
		/bin/mount ${BOOT_DEV}6 $TMP_CONFIG
	fi
	[ ! -f ${TMP_CONFIG}/system.map.key ] || rm -f ${TMP_CONFIG}/system.map.key
	[ ! -f ${TMP_CONFIG}/smb.conf ] || rm -f ${TMP_CONFIG}/smb.conf
	[ ! -f ${TMP_CONFIG}/smb.conf.cksum ] || rm -f ${TMP_CONFIG}/smb.conf.cksum
	/bin/umount $TMP_CONFIG
	/bin/touch /tmp/donotrecover
}

img_update()
{
	/bin/grep ram2 /proc/mounts 2>/dev/null 1>/dev/null
	if [ $? = 0 ]; then
		BLOCK_SIZE=2
	fi
	[ -d ${FLASH_BOOT_MP} ] || /bin/mkdir ${FLASH_BOOT_MP}
	[ -d ${FLASH_RFS1_MP} ] || /bin/mkdir ${FLASH_RFS1_MP}
	[ -d ${FLASH_RFS2_MP} ] || /bin/mkdir ${FLASH_RFS2_MP}
	echo "Update RFS1..."
	echo 1 > ${UPDATE_PROCESS}
	echo 1 > ${UPDATE_FW_PROCESS}
	/bin/mount | /bin/grep "${FLASH_RFS1}"
	[ $? != 0 ] || /bin/umount ${FLASH_RFS1}
	/sbin/mke2fs -m0 -F -b 1024 -I 128 ${FLASH_RFS1}
	/bin/sync
	sleep 1
	/bin/mount ${FLASH_RFS1} ${FLASH_RFS1_MP}
	if [ $? = 0 ]; then
	[ -d ${FLASH_RFS1_MP}/boot ] || /bin/mkdir ${FLASH_RFS1_MP}/boot
	[ -f ${UPDATE_FOLDER}/bzImage ] && /bin/cp ${UPDATE_FOLDER}/bzImage* ${FLASH_RFS1_MP}/boot
	[ -f ${UPDATE_FOLDER}/initrd.boot ] && /bin/cp ${UPDATE_FOLDER}/initrd.boot* ${FLASH_RFS1_MP}/boot
	[ -f ${UPDATE_FOLDER}/rootfs2.bz ] && /bin/cp ${UPDATE_FOLDER}/rootfs2.bz* ${FLASH_RFS1_MP}/boot
	[ -f ${UPDATE_FOLDER}/rootfs_ext.tgz ] && /bin/cp ${UPDATE_FOLDER}/rootfs_ext.tgz* ${FLASH_RFS1_MP}/boot
		if [ $FLASH_RFS1_SIZE = $FLASH_RFS1_SIZE_X59 -o $FLASH_RFS1_SIZE = $FLASH_RFS1_SIZE_X59H ] && [ -f ${UPDATE_FOLDER}/qpkg.tar ]; then
			check_sum "$UPDATE_FOLDER/qpkg.tar" "$UPDATE_FOLDER/qpkg.tar.cksum"
			[ $? = 0 ] && /bin/cp ${UPDATE_FOLDER}/qpkg.tar* ${FLASH_RFS1_MP}/boot
			touch ${UPDATE_FOLDER}/update_qpkg_f
		fi
	else
	/sbin/write_log "A read/write error occurred on the first boot partition of the flash disk during system update." 1
	FLASH_RFS_ERR=1
	fi
	/bin/sync
	[ ${FLASH_RFS_ERR} = 0 ] && DOM_CRC_check "${FLASH_RFS1_MP}"
	/bin/umount ${FLASH_RFS1} 1>>/dev/null 2>>/dev/null
	[ ${FLASH_RFS_ERR} = 0 ] || do_exit 4
	/bin/sleep 2
	echo "Update RFS2..."
	echo 2 > ${UPDATE_PROCESS}
	echo 2 > ${UPDATE_FW_PROCESS}
	/bin/mount | /bin/grep "${FLASH_RFS2}"
	[ $? != 0 ] || /bin/umount ${FLASH_RFS2}
	[ ${FLASH_RFS_ERR} = 0 ] && /sbin/mke2fs -m0 -F -b 1024 -I 128 ${FLASH_RFS2}
	/bin/sync
	sleep 1
	/bin/mount ${FLASH_RFS2} ${FLASH_RFS2_MP}
	if [ $? = 0 ] && [ ${FLASH_RFS_ERR} = 0 ]; then
	[ -d ${FLASH_RFS2_MP}/boot ] || /bin/mkdir ${FLASH_RFS2_MP}/boot
	[ -f ${UPDATE_FOLDER}/bzImage ] && /bin/cp ${UPDATE_FOLDER}/bzImage* ${FLASH_RFS2_MP}/boot
	[ -f ${UPDATE_FOLDER}/initrd.boot ] && /bin/cp ${UPDATE_FOLDER}/initrd.boot* ${FLASH_RFS2_MP}/boot
	[ -f ${UPDATE_FOLDER}/rootfs2.bz ] && /bin/cp ${UPDATE_FOLDER}/rootfs2.bz* ${FLASH_RFS2_MP}/boot
	[ -f ${UPDATE_FOLDER}/rootfs_ext.tgz ] && /bin/cp ${UPDATE_FOLDER}/rootfs_ext.tgz* ${FLASH_RFS2_MP}/boot
		if [ $FLASH_RFS1_SIZE = $FLASH_RFS1_SIZE_X59 -o $FLASH_RFS1_SIZE = $FLASH_RFS1_SIZE_X59H ] && [ -f ${UPDATE_FOLDER}/qpkg.tar ]; then
			check_sum "$UPDATE_FOLDER/qpkg.tar" "$UPDATE_FOLDER/qpkg.tar.cksum"
			[ $? = 0 ] && /bin/cp ${UPDATE_FOLDER}/qpkg.tar* ${FLASH_RFS2_MP}/boot
			touch ${UPDATE_FOLDER}/update_qpkg_f
		fi
	else
	/sbin/write_log "A read/write error occurred on the second boot partition of the flash disk during system update." 1
	[ ${FLASH_RFS_ERR} = 0 ] || FLASH_RFS_ERR=2
	fi
	/bin/sync
	/bin/umount ${FLASH_RFS2}
	
#	/usr/bin/readlink /usr 1>>/dev/null 2>>/dev/null
#	if [ $? = 0 ]; then
#		[ -f ${UPDATE_FOLDER}/qpatch.tgz ] && /bin/tar -xzf ${UPDATE_FOLDER}/qpatch.tgz -C /
#	fi
	/bin/sleep 3
	[ ${FLASH_RFS_ERR} = 0 ] || do_exit 4
	set_status_flag 1
	refresh_flash_config_file
	echo "Update Finished."
	echo 3 > ${UPDATE_FW_PROCESS}
	/bin/touch /etc/config/update_fw_ok
	
	return 0
}

# $1: Kernel Image 
# $2: Basic RootFS
# $3: Basic RootFS2
# $4: ARM: Ignore, PPC: FDT blob
img_update_16M()
{
	/bin/grep ram2 /proc/mounts 2>/dev/null 1>/dev/null
	if [ $? = 0 ]; then
		BLOCK_SIZE=2
	fi

	/bin/echo 1 > ${UPDATE_PROCESS}
	/bin/echo 1 > ${UPDATE_FW_PROCESS}
	if [ -x /sbin/badblocks ]; then
		/bin/echo "Check Flash ROM"
		/sbin/badblocks ${FLASH_KERNEL} 1>/dev/null 2>/dev/null
		[ $? = 0 ] || do_exit 4
		/sbin/badblocks ${FLASH_ROOTFS_BASIC} 1>/dev/null 2>/dev/null
		[ $? = 0 ] || do_exit 4
		/sbin/badblocks ${FLASH_ROOTFS2} 1>/dev/null 2>/dev/null
		[ $? = 0 ] || do_exit 4
	fi
	/bin/echo "Update Kernel ..."
	/bin/dd if=${UPDATE_FOLDER}/${1} of=${FLASH_KERNEL} bs=${BLOCK_SIZE} 2>/dev/null
	if [ $? != 0 ]; then
		#try again
		/bin/dd if=${UPDATE_FOLDER}/${1} of=${FLASH_KERNEL} bs=${BLOCK_SIZE}
		[ $? = 0 ] || do_exit 4
	fi
	
	/bin/echo "Update Basic RootFS ..."
	/bin/echo 2 > ${UPDATE_PROCESS}
	/bin/echo 2 > ${UPDATE_FW_PROCESS}
	/bin/dd if=${UPDATE_FOLDER}/${2} of=${FLASH_ROOTFS_BASIC} bs=${BLOCK_SIZE} 2>/dev/null
	if [ $? != 0 ]; then
		/bin/dd if=${UPDATE_FOLDER}/${2} of=${FLASH_ROOTFS_BASIC} bs=${BLOCK_SIZE}
		[ $? = 0 ] || do_exit 4
	fi
	
	/bin/echo "Update Basic RootFS2 ..."
	/bin/dd if=${UPDATE_FOLDER}/${3} of=${FLASH_ROOTFS2} bs=${BLOCK_SIZE} 2>/dev/null
	if [ $? != 0 ]; then
		/bin/dd if=${UPDATE_FOLDER}/${3} of=${FLASH_ROOTFS2} bs=${BLOCK_SIZE}
		[ $? = 0 ] || do_exit 4
	fi
	
	if [ x${4} != x ]; then
		/bin/echo "Update Kernel Device Tree Blob ..."
		/bin/cp ${UPDATE_FOLDER}/${4} ${FLASH_FDT}
	fi

	set_status_flag 1
	refresh_flash_config_file
	echo "Update Finished."
	echo 3 > ${UPDATE_FW_PROCESS}
	/bin/touch /etc/config/update_fw_ok
	
	return 0
}

config_add_endflag()
{
	ISCSI_CONFIG=/etc/config/iscsi_trgt.conf

	/bin/grep "\[END_FLAG\]" $DEFAULT_CFG_FILE 1>>/dev/null 2>>/dev/null
	if [ $? != 0 ] ; then
	 [ `/sbin/getcfg Misc Configured -u -d FALSE` = TRUE ] && /bin/echo "[END_FLAG]" >> $DEFAULT_CFG_FILE
	fi
#also check/backup configs
	if [ -x /usr/bin/du ] && [ -f $ISCSI_CONFIG ]; then
		conf_size=`/usr/bin/du -b $ISCSI_CONFIG | /bin/awk '{print $1}'`
		if [ ! -z "$conf_size" ] && [ "$conf_size" -ge $MIN_FSIZE ]; then
			[ -f ${ISCSI_CONFIG}.last ] || /bin/cp $ISCSI_CONFIG ${ISCSI_CONFIG}.last
		fi
	fi
	/bin/sync
	return 0
}

change_pm_cmd()
{
	/bin/touch /sbin/qpm.sh
	/bin/chmod 755 /sbin/qpm.sh
	/bin/ln -sf /sbin/qpm.sh /sbin/poweroff
	/bin/ln -sf /sbin/qpm.sh /sbin/halt
	/bin/ln -sf /sbin/qpm.sh /sbin/reboot
	/bin/ln -sf /bin/busybox /root/reboot
	/bin/ln -sf /bin/busybox /root/poweroff
	/bin/ln -sf /bin/busybox /root/halt
	/bin/echo -e "#!/bin/sh
  /bin/sleep 5
  /bin/ps | /bin/grep \"/etc/init.d/update.sh\" | /bin/grep -v grep 1>>/dev/null 2>>/dev/null
  [ \$? != 0 ] || exit 1
  /root/\`/usr/bin/basename \${0}\`
  exit 0" > /sbin/qpm.sh
  /bin/chmod 755 /sbin/qpm.sh
}

recover_pm_cmd()
{
	/bin/ln -sf /sbin/busybox /sbin/poweroff
	/bin/ln -sf /sbin/busybox /sbin/halt
	/bin/ln -sf /sbin/busybox /sbin/reboot
}

path_name=$1
if [ "x${BOOT_CONF}" = "xTS-NASARM" ]; then
	IMG_FILE_LIST=$IMG_FILE_LIST_ARM
	FLASH_RFS1_SIZE=$FLASH_SIZE_ARM
elif [ "x${BOOT_CONF}" = "xTS-NASPPC" ]; then
	IMG_FILE_LIST=$IMG_FILE_LIST_PPC
	FLASH_RFS1_SIZE=$FLASH_SIZE_PPC
fi
now_process=`/bin/cat /tmp/update_process 2>>/dev/null`
if [ ! -z "$now_process" ] && [ $now_process = 3 ]; then
	/bin/rm -f /tmp/update_process 2>/dev/null
	[ ! -f /tmp/do_reboot ] || /bin/rm -f /tmp/do_reboot
	/bin/echo "/tmp/update_process is removed. Please try again."
	do_exit 7
fi 
[ -x /usr/bin/basename ] || /bin/ln -sf /bin/busybox /usr/bin/basename
/bin/ps | /bin/grep "/sbin/reboot" | /bin/grep -v grep 1>>/dev/null 2>>/dev/null
doing_reboot=$?
/bin/ps | /bin/grep "/sbin/poweroff" | /bin/grep -v grep 1>>/dev/null 2>>/dev/null
doing_poweroff=$?
/bin/ps | /bin/grep "/sbin/halt" | /bin/grep -v grep 1>>/dev/null 2>>/dev/null
doing_halt=$?
/bin/ps | /bin/grep "/etc/init.d/rcK" | /bin/grep -v grep 1>>/dev/null 2>>/dev/null
doing_rcK=$?
if [ $doing_reboot = 0 ] || [ $doing_poweroff = 0 ] || [ $doing_halt = 0 ] || [  $doing_rcK = 0 ]; then
	/bin/rm -f /tmp/update_process 2>/dev/null
	[ ! -f /tmp/do_reboot ] || /bin/rm -f /tmp/do_reboot
	do_exit 7
fi 
change_pm_cmd

#===== ready to update F/W =====
USING_HDD=0
/bin/grep HDA_ROOT /proc/mounts 2>/dev/null 1>/dev/null
if [ $? = 0 ]; then
	USING_HDD=1
else
	/bin/grep HDA_ROOT /proc/mounts 2>/dev/null 1>/dev/null
	if [ $? = 0 ]; then
		USING_HDD=1
	fi
fi

if [ "x${BOOT_CONF}" = "xTS-NASARM" ]; then
	/bin/mv /usr/local/sbin/blkid /usr/local/sbin/blkid_ 2>/dev/null
	check_tune_config_partition /dev/mtdblock5 1024
elif [ "x${BOOT_CONF}" = "xTS-NASPPC" ]; then
	check_tune_config_partition /dev/mtdblock5 1024
else
	check_tune_config_partition ${BOOT_DEV}6 4096
fi

if [ "x${BOOT_CONF}" = "xTS-NASX86" ] && [ $FLASH_RFS1_BLOCK -ge $FLASH_RFS1_BLOCK_X59 ]; then
	FLASH_RFS1_SIZE=$FLASH_RFS1_SIZE_X59
elif [ "x${BOOT_CONF}" = "xTS-NASX86" ] && [ $FLASH_RFS1_BLOCK -ge $FLASH_RFS1_BLOCK_X59H ]; then
        FLASH_RFS1_SIZE=$FLASH_RFS1_SIZE_X59H
fi


if [ -f $UPDATE_FOLDER/update_check ]; then
	$UPDATE_FOLDER/update_check
	if [ $? != 0 ]; then
		[ ! -x /sbin/lock_system_update ] || /sbin/lock_system_update unlock
		do_exit 3
	fi
fi	

/sbin/write_log "[Firmware Upgrade] Start to update the firmware." 4

if [ -f $UPDATE_FOLDER/update_bios.sh ]; then
	$UPDATE_FOLDER/update_bios.sh $UPDATE_FOLDER/flashrom.img
fi

if [ -f $UPDATE_FOLDER/sas_fw/sas_fw_update.sh ]; then
        $UPDATE_FOLDER/sas_fw/sas_fw_update.sh
fi

if [ "x${BOOT_CONF}" = "xTS-NASX86" ]; then
	if [ $USING_HDD = 1 ]; then
		echo "Update image using HDD ..."
		CRC_check
		size_check
		img_update
		remove_update 2
	else
		echo "Update image using RAM disk ..."
		CRC_check
		size_check
		img_update
		remove_update 1
	fi
elif [ "x${BOOT_CONF}" = "xTS-NASARM" ]; then
	if [ $USING_HDD = 1 ]; then
		echo "Update image using HDD ..."
		CRC_check
		size_check_arm
		img_update_16M uImage initrd.boot rootfs2.img
		remove_update 2
	else
		echo "Update image using RAM disk ..."
		CRC_check
		size_check_arm
		img_update_16M uImage initrd.boot rootfs2.img
		remove_update 1
	fi
elif [ "x${BOOT_CONF}" = "xTS-NASPPC" ]; then
	if [ $USING_HDD = 1 ]; then
		echo "Update image using HDD ..."
		CRC_check
		size_check
		img_update_16M uImage initrd.uboot rootfs2.img canyonlands_qnap.dtb
		remove_update 2
	else
		echo "Update image using RAM disk ..."
		CRC_check
		size_check
		img_update_16M uImage initrd.uboot rootfs2.img canyonlands_qnap.dtb
		remove_update 1
	fi
fi
/bin/mv /usr/local/sbin/blkid_ /usr/local/sbin/blkid 2>/dev/null
config_add_endflag
/sbin/setcfg -e System "QPKG CENTER XML"
if [ x`/bin/cat ${UPDATE_FW_PROCESS} 2>/dev/null` = x3 ]; then
	/bin/grep do_beep_check_flag /etc/init.d/update.sh 1>/dev/null 2>/dev/null
	[ $? = 0 ] && do_beep
	[ ! -x /sbin/lock_system_update ] || /sbin/lock_system_update unlock
	/bin/echo 3 > ${UPDATE_PROCESS}
	exit 0
else
	exit 1
fi

