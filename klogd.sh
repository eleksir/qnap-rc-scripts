#!/bin/sh
_lock_file=/var/lock/subsys/klogd.sh
_klogd_path=/mnt/HDA_ROOT/.logs
_klogd_file=kmsg

dump()
{
	[ -f ${_klogd_path}/${_klogd_file}.2 ] && /bin/cat ${_klogd_path}/${_klogd_file}.2
	[ -f ${_klogd_path}/${_klogd_file}.1 ] && /bin/cat ${_klogd_path}/${_klogd_file}.1
	[ -f ${_klogd_path}/${_klogd_file} ] && /bin/cat ${_klogd_path}/${_klogd_file}
}

start()
{
	_max_size=1024000

	if [ -f ${_lock_file} ]; then
		/bin/echo "klogd.sh is already run"
		exit 1
	fi
	/bin/touch ${_lock_file}
	if [ -d ${_klogd_path} ]; then
		/bin/cat ${_klogd_path}/${_klogd_file} >> ${_klogd_path}/${_klogd_file}.1
		_tmp=`/usr/bin/du -b ${_klogd_path}/${_klogd_file}.1`
		_size=`/bin/echo ${_tmp} | /bin/cut  -d ' ' -f 1`
		if [ ${_size} -gt ${_max_size} ]; then
			/bin/mv ${_klogd_path}/${_klogd_file}.1 ${_klogd_path}/${_klogd_file}.2
		fi
		/bin/rm ${_klogd_path}/${_klogd_file}
	else
		/bin/mkdir -p ${_klogd_path}
	fi

	while [ 1 ]; do
		/bin/dd if=/proc/kmsg of=${_klogd_path}/${_klogd_file} bs=1 count=${_max_size}
		/bin/mv ${_klogd_path}/${_klogd_file} ${_klogd_path}/${_klogd_file}.2
	done
}

stop()
{
	/bin/rm -f ${_lock_file}
	/bin/kill -TERM `pidof klogd.sh dd`
}

case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
	stop
	start
        ;;
    dump)
	dump
	;;
    *)
        echo $"Usage: $0 {start|stop|restart|dump}"
        exit 1
esac

exit 0

