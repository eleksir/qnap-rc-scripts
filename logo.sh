#!/bin/sh
#
# description: logo.sh provide the logo files on the appropriate directory of NAS machine.
# 2002/02/01 Johnson Hsu created
# 2002/02/23 Kent re-format
# 2002/03/01 Johnson Hsu modify (pic5 - system logo)
#

SYSTEM_DEVICE=`/sbin/getcfg System "System Device" -d /dev/hda`

HTTPD_IMG_PATH=/home/httpd/v3_menu/images
DEFAULT_IMG=/etc/config/logo/default_logo_img.gif
SHOW_IMG=$HTTPD_IMG_PATH/show_logo_img.gif

copy_file()
{
	if [ ! -f $1 ]; then
		/bin/cp -f $DEFAULT_IMG $1 2>/dev/null 1>/dev/null
		/bin/cp -f $1 $HTTPD_IMG_PATH 2>/dev/null 1>/dev/null
	else
		/bin/cp -f $1 $HTTPD_IMG_PATH 2>/dev/null 1>/dev/null
	fi
}

# check if any file exist???
if [ ! -f $DEFAULT_IMG ]; then
        /bin/cp -f $HTTPD_IMG_PATH/spacer.gif $DEFAULT_IMG
fi

pic1=`/sbin/getcfg LOGO "PIC1 LOGO Image Path" -d /etc/config/logo/pic1_logo_img.gif`
copy_file $pic1

pic2=`/sbin/getcfg LOGO "PIC2 LOGO Image Path" -d /etc/config/logo/pic2_logo_img.gif`
copy_file $pic2

pic3=`/sbin/getcfg LOGO "PIC3 LOGO Image Path" -d /etc/config/logo/pic3_logo_img.gif`
copy_file $pic3

pic4=`/sbin/getcfg LOGO "PIC4 LOGO Image Path" -d /etc/config/logo/pic4_logo_img.gif`
copy_file $pic4

upload=`/sbin/getcfg LOGO "UPLOAD LOGO Image Path" -d /etc/config/logo/upload_logo_img.gif`
copy_file $upload

index=`/sbin/getcfg "LOGO" "Show PIC Index" -u -d "0"`
	
case $index in
	0)
		# disable logo function, use default image
		pic=$DEFAULT_IMG
		;;
	1)
		pic=`/sbin/getcfg "LOGO" "PIC1 LOGO Image Path" -d /etc/config/logo/pic1_logo_img.gif`
	       	;;
	2)
                pic=`/sbin/getcfg "LOGO" "PIC2 LOGO Image Path" -d /etc/config/logo/pic2_logo_img.gif`
                ;;
	3)
                pic=`/sbin/getcfg "LOGO" "PIC3 LOGO Image Path" -d /etc/config/logo/pic3_logo_img.gif`
		;;	
	4)
		pic=`/sbin/getcfg "LOGO" "PIC4 LOGO Image Path" -d /etc/config/logo/pic4_logo_img.gif`
		;;	
	5)
		pic=`/sbin/getcfg "LOGO" "Upload LOGO Image Path" -d /etc/config/logo/upload_logo_img.gif`
	       	;;
	*)
	       	echo "LOGO session error !!"
	       	exit 1	
esac

echo use PIC$index as default image

if [ -f $pic ]; then
	/bin/cp -f $pic $SHOW_IMG 2>/dev/null 1>/dev/null
fi

pic5=`/sbin/getcfg LOGO "PIC5 LOGO Image Path" -d /etc/config/logo/pic5_logo_img.gif`

if [ -f $SHOW_IMG ]; then
	/bin/cp -f $SHOW_IMG $pic5 2>/dev/null 1>/dev/null
	/bin/cp -f $pic5 $HTTPD_IMG_PATH 2>/dev/null 1>/dev/null
fi

exit 0
