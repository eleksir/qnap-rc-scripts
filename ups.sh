#!/bin/sh

case "$1" in
  start)	
        # Start daemons.
	UPS_DRV=`/sbin/getcfg qnapups driver -d "" -f /etc/config/ups/ups.conf`
	/etc/init.d/usb_ups.sh start "$UPS_DRV"
	/etc/init.d/snmp_ups.sh start
        ;;
  stop)
        # Stop daemons.
	/etc/init.d/usb_ups.sh stop
        /etc/init.d/snmp_ups.sh stop
        ;;
  restart)
	$0 stop
	$0 start
	;;
  *)
        echo "Usage: $0 {start|stop|restart}"
        exit 1
esac


