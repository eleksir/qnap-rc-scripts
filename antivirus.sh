#/bin/sh

ANTIVIRUS_SCRIPT="/etc/init.d/antivirus.sh"
ANTIVIRUS_CONFIG="/etc/config/antivirus.global"
ANTIVIRUS_JOBS="/etc/config/antivirus.jobs"
CLAMSCAN="/usr/local/bin/clamscan"
FRESHCLAM="/usr/local/bin/freshclam"
FRESHCLAM_CONFIG="/etc/config/freshclam.conf"
CRONTAB="/etc/config/crontab"
TEMP_CRONTAB="/tmp/crontab"

VOLUME=`/sbin/getcfg Public path -f /etc/smb.conf | cut -d '/' -f 3`
ANTIVIRUS_FOLDER="/share/$VOLUME/.antivirus"
LOG_FOLDER="$ANTIVIRUS_FOLDER/log"
QUARANTINE_CONFIG="/etc/config/antivirus.quarantine"
QUARANTINE_FOLDER="$ANTIVIRUS_FOLDER/quarantine"
UPDATE_PKG="/mnt/HDA_ROOT/update_pkg/antivirus.tgz"
TEMP="$ANTIVIRUS_FOLDER/tmp"
TMPDIR="${ANTIVIRUS_FOLDER}/.temp"
[ -d "${TMPDIR}" ] && export "TMPDIR=${TMPDIR}"
ANTIVIRUS_LIB="$ANTIVIRUS_FOLDER/usr/lib"
ANTIVIRUS_CLAMAV="$ANTIVIRUS_FOLDER/usr/share/clamav"

usage()
{
	/bin/echo "Usage: $0 {start|stop|scan|stop_scan|update_db}"
}

check_config()
{
	if [ ! -f $ANTIVIRUS_CONFIG ]; then
		/bin/echo "[Antivirus]" > $ANTIVIRUS_CONFIG
    		/bin/echo "	AntivirusEnable = FALSE" >> $ANTIVIRUS_CONFIG
		/bin/echo "	AntivirusStatus =" >> $ANTIVIRUS_CONFIG
		/bin/echo "	LastScan =" >> $ANTIVIRUS_CONFIG
		/bin/echo "	LastInfected =" >> $ANTIVIRUS_CONFIG
		/bin/echo "	AutoUpdateDBEnable = FALSE" >> $ANTIVIRUS_CONFIG
		/bin/echo "	AutoUpdateDBCheckTimes = 1" >> $ANTIVIRUS_CONFIG
		/bin/echo "	AutoUpdateDBTime = 0" >> $ANTIVIRUS_CONFIG
		/bin/echo "	ArchiveLogEnable = FALSE" >> $ANTIVIRUS_CONFIG
		if [ -d "/share/$VOLUME/Qdownload" ]; then
			/bin/echo "	ArchiveLogPath = /Qdownload" >> $ANTIVIRUS_CONFIG
		else
			/bin/echo "	ArchiveLogPath = /Download" >> $ANTIVIRUS_CONFIG
		fi
		/bin/echo "	KeepLogLive = 10" >> $ANTIVIRUS_CONFIG
		/bin/echo "	TGZChecksum =" >> $ANTIVIRUS_CONFIG
	fi

	[ -f $ANTIVIRUS_JOBS ] || /bin/echo "" > $ANTIVIRUS_JOBS
}

check_env()
{
	/bin/echo "DatabaseMirror database.clamav.net" > $FRESHCLAM_CONFIG

	[ -f "$QUARANTINE_CONFIG" ] || /bin/echo "" > $QUARANTINE_CONFIG

	/bin/mount | /bin/grep $VOLUME &> /dev/null
	if [ $? -eq 0 ]; then
		[ -d "$ANTIVIRUS_FOLDER" ] || /bin/mkdir -p $ANTIVIRUS_FOLDER
		[ -d "$LOG_FOLDER" ] || /bin/mkdir -p $LOG_FOLDER
		[ -d "$QUARANTINE_FOLDER" ] || /bin/mkdir -p $QUARANTINE_FOLDER
		[ -d "${TMPDIR}" ] || /bin/mkdir -p "${TMPDIR}"
		[ -d "${TMPDIR}" ] && export "TMPDIR=${TMPDIR}"
		[ -d "$ANTIVIRUS_CLAMAV" ] || /bin/mkdir -p $ANTIVIRUS_CLAMAV
		if [ -f $UPDATE_PKG ]; then 
			local tgzchecksum=`/sbin/getcfg "Antivirus" "TGZChecksum" -d "sum" -f "$ANTIVIRUS_CONFIG"`
			local checksum=`/bin/md5sum $UPDATE_PKG |/bin/cut -d' ' -f 1`
			if [ "$checksum" != "$tgzchecksum" ] || [ ! -d "$ANTIVIRUS_FOLDER/usr/" ] || [ ! -f /usr/lib/libclamav.so ]; then
				[ ! -d "${ANTIVIRUS_LIB}" ] || /bin/rm $ANTIVIRUS_LIB/*
				/bin/tar xf $UPDATE_PKG -C $ANTIVIRUS_FOLDER 
				/sbin/setcfg "Antivirus" "TGZChecksum" "$checksum" -f "$ANTIVIRUS_CONFIG"
			fi
		fi
		cd ${ANTIVIRUS_LIB}
		SUB_VER=`ls libclamav.so* | head -1 |tr -d '@*' | cut -d '.' -f 3-5`
		/bin/rm -f /usr/share/clamav/clamav 2>/dev/null
		/bin/ln -sf $ANTIVIRUS_FOLDER/usr/share/clamav /usr/share/
		/bin/ln -sf $ANTIVIRUS_FOLDER/usr/lib/libclamav.so.$SUB_VER /usr/lib/libclamav.so.$SUB_VER
		/bin/ln -sf /usr/lib/libclamav.so.$SUB_VER /usr/lib/libclamav.so.6
		/bin/ln -sf /usr/lib/libclamav.so.$SUB_VER /usr/lib/libclamav.so
	fi

	/bin/touch /tmp/antivirus.lock
	/bin/touch /tmp/antivirus.jobs
}

log_rename()
{
	local type="$1"
	local log_folder="$2"
	[ "x$log_folder" = "x" -o "${log_folder:0:1}" != "/" ] && exit 1
	
	if [ "x$type" = "xall" ]; then
		/usr/bin/find "$log_folder" -type f > $TEMP
	elif [ "x$type" = "xmtime" ]; then
		local keep=`/sbin/getcfg "Antivirus" "KeepLogLive" -f "$ANTIVIRUS_CONFIG"`
		/usr/bin/find "$log_folder" -type f -mtime +$keep > $TEMP
	else
		exit 1
	fi

	while read LINE
	do
		local job_name=`/sbin/getcfg "" "JobName" -f "$LINE"`
		local execution_time=`/bin/echo $LINE|/bin/cut -d'/' -f 6|/bin/cut -d'-' -f 2`
		local job_scan_time=`/sbin/getcfg "" "LogLastScanTime" -f "$LINE"`
		local year=`/bin/echo $job_scan_time|/bin/cut -d'/' -f 1`
		local month=`/bin/echo $job_scan_time|/bin/cut -d'/' -f 2`
		local day=`/bin/echo $job_scan_time|/bin/cut -d'/' -f 3|/bin/cut -d' ' -f 1`

		/bin/sed 's/^\/share\//\//g' "$LINE" > "$log_folder"/Antivirus_"$job_name"_"$execution_time"_"$year"_"$month"_"$day".log

	done < $TEMP
	/bin/rm -f $TEMP
}

archive_log()
{
	local enable="$1"

	if [ "x$enable" != "x" ]; then
		/bin/cat $CRONTAB | /bin/grep -v "archive_log" > "$TEMP_CRONTAB"
		/bin/mv $TEMP_CRONTAB $CRONTAB
		if [ $(($enable)) -eq 1 ]; then
			/bin/echo "0 0 * * * $ANTIVIRUS_SCRIPT archive_log" >> $CRONTAB
		fi
		/usr/bin/crontab $CRONTAB
	else
		local share=`/sbin/getcfg "Antivirus" "ArchiveLogPath" -f "$ANTIVIRUS_CONFIG"`
		local keep=`/sbin/getcfg "Antivirus" "KeepLogLive" -f "$ANTIVIRUS_CONFIG"`
		for i in `/bin/mount |/bin/grep _DATA |/bin/cut -d ' ' -f 3|/usr/bin/xargs /bin/echo -n`
		do
			if [ -d "$i$share" ]; then
				local enable=`/sbin/getcfg "Antivirus" "ArchiveLogEnable" -f "$ANTIVIRUS_CONFIG"`
				if [ "$enable" = "TRUE" ]; then
					log_rename "mtime" "$LOG_FOLDER" &> /dev/null
					cd $LOG_FOLDER && /usr/local/sbin/zip "$i$share/Antivirus_logs_`date +"%Y_%m_%d"`.zip" Antivirus_* 2> /dev/null && cd -
				fi
				cd $LOG_FOLDER && /bin/rm -f Antivirus_* && /usr/bin/find . -type f -mtime +$keep | /usr/bin/xargs /bin/rm -f 2> /dev/null && cd -
				break
			fi
		done
	fi
}

restart_crond()
{
	[ ! -f "$CRONTAB" ] && /bin/echo "$CRONTAB is not exit." && exit 0

	/bin/cat $CRONTAB | /bin/grep -v "antivirus.sh scan" > "$TEMP_CRONTAB"
	/bin/mv $TEMP_CRONTAB $CRONTAB

	local i=0
	while read LINE
	do
		local session_id=`/bin/echo $LINE |/bin/grep '\['|/bin/cut -d'[' -f2|/bin/cut -d']' -f1`
		[ "x$session_id" = "x" ] && continue

		local job_id=`/bin/echo $session_id |/bin/cut -d' ' -f2`
		# initial LogLastScanTime
		/sbin/setcfg "Antivirus" "AntivirusStatus" "" -f "$ANTIVIRUS_CONFIG"
		local scan_time=`/sbin/getcfg "Job $job_id" "LogLastScanTime" -d "" -f "$ANTIVIRUS_JOBS"`
		if [ "x$scan_time" = "xIn progress" ]; then
			/sbin/setcfg "Job $job_id" "JobStatus" "2" -f "$ANTIVIRUS_JOBS"
			/sbin/setcfg "Job $job_id" "LogLastScanTime" "" -f "$ANTIVIRUS_JOBS"
		fi

		local schedule=`/sbin/getcfg "$session_id" "JobSchedule" -f "$ANTIVIRUS_JOBS"`

		if [ "$schedule" = "now" ]; then
			/bin/echo ""
		elif [ "$schedule" = "interval" ]; then
			local interval=`/sbin/getcfg "$session_id" "JobScheduleInterval" -f "$ANTIVIRUS_JOBS"`
			local num=`/bin/echo $interval|/bin/cut -d',' -f 1`
			local unit=`/bin/echo $interval|/bin/cut -d',' -f 2`
			if [ "$unit" = "m" ]; then
				/bin/echo "*/$num * * * * $ANTIVIRUS_SCRIPT scan $job_id" >> $CRONTAB
			elif [ "$unit" = "h" ]; then
				/bin/echo "0 */$num * * * $ANTIVIRUS_SCRIPT scan $job_id" >> $CRONTAB
			elif [ "$unit" = "d" ]; then
				/bin/echo "0 0 */$num * * $ANTIVIRUS_SCRIPT scan $job_id" >> $CRONTAB
			else
				/bin/echo "No such interval." && exit 1
			fi
		elif [ "$schedule" = "daily" ]; then
			local daily=`/sbin/getcfg "$session_id" "JobScheduleDaily" -f "$ANTIVIRUS_JOBS"`
			local hour=`/bin/echo $daily|/bin/cut -d',' -f 1`
			local minute=`/bin/echo $daily|/bin/cut -d',' -f 2`
			/bin/echo "$minute $hour * * * $ANTIVIRUS_SCRIPT scan $job_id" >> $CRONTAB
		elif [ "$schedule" = "weekly" ]; then
			local weekly=`/sbin/getcfg "$session_id" "JobScheduleWeekly" -f "$ANTIVIRUS_JOBS"`
			local hour=`/bin/echo $weekly|/bin/cut -d',' -f 1`
			local minute=`/bin/echo $weekly|/bin/cut -d',' -f 2`
			local week=`/bin/echo $weekly|/bin/cut -d',' -f 3`
			/bin/echo "$minute $hour * * $week $ANTIVIRUS_SCRIPT scan $job_id" >> $CRONTAB
		else
			/bin/echo "No such schedule type." && return
		fi

		i=$(($i + 1))
	done < $ANTIVIRUS_JOBS

	[ $i -ge 0 ] && /usr/bin/crontab $CRONTAB
}

stop_crond()
{
	[ ! -f "$CRONTAB" ] && /bin/echo "$CRONTAB is not exit." && exit 0

	/bin/cat $CRONTAB | /bin/grep -v "antivirus.sh" > "$TEMP_CRONTAB"
	/bin/mv $TEMP_CRONTAB $CRONTAB
	/usr/bin/crontab $CRONTAB
}

start()
{
	check_config
	check_env
	local enable=`/sbin/getcfg "Antivirus" "AntivirusEnable" -d "FALSE" -f "$ANTIVIRUS_CONFIG"`
	[ "x$enable" != "xTRUE" ] && exit 0
	restart_crond
	archive_log 1
	local in_days=`/sbin/getcfg "Antivirus" "AutoUpdateDBCheckTimes" -f "$ANTIVIRUS_CONFIG"`
	update_db $in_days &> /dev/null

	#20411 check auto update DB after reboot
	local last_update=`/sbin/getcfg "Antivirus" "AutoUpdateDBTime" -f "$ANTIVIRUS_CONFIG"`
	[ "x$last_update" = "x" ] && last_update=0
	local now_time=`date +%s`
	local _enable=`/sbin/getcfg "Antivirus" "AutoUpdateDBEnable" -f "$ANTIVIRUS_CONFIG"`
	if [ ! -f ${ANTIVIRUS_CLAMAV}/main.cld ] || [ o"$_enable" = oTRUE ]; then
	[ $(($now_time-$last_update)) -ge $(($in_days*24*60*60)) ] && update_db &> /dev/null
	fi
}

stop()
{
	archive_log 0
	stop_crond
	i=1
	while [ $i -le 64 ]; do
		stop_scan $i
		i=$(($i+1))
		[ x`pidof clamscan` != x ] || break
	done
	/sbin/setcfg "Antivirus" "AntivirusStatus" "" -f "$ANTIVIRUS_CONFIG"
}

set_quarantine_conf()
{
	local job_id="$1"
	local job_execution_times="$2"
	local logfile="$LOG_FOLDER/log.$job_id-$job_execution_times"
	local action=`/sbin/getcfg "Job $job_id" "JobAction" -f "$ANTIVIRUS_JOBS"`

	[ ! -f $logfile ] && /bin/echo "No such file \"$1\"." && exit 1

	local job_name=`/sbin/getcfg "" "JobName" -d "" -f "$logfile"`
	/sbin/setcfg "Infected-$job_id.$job_execution_times" "JobName" "$job_name" -f "$QUARANTINE_CONFIG"

	local i=1
	while read LINE
	do
		[ "x$LINE" = "x" ] && continue	
		#echo "LINE [$LINE]"
		local file=`/bin/echo -n $LINE|/bin/grep FOUND|/bin/cut -d':' -f 1`
		local virus=`/bin/echo -n $LINE|/bin/grep FOUND|/bin/cut -d':' -f 2|/bin/cut -d' ' -f 2`
		
		#echo "logfile [$logfile] LINE [$LINE] file [$file] virus [$virus] i [$i]"
		[ ! -f "$file" ] && continue
		[ "x$virus" = "x" ] && continue
		if [ $(($action)) -eq 1 ]; then
			
			local volume=`/bin/echo $file|/bin/cut -d '/' -f 3`
			local folder="/share/$volume"

			#check iso mount and ignore
			/bin/df|/bin/grep "/dev/loop"|/bin/grep "/share/$VOLUME/$volume" &> /dev/null
			[ $? -eq 0 ] && continue

			local path=`/usr/bin/readlink "/share/$volume"`
			if [ $? -ne 0 ]; then
				#FIXME:quarantined into default volume
				local quarantine_folder="/share/$VOLUME/.antivirus/quarantine"
			elif [ "${file:0:14}" = "/share/USBDisk" -o "${file:0:16}" = "/share/eSATADisk" ]; then
				local quarantine_folder="/share/$path/.antivirus/quarantine"
			else
				local quarantine_folder="/share/${path:0:$((${#path}-${#volume}-1))}/.antivirus/quarantine"
			fi

			# bug [19603]: It should be renamed if there is the same path and filename in quarantine
			local ofile="$file"
			local num=1
			while [ 1 ]; do
				[ ! -f "$quarantine_folder/$ofile" ] && break
				ofile="$file($num)"
				num=$(($num+1))
			done
			mv $file $ofile &> /dev/null
			file="$ofile"

			[ ! -d $quarantine_folder ] && /bin/mkdir -p $quarantine_folder
			/bin/tar cf - "$file" | /bin/tar xf - -C "$quarantine_folder" &> /dev/null
			/bin/rm -f "$file"
			/sbin/setcfg "Infected-$job_id.$job_execution_times" "InfectedFile$i" "$file" -f "$QUARANTINE_CONFIG"
			/sbin/setcfg "Infected-$job_id.$job_execution_times" "InfectedVirus$i" "$virus" -f "$QUARANTINE_CONFIG"
		fi
		i=$(($i+1))
	done < $logfile
	[ $(($i-1)) -ne 0 ] && /sbin/write_log "[Antivirus] Virus Found by the scan job $job_name." 2
	/sbin/setcfg "Infected-$job_id.$job_execution_times" "InfectedFileNum" "$(($i-1))" -f "$QUARANTINE_CONFIG"
}

send_antivirus_alert_mail()
{
	local type="$1"
	local job_id="$2"
	local job_execution_times="$3"
	local logfile="$LOG_FOLDER/log.$job_id-$job_execution_times"
	local hostname=`/bin/hostname`
	local ip=`/bin/ls /sys/class/net/|/bin/sed -e 's/\//g' 2>/dev/null|/usr/bin/xargs /sbin/ifconfig |/bin/grep "inet addr"|/bin/cut -d: -f 2|/bin/cut -d ' ' -f 1|/bin/grep -v 127.0.0.1|/usr/bin/xargs echo -n`
	local job_name=`/sbin/getcfg "Job $job_id" "JobName" -f "$ANTIVIRUS_JOBS"`
	local start_time=`/sbin/getcfg "Job $job_id" "LogLastScanTime" -f "$ANTIVIRUS_JOBS"`
	local duration=`/bin/cat $logfile|/bin/grep "Time:"|/bin/cut -d '(' -f 2|/bin/cut -d ')' -f 1`
	local virus=`/bin/cat $logfile|/bin/grep "Infected files:"|/bin/cut -d ' ' -f 3`
	local action=`/sbin/getcfg "Job $job_id" "JobAction" -f "$ANTIVIRUS_JOBS"`
	local http_port=`/sbin/getcfg "System" "Web Access Port" -d "8080"`
	local https_port=`/sbin/getcfg "Stunnel" "Port" -d "443"`
	[ $(($action)) -eq 0 ] && action="nothing"
	[ $(($action)) -eq 1 ] && action="quarantine"
	[ $(($action)) -eq 2 ] && action="delete"

	local message="NAS hostname :$hostname \nNAS IP : $ip \nScan Job : $job_name \nStart Date: $start_time\nDuration : $duration \nVirus found : $virus \nDefault action : $action \n\nAdminstration interface:"
	for i in $ip
	do
		message="$message \nNormal:http://$i:$http_port"
	done
	for i in $ip
	do
		if [ "$https_port" = "443" ]; then
			message="$message \nSecure:https://$i"
		else
			message="$message \nSecure:https://$i:$https_port"
		fi
	done
	if [ "$type" = "alert" ]; then
		local object="Antivirus report from $hostname for $job_name"
		message=`/bin/echo -e $message`
	elif [ "$type" = "virus" ]; then
		local object="Antivirus report from $hostname for $job_name - $virus virus found"
		local files=`/bin/cat $logfile|/bin/grep FOUND|/bin/cut -d':' -f 1|sed -e 's/^\/share//g'|sed -e 's/_DATA/#/g'|cut -d# -f2`
		# if message is bigger than 128Kb, alert mail can't send
		if [ ${#files} -gt 131072 ]; then
			files="`/bin/cat $logfile|/bin/grep FOUND|/bin/cut -d':' -f 1|sed -e 's/_DATA/#/g'|cut -d# -f2|/usr/bin/head -n 10` `/bin/echo -e "\n...\n\nPlease check the report for the complete file list.\n"`"
		fi
		message="$message \n\nInfected files:\n"
		message="`/bin/echo -e $message` $files"
	fi
	local level="Warning"
	/sbin/send_antivirus_alert_mail "$object" "$message" "$level"
}

scan()
{
	#check antivirus jobs exist
	[ ! -f "$ANTIVIRUS_JOBS" ] && /bin/echo "$ANTIVIRUS_JOBS is not exit." && exit 0

	local job_id="$1"
	[ "$job_id" = "" ] && usage && exit 0

	local status=`/sbin/getcfg "Job $job_id" "JobStatus" -f "$ANTIVIRUS_JOBS"`
	[ "$status" != "0" -a "$status" != "2" ] && /bin/echo "Job $job_id is running." && exit 1

	/sbin/setcfg "Job $job_id" "LogLastScanTime" "In progress" -f "$ANTIVIRUS_JOBS"
	/sbin/setcfg "Job $job_id" "LogLastSpendTime" "" -f "$ANTIVIRUS_JOBS"
	/sbin/setcfg "Job $job_id" "LogLastInfected" "0" -f "$ANTIVIRUS_JOBS"

	#only show infected file info and scan recursive
	local OPTIONS="-i -r"

	local scan_all_dirs=`/sbin/getcfg "Job $job_id" "JobScanAllDirs" -f "$ANTIVIRUS_JOBS"`
	if [ "$scan_all_dirs" = "1" ]; then
		/usr/bin/find /share/ -type l > $TEMP
		while read LINE
		do
			local tmp=`/bin/echo $LINE|/bin/cut -d '/' -f 4`
			[ ${#tmp} -gt 0 ] && continue
			[ -L "$LINE" ] && OPTIONS="$OPTIONS \"$LINE/\""
		done < $TEMP
		/bin/rm -f $TEMP
	elif [ "$scan_all_dirs" = "0" ]; then
		local scan_dirs=`/sbin/getcfg "Job $job_id" "JobScanDirs" -f "$ANTIVIRUS_JOBS"`
		if [ "x$scan_dirs" != "x" ]; then
			local i=1
			while [ 1 ]; do
				local dirs=`/bin/echo $scan_dirs|/bin/cut -d ';' -f $i`
				[ "x$dirs" = "x" ] && break

				[ $i -ge 2 -a "$dirs" = "$scan_dirs" ] && break

				[ -d "/share$dirs" ] && OPTIONS="$OPTIONS \"/share$dirs\""

				i=$(($i+1))
			done
		fi
	fi

	local include_all_files=`/sbin/getcfg "Job $job_id" "JobIncludeAllFiles" -f "$ANTIVIRUS_JOBS"`
	local get_session=0
	local include_files=""
	while read LINE
	do
		[ "$LINE" = "[Job $job_id]" ] && get_session=1	
		if [ $get_session -eq 1 ]; then
			include_files=`/bin/echo $LINE |/bin/cut -d ' ' -f 1`
			[ "$include_files" = "JobIncludeFiles" ] && include_files=`/bin/echo $LINE |/bin/cut -d ' ' -f 3` && break
		fi
	done < $ANTIVIRUS_JOBS
	include_files="$include_files;"
	if [ "$include_all_files" = "0" -a "x$include_files" != "x" ]; then
		local i=1
		while [ 1 ]; do
			local files=`/bin/echo $include_files|/bin/cut -d ';' -f $i`
			[ "x$files" = "x" ] && break
			[ $i -ge 2 -a "$files" = "$include_files" ] && break
			if [ "${files:0:1}" =  "*" -o "${files:0:1}" =  "?" ]; then
				OPTIONS="$OPTIONS --include=.$files"
			else
				OPTIONS="$OPTIONS --include=$files"
			fi
			i=$(($i+1))
		done
	fi

	#exclude quarantined directory
	[ -d $ANTIVIRUS_FOLDER ] && OPTIONS="$OPTIONS --exclude=\".antivirus\""

	local enable_exclude=`/sbin/getcfg "Job $job_id" "JobExcludeEnable" -f "$ANTIVIRUS_JOBS"`
	if [ "$enable_exclude" = "1" ]; then
		get_session=0
		local exclude_files=""
		while read LINE
		do
			[ "$LINE" = "[Job $job_id]" ] && get_session=1	
			if [ $get_session -eq 1 ]; then
				exclude_files=`/bin/echo $LINE |/bin/cut -d ' ' -f 1`
				[ "$exclude_files" = "JobExcludeFilesDirs" ] && exclude_files=`/bin/echo $LINE |/bin/cut -c 23-` && break
			fi
		done < $ANTIVIRUS_JOBS
		exclude_files="$exclude_files;"
		if [ "x$exclude_files" != "x" ]; then
			i=1
			while [ 1 ]; do
				local files=`/bin/echo $exclude_files|/bin/cut -d ';' -f $i`
				[ "x$files" = "x" ] && break
				files="/share$files"
				if [ -d "$files" ]; then
					files=$(/bin/echo $files|/bin/sed 's/+/\\+/g')
					OPTIONS="$OPTIONS --exclude-dir=\"$files\""
				else
					files=$(/bin/echo $files|/bin/sed 's/+/\\+/g')
					files=$(/bin/echo $files|/bin/sed 's/*/.*/g')
					files=$(/bin/echo $files|/bin/sed 's/?/.?/g')
					OPTIONS="$OPTIONS --exclude=\"$files\""
				fi
				i=$(($i+1))
			done
		fi
	fi

	local max_file_size_enable=`/sbin/getcfg "Job $job_id" "JobMaxFileSizeEnable" -f "$ANTIVIRUS_JOBS"`
	if [ $(($max_file_size_enable)) -eq 1 ]; then
		local max_file_size=`/sbin/getcfg "Job $job_id" "JobMaxFileSize" -f "$ANTIVIRUS_JOBS"`
		[ $(($max_file_size)) -ge 1 -a $(($max_file_size)) -le 4096 ] && OPTIONS="$OPTIONS --max-filesize=$(($max_file_size))M"
	else
		OPTIONS="$OPTIONS --max-filesize=0"
	fi
	local max_scan_size_enable=`/sbin/getcfg "Job $job_id" "JobMaxScanSizeEnable" -f "$ANTIVIRUS_JOBS"`
	if [ $(($max_scan_size_enable)) -eq 1 ]; then
		local max_scan_size=`/sbin/getcfg "Job $job_id" "JobMaxScanSize" -f "$ANTIVIRUS_JOBS"`
		[ $(($max_scan_size)) -ge 1 -a $(($max_scan_size)) -le 4096 ] && OPTIONS="$OPTIONS --max-scansize=$(($max_scan_size))M"
	else
		OPTIONS="$OPTIONS --max-scansize=0"
	fi
	local scan_compress_file=`/sbin/getcfg "Job $job_id" "JobScanCompressFile" -f "$ANTIVIRUS_JOBS"`
	[ "$scan_compress_file" = "0" ] && OPTIONS="$OPTIONS --scan-archive=no --scan-elf=no --scan-pe=no"
	local scan_document_file=`/sbin/getcfg "Job $job_id" "JobScanDocumentFile" -f "$ANTIVIRUS_JOBS"`
	[ "$scan_document_file" = "0" ] && OPTIONS="$OPTIONS --scan-ole2=no --scan-html=no --scan-pdf=no"
	local scan_mail_file=`/sbin/getcfg "Job $job_id" "JobScanMailFile" -f "$ANTIVIRUS_JOBS"`
	[ "$scan_mail_file" = "0" ] && OPTIONS="$OPTIONS --scan-mail=no"

	local action=`/sbin/getcfg "Job $job_id" "JobAction" -f "$ANTIVIRUS_JOBS"`
	#if [ $(($action)) -eq 1 ]; then
	#	OPTIONS="$OPTIONS --move=$QUARANTINE_FOLDER"
	#elif [ $(($action)) -eq 2 ]; then
	if [ $(($action)) -eq 2 ]; then
		OPTIONS="$OPTIONS --remove"
	fi

	#log file and execution times
	local job_execution_times=`/sbin/getcfg "Job $job_id" "JobExecutionTimes" -f "$ANTIVIRUS_JOBS"`
	job_execution_times=$(($job_execution_times + 1))
	/sbin/setcfg "Job $job_id" "JobExecutionTimes" "$job_execution_times" -f "$ANTIVIRUS_JOBS"
	[ -f "$LOG_FOLDER/log.$job_id-$job_execution_times" ] && rm -f $LOG_FOLDER/log.$job_id-$job_execution_times
	[ $(($job_execution_times)) -gt 0 ] && OPTIONS="$OPTIONS --log=$LOG_FOLDER/log.$job_id-$job_execution_times"

	#set job status "run"
	/sbin/setcfg "Job $job_id" "JobStatus" "1" -f "$ANTIVIRUS_JOBS"
	[ $? -ne 0 ] && /bin/echo "set job status \"run\" fail." && exit 0

	# set antivirus status scanning
	/sbin/setcfg "Antivirus" "AntivirusStatus" "0" -f "$ANTIVIRUS_CONFIG"
	local job_name=`/sbin/getcfg "Job $job_id" "JobName" -f "$ANTIVIRUS_JOBS"`
	/sbin/write_log "[Antivirus] Scan job $job_name started." 4
	/bin/echo "JobName = $job_name" >> $LOG_FOLDER/log.$job_id-$job_execution_times
	local finish_time=`date +"%Y/%m/%d %H:%M:%S"`
	/bin/echo "LogLastScanTime = $finish_time" >> $LOG_FOLDER/log.$job_id-$job_execution_times
	#echo "$CLAMSCAN $OPTIONS"
	/bin/sh -c "$CLAMSCAN $OPTIONS &> /dev/null"
	
	#[ $(($action)) -eq 1 ] && set_quarantine_conf $job_id $job_execution_times
	set_quarantine_conf $job_id $job_execution_times
	# convert into doc format
	/sbin/unix2dos -d $LOG_FOLDER/log.$job_id-$job_execution_times

	/sbin/setcfg "Job $job_id" "JobStatus" "0" -f "$ANTIVIRUS_JOBS"
	# set antivirus status scan complete
	[ "`/bin/pidof clamscan`" = "" ] && /sbin/setcfg "Antivirus" "AntivirusStatus" "1" -f "$ANTIVIRUS_CONFIG"
	/sbin/setcfg "Antivirus" "LastScan" "$finish_time" -f "$ANTIVIRUS_CONFIG"
	/sbin/setcfg "Job $job_id" "LogLastScanTime" "$finish_time" -f "$ANTIVIRUS_JOBS"
	local last_spend_time=`/bin/cat $LOG_FOLDER/log.$job_id-$job_execution_times | /bin/grep "Time: "|/bin/cut -d':' -f 2|/bin/cut -d'.' -f 1`
	/sbin/setcfg "Job $job_id" "LogLastSpendTime" "$last_spend_time" -f "$ANTIVIRUS_JOBS"
	local last_infected=`/bin/cat $LOG_FOLDER/log.$job_id-$job_execution_times | /bin/grep "Infected files: "|/bin/cut -d':' -f 2`
	/sbin/setcfg "Job $job_id" "LogLastInfected" "$last_infected" -f "$ANTIVIRUS_JOBS"

	/bin/cat $LOG_FOLDER/log.$job_id-$job_execution_times | /bin/grep FOUND &> /dev/null
	if [ $? -eq 0 ]; then
		/sbin/setcfg "Antivirus" "LastInfected" "$finish_time" -f "$ANTIVIRUS_CONFIG"
		local infected_mail=`/sbin/getcfg "Job $job_id" "JobInfectedMailEnable" -f "$ANTIVIRUS_JOBS"`
		[ $(($infected_mail)) -eq 1 ] && send_antivirus_alert_mail virus "$job_id" "$job_execution_times"
	fi

	local everytime_mail=`/sbin/getcfg "Job $job_id" "JobEndScanMailEnable" -f "$ANTIVIRUS_JOBS"`
	[ $(($everytime_mail)) -eq 1 ] && send_antivirus_alert_mail alert "$job_id" "$job_execution_times"

	/sbin/write_log "[Antivirus] Scan job $job_name completed." 4
}

stop_scan()
{
	#check antivirus jobs exist
	[ ! -f "$ANTIVIRUS_JOBS" ] && /bin/echo "$ANTIVIRUS_JOBS is not exit." && return 0

	local job_id="$1"
	local status=`/sbin/getcfg "Job $job_id" "JobStatus" -f "$ANTIVIRUS_JOBS"`
	[ "$status" != "1" ] && /bin/echo "Job $job_id is not running." && return 1

	local job_execution_times=`/sbin/getcfg "Job $job_id" "JobExecutionTimes" -f "$ANTIVIRUS_JOBS"`
	/bin/echo "Time: stopped" >> $LOG_FOLDER/log.$job_id-$job_execution_times
	/bin/ps | /bin/grep "$ANTIVIRUS_SCRIPT scan $job_id"| /bin/cut -d'a' -f 1| /bin/sed 's/\ //g'| /usr/bin/xargs kill

	for i in `/bin/pidof clamscan`
	do
		/bin/cat /proc/$i/cmdline | /bin/grep "log.$job_id-$job_execution_times"
		[ $? -eq 0 ] && /bin/kill -9 $i &> /dev/null
	done

	#set job status "stop"
	/sbin/setcfg "Job $job_id" "JobStatus" "2" -f "$ANTIVIRUS_JOBS"
	# set antivirus status scan stopped
	[ "`/bin/pidof clamscan`" = "" ] && /sbin/setcfg "Antivirus" "AntivirusStatus" "2" -f "$ANTIVIRUS_CONFIG"
	/sbin/setcfg "Antivirus" "LastScan" "`date +%Y/%m/%d\" \"%k:%M:%S`" -f "$ANTIVIRUS_CONFIG"
	/sbin/setcfg "Job $job_id" "LogLastScanTime" "`date +%Y/%m/%d\" \"%k:%M:%S`" -f "$ANTIVIRUS_JOBS"
	/sbin/setcfg "Job $job_id" "LogLastSpendTime" "stopped" -f "$ANTIVIRUS_JOBS"
	local job_name=`/sbin/getcfg "Job $job_id" "JobName" -f "$ANTIVIRUS_JOBS"`
	/sbin/write_log "[Antivirus] Scan job $job_name stopped by user." 4
}

update_db()
{
	[ ! -f "$FRESHCLAM_CONFIG" ] && /bin/echo "$FRESHCLAM_CONFIG is not exit." && exit 0
	local update_lock="/tmp/FRESHCLAM_CONFIG.lock"
	local TMP_FRESHCLAM_CONFIG="/tmp/FRESHCLAM_CONFIG.tmp"

	if [ -f $update_lock ]; then
		/bin/rm -f $update_lock
		/bin/echo "$FRESHCLAM_CONFIG is locked"
		exit 0
	else
		/bin/touch $update_lock 
	fi

	/bin/sed -e "s/HTTPProxyServer.*//g" -e "s/HTTPProxyPort.*//g" -e "s/HTTPProxyUsername.*//g" -e "s/HTTPProxyPassword.*//g" -e "/^$/d" $FRESHCLAM_CONFIG > $TMP_FRESHCLAM_CONFIG
	
	# check proxy setting
	local proxy_server=`/sbin/getcfg "System" "proxy server" -d ""`
	if [ "$proxy_server" != "" ]; then
		local HTTPProxyServer=${proxy_server%:*}
		local HTTPProxyPort=${proxy_server##*:}
		/bin/echo "HTTPProxyServer $HTTPProxyServer" >> $TMP_FRESHCLAM_CONFIG
		/bin/echo "HTTPProxyPort $HTTPProxyPort" >> $TMP_FRESHCLAM_CONFIG
		local proxy_user=`/sbin/getcfg "System" "proxy username" -d ""`
		local proxy_password=`/sbin/getcfg "System" "proxy password" -d ""`
		if [ "$proxy_user" != "" ]; then
			/bin/echo "HTTPProxyUsername \"$proxy_user\"" >> $TMP_FRESHCLAM_CONFIG
			/bin/echo "HTTPProxyPassword \"$proxy_password\"" >> $TMP_FRESHCLAM_CONFIG
		fi
	fi
	/bin/cp $TMP_FRESHCLAM_CONFIG $FRESHCLAM_CONFIG
	/bin/rm -f $TMP_FRESHCLAM_CONFIG 
	/bin/rm -f $update_lock
	/bin/chmod 700 $FRESHCLAM_CONFIG

	local in_days="$1"
	if [ "x$in_days" != "x" ]; then
		/bin/cat $CRONTAB | /bin/grep -v "update_db" > "$TEMP_CRONTAB"
		/bin/mv $TEMP_CRONTAB $CRONTAB
		local enable=`/sbin/getcfg "Antivirus" "AutoUpdateDBEnable" -f "$ANTIVIRUS_CONFIG"`
		if [ "$enable" = "TRUE" -a $(($in_days)) -ge 1 -a $(($in_days)) -le 999 ]; then
			/bin/echo "0 0 */$in_days * * $ANTIVIRUS_SCRIPT update_db" >> $CRONTAB
		fi
		/usr/bin/crontab $CRONTAB
	else
		#check clamav.net
		/sbin/curl --connect-timeout 10 -I clamav.net 1>>/dev/null 2>>/dev/null
		if [ $? != 0 ]; then
			/sbin/setcfg "Antivirus" "AntivirusStatus" "5" -f "$ANTIVIRUS_CONFIG"
			/sbin/write_log "[Antivirus] Failed to update virus definition." 2
			return 1
		fi
		# set antivirus status updating
		/sbin/setcfg "Antivirus" "AntivirusStatus" "3" -f "$ANTIVIRUS_CONFIG"
		$FRESHCLAM -u admin -l /tmp/.freshclam.log 2>/dev/null
		if [ $? = "0" ]; then
			# set antivirus status update complete
			/sbin/setcfg "Antivirus" "AntivirusStatus" "4" -f "$ANTIVIRUS_CONFIG"
			/bin/grep "Database updated" /tmp/.freshclam.log 1>>/dev/null 2>>/dev/null
			[ $? = 0 ] && /sbin/write_log "[Antivirus] Virus definition updated." 4
			[ ! -f /tmp/.freshclam.log ] || /bin/rm -f /tmp/.freshclam.log
			local now_time=`date +%s`
			/sbin/setcfg "Antivirus" "AutoUpdateDBTime" "$now_time" -f "$ANTIVIRUS_CONFIG"
			return 0
		else
			# set antivirus status update fail
			/sbin/setcfg "Antivirus" "AntivirusStatus" "5" -f "$ANTIVIRUS_CONFIG"
			/sbin/write_log "[Antivirus] Failed to update virus definition." 2
			return 1
		fi
	fi
	return 0
}

case "$1" in
start)
	start
	;;
stop)
	stop
	;;
restart)
	stop
	start
	;;
scan)
	scan $2
	;;
stop_scan)
	stop_scan $2
	;;
update_db)
	update_db $2
	[ $? = 0 ] || exit 1
	;;
set_q)
	set_quarantine_conf $2 $3
	;;
restart_crond)
	restart_crond
	;;
archive_log)
	archive_log $2
	;;
send_mail)
	send_antivirus_alert_mail $2 $3 $4
	;;
init)
	check_config
	;;
log_rename)
	log_rename $2 $3
	;;
*)
	usage
	exit 1
esac

exit 0
