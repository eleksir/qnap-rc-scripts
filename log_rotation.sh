#!/bin/sh

if [ "$1" == "" ]; then
	echo "no file"
elif [ "$1" = "/tmp/rsyslog/root_file" ]; then
	/bin/rm -f "$1"
else
	new_file="${1}_$(/bin/date '+20%y_%m_%d')"
	i=0
	test_file="$new_file"
	while [ -f "$test_file" ]
	do
		i=$(($i+1))
		test_file="${new_file}.${i}"
	done
	
	/bin/mv -f "$1" "$test_file"
fi

exit 0
