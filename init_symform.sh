#!/bin/sh

LOG="/sbin/write_log"
SETCFG="/sbin/setcfg"
OPENSSL="/usr/bin/openssl"
. /etc/init.d/functions

FindDefVol
_ret=$?
if [ $_ret != 0 ]; then
	echo "Default volume not found."
	exit 1
fi

SYMFORM_DIR="${DEF_VOLMP}/.qpkg/Symform"

if [ -d $SYMFORM_DIR ] ; then
	/bin/ln -sf "${SYMFORM_DIR}/Symform.sh" /etc/init.d/Symform.sh
fi

if [ ! -f /etc/config/symform/public.pem ] ; then
	[ -d /etc/config/symform ] || /bin/mkdir -p /etc/config/symform
	/bin/cp -f /etc/default_config/symform/public.pem /etc/config/symform/public.pem
fi

case "$1" in
	start)
		symformEnable=`/sbin/getcfg Symform Enable -d 0`
		if [ $symformEnable -eq 1 ] ; then
			[ -f $SYMFORM_DIR/Symform.sh ] || exit 1
			$SYMFORM_DIR/Symform.sh start
		fi
		;;
	stop)
		[ -f $SYMFORM_DIR/Symform.sh ] || exit 1
		$SYMFORM_DIR/Symform.sh stop
		;;
	restart)
		[ -f $SYMFORM_DIR/Symform.sh ] || exit 1
		$SYMFORM_DIR/Symform.sh restart
		;;
	uninstall)
		$SYMFORM_DIR/Symform.sh stop
		if [ -d $SYMFORM_DIR ] ; then
			$SYMFORM_DIR/.uninstall.sh
			/bin/rm -rf "${SYMFORM_DIR}"
			/bin/rm -rf "${DEF_VOLMP}/.symform"
		fi
		;;
	*)
		echo "Usage: init_symform {start|stop|restart|uninstall}"	
		exit 1
		;;
esac

exit 0
