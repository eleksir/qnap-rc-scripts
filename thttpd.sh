#!/bin/sh
# For Administration
# default port: 6000
THTTPD_PATH=/usr/local/sbin/_thttpd_
#/sbin/http_link
PORT=`/sbin/getcfg System "Web Access Port"`

case "$1" in
    start)
	echo -n "Starting thttpd services:" 
	/usr/bin/readlink /home/httpd/favicon.ico 1>>/dev/null 2>>/dev/null
	if [ $? != 0 ]; then
		[ -f /home/httpd/v3_menu/images/favicon.ico ] && /bin/ln -sf v3_menu/images/favicon.ico /home/httpd/favicon.ico
	fi
	[ -f /tmp/error_thttpd.html ] || /bin/cp /home/httpd/errors/error_thttpd.html /tmp/
	/bin/echo ${PORT} > /var/lock/._thttpd_.port
	if [ -d /home/httpd/cgi-bin/apps ]; then
	/sbin/user_cmd -1 x > /tmp/.x.json
	[ $? != 0 ] || /bin/cp -f /tmp/.x.json /home/httpd/cgi-bin/apps/start.json
	/sbin/user_cmd -2 x > /tmp/.x.json
	[ $? != 0 ] || /bin/cp -f /tmp/.x.json /home/httpd/cgi-bin/apps/systemPreferences/systemPreferences.json
	[ ! -f /tmp/.x.json ] || /bin/rm -f /tmp/.x.json 
	fi
	/sbin/daemon_mgr _thttpd_ start \
		"${THTTPD_PATH} -p ${PORT} -nor -nos -u admin -d /home/httpd -c '**.*' -i /var/lock/._thttpd_.pid"
	echo -n " thttpd"
	touch /var/lock/subsys/thttpd
	[ x`/bin/pidof bcclient` = x ] || /bin/kill -USR1 `/bin/pidof bcclient`
	echo "."
	;;
    stop)
	echo -n "Shutting down thttpd services:" 
	/sbin/daemon_mgr _thttpd_ stop ${THTTPD_PATH}
	/bin/rm -f /var/lock/subsys/thttpd 2>/dev/null
	/bin/rm -f /var/lock/._thttpd_.pid 2>/dev/null
	/bin/sleep 1
	/bin/pidof _thttpd_ 1>>/dev/null 2>>/dev/null
	if [ $? = 0 ]; then
		/bin/kill -9 `/bin/pidof _thttpd_`
		/bin/sleep 1
	fi
	echo -n " thttpd"
	echo "."
	;;
    restart)
	$0 stop
	$0 start
	;;	
    *)
        echo "Usage: /etc/init.d/thttpd {start|stop|restart}"
        exit 1
esac

exit 0
