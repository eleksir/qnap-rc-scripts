#!/bin/sh
# echo "Start ipchange_notify.sh:"
BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf 2>/dev/null`
GETCFG="/sbin/getcfg"
DEFAULT_GW_DEV=`$GETCFG "Network" "Default GW Device" -d eth0`
BOND0_VLAN=`$GETCFG "bond0" "vlan_enable" -d FALSE`
BOND0_VLAN_ID=`$GETCFG "bond0" "vlan_id"`
BOND1_VLAN=`$GETCFG "bond1" "vlan_enable" -d FALSE`
BOND1_VLAN_ID=`$GETCFG "bond1" "vlan_id"`
ETH0_VLAN=`$GETCFG "eth0" "vlan_enable" -d FALSE`
ETH0_VLAN_ID=`$GETCFG "eth0" "vlan_id"`
ETH1_VLAN=`$GETCFG "eth1" "vlan_enable" -d FALSE`
ETH1_VLAN_ID=`$GETCFG "eth1" "vlan_id"`
ETH2_VLAN=`$GETCFG "eth2" "vlan_enable" -d FALSE`
ETH2_VLAN_ID=`$GETCFG "eth2" "vlan_id"`
ETH3_VLAN=`$GETCFG "eth3" "vlan_enable" -d FALSE`
ETH3_VLAN_ID=`$GETCFG "eth3" "vlan_id"`
NIC_NUM=`$GETCFG "Network" "Interface Number" -d "2"`
BOND_NUM=`/usr/bin/expr ${NIC_NUM} / 2`
ADV_BOND_SUPPORT=`$GETCFG "Network" "Advanced Bonding Support" -d "FALSE"`

NSS_ENABLED=`/sbin/getcfg NVR "NSS Enabled" -d no`

use_source_route()
{
  # for each eth, bond,add a dedicate routing table with a default route.
  for LAN_PREFIX in eth bond
  do
    for (( i=0; i <=$NIC_NUM; i++))
    do
      case "$LAN_PREFIX" in
      eth)
        let ROUTE_TBL=$i+1
        DEFAULT_PRIORITY=500
        ;;
      bond)
        let ROUTE_TBL=$i+11
        DEFAULT_PRIORITY=400
        ;;
      esac
      LAN=$LAN_PREFIX$i
      # obtain the ipv4 ip for the specified eth device
      IP_ADDR="$(/sbin/ifconfig $LAN 2> /dev/null | sed -n '/inet /{s/.*addr://;s/ .*//;p}')"
      if [ x$IP_ADDR != "x" ]; then
        # find the default gateway via this interface $LAN
        # default via 172.17.22.1 dev $LAN
        GATEWAY="$(/bin/ip route show | /bin/awk -v NIC=$LAN '{if ($1 == "default" && $5 == NIC) print $3}')"
        SUBNET="$(/bin/ip route show | /bin/awk -v NIC=$LAN -v IP=$IP_ADDR '{if ($9 == IP && $3 == NIC) print $1}')"
        # add a subnet route
        /bin/ip route delete $SUBNET tab $ROUTE_TBL &> /dev/null
        /bin/ip route add $SUBNET via $IP_ADDR dev $LAN tab $ROUTE_TBL &> /dev/null
        # add a source routing rule
        /bin/ip rule delete from $IP_ADDR/32 tab $ROUTE_TBL &> /dev/null
        /bin/ip rule add from $IP_ADDR/32 tab $ROUTE_TBL priority $DEFAULT_PRIORITY
        if [ x$GATEWAY != "x" ]; then
          echo $GATEWAY
          /bin/ip route delete default tab $ROUTE_TBL &> /dev/null
          /bin/ip route add default via $GATEWAY dev $LAN tab $ROUTE_TBL
        fi
      fi
    done
  done
  /bin/ip route flush cache
}

update_ddns()
{
  if [ `/sbin/getcfg DDNS Enable -u -d FALSE` = TRUE ];then
    # echo "ddns enable"
    /sbin/test -f /usr/sbin/create_ddns_conf || return 0
    # echo -n "ddns update"
    /etc/init.d/ddns_update.sh > /dev/null
    # echo "."
    
    if [ -f /var/ddns_tmp.conf ];then
      ddns_tmp=1
      server_type=`/sbin/getcfg "DDNS" "Server Type" -u -d "0" -f "/var/ddns_tmp.conf"`
    else
      ddns_tmp=0
      server_type=`/sbin/getcfg "DDNS" "Server Type" -u -d "0"`
    fi
    if [ "x${server_type}" != x5 ];then
      # check the crontab
      /bin/cat /etc/config/crontab | /bin/grep ddns_update.sh > /dev/null
      result=$?
      if [ $result -ne 0 ]
        then
        echo "0 5 * * * /etc/init.d/ddns_update.sh" >> /etc/config/crontab
        /usr/bin/crontab /etc/config/crontab
        echo "add success"
      fi
      
      if [ ${ddns_tmp} = 1 ]; then
        set_time=`/sbin/getcfg "DDNS" "Check External IP" -d "0" -f "/var/ddns_tmp.conf"`
        else
        set_time=`/sbin/getcfg "DDNS" "Check External IP" -d "0"`
      fi
      /bin/sed -i '/check_ddns_external_ip.sh/d' /etc/config/crontab
      if [ ${set_time} -eq 60 ]; then
        /bin/echo "0 * * * * /etc/init.d/check_ddns_external_ip.sh 1>/dev/null 2>/dev/null" >> /etc/config/crontab
      elif [ ${set_time} -ne 0 ]; then
        /bin/echo "*/${set_time} * * * * /etc/init.d/check_ddns_external_ip.sh 1>/dev/null 2>/dev/null" >> /etc/config/crontab
      fi
      /usr/bin/crontab /etc/config/crontab
      echo "add success"
    fi
    # else
    # echo "ddns disable"
  fi
}

/bin/sleep 2
update_ddns
/etc/init.d/winbind restart &
[ -f /etc/init.d/upnpd.sh ] && /etc/init.d/upnpd.sh restart &
/etc/init.d/gen_issue.sh
[ -f /sbin/lcd_tool ] && /sbin/lcd_tool -n 1

# restart nvrd
if [ -f /etc/init.d/nvrd.sh ] && [ -f /tmp/.boot_done ]; then
#echo "`date`" "begin ipchange_notify.sh restart nvrd" >> /tmp/nvrd_dbg
  if [ "x${NSS_ENABLED}" = "xyes" ]; then
    if [ "`/etc/init.d/nvrd.sh started`" == "1" ]; then   
      /etc/init.d/nvrd.sh _restart &
    fi
  else
  /etc/init.d/nvrd.sh restart &
  fi
#echo "`date`" "end ipchange_notify.sh restart nvrd" >> /tmp/nvrd_dbg
fi

[ -d /sys/kernel/config/target/iscsi ] && /sbin/iscsi_util --ip_change
[ "x`/bin/pidof bcclient`" = x ] || /bin/kill -USR1 `/bin/pidof bcclient`
model_name=`/bin/cat /etc/config/BOOT.conf`

# add the source routing policy
# it must be placed before default route removal
use_source_route

if [ "x$ADV_BOND_SUPPORT" = "xTRUE" ]; then
  for (( i=0; i<$NIC_NUM; i=i+1 ))
  do
    if [ "x${DEFAULT_GW_DEV}" != "xeth$i" ]; then
      /sbin/route del default eth$i &>/dev/null
      VLAN_ENABLE=`$GETCFG "eth$i" "vlan_enable" -d FALSE`
      if [ "x$VLAN_ENABLE" = "xTRUE" ]; then
        VLAN_ID=`$GETCFG "eth$i" "vlan_id"`
        /sbin/route del default eth$i.${VLAN_ID} &>/dev/null
      fi
    fi
  done
  for (( i=0; i<$BOND_NUM; i=i+1 ))
  do
    if [ "x${DEFAULT_GW_DEV}" != "xbond$i" ]; then
      /sbin/route del default bond$i &>/dev/null
      VLAN_ENABLE=`$GETCFG "bond$i" "vlan_enable" -d FALSE`
      if [ "x$VLAN_ENABLE" = "xTRUE" ]; then
        VLAN_ID=`$GETCFG "bond$i" "vlan_id"`
        /sbin/route del default bond$i.${VLAN_ID} &>/dev/null
      fi
    fi
  done
else
  if [ "$NIC_NUM" = "4" ]; then
    if [ "x${DEFAULT_GW_DEV}" != "xeth0" ]; then
      /sbin/route del default eth0 2>/dev/null 1>/dev/null
      if [ "x${ETH0_VLAN}" = "xTRUE" ]; then
        /sbin/route del default eth0.${ETH0_VLAN_ID} 2>/dev/null 1>/dev/null
      fi
    fi
    if [ "x${DEFAULT_GW_DEV}" != "xeth1" ]; then
      /sbin/route del default eth1 2>/dev/null 1>/dev/null
      if [ "x${ETH1_VLAN}" = "xTRUE" ]; then
        /sbin/route del default eth1.${ETH1_VLAN_ID} 2>/dev/null 1>/dev/null
      fi
    fi
    if [ "x${DEFAULT_GW_DEV}" != "xeth2" ]; then
      /sbin/route del default eth2 2>/dev/null 1>/dev/null
      if [ "x${ETH2_VLAN}" = "xTRUE" ]; then
        /sbin/route del default eth2.${ETH2_VLAN_ID} 2>/dev/null 1>/dev/null
      fi
    fi
    if [ "x${DEFAULT_GW_DEV}" != "xeth3" ]; then
      /sbin/route del default eth3 2>/dev/null 1>/dev/null
      if [ "x${ETH3_VLAN}" = "xTRUE" ]; then
        /sbin/route del default eth3.${ETH3_VLAN_ID} 2>/dev/null 1>/dev/null
      fi
    fi
    if [ "x${DEFAULT_GW_DEV}" != "xbond0" ]; then
      /sbin/route del default bond0 2>/dev/null 1>/dev/null
      if [ "x${BOND0_VLAN}" = "xTRUE" ]; then
        /sbin/route del default bond0.${BOND0_VLAN_ID} 2>/dev/null 1>/dev/null
      fi
    fi
    if [ "x${DEFAULT_GW_DEV}" != "xbond1" ]; then
      /sbin/route del default bond1 2>/dev/null 1>/dev/null
      if [ "x${BOND1_VLAN}" = "xTRUE" ]; then
        /sbin/route del default bond1.${BOND1_VLAN_ID} 2>/dev/null 1>/dev/null
      fi
    fi  
    if [ "x${DEFAULT_GW_DEV}" != "xwlan0" ]; then
      /sbin/route del default wlan0 2>/dev/null 1>/dev/null
    fi
  else
    if [ "x${DEFAULT_GW_DEV}" = "xeth1" ]; then
      /sbin/route del default eth0 2>/dev/null 1>/dev/null
      /sbin/route del default wlan0 2>/dev/null 1>/dev/null
      if [ "x${ETH0_VLAN}" = "xTRUE" ]; then
        /sbin/route del default eth0.${ETH0_VLAN_ID} 2>/dev/null 1>/dev/null        
      fi    
      if [ "x$model_name" == "xTS-NASARM" ] && [ ! -x /sbin/hal_app ]; then
        /sbin/route add default gw `/sbin/getcfg "" GATEWAYS -f /etc/dhcpc/dhcpcd-eth1.info` eth1
      fi
    elif [ "x${DEFAULT_GW_DEV}" = "xeth0" ]; then
      /sbin/route del default eth1 2>/dev/null 1>/dev/null
      /sbin/route del default wlan0 2>/dev/null 1>/dev/null
      if [ "x${ETH1_VLAN}" = "xTRUE" ]; then
        /sbin/route del default eth1.${ETH1_VLAN_ID} 2>/dev/null 1>/dev/null
      fi    
      if [ "x$model_name" == "xTS-NASARM" ] && [ ! -x /sbin/hal_app ]; then
        /sbin/route add default gw `/sbin/getcfg "" GATEWAYS -f /etc/dhcpc/dhcpcd-eth0.info` eth0
      fi
    elif [ "x${DEFAULT_GW_DEV}" = "xbond0" ]; then
      /sbin/route del default eth1 2>/dev/null 1>/dev/null
      /sbin/route del default eth0 2>/dev/null 1>/dev/null
      /sbin/route del default wlan0 2>/dev/null 1>/dev/null
      #/sbin/route add default gw `/sbin/getcfg "" GATEWAY -f /etc/dhcpc/dhcpcd-bond0.info` bond0
    elif [ "x${DEFAULT_GW_DEV}" = "xwlan0" ]; then
      /sbin/route del default eth1 2>/dev/null 1>/dev/null
      /sbin/route del default eth0 2>/dev/null 1>/dev/null
      /sbin/route del default bond0 2>/dev/null 1>/dev/null
      if [ "x${ETH1_VLAN}" = "xTRUE" ]; then
        /sbin/route del default eth1.${ETH1_VLAN_ID} 2>/dev/null 1>/dev/null
      fi
      if [ "x${ETH0_VLAN}" = "xTRUE" ]; then
        /sbin/route del default eth0.${ETH0_VLAN_ID} 2>/dev/null 1>/dev/null
      fi
      if [ "x${BOND0_VLAN}" = "xTRUE" ]; then
        /sbin/route del default bond0.${BOND0_VLAN_ID} 2>/dev/null 1>/dev/null
      fi
      /sbin/route add default gw `/sbin/getcfg "" GATEWAY -f /etc/dhcpc/dhcpcd-wlan0.info` wlan0
    fi
  fi
fi

/etc/init.d/ads_register_dns.sh &

/usr/bin/killall -HUP upnpcd
[ -f /var/run/nat_pmp.bin ] && /bin/rm /var/run/nat_pmp.bin
[ -f /etc/init.d/yoics.sh ] && /etc/init.d/yoics.sh restart &

/etc/init.d/hostname.sh
