#!/bin/sh

ROOT_PART="/mnt/HDA_ROOT"
MNT_POINT="/mnt/ext"
HIDDEN_CONF_FILE="${ROOT_PART}/.conf"
HIDDEN_CONF_DIR="${ROOT_PART}/.config"
TS_CONF_DIR=/etc/config
HIDDEN_LOG_DIR="${ROOT_PART}/.logs"
TS_LOG_DIR=/etc/logs
SYSTEM_TEMP_DIR=/tmp
QPKG_RSS_DOC_LINK="/home/httpd/RSS/rssdoc"
QPKG_RSS_DOC_DIR="/etc/config/rssdoc"
GETCFG="/sbin/getcfg"
SETCFG="/sbin/setcfg"
FUNC_SWITCH_CONF="/etc/config/.funcSwitch.conf"
APP_RUNTIME_CONF="/var/.application.conf"

_init_smart_conf()
{
	_SMART_CONF=/etc/config/smart.conf
	if [ ! -f ${_SMART_CONF} ]; then
		/bin/touch /etc/config/smart.conf
		_tmp=`${GETCFG} SMART "Temperature Type" -d NONE`
		if [ "x$_tmp" != "xNONE" ]; then
			${SETCFG} SMART "Temperature Type" "${_tmp}" -f /etc/config/smart.conf
		fi
		_disk_no=`${GETCFG} Storage "Disk Drive Number" -d 8 -f /etc/default_config/uLinux.conf`
		_cnt=1
		while [ ${_cnt} -le ${_disk_no} ]; do
			_tmp=`${GETCFG} SMART "HD ${_cnt} Now Test Type" -d NONE`
			if [ "x$_tmp" != "xNONE" ]; then
				${SETCFG} SMART "HD ${_cnt} Now Test Type" "${_tmp}" -f /etc/config/smart.conf
			fi
			_tmp=`${GETCFG} SMART "HD ${_cnt} Last Test Time" -d NONE`
			if [ "x$_tmp" != "xNONE" ]; then
				${SETCFG} SMART "HD ${_cnt} Last Test Time" "${_tmp}" -f /etc/config/smart.conf
			fi
			_tmp=`${GETCFG} SMART "HD ${_cnt} Last Test Type" -d NONE`
			if [ "x$_tmp" != "xNONE" ]; then
				${SETCFG} SMART "HD ${_cnt} Last Test Type" "${_tmp}" -f /etc/config/smart.conf
			fi
			_tmp=`${GETCFG} SMART "Enable Temperature Threshold ${_cnt}" -d NONE`
			if [ "x$_tmp" != "xNONE" ]; then
				${SETCFG} SMART "Enable Temperature Threshold ${_cnt}" "${_tmp}" -f /etc/config/smart.conf
			fi
			_tmp=`${GETCFG} SMART "Temperature Threshold ${_cnt}" -d NONE`
			if [ "x$_tmp" != "xNONE" ]; then
				${SETCFG} SMART "Temperature Threshold ${_cnt}" "${_tmp}" -f /etc/config/smart.conf
			fi
			_tmp=`${GETCFG} SMART "Short Test Schedule ${_cnt}" -d NONE`
			if [ "x$_tmp" != "xNONE" ]; then
				${SETCFG} SMART "Short Test Schedule ${_cnt}" "${_tmp}" -f /etc/config/smart.conf
			fi
			_tmp=`${GETCFG} SMART "Enable Short Schedule ${_cnt}" -d NONE`
			if [ "x$_tmp" != "xNONE" ]; then
				${SETCFG} SMART "Enable Short Schedule ${_cnt}" "${_tmp}" -f /etc/config/smart.conf
			fi
			_tmp=`${GETCFG} SMART "Extended Test Schedule ${_cnt}" -d NONE`
			if [ "x$_tmp" != "xNONE" ]; then
				${SETCFG} SMART "Extended Test Schedule ${_cnt}" "${_tmp}" -f /etc/config/smart.conf
			fi
			_tmp=`${GETCFG} SMART "Enable Extended Schedule ${_cnt}" -d NONE`
			if [ "x$_tmp" != "xNONE" ]; then
				${SETCFG} SMART "Enable Extended Schedule ${_cnt}" "${_tmp}" -f /etc/config/smart.conf
			fi
			_cnt=`/usr/bin/expr ${_cnt} + 1`
		done
	fi	
}

_hide_apps()
{
	[ -f "$FUNC_SWITCH_CONF" ] || return 1
	DisHF=`$GETCFG DISABLE HomeFeature -d x -f "${FUNC_SWITCH_CONF}"`
	[ x"${DisHF}" = "xx" ] || $SETCFG DISABLE HomeFeature "${DisHF}" -f "${APP_RUNTIME_CONF}"
	_buf=`$GETCFG DISABLE qdownload -d x -f "${FUNC_SWITCH_CONF}"`
	[ x"${_buf}" = "xx" ] || $SETCFG DISABLE qdownload "${_buf}" -f "${APP_RUNTIME_CONF}"
	_buf=`$GETCFG DISABLE musics -d x -f "${FUNC_SWITCH_CONF}"`
	[ x"${_buf}" = "xx" ] || $SETCFG DISABLE musics "${_buf}" -f "${APP_RUNTIME_CONF}"
	_buf=`$GETCFG DISABLE photos -d x -f "${FUNC_SWITCH_CONF}"`
	[ x"${_buf}" = "xx" ] || $SETCFG DISABLE photos "${_buf}" -f "${APP_RUNTIME_CONF}"
	_buf=`$GETCFG DISABLE qmultimedia -d x -f "${FUNC_SWITCH_CONF}"`
	[ x"${_buf}" = "xx" ] || $SETCFG DISABLE qmultimedia "${_buf}" -f "${APP_RUNTIME_CONF}"
	_buf=`$GETCFG DISABLE iTunes -d x -f "${FUNC_SWITCH_CONF}"`
	[ x"${_buf}" = "xx" ] || $SETCFG DISABLE iTunes "${_buf}" -f "${APP_RUNTIME_CONF}"	
	_buf=`$GETCFG DISABLE upnp -d x -f "${FUNC_SWITCH_CONF}"`
	[ x"${_buf}" = "xx" ] || $SETCFG DISABLE upnp "${_buf}" -f "${APP_RUNTIME_CONF}"
	_buf=`$GETCFG DISABLE survielance -d x -f "${FUNC_SWITCH_CONF}"`
	[ x"${_buf}" = "xx" ] || $SETCFG DISABLE survielance "${_buf}" -f "${APP_RUNTIME_CONF}"
	
}

case "$1" in
    start)
##[ -f /sbin/zfs-fuse ] && /sbin/daemon_mgr zfs-fuse start /sbin/zfs-fuse && /bin/mknod /dev/fuse c 10 229
	[ x`/bin/pidof bcclient` != x  ] || /sbin/daemon_mgr bcclient start "/sbin/bcclient"
	[ -f /sbin/zfs-fuse ] && /sbin/zfs-fuse && /bin/mknod /dev/fuse c 10 229 
	if [ -f /tmp/booting ]; then
		tmpdir=/tmp/install
        	CNT=0
		while [ $CNT -lt 10 ]; do
			if [ -f $tmpdir/ffmpeg ] && [ -f $tmpdir/ImageMagick ] && [ -f $tmpdir/radius ] &&\
			   [ -f $tmpdir/vim ] && [ -f $tmpdir/samba ] && [ -f $tmpdir/libtorrent ] && \
			   [ -f $tmpdir/printer ] && [ -f $tmpdir/vaultServices ] && [ -f $tmpdir/language ]; then
				rm -rf /tmp/install
				break
			else
				[ ! -d $tmpdir ] && break
				sleep 1
				CNT=`expr $CNT + 1`
			fi
		done
                if [ -f /sbin/storage_boot_init ]; then
        		/bin/umount /dev/md9 1>/dev/null 2>&1
	        	[ $? = 0 ] || /bin/echo "mountall: umount dev/md9 failed."
		fi
		/bin/rm -rf /tmp/booting
	fi
        if [ -f /sbin/storage_boot_init ]; then	
		[ "x$2" = "x" ] && /sbin/storage_boot_init 1
		driveno=`/sbin/getcfg "Storage" "Disk Drive Number" -d 0`
		driveno_ram=`/sbin/getcfg "Storage" "Disk Drive Number" -d 0 -f /etc/default_config/uLinux.conf`
		if [ $driveno != $driveno_ram ] && [ $driveno_ram -gt 0 ]; then
			/sbin/setcfg "Storage" "Disk Drive Number" $driveno_ram
		fi
	fi
	/bin/sync
	/sbin/ldconfig
	/sbin/hwclock --hctosys
	# create config symbolic link and event log symbolic link
	/usr/bin/readlink $TS_CONF_DIR | grep ".config" 1>/dev/null 2>&1
	# for HAL enable system, the symbolic link is created in storage_util
	if [ $? != 0 -o -x "/sbin/hal_app" ] && [ -f $HIDDEN_CONF_FILE ] && [ -d $HIDDEN_CONF_DIR ]; then
		/bin/echo config hidden config directory exist, create symbolic link $TS_CONF_DIR... [OK]
		# create $TS_CONF_DIR
		/bin/rm -rf $TS_CONF_DIR
		/bin/ln -sf $HIDDEN_CONF_DIR $TS_CONF_DIR
		[ -f /etc/init.d/init_config.sh ] && /etc/init.d/init_config.sh dhcp_info
		/bin/chown admin.administrators $HIDDEN_CONF_DIR
		/bin/touch ${SYSTEM_TEMP_DIR}/check.status
		/bin/touch ${SYSTEM_TEMP_DIR}/format.status
		/bin/cp -a ${HIDDEN_CONF_DIR}/TZ /etc/
		/sbin/hwclock --hctosys
		
		echo Writing model version information...
		if [ -f /etc/config/update_fw_ok ]; then
			TS_VERSION=`/sbin/getcfg System Version -f /etc/default_config/uLinux.conf`
			[ "x`echo -n $TS_VERSION`" != "x" ] && /sbin/setcfg System Version $TS_VERSION
			TS_BUILDVER=`/sbin/getcfg System "Build Version" -f /etc/default_config/uLinux.conf`
			[ "x`echo -n $TS_BUILDVER`" != "x" ] && /sbin/setcfg System "Build Version" $TS_BUILDVER
			TS_BUILDNUM=`/sbin/getcfg System "Build Number" -f /etc/default_config/uLinux.conf`
			[ "x`echo -n $TS_BUILDNUM`" != "x" ] && /sbin/setcfg System "Build Number" $TS_BUILDNUM
			TS_BUILDDATE=`/sbin/getcfg System "Build Date" -f /etc/default_config/uLinux.conf`
			[ "x`echo -n $TS_BUILDDATE`" != "x" ] && /sbin/setcfg System "Build Date" $TS_BUILDDATE
		fi
		MANUFACTURER=`/sbin/getcfg System Manufacturer -f /etc/default_config/uLinux.conf -d ""`
		MANUFACTURER_HD=`/sbin/getcfg System Manufacturer -d ""`
		[ x"${MANUFACTURER}" = x"${MANUFACTURER_HD}" ] || /sbin/setcfg System Manufacturer "${MANUFACTURER}"
		MANUFACTURER_URL=`/sbin/getcfg System ManufacturerURL -f /etc/default_config/uLinux.conf -d ""`
		MANUFACTURER_URL_HD=`/sbin/getcfg System ManufacturerURL -d ""`
		[ x"${MANUFACTURER_URL}" = x"${MANUFACTURER_URL_HD}" ] || /sbin/setcfg System ManufacturerURL "${MANUFACTURER_URL}"
		COMPANY_INFO=`/sbin/getcfg iSCSI "Company Info" -f /etc/default_config/uLinux.conf -d ""`
		COMPANY_INFO_HD=`/sbin/getcfg iSCSI "Company Info" -d ""`
		[ x"${COMPANY_INFO}" = x"${COMPANY_INFO_HD}" ] || /sbin/setcfg iSCSI "Company Info" "${COMPANY_INFO}"
		MODEL_NAME=`/sbin/getcfg iSCSI "Model Name" -f /etc/default_config/uLinux.conf -d ""`
		MODEL_NAME_HD=`/sbin/getcfg iSCSI "Model Name" -d ""`
		[ x"${MODEL_NAME}" = x"${MODEL_NAME_HD}" ] || /sbin/setcfg iSCSI "Model Name" "${MODEL_NAME}"
		
		TS_MODEL=`/sbin/getcfg System Model -f /etc/default_config/uLinux.conf`
		[ "x`echo -n $TS_MODEL`" != "x" ] && /sbin/setcfg System Model $TS_MODEL
		TS_INTERNAL_MODEL=`/sbin/getcfg System "Internal Model" -f /etc/default_config/uLinux.conf`
		[ "x`echo -n $TS_INTERNAL_MODEL`" != "x" ] && /sbin/setcfg System "Internal Model" $TS_INTERNAL_MODEL
		if [ $? = 0 ]; then
			echo Writing model version info...[OK]
		else
			echo Writing Internal Model...[Failed]
		fi
		_hide_apps
		#QTS UI
		/bin/rm -rf /home/httpd/cgi-bin/loginTheme/theme
		/bin/ln -sf theme`/sbin/getcfg System LoginTheme -d 2` /home/httpd/cgi-bin/loginTheme/theme
		/bin/ln -sf "../../images/desktop/desktop-`/sbin/getcfg System LoginBgNum -d 1`.jpg" /home/httpd/cgi-bin/loginTheme/theme/desktop.jpg
		if [ ! -f /home/httpd/cgi-bin/images/desktop/desktop-5.jpg ] && [ ! -f /mnt/HDA_ROOT/update_pkg/jsLib.tgz ]; then
			/bin/mv /home/httpd/cgi-bin/user-settings.json /home/httpd/cgi-bin/user-settings.json.bg6 2>/dev/null
			/bin/cp /home/httpd/cgi-bin/user-settings.json.bg2 /home/httpd/cgi-bin/user-settings.json 2>/dev/null
		fi
	fi

	/sbin/set_home_directory 
	/sbin/sss_convert 
	if [ -f $HIDDEN_CONF_FILE ] && [ -d $HIDDEN_LOG_DIR ]; then
		# create $TS_LOG_DIR
		if [ ! -f $HIDDEN_LOG_DIR/event.log ]; then
			echo log file not found, create symbolic link $TS_LOG_DIR and initial log file... [OK]
			if [ -f $TS_LOG_DIR/event.log ]; then
				/bin/cp $TS_LOG_DIR/event.log $HIDDEN_LOG_DIR
				/bin/cp $TS_LOG_DIR/conn.log $HIDDEN_LOG_DIR
				/bin/rm -rf $TS_LOG_DIR
				/bin/ln -sf $HIDDEN_LOG_DIR $TS_LOG_DIR
				/sbin/log_tool -v -b 10000
				/sbin/conn_log_tool -v -b 10000
			else
				/bin/rm -rf $TS_LOG_DIR
				/bin/ln -sf $HIDDEN_LOG_DIR $TS_LOG_DIR
				/sbin/log_tool -v -c
				/sbin/log_tool -v -b 10000
				/sbin/conn_log_tool -v -c
				/sbin/conn_log_tool -v -b 10000
				# delete old nas logs
				if [ -f $TS_LOG_DIR/nas4000.cnt ]; then
					rm $TS_LOG_DIR/nas4000.cnt
				fi
				if [ -f $TS_LOG_DIR/nas4000.log ]; then
					echo delete old nas logs... [OK]
					rm $TS_LOG_DIR/nas4000.log
				fi
			fi
		else
			/usr/bin/readlink $TS_LOG_DIR 1>/dev/null 2>&1
			if [ $? != 0 ]; then
				# TS_LOG_DIR link not exist
				echo log hidden config directory exist, create symboloc link $TS_LOG_DIR... [OK]
				/bin/ln -sf $HIDDEN_LOG_DIR $TS_LOG_DIR
			fi
		fi
	else
		echo log hidden config directory not exist, initial 10K log file... [OK]
		/bin/mkdir $TS_LOG_DIR
		/sbin/log_tool -v -c
		/sbin/log_tool -v -b 50
		/sbin/conn_log_tool -v -c
		/sbin/conn_log_tool -v -b 50
	fi

	#QFIX
	[ -f /etc/config/qpkg.conf ] || /bin/touch /etc/config/qpkg.conf &
	/etc/init.d/init_qpkg.sh qfix
		
	/sbin/setcfg Printers "Phy exist" 0
	/sbin/setcfg Printers "Phy exist2" 0
	/sbin/setcfg Printers "Phy exist3" 0
	/sbin/setcfg Printers "Phy exist4" 0
	/sbin/setcfg Printers "Phy exist5" 0
	/bin/echo "" > /etc/config/printcap
        [ -f /etc/init.d/nvrd.sh ] && /etc/init.d/nvrd.sh init &
	#[ -d /mnt/HDA_ROOT/.spool ] || /etc/init.d/printer.sh start
	#create QPKG_RSS_DOC_DIR
	/usr/bin/readlink $TS_CONF_DIR | grep ".config" 1>/dev/null 2>&1
	if [ $? = 0 ] && [ ! -d ${QPKG_RSS_DOC_LINK} ]; then
		[ -d ${QPKG_RSS_DOC_DIR} ] || /bin/mkdir ${QPKG_RSS_DOC_DIR}
		/bin/ln -sf ${QPKG_RSS_DOC_DIR} ${QPKG_RSS_DOC_LINK}
		/bin/rm -f ${QPKG_RSS_DOC_DIR}/qpkgcenter* 2>/dev/null
		/bin/rm -f ${QPKG_RSS_DOC_DIR}/last_uptime* 2>/dev/null
		/bin/rm -f ${QPKG_RSS_DOC_DIR}/newsrss_* 2>/dev/null
	fi

	/bin/touch /etc/config/usb_share_mapping
	/bin/touch /etc/default_config/usb_share_mapping
	/bin/touch /var/lock/add_ext_dev
	/bin/touch /etc/config/esata_share_mapping
        /bin/touch /etc/default_config/esata_share_mapping
	/bin/touch /etc/config/usb_share_setting
	/bin/touch /etc/default_config/usb_share_setting
	/bin/touch /etc/config/schedule_boot_setting
	/bin/touch /etc/default_config/schedule_boot_setting
	_init_smart_conf
	[ -d /etc/config/vaultServices ] || /bin/mkdir /etc/config/vaultServices &
	
	[ `/sbin/getcfg "System" "Date Format Index" -d 0` = 0 ] && /sbin/setcfg "System" "Date Format Index" 1 
	#/sbin/setcfg "System" "Date Format Index" 0
	# init rcv_port
	if [ -f /etc/init.d/rcv_port.sh ]; then
		/etc/init.d/rcv_port.sh start
	fi
	/sbin/modagent
	/etc/init.d/snapshot.sh start
#	/etc/init.d/iscsiinit.sh start

	# For locale en_US.utf-8
	if [ -d /usr/share/i18n ]; then
		[ -d /usr/lib/locale ] || /bin/mkdir /usr/lib/locale
		[ -f /usr/lib/locale/locale-archive ] || /usr/bin/localedef -i en_US -f UTF-8 en_US.UTF-8
	fi
	/etc/init.d/init_dns_type.sh
	if [ -f /.disable_home_features ]; then
		/sbin/setcfg QPHOTO Enable FALSE
		/sbin/setcfg BTDownload Enable FALSE
		/sbin/setcfg iTune Enable FALSE
		/sbin/setcfg TwonkyMedia Enable FALSE
	fi

	;;
    stop)
	# nothing to do
	/etc/init.d/iscsiinit.sh stop
	/etc/init.d/snapshot.sh stop
	/usr/bin/killall modagent
	;;
    restart)
	$0 stop
	$0 start
	;;
    *)
	echo "Usage: /etc/init.d/sysinit.sh {start|stop|restart}"
	exit 1
esac

exit 0
