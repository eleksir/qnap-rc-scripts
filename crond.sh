#!/bin/sh

DAEMON_MGR="/sbin/daemon_mgr"
CROND="/usr/sbin/crond"

smart_entry_convert() {
    hd=16
    while [ $hd -gt 0 ]
    do
        old_str="qsmart -t short -d $hd"
        index=$(printf "%02x" $hd)
        new_str="hal_event --pd_self_test dev_id=0x000000${index},action=1"
        /bin/sed -i "s/${old_str}/${new_str}/" /etc/config/crontab
        hd=$(($hd - 1))
    done

    hd=16
    while [ $hd -gt 0 ]
    do
        old_str="qsmart -t extend -d $hd"
        index=$(printf "%02x" $hd)
        new_str="hal_event --pd_self_test dev_id=0x000000${index},action=2"
        /bin/sed -i "s/${old_str}/${new_str}/" /etc/config/crontab
        hd=$(($hd - 1))
    done
}

test -f $CROND || exit 0 # check if crond exists...
_crontab_path=/tmp/cron/crontabs
[ -d ${_crontab_path} ] || mkdir -p ${_crontab_path}
[ -f ${_crontab_path}/admin ] || ln -sf /etc/config/crontab ${_crontab_path}/admin

case "$1" in
	start)
		if [ x"`/bin/cat /etc/config/crontab | /bin/grep "hwclock"`" = x ]; then
			echo "0 4 * * * /sbin/hwclock -s" >> /etc/config/crontab
		fi
		if [ x"`/bin/cat /etc/config/crontab | /bin/grep "vs_refresh"`" = x ]; then
                        echo "0 3 * * * /sbin/vs_refresh" >> /etc/config/crontab
                fi

		/bin/sed -i 's/0 \* \* \* \* \/sbin\/hwclock/0 4 \* \* \* \/sbin\/hwclock/' /etc/config/crontab
		/bin/sed -i '/hdusb_copy/d' /etc/config/crontab
		/bin/cat /etc/config/crontab | /bin/grep -v "backup_conf.sh" > /tmp/crontab 
		/bin/mv /tmp/crontab /etc/config/crontab
		backup_en=`/sbin/getcfg BackupConfig Enable -d null`
		if [ "x$backup_en" != "xFALSE" -a -x "/etc/init.d/backup_conf.sh" ]; then
			backup_period=`/sbin/getcfg BackupConfig "period" -d 7`
			case "$backup_period" in
				1)	#once a day
				echo "4 3 * * * /etc/init.d/backup_conf.sh" >> /etc/config/crontab
				;;
				30)	#once a month
				echo "4 3 3 * * /etc/init.d/backup_conf.sh" >> /etc/config/crontab
				;;
				7)	#once a week
				echo "4 3 * * 3 /etc/init.d/backup_conf.sh" >> /etc/config/crontab
				;;
				*)	#for testing
				echo "*/3 * * * * /etc/init.d/backup_conf.sh" >> /etc/config/crontab
				;;
			esac
		fi
		if [ x"`/bin/cat /etc/config/crontab | /bin/grep "clean_reset_pwd"`" = x ]; then
			/bin/echo "0 3 * * * /sbin/clean_reset_pwd" >> /etc/config/crontab
		fi
		if [ x"`/bin/cat /etc/config/crontab | /bin/grep "nss2_dusg"`" = x ]; then
			echo "0-59/15 * * * * /etc/init.d/nss2_dusg.sh" >> /etc/config/crontab
		fi
		if [ -x /sbin/hal_app ]; then
		    smart_entry_convert
		fi
		/bin/sync
		echo -n "Starting periodic command scheduler: "
		$DAEMON_MGR crond start "$CROND -l 9 -c ${_crontab_path}"
		echo "crond."
		;;

	stop)
		echo -n "Stopping periodic command scheduler: "
		$DAEMON_MGR crond stop "$CROND"
		echo "crond."
		;;

	restart)
		$0 stop
		$0 start
		;;

	*)
		echo "Usage: $1 start|stop|restart"
		exit 1
		;;
esac

exit 0
