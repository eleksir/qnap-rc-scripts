#!/bin/sh
#
# ups.sh This shell script takes care of that starting or stoping ups daemon
#

# See how we were called.
DAEMON_TYPE=`/sbin/getcfg "UPS" "Daemon Type"`

case "$1" in
  start)	
        # Start daemons.
	if [ `/sbin/getcfg "UPS" "Enable" -u -d "FALSE"` = FALSE ]
        then
                echo disabled
                exit 0
        fi
	if [ $DAEMON_TYPE = "2" ]
        then
                UPS_IP=`/sbin/getcfg "UPS" "UPS IP" -d "127.0.0.1"`
                /sbin/daemon_mgr min_snmptrapd start "/sbin/min_snmptrapd /etc/config/ups_snmptrapd.conf $UPS_IP"
                touch /var/lock/subsys/genpowerd
	fi

        ;;
  stop)
        # Stop daemons.
	/sbin/daemon_mgr min_snmptrapd stop /sbin/min_snmptrapd 2> /dev/null
        /bin/rm -f /var/lock/subsys/genpowerd

        ;;
  restart)
	$0 stop
	$0 start
	;;
  *)
        echo "Usage: $0 {start|stop|restart}"
        exit 1
esac


