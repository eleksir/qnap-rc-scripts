#!/bin/sh

if [ "$(/sbin/getcfg BTDownload Version -u -d 1.0 -f /etc/default_config/uLinux.conf)" == '1.0' ]; then
	exit 1
fi

case "$1" in
	"start")
		if [ -f /sbin/bt_scheduler ] && [ $(/sbin/getcfg BTDownload Enable -u -d FALSE) == 'TRUE' ]; then
			echo -n "Starting DS scheduler: "
			/bin/pidof bt_scheduler 1>>/dev/null 2>>/dev/null

			if [ $? != 0 ]; then
				/sbin/bt_scheduler
			fi
		fi
		;;

	stop)
		echo -n "Stop DS scheduler: "
		killall bt_scheduler
		;;

	restart)
		"$0" stop
		"$0" start
		;;

	*)
		echo "Usage: $0 {start|stop|restart}"
		exit 1
esac
