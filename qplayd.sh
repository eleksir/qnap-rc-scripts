#!/bin/sh
# For Administration

test -f /usr/bin/qplayd || exit 0

case "$1" in
    start)
	# dont't do reentry control here!, do it in qplayd
	echo -n "Starting qplayd services:" 
	/sbin/daemon_mgr.nvr qplayd start "/usr/bin/qplayd  2>/dev/null"
	echo -n " qplayd"
	echo "."
	;;
    stop)
	echo -n "Shutting down qplayd services:" 
	/sbin/daemon_mgr.nvr qplayd stop /usr/bin/qplayd
	echo -n " qplayd"
	echo "."
	;;
    restart)
	$0 stop
	$0 start
	;;	
    *)
        echo "Usage: /etc/init.d/qplayd {start|stop|restart}"
        exit 1
esac

exit 0
