#!/bin/sh

AFP_TIMEMACHINE_SHARE=`/sbin/getcfg "TimeMachine" "Display Name" -d "TMBackup"`
TM_CONF_FILE=/etc/config/timemachine.conf
TM_VOL_UUID_CONF=/usr/local/etc/netatalk/afp_voluuid.conf
AVAHI_CONF_DIR=/etc/avahi/services
AVAHI_TM_CONF=${AVAHI_CONF_DIR}/timemachine.service
AVAHI_DAEMON=/usr/sbin/avahi-daemon

#/usr/bin/readlink /share/${AFP_TIMEMACHINE_SHARE} 2>/dev/null 1>/dev/null
#if [ "$?" != "0" ]; then
#	echo "TimeMachine share \"${AFP_TIMEMACHINE_SHARE}\" is not exist !!"
#	exit 0
#fi

create_tm_conf()
{
	BOND=`/sbin/getcfg "Network" "BONDING Support" -d "FALSE"`
	if [ "x$BOND" = "xTRUE" ]; then
		MAC_ADDR=`/bin/cat /sys/class/net/bond0/address`
	else
		MAC_ADDR=`/bin/cat /sys/class/net/eth0/address`
	fi

	VOL_MNT=`/usr/bin/readlink /share/${AFP_TIMEMACHINE_SHARE} | /bin/cut -d '/' -f 1`
	VOL_DEV1=`/bin/mount | /bin/grep "${VOL_MNT} " | /bin/cut -d ' ' -f 1`
	VOL_DEV=`/bin/echo $VOL_DEV1 | /bin/cut -d ' ' -f 1`
	VOL_UUID=`/usr/local/sbin/blkid | /bin/grep "${VOL_DEV}" | /bin/cut -d\" -f2`
	SERVER_NAME=`/sbin/getcfg System "Server Name" -d "NAS"`

	echo "${SERVER_NAME}(TimeMachine)" > ${TM_CONF_FILE}
	echo "_adisk._tcp." >> ${TM_CONF_FILE}
	echo "9" >> ${TM_CONF_FILE}
	echo "sys=waMA=${MAC_ADDR}" >> ${TM_CONF_FILE}
	echo "dk0=adVF=0x83,adVN=${AFP_TIMEMACHINE_SHARE},adVU=${VOL_UUID}" >> ${TM_CONF_FILE}

	echo "${AFP_TIMEMACHINE_SHARE} ${VOL_UUID}" > $TM_VOL_UUID_CONF

	/bin/touch /share/${AFP_TIMEMACHINE_SHARE}/.com.apple.timemachine.supported
cat > ${AVAHI_TM_CONF} <<__EOF__
<?xml version="1.0" standalone='no'?><!--*-nxml-*-->
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
<name replace-wildcards="yes">%h(TimeMachine)</name>
<service>
<type>_adisk._tcp.</type>
<port>9</port>
<txt-record>sys=waMA=${MAC_ADDR}</txt-record>
<txt-record>dk0=adVF=0x83,adVN=${AFP_TIMEMACHINE_SHARE},adVU=${VOL_UUID}</txt-record>
</service>
</service-group>
__EOF__
}

case "$1" in
	start)
		/sbin/tm_quota_check
		create_tm_conf
		${AVAHI_DAEMON} --reload > /dev/null 2>&1
		;;
	stop)
		if [ -f ${TM_CONF_FILE} ]; then
			/bin/rm ${TM_CONF_FILE}
		fi
		/bin/kill -INT `/bin/cat /var/lock/bonjour.timemachine 2>/dev/null` 2>/dev/null 1>/dev/null
		/bin/rm -f /var/lock/bonjour.timemachine 2>/dev/null 1>/dev/null
		/bin/rm -f ${AVAHI_TM_CONF}
		${AVAHI_DAEMON} --reload > /dev/null 2>&1
		;;
	restart)
		$0 stop
		$0 start
		;;
	*)
		echo "Usage: $0 {start|stop|restart}"
		exit 1
esac

exit 0
