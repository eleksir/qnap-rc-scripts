#!/bin/sh
PATH=/bin:/sbin:/usr/bin:/usr/sbin
ROOT_PART="/mnt/HDA_ROOT"
HIDDEN_CONF_DIR="${ROOT_PART}/.config"
UPDATEPKG_DIR="${ROOT_PART}/update_pkg"
RET_RM_QPKG_SOURCE=10
JOOMLA_QPKG="${UPDATEPKG_DIR}/Joomla.qpkg"
PHPMYADMIN_QPKG="${UPDATEPKG_DIR}/phpMyAdmin.qpkg"
WORDPRESS_QPKG="${UPDATEPKG_DIR}/WordPress.qpkg"
Project_Name=`/sbin/getcfg Project Name -d null -f /var/default`
. /etc/init.d/functions

make_qpkg_base(){
FindDefVol
_ret=$?
if [ $_ret != 0 ]; then
	echo "Default volume not found."
	exit 1
fi
QPKG_BASE="${DEF_VOLMP}"
[ -d "${QPKG_BASE}/.qpkg" ] || /bin/mkdir -p "${QPKG_BASE}/.qpkg"
/bin/chmod 777 "${QPKG_BASE}/.qpkg"
if [ "x${Project_Name}" = "xAthens" ]; then
	/bin/rm -rf "${QPKG_BASE}/.pkg"
	[ -d "${QPKG_BASE}/.pkg" ] || /bin/ln -sf ".qpkg" "${QPKG_BASE}/.pkg"
fi
}

case "$1" in
	start)
	/usr/bin/readlink /etc/config | grep ".config" 1>>/dev/null 2>>/dev/null
	make_qpkg_base
	
	
	if [ $? = 0 ] && [ -d $HIDDEN_CONF_DIR ]; then
		[ -f /etc/config/qpkg.conf ] || /bin/touch /etc/config/qpkg.conf
		/sbin/qpkg --set-rc
	if [ "x${Project_Name}" = "xAthens" ]; then
		PHPMYADMIN_QPKG="${UPDATEPKG_DIR}/phpMyAdmin.pkg"
		WORDPRESS_QPKG="${UPDATEPKG_DIR}/WordPress.pkg"
		for i in ${UPDATEPKG_DIR}/*.pkg ;do
			echo "Install ${i} ..."
			if [ x`/sbin/qpkg --check ${i}` = xTRUE ]; then
				if [ "x${i}" = "x${PHPMYADMIN_QPKG}" ]; then
					/bin/sh ${i}
					/bin/rm -f ${i}
				elif [ "x${i}" = "x${WORDPRESS_QPKG}" ]; then
					/bin/sh ${i}
					/bin/rm -f ${i}					
				else
				 /bin/sh ${i}
				 [ $? == 0 ] || /bin/rm -f ${i}
				fi
			else
				[ ! -f ${i} ] || /bin/rm -f ${i}
			fi
		done
	fi
		for i in ${UPDATEPKG_DIR}/*.qpkg ;do
			echo "Install ${i} ..."
			if [ x`/sbin/qpkg --check ${i}` = xTRUE ]; then
				if [ "x${i}" = "x${PHPMYADMIN_QPKG}" ]; then
					/bin/sh ${i}
					/bin/rm -f ${i}
				elif [ "x${i}" = "x${WORDPRESS_QPKG}" ]; then
					/bin/sh ${i}
					/bin/rm -f ${i}					
				else
				 /bin/sh ${i}
				 [ $? == 0 ] || /bin/rm -f ${i}
				fi
			else
				[ ! -f ${i} ] || /bin/rm -f ${i}
			fi
		done
	[ ! -f ${UPDATEPKG_DIR}/update_qpkg_f ] || /bin/rm -f ${UPDATEPKG_DIR}/update_qpkg_f
	fi
	;;
	stop)
	# nothing to do
	;;
	restart)
	$0 stop
	$0 start
	;;
	qfix)
	if [ -d $HIDDEN_CONF_DIR ]; then
		for i in ${UPDATEPKG_DIR}/*.qfix ;do
			echo "Install ${i} ..."
			if [ x`/sbin/qpkg --check ${i}` = xTRUE ]; then
				/bin/sh ${i}
			else
				[ ! -f ${i} ] || /bin/rm -f ${i}
			fi
		done
	fi
	;;	*)
	echo "Usage: /etc/init.d/qpkg.sh {start|stop|restart}"
	exit 1
esac

exit 0

