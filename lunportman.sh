#!/bin/sh

start() {
	if [ -z "$(/sbin/lsmod | /bin/grep fbdisk | /bin/grep -v grep)" ];then
		/etc/init.d/fbdisk.sh start
	fi

	if [ -f /usr/bin/lunportman ]; then
		echo "Starting LUN Porter manitor: lunportman..."
		[ -d /etc/config/lunporter ] || /bin/mkdir /etc/config/lunporter
		[ -h /etc/lunporter ] || /bin/ln -sf config/lunporter /etc/lunporter
		[ -f /etc/config/lunporter/lunporter.conf ] || /bin/cp -p /etc/default_config/lunporter/lunporter.conf /etc/config/lunporter/lunporter.conf
		/usr/bin/lunportman
	fi
}

stop() {
	echo "Shutting down LUN Porter manitor: lunportman..."
	/usr/bin/killall -q lunportman
	CNT=0

	while [ $CNT -lt 10 ]; do
		sleep 3
		/bin/pidof lunporter 1>>/dev/null 2>>/dev/null
		[ $? = 0 ] || break
		CNT=$(($CNT + 1))
	done
}

case "$1" in

	"start")
		start
		;;

	"stop")
		stop
		;;

	"restart")
		stop
		sleep 1
		start
		;;

	*)
		echo "Usage: /etc/init.d/lunportman.sh {start|stop|restart}"
		exit 1
esac

exit 0
