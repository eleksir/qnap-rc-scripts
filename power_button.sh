#This script is for pressing power button
#!/bin/sh

BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf 2>/dev/null`

# check if the HAL subsystem exist
if [ -x /sbin/hal_app ]; then
    /sbin/hal_app --power_led_blink enc_id=0,enable=1
else
    Internal_Model=`/sbin/getcfg System "Internal Model"`
    if [ "x$Internal_Model" = "xTS-609" ] || [ "x$Internal_Model" = "xTS-239" ] || [ "x$Internal_Model" = "xTS-439" ] || [ "x$Internal_Model" = "xTS-639" ] || [ "x$Internal_Model" = "xTS-839" ] || [ "x$Internal_Model" = "xTS-259" ] || [ "x$Internal_Model" = "xTS-459" ] || [ "x$Internal_Model" = "xTS-559" ]|| [ "x$Internal_Model" = "xTS-659" ] || [ "x$Internal_Model" = "xTS-859" ] || [ "x$Internal_Model" = "xTS-1279" ] || [ "x$Internal_Model" = "xTS-879" ] || [ "x$Internal_Model" = "xTS-1079" ] || [ "x$Internal_Model" = "xTS-469" ] || [ "x$Internal_Model" = "xTS-569" ] || [ "x$Internal_Model" = "xTS-669" ] || [ "x$Internal_Model" = "xTS-869" ]; then
    	# power LED blinking
    	/sbin/pic_raw 76
    fi
fi
if [ "x${BOOT_CONF}" = "xTS-NASARM" ]; then
    if [ -x /sbin/hal_app ]; then
        /sbin/hal_app --se_sys_set_pic_wdt enc_sys_id=root,mode=2
    else
        /sbin/pic_raw 105 #enable pic watch dog timer 5 min,power-off
    fi        
fi

/sbin/poweroff

[ -f /sbin/lcd_tool ] && /sbin/lcd_tool -s 3

