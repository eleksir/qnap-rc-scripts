#!/bin/sh

RETVAL=0
case "$1" in
  start)
	# check if it is necessary startup ntpdate daemon : ntpdated
	mypid=`/bin/pidof ntpdated`
	if [ ! -z $mypid ]; then
		echo "ntpdate daemon has been started."
		exit 1
	else
	        echo -n "Starting NTP services: "
		/sbin/daemon_mgr ntpdated start "/usr/sbin/ntpdated"
		echo "ntpdated."
        	RETVAL=$?
	fi
	;;
  stop)
        echo "Shutting down NTP services: "
	/sbin/daemon_mgr ntpdated stop "/usr/sbin/ntpdated"
	echo "ntpdated."
	RETVAL=$?
        ;;
  restart)
        $0 stop
        $0 start
        RETVAL=$?
        ;;
  *)
        echo "Usage: $0 {start|stop|restart}"
        exit 1
esac

exit $RETVAL


