#!/bin/sh

start()
{
	if [ -f /usr/bin/qsyncman ]; then
		echo "Remove old configuration files"
		[ ! -f /etc/config/hdcopyusb.conf ] || /bin/rm -f /etc/config/hdcopyusb.conf
		/bin/sed -i '/hdusb_copy/d' /etc/config/crontab
		/usr/bin/crontab /etc/config/crontab
		echo "Starting QSync manitor: qsyncman..."
		[ -d /etc/config/qsync ] || /usr/bin/install -d /etc/config/qsync
		[ -f /etc/config/qsync/qhost.conf ] || /bin/touch /etc/config/qsync/qhost.conf
		[ -f /etc/config/qsync/qsyncjobdef.conf ] || /bin/touch /etc/config/qsync/qsyncjobdef.conf
		[ -f /etc/config/qsync/qsync.conf ] || /bin/cp -p /etc/default_config/qsync/qsync.conf /etc/config/qsync/qsync.conf
		[ -f /etc/config/qsync/extdrv.conf ] || /bin/cp -p /etc/default_config/qsync/extdrv.conf /etc/config/qsync/extdrv.conf
		[ -f /etc/config/qsync/qsyncd.conf ] || /bin/cp -p /etc/default_config/qsync/qsyncd.conf /etc/config/qsync/qsyncd.conf
		/usr/bin/qsyncman
	fi
}

stop()
{
	echo "Shutting down QSync manitor: qsyncman..."
	/usr/bin/killall -q qsyncman
}

case "$1" in

start)
	start
	;;

stop)
	stop
	;;

restart)
	stop
	sleep 1
	start
	;;

*)
	echo "Usage: /etc/init.d/qsyncman.sh {start|stop|restart}"
	exit 1
esac

exit 0
