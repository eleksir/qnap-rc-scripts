#!/bin/sh

if [ "x$1" = "x" ]; then
	echo "getmac.sh [dev]"
	exit 1
fi

MACFILE=/var/MacAddress
# check if the HAL subsystem exist
if [ -x /sbin/hal_app ]; then
	/sbin/getcfg MAC $1 -f ${MACFILE}
else
    Model_Name=`/bin/cat /etc/default_config/Model_Name.conf 2>/dev/null`
    Model_NameX=`/bin/cat /etc/default_config/uLinux.conf | grep "^Model =" | cut -d ' ' -f3 2>/dev/null`
    if [ "x${Model_Name}" = "xTS-509" ] || [ "x${Model_Name}" = "xTS-419" ] || [ "x${Model_Name}" = "xTS-809" ] || [ "x${Model_Name}" = "xTS-259" ] || [ "x${Model_Name}" = "xTS-459" ] || [ "x${Model_Name}" = "xTS-559" ] || [ "x${Model_Name}" = "xTS-659" ] || [ "x${Model_Name}" = "xTS-859" ] || [ "x${Model_NameX}" = "xTS-1279" ] || [ "x${Model_NameX}" = "xTS-879U" ] || [ "x${Model_NameX}" = "xTS-1279U" ] || [ "x${Model_NameX}" = "xTS-1279-G" ] || [ "x${Model_NameX}" = "xTS-879U-G" ] || [ "x${Model_NameX}" = "xTS-1279U-G" ]; then
    	# change eth0/eth1
    	if [ "x$1" = "xeth0" ]; then
    		/sbin/getcfg MAC eth1 -f ${MACFILE}
    	elif [ "x$1" = "xeth1" ]; then
    		/sbin/getcfg MAC eth0 -f ${MACFILE}
    	fi
    else
    	/sbin/getcfg MAC $1 -f ${MACFILE}
    fi
fi

