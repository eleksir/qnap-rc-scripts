#!/bin/sh

ROOT_PART="/mnt/HDA_ROOT"
UPDATEPKG_DIR="${ROOT_PART}/update_pkg"
RETVAL=0
APP_RUNTIME_CONF="/var/.application.conf"
QGET_TMP_DIR=/tmp/qget
QGET_INFO_DIR=/etc/config/qget
DownloadShare=`/sbin/getcfg SHARE_DEF defDownload -d Qdownload -f /etc/config/def_share.info`
REAL_PATH=`/sbin/getcfg ${DownloadShare} path -f /etc/smb.conf -d /ERROR`
BTDownloadFolder=`/sbin/getcfg BTDownload "BT Download Folder"`
#BTCompleteFolder=`/sbin/getcfg BTDownload "BT Move Completed Task Folder"`
HTTPDownloadFolder=`/sbin/getcfg BTDownload "HTTP_FTP Download Folder"`
#HTTPCompleteFolder=`/sbin/getcfg BTDownload "HTTP_FTP Move Completed Task Folder"`
TORRENT_FOLDER=`/sbin/getcfg BTDownload "Torrent Folder"`
Volume_Str=`/sbin/getcfg Public path -f /etc/smb.conf | cut -d '/' -f 3`
DB_FILE="/mnt/HDA_ROOT/ds.db"
BTD_PID="/var/lock/btd.pid"
DS_Enable=`/sbin/getcfg BTDownload Enable -u -d TRUE`

check_share()
{
	local share="$1"
	local conf_name="$2"
	if [ "$share" = "" ]; then
		/bin/echo "set $2 to default"
		/sbin/setcfg BTDownload "$2" "${DownloadShare}"
		return 0
	fi

	if [ ! -d "/share/$share" ]; then
		/bin/echo "Share folder $share does not exist. Failed to start btd."
		/sbin/write_log "[Download Station] Share folder $share does not exist. Failed to start btd." 1
		exit 1
	fi
	return 0
}

DS_Hide=`/sbin/getcfg DISABLE qdownload -d 0 -f $APP_RUNTIME_CONF`
if [ "x$DS_Hide" = "x1" ]; then
	/sbin/setcfg BTDownload Enable FALSE
	DS_Enable="FALSE"
fi

if [ x`/sbin/getcfg BTDownload addonEnable -u -d X` = xX ]; then
	/bin/echo "default Enable addonEnable."
	/sbin/setcfg BTDownload addonEnable TRUE
fi

_debug="0"
case "$2" in
  debug)
	_debug="1"
	;;
  DEBUG)
	_debug="1"
	;;
  *)
    _debug="0"
esac

case "$1" in
  start)
	if [ ! -f /tmp/.boot_done ] && [ `/bin/pidof btd | /usr/bin/wc -w` == "0" ]; then
		[ ! -f ${UPDATEPKG_DIR}/DSv3.tgz ] || /etc/init.d/installtgz.sh DSv3 ${UPDATEPKG_DIR}/DSv3.tgz
	else
		_MNT_POINT="/mnt/ext"
		if [ -f ${_MNT_POINT}/opt/DSv3/gt/gt_run.sh ]; then
			/bin/ln -sf ${_MNT_POINT}/opt/DSv3/gt/gt_run.sh /etc/init.d/gt_run.sh
			/bin/chmod 755 ${_MNT_POINT}/opt/DSv3/gt/gt_run.sh
		fi
	fi
	if [ ! -d $REAL_PATH ]; then
		/bin/echo "Volume is not ready for btd."
		exit 1
	fi
	if [ ! -f $DB_FILE ]; then
		/sbin/bt_tool -cv
	else
		if [ `/sbin/getcfg "BTDownload" "Version" -u -d 1.0 -f /etc/config/uLinux.conf` != 3.0 ]; then
			/sbin/setcfg "BTDownload" "Version" "3.0" -f /etc/config/uLinux.conf
			/sbin/bt_tool -cv
		fi
	fi

	if [ ! -s $DB_FILE ]; then
		/bin/rm "$DB_FILE"
		/sbin/bt_tool -cv
	fi
	
	if [ ! -f /sbin/btd ]; then
		/bin/echo "Failed to locate the daemon."
#		/sbin/write_log "[Download Station] Failed to locate the daemon." 1
		exit 1
	fi
	
	if [ ! -d "/share/$Volume_Str/.torrent" ]; then
		 /bin/mkdir "/share/$Volume_Str/.torrent"
	fi
	if [ ! -d "/share/$Volume_Str/.torrent/.temp" ]; then
		/bin/mkdir "/share/$Volume_Str/.torrent/.temp"
	else
		/bin/rm -f "/share/$Volume_Str/.torrent/.temp/*"
	fi
	if [ -d /etc/config/torrent/torrent ] && [ `ls "/etc/config/torrent/torrent/" | /usr/bin/wc -l` != 0  ]; then
		/bin/rm -rf /etc/config/torrent/torrent/*
	fi

#	if [ ! -d "/mnt/HDA_ROOT/.ds/engines" ]; then
#		/bin/mkdir -p "/mnt/HDA_ROOT/.ds/engines"
#		/bin/cp -rf "/home/httpd/cgi-bin/Qdownload/engines" "/mnt/HDA_ROOT/.ds/"
#	fi
#	if [ -d "/home/httpd/cgi-bin/Qdownload/engines" ]; then
#		[ -f "/mnt/HDA_ROOT/.ds/engines/isohunt.php" ] || /bin/cp -rf "/home/httpd/cgi-bin/Qdownload/engines" "/mnt/HDA_ROOT/.ds/"
#		/bin/rm -rf "/home/httpd/cgi-bin/Qdownload/engines"
#		/bin/ln -sf "/mnt/HDA_ROOT/.ds/engines" "/home/httpd/cgi-bin/Qdownload/engines"
#	fi
	
	if [ x"$TORRENT_FOLDER" = x ]; then
		/sbin/setcfg BTDownload "Torrent Folder" "/share/$Volume_Str/.torrent"
		TORRENT_FOLDER="/share/$Volume_Str/.torrent"
	fi
	if [ x"$TORRENT_FOLDER" != x"/share/$Volume_Str/.torrent" ]; then
		/sbin/setcfg BTDownload "Torrent Folder" "/share/$Volume_Str/.torrent"
		TORRENT_FOLDER="/share/$Volume_Str/.torrent"
	fi

	if [ -f /sbin/btd ] && [ "x$DS_Enable" = "xTRUE" ]; then
       	/bin/echo "Starting BT management services: "
		
		check_share "$BTDownloadFolder" "BT Download Folder"
		[ x"$BTDownloadFolder" != x ] || BTDownloadFolder=${DownloadShare}
#		check_share "$BTCompleteFolder"	"BT Move Completed Task Folder"
#		[ x"$BTCompleteFolder" != x ] || BTCompleteFolder=${DownloadShare}
#		check_share "$HTTPDownloadFolder" "HTTP_FTP Download Folder"
#		[ x"$HTTPDownloadFolder" != x ] || HTTPDownloadFolder=${DownloadShare}
#		check_share "$HTTPCompleteFolder" "HTTP_FTP Move Completed Task Folder"
#		[ x"$HTTPCompleteFolder" != x ] || HTTPCompleteFolder=${DownloadShare}
		[ x"$BTDownloadFolder" = x"HTTPDownloadFolder" ] || `/sbin/setcfg BTDownload "HTTP_FTP Download Folder" "$BTDownloadFolder"`
		
		if [ `/bin/pidof btd | /usr/bin/wc -w` != "0" ]; then
			/bin/echo "bt daemon is running, exit."
			exit 1
		fi
		#add column(s)
		if [ -f /usr/local/apache/bin/php ] && [ -f /home/httpd/cgi-bin/Qdownload/db_alter.php ]; then
			/usr/local/apache/bin/php /home/httpd/cgi-bin/Qdownload/db_alter.php BT_TASK task_queue_position 1 2>>/dev/null
			/usr/local/apache/bin/php /home/httpd/cgi-bin/Qdownload/db_alter.php BT_TASK task_move_folder 0 2>>/dev/null
			/usr/local/apache/bin/php /home/httpd/cgi-bin/Qdownload/db_alter.php HTTP_FTP_TASK task_move_folder 0 2>>/dev/null
			/usr/local/apache/bin/php /home/httpd/cgi-bin/Qdownload/db_alter.php BT_TASK task_hash 0 2>>/dev/null
			/usr/local/apache/bin/php /home/httpd/cgi-bin/Qdownload/db_alter.php BT_TASK task_type 1 2>>/dev/null
			/usr/local/apache/bin/php /home/httpd/cgi-bin/Qdownload/db_alter.php BT_TASK task_magnet_link 0 2>>/dev/null
			/usr/local/apache/bin/php /home/httpd/cgi-bin/Qdownload/db_alter.php BT_SESSION pause_all_time 1 2>>/dev/null
			/usr/local/apache/bin/php /home/httpd/cgi-bin/Qdownload/db_alter.php HTTP_FTP_TASK task_savename 0 2>>/dev/null
			/usr/local/apache/bin/php /home/httpd/cgi-bin/Qdownload/db_alter.php HTTP_FTP_TASK task_queue_position 1 2>>/dev/null
		fi
		if [ ! -d $QGET_TMP_DIR ]; then
			/bin/mkdir $QGET_TMP_DIR
		fi
		if [ ! -d $QGET_INFO_DIR ]; then
			/bin/mkdir $QGET_INFO_DIR
		fi
		[ -f libtorrent-rasterbar.so.6 ] || /bin/ln -sf libtorrent.so /usr/lib/libtorrent-rasterbar.so.6
		if [ `/sbin/getcfg BTDownload Encrypt -u -d 0` = 1 ]; then
			cd /usr/lib; /bin/rm libtorrent-0.15.9.so; /bin/ln -sf libtorrent-0.15.9.so.enc libtorrent-0.15.9.so
		else
			cd /usr/lib; /bin/rm libtorrent-0.15.9.so; /bin/ln -sf libtorrent-0.15.9.so.nonenc libtorrent-0.15.9.so
		fi
		#scan torrent and check DB
		/sbin/bt_tool -s -v
		if [ ${_debug} == "0" ]; then
			/sbin/btd -m "/share/$Volume_Str/.torrent" -s "/share/${BTDownloadFolder}" 1>/dev/null 2>/dev/null &
		else
			/sbin/btd -m "/share/$Volume_Str/.torrent" -s "/share/${BTDownloadFolder}" --log-file "/share/${BTDownloadFolder}/btd.log" 1>/dev/null 2>/dev/null &
		fi
		#/sbin/daemon_mgr btd start "/sbin/btd"
		Addon_Enable=`/sbin/getcfg BTDownload addonEnable -u -d FALSE`
		if [ "x$Addon_Enable" = "xTRUE" ]; then
			[ ! -f /etc/init.d/gt_run.sh ] || /etc/init.d/gt_run.sh start
		fi
	fi
        RETVAL=$?
        ;;
  reset)
        /bin/rm "$DB_FILE"
        /sbin/bt_tool -cv
		/bin/rm /etc/config/torrent/torrent/*
		/bin/rm /share/$Volume_Str/.torrent/*
        ;;
  stop)
        /bin/echo -n "Shutting down BT management services: "
	#/sbin/daemon_mgr btd stop "/sbin/btd"
	/bin/echo "btd."
	/bin/chmod -R +rw /share/${DownloadShare}/
	/usr/bin/killall curl
	/usr/bin/killall btd
	/usr/bin/killall curl_download
	[ ! -f /etc/init.d/gt_run.sh ] || /etc/init.d/gt_run.sh stop
	/bin/echo -n "Killing BT processes. Please wait...."
	count=15
	btdP=1
	while [ $btdP = 1 ] && [ $count -gt 0 ]; do
		/bin/echo -n "."
		count=$(($count-1));
		/bin/pidof btd 1>>/dev/null 2>>/dev/null
		if [ $? -ne 0 ]; then
			btdP=0
		else
			/bin/sleep 1
		fi
	done
	/bin/echo
	/usr/bin/killall -9 curl
	/usr/bin/killall -9 curl_download
	/usr/bin/killall -9 btd
	[ ! -d $QGET_TMP_DIR ] || /bin/rm ${QGET_TMP_DIR}/* 1>/dev/null 2>/dev/null
	[ ! -f $BTD_PID ] || /bin/rm ${BTD_PID} 1>/dev/null 2>/dev/null
	/bin/sleep 1
	RETVAL=$?
        ;;
  restart)
        $0 stop
        $0 start
        RETVAL=$?
        ;;
  *)
        /bin/echo "Usage: $0 {start|stop|restart|reset}"
        exit 1
esac

exit $RETVAL

