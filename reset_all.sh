#!/bin/sh

case "$1" in
	stop_all)
		/etc/init.d/smb stop
		/etc/init.d/mars-nwe stop
		/etc/init.d/ftp.sh stop
		/etc/init.d/atalk stop
		/etc/init.d/nfs stop
		/etc/init.d/cron stop
		;;
	start_all)
		/etc/init.d/smb start
		/etc/init.d/mars-nwe start
		/etc/init.d/ftp.sh start
		/etc/init.d/atalk start
		/etc/init.d/nfs start
		/etc/init.d/cron start
		;;
	copy_default)
		/bin/cp -af /etc/default_config/* /mnt/config
		/etc/init.d/update_config init
		;;
	reset_cache)
		/sbin/init_nas_cache
		;;
	delete_unused)
		/bin/rm -f /etc/config/raidtab
		/bin/rm -f /etc/config/storage.conf
		/bin/rm -f /etc/storage.conf
		/bin/rm -f /etc/config/pdcgroup
		/bin/rm -f /etc/config/fw*
		/bin/rm -f /etc/config/named.conf
		/bin/rm -f /etc/config/dhcp_log
		/bin/rm -f /etc/config/usr_quota.conf
		/bin/rm -f /etc/config/grp_quota.conf
		/bin/rm -f /etc/config/backup_schedule.conf
		;;
	clear_log)
		/bin/rm -f /etc/logs/nas4000.cnt
		;;
 	*)
        echo "Usage: /etc/init.d/reset_all {copy_default|reset_cache|delete_unused|clear_log|save_all}"
        exit 1
esac

exit 0

