#!/bin/sh
# ddns_update.sh         This shell script takes care of ddns updating
#
# description: ez_ipupdate provide register domain name from special server.

result=0
exec /etc/init.d/ddns_update_for_cgi.sh &
result=$?

#if [ $result -eq 0 ]; then
#	/sbin/write_log "DDNS was updated successfully." 4
#else [ $result -eq 1 ]
#	/sbin/write_log "Failed to update DDNS." 2
#fi

exit $result
