#!/bin/sh
FBDISK_SH=/etc/init.d/fbdisk.sh
ISCSI_TARGET_KO=/lib/modules/misc/target_core_mod.ko
ISCSI_UTILITY=/sbin/iscsi_util
ISCSI_LOG_DAEMON=/sbin/iscsi_logd
ISNS_CLIENT_DAEMON=/sbin/isns_cd
ISCSI_CONFIG=/etc/config/iscsi_trgt.conf
MIN_FSIZE=200 #minimum config file size required

start_target()
{
    # check if fbdisk loaded
    if [ -z "`/sbin/lsmod | /bin/grep fbdisk | /bin/awk {'print $1'}`" ];
    then
        $FBDISK_SH start;
    fi
    [ -d "/var/target/pr" ] || mkdir -p /var/target/pr 
    [ -f $ISCSI_CONFIG ] || $ISCSI_UTILITY --new
	if [ -x /usr/bin/du ] && [ -f $ISCSI_CONFIG ]; then
		conf_size=`/usr/bin/du -b $ISCSI_CONFIG | /bin/awk '{print $1}'`
		if [ -z "$conf_size" ] || [ "$conf_size" -lt $MIN_FSIZE ]; then
			/bin/cp $ISCSI_CONFIG ${ISCSI_CONFIG}.replaced
			last_conf_size=0
			[ -f ${ISCSI_CONFIG}.last ] && last_conf_size=`/usr/bin/du -b ${ISCSI_CONFIG}.last | /bin/awk '{print $1}'`
			if [ ! -z "$last_conf_size" ] && [ "$last_conf_size" -ge $MIN_FSIZE ]; then
				/bin/cp ${ISCSI_CONFIG}.last $ISCSI_CONFIG 
			else
				[ -x /etc/init.d/backup_conf.sh ] && /etc/init.d/backup_conf.sh restore iscsi_trgt.conf
			fi
		fi
	fi
    ENABLE_ISCSI=`/sbin/getcfg iSCSIPortal bServiceEnable -f $ISCSI_CONFIG -u -d 1`
    if [ "$ENABLE_ISCSI" != "TRUE" ];
    then
	$ISCSI_UTILITY --config
	exit 0;
    fi
    ENABLE_ISNS=`/sbin/getcfg iSCSIPortal bISNSEnable -f $ISCSI_CONFIG -u -d 1`
    if [ "$ENABLE_ISNS" == "TRUE" ];
    then
        ISNS_IP=`/sbin/getcfg iSCSIPortal ISNSIP -f $ISCSI_CONFIG -u -d 127.0.0.1`
        $ISNS_CLIENT_DAEMON --address $ISNS_IP
    fi
    $ISCSI_UTILITY --load --config
    # log daemon should be launched after the target service
    $ISCSI_LOG_DAEMON
}

stop_target()
{
    if [[ $0 == *K* ]]
    then
        /usr/bin/killall iscsi_lun_setting.cgi &> /dev/null
    fi

    /usr/bin/killall isns_cd &> /dev/null
    /usr/bin/killall iscsi_logd &> /dev/null
    $ISCSI_UTILITY --unload
}

unload_fbdisk()
{
    # check if fbdisk loaded
    if [ -n "`/sbin/lsmod | /bin/grep fbdisk | /bin/awk {'print $1'}`" ];
    then
        $FBDISK_SH stop;
    fi
}

# Check that iscsi_trgt.ko exists.
[ -f $ISCSI_TARGET_KO ] || exit 0

# See how we were called.
case "$1" in
    start)
        start_target;
        ;;
    stop)
        stop_target;
        unload_fbdisk;
        ;;
    restart)
        stop_target;
        sleep 1
        start_target;
        ;;
    *)
        echo "Usage: $0 {start|stop|restart}"
        exit 1
esac

exit 0
