#!/bin/sh

BOND_SUP=$(/sbin/getcfg "Network" "BONDING Support" -d "FALSE")

if [ "$BOND_SUP" = "TRUE" ]; then
	IP=`/sbin/ifconfig bond0 | /bin/awk '{ if ($1 == "inet") { str = $2; split(str, a, ":"); print a[2];} }'`
else
	IP=`/sbin/ifconfig eth0 | /bin/awk '{ if ($1 == "inet") { str = $2; split(str, a, ":"); print a[2];} }'`
fi

if [ "$(/sbin/getcfg "Project" "Name" -d "null" -f /var/default)" != "null" ]; then
	echo "" > /etc/issue
	echo -n "Welcome to `/sbin/getcfg System Model -d NAS`, " >> /etc/issue
	echo "$(/sbin/getcfg System Manufacturer -f /etc/default_config/uLinux.conf)." >> /etc/issue
	echo "" >> /etc/issue
elif [ -f /etc/IS_G ]; then
	echo "" > /etc/issue
	echo -n "Welcome to NAS Server" >> /etc/issue
	echo "" >> /etc/issue
else
	echo "" > /etc/issue
	echo "Welcome to $(/sbin/getcfg System Model -d NAS)($IP), QNAP Systems, Inc." >> /etc/issue
	echo "" >> /etc/issue
fi
