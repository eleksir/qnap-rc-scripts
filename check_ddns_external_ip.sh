#!/bin/ash
#
# check ddns external ip change or not

DDNS_TMP_FILE="/var/ddns_tmp.conf"
DDNS_EX_IP_FILE="/var/ddns_extranl_ip"

check_ddns_temp_file()
{
	if [ ! -f ${DDNS_TMP_FILE} ]; then
		/bin/echo "${DDNS_TMP_FILE} is not exist"
		/bin/echo "[DDNS]" > ${DDNS_TMP_FILE}
		/bin/echo "Support = "`/sbin/getcfg "DDNS" "Support" -u -d "TRUE"` >> ${DDNS_TMP_FILE}
		/bin/echo "Enable = "`/sbin/getcfg "DDNS" "Enable" -u -d "FALSE"` >> ${DDNS_TMP_FILE}
		/bin/echo "Server Type = "`/sbin/getcfg "DDNS" "Server Type" -u -d "0"` >> ${DDNS_TMP_FILE}
		/bin/echo "Config File = "`/sbin/getcfg "DDNS" "Config File" -d "/etc/ddns_update.conf"` >> ${DDNS_TMP_FILE}
		/bin/echo "Check External IP = "`/sbin/getcfg "DDNS" "Check External IP" -d "0"` >> ${DDNS_TMP_FILE}
		/bin/echo "User Name = "`/sbin/getcfg "DDNS" "User Name" -d ""` >> ${DDNS_TMP_FILE}
		/bin/echo "Password = "`/sbin/getcfg "DDNS" "Password" -d ""` >> ${DDNS_TMP_FILE}
		/bin/echo "Host Name = "`/sbin/getcfg "DDNS" "Host Name" -d ""` >> ${DDNS_TMP_FILE}
		/bin/echo "DDNS Type = "`/sbin/getcfg "DDNS" "DDNS Type" -d "0"` >> ${DDNS_TMP_FILE}
	fi
	[ ! -f ${DDNS_EX_IP_FILE} ] && /bin/echo `/sbin/getcfg "DDNS" "IP Address" -f ${DDNS_TMP_FILE}` > ${DDNS_EX_IP_FILE}
}

SEVER_TYPE=`grep "Server Type" ${DDNS_TMP_FILE} | /bin/tr -cs '[0-9\.]' '\012'`
if [ ${SEVER_TYPE} = 5 ]; then
	/bin/echo "no-ip don't need use this functin"
	exit 0
fi

NEW_IP=`/etc/init.d/get_external_ip.sh`
result=$?
#[ -f /sbin/set_ipupatetime ] && /sbin/set_ipupatetime "%T"
[ -f /sbin/set_ipupatetime ] && /sbin/set_ipupatetime "0"
if [ $result -eq 0 ] && [ "x${NEW_IP}" != "x" ]; then
	check_ddns_temp_file
	OLD_IP=`/bin/cat ${DDNS_EX_IP_FILE}`
	if [ "x${OLD_IP}" = "x${NEW_IP}" ]; then
		/bin/echo "external ip is not change"
		exit 0;
	else
		/bin/echo "external ip is change ,update ddns [${OLD_IP}]=>[${NEW_IP}]"
		/sbin/setcfg "DDNS" "IP Address" ${NEW_IP} -f ${DDNS_TMP_FILE}
		/bin/echo ${NEW_IP} > ${DDNS_EX_IP_FILE}
		/etc/init.d/ddns_update.sh
		/sbin/write_log "external ip is change ,update ddns." 2
		exit 1;
	fi
else
	/bin/echo "can not get external ip"
fi

exit 0
