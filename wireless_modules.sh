#!/bin/sh
#
# Copyright @ 2005 QNAP Systems Inc.
#
# network       This script insert/remove wireless modules.
#
# Version       1.0.0
#
case "$1" in
start)
	if [ "x$2" = "xWIFI_RT5370" ]; then
                while [ 1 ]; do
                        /sbin/lsmod | /bin/grep rt5370sta 1>/dev/null 2>/dev/null
                        if [ "$?" = "0" ]; then
                                exit 0
                        fi
                        echo "insert modules $2..." >> /tmp/wifidebug
                        /sbin/insmod /usr/local/modules/rt5370sta.ko
                        if [ "$?" = "0" ]; then
                                echo "insert modules $2 success..." >> /tmp/wifidebug
                                /bin/sleep 1
                                /etc/init.d/wireless.sh auto_connect &
                                exit 0
                        fi
                        /bin/sleep 5
                done
	elif [ "x$2" = "xWIFI_RT2870" ]; then
	        while [ 1 ]; do
			/sbin/lsmod | /bin/grep rt2870sta 1>/dev/null 2>/dev/null
			if [ "$?" = "0" ]; then
				exit 0
			fi
			echo "insert modules $2..." >> /tmp/wifidebug
        	        /sbin/insmod /usr/local/modules/rt2870sta.ko
	                if [ "$?" = "0" ]; then
        	                echo "insert modules $2 success..." >> /tmp/wifidebug
				/bin/sleep 1
	                        /etc/init.d/wireless.sh auto_connect &
				exit 0
	                fi
        	        /bin/sleep 5
	        done
	elif [ "x$2" = "xWIFI_RT3070" ]; then
		while [ 1 ]; do
			/sbin/lsmod | /bin/grep rt3070sta 1>/dev/null 2>/dev/null
			if [ "$?" = "0" ]; then
				exit 0
			fi
			echo "insert modules $2..." >> /tmp/wifidebug
			/sbin/insmod /usr/local/modules/rt3070sta.ko
			if [ "$?" = "0" ]; then
				echo "insert modules $2 success..." >> /tmp/wifidebug
				/bin/sleep 1
				/etc/init.d/wireless.sh auto_connect &
				exit 0
			fi
			/bin/sleep 5
			done
        elif [ "x$2" = "xWIFI_RT3572" ]; then
                while [ 1 ]; do
                        /sbin/lsmod | /bin/grep rt3572sta 1>/dev/null 2>/dev/null
                        if [ "$?" = "0" ]; then
                                exit 0
                        fi			
                        echo "insert modules $2..." >> /tmp/wifidebug
                        /sbin/insmod /usr/local/modules/rt3572sta.ko
                        if [ "$?" = "0" ]; then
                                echo "insert modules $2 success..." >> /tmp/wifidebug
                                /bin/sleep 1
                                /etc/init.d/wireless.sh auto_connect &
				exit 0
                        fi
                        /bin/sleep 5
                done
        elif [ "x$2" = "xWIFI_RTL8192CU" ]; then
                while [ 1 ]; do
                        /sbin/lsmod | /bin/grep 8192cu 1>/dev/null 2>/dev/null
                        if [ "$?" = "0" ]; then
                                exit 0
                        fi
                        echo "insert modules $2..." >> /tmp/wifidebug
                        /sbin/insmod /usr/local/modules/8192cu.ko
                        if [ "$?" = "0" ]; then
                                echo "insert modules $2 success..." >> /tmp/wifidebug
                                                /bin/sleep 1
                                /etc/init.d/wireless.sh auto_connect &
                                exit 0
                        fi
                        /bin/sleep 5
                done
	elif [ "x$2" = "xWIFI_RTL8712" ]; then
                while [ 1 ]; do
                        /sbin/lsmod | /bin/grep 8712u 1>/dev/null 2>/dev/null
                        if [ "$?" = "0" ]; then
                                exit 0
                        fi
			echo "insert modules $2..." >> /tmp/wifidebug
                        /sbin/insmod /usr/local/modules/8712u.ko
                        if [ "$?" = "0" ]; then
                                echo "insert modules $2 success..." >> /tmp/wifidebug
				                /bin/sleep 1
                                /etc/init.d/wireless.sh auto_connect &
				exit 0
                        fi
                        /bin/sleep 5
                done
        elif [ "x$2" = "xWIFI_RT5592" ]; then
                while [ 1 ]; do
                        /sbin/lsmod | /bin/grep rt5592sta 1>/dev/null 2>/dev/null
                        if [ "$?" = "0" ]; then
                                exit 0
                        fi			
                        echo "insert modules $2..." >> /tmp/wifidebug
                        /sbin/insmod /usr/local/modules/rt5592sta.ko
                        if [ "$?" = "0" ]; then
                                echo "insert modules $2 success..." >> /tmp/wifidebug
                                /bin/sleep 1
                                /etc/init.d/wireless.sh auto_connect &
                                exit 0
                        fi
                        /bin/sleep 5
                done
        elif [ "x$2" = "xWIFI_RTL8192" ]; then
                while [ 1 ]; do
                        /sbin/lsmod | /bin/grep rtl8192ce 1>/dev/null 2>/dev/null
                        if [ "$?" = "0" ]; then
                                exit 0
                        fi			
                        echo "insert modules $2..." >> /tmp/wifidebug
                        /sbin/insmod /usr/local/modules/rtlwifi.ko
                        /sbin/insmod /usr/local/modules/rtl8192ce.ko
                        if [ "$?" = "0" ]; then
                                echo "insert modules $2 success..." >> /tmp/wifidebug
                                /bin/sleep 1
                                /etc/init.d/wireless.sh auto_connect &
                                exit 0
                        fi
                        /bin/sleep 5
                done
	fi
	;;
stop)
    /etc/init.d/wireless.sh stop 1>>/dev/null 2>>/dev/null
    /bin/sleep 1
    echo "remove modules $2..." >> /tmp/wifidebug
    if [ "x$2" = "xWIFI_RT5370" ]; then
        /sbin/rmmod /usr/local/modules/rt5370sta.ko
    elif [ "x$2" = "xWIFI_RT2870" ]; then
        /sbin/rmmod /usr/local/modules/rt2870sta.ko
    elif [ "x$2" = "xWIFI_RT3070" ]; then
        /sbin/rmmod /usr/local/modules/rt3070sta.ko
    elif [ "x$2" = "xWIFI_RT3572" ]; then
        /sbin/rmmod /usr/local/modules/rt3572sta.ko
    elif [ "x$2" = "xWIFI_RTL8192CU" ]; then
        /sbin/rmmod /usr/local/modules/8192cu.ko
    elif [ "x$2" = "xWIFI_RTL8712" ]; then
        /sbin/rmmod /usr/local/modules/8712u.ko
    elif [ "x$2" = "xWIFI_RT5592" ]; then
        /sbin/rmmod /usr/local/modules/rt5592sta.ko
    elif [ "x$2" = "xWIFI_RTL8192" ]; then
        /sbin/rmmod /usr/local/modules/rtl8192ce.ko
        /sbin/rmmod /usr/local/modules/rtlwifi.ko
    fi
	;;

*)
	/bin/echo $"Usage: $0 {start|stop} {chipset}"
        exit 1
esac

