#!/bin/sh
#This shell script was used to get DNS type value from /etc/config/uLinux.conf
#If DNS type was empty, then we will set default value.


GETCFG="/sbin/getcfg"
SETCFG="/sbin/setcfg"

DNS_type=`$GETCFG "Network" "DNS type" -d "NoValue"`
Interface_number=`$GETCFG "Network" "Interface Number" -d "No_eth"`
BOND_SUP=`$GETCFG "Network" "BONDING Support" -d "FALSE"|tr a-z A-Z`
DEFAULT_GW_DEV=`$GETCFG "Network" "Default GW Device" -d eth0`

check_dns_type() 
{
	if [ "x$DNS_type" = "xNoValue" ]; then
		_local_dns1=`$GETCFG "Network" "Domain Name Server 1" -d "0.0.0.0"`
		_local_dns2=`$GETCFG "Network" "Domain Name Server 2" -d "0.0.0.0"`
		if [ "x${_local_dns1}" != "x0.0.0.0" ] || [ "x${_local_dns2}" != "x0.0.0.0" ]; then
			$SETCFG "Network" "DNS type" manual
		else
			if [ "x$Interface_number" = "x1"  ]; then
				if [ "x`$GETCFG eth0 "Use DHCP" -u -d TRUE`" = "xTRUE" ]; then
					$SETCFG "Network" "DNS type" auto
				else
					$SETCFG "Network" "DNS type" manual
				fi
			elif [ "x$Interface_number" = "x2"  ] && [ "x$BOND_SUP" = "xTRUE"  ]; then 
					if [ "x`$GETCFG bond0 "Use DHCP" -u -d TRUE`" = "xTRUE" ]; then
						$SETCFG "Network" "DNS type" auto
					else
						$SETCFG "Network" "DNS type" manual
					fi
			elif [ "x$Interface_number" = "x2"  ] && [ "x$BOND_SUP" = "xFALSE" ]; then
					if [ "x`$GETCFG $DEFAULT_GW_DEV "Use DHCP" -u -d TRUE`" = "xTRUE" ]; then
						$SETCFG "Network" "DNS type" auto
					else
						$SETCFG "Network" "DNS type" manual
					fi
			elif [ $Interface_number -ge 4  ]; then
				if [ "x`$GETCFG $DEFAULT_GW_DEV "Use DHCP" -u -d TRUE`" = "xTRUE" ]; then
					$SETCFG "Network" "DNS type" auto
				else
					$SETCFG "Network" "DNS type" manual
				fi
			else
				echo Please check your setting
			fi
		fi
	fi
}

check_dns_type;
