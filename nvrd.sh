#!/bin/sh
# For Administration
INSTALL_FLAG_FILE=/tmp/check_install.flag
if [ -f $INSTALL_FLAG_FILE ] ; then
	INSTALLING=`/bin/cat $INSTALL_FLAG_FILE`
	[ x"$INSTALLING" != x"1" ] || exit 0
fi
NVR_ROOTFS=
export NVR_ROOTFS
RecordingsShare=`/sbin/getcfg SHARE_DEF defRecordings -d Qrecordings -f /etc/config/def_share.info`

ROOTFS=$NVR_ROOTFS
WGET=/usr/bin/wget-s
[ -x ${WGET} ] || WGET=/usr/bin/wget
# Set some defaults
MNT_POINT="/mnt/ext"
NVR_SOURCE="${MNT_POINT}/opt/source/nvr_app.tar.gz"
NVR_DIR="/"
ROOT_PART="/mnt/HDA_ROOT"
UPDATEPKG_DIR="${ROOT_PART}/update_pkg"

if [ "`/sbin/getcfg -f /etc/default_config/uLinux.conf NVR Support -d FALSE`" == "TRUE" ]; then
    SUPPORT_NVR=yes
else
    SUPPORT_NVR=no
fi

init_nvrd()
{
if [ ! -f /usr/bin/nvrd ]; then
	_nvr_target=/mnt/ext/opt/nvr
	_nvr_create_link=0
	if [ -f ${NVR_SOURCE} ]; then
		/bin/rm -rf /usr/msb 2>/dev/null; mkdir /usr/msb
		cd /
		/bin/tar zxf ${NVR_SOURCE} --exclude *.cab
		[ -d /mnt/ext/opt/nvr ] || /bin/mkdir -p ${_nvr_target}
		/bin/tar zxvf ${NVR_SOURCE} -C /mnt/ext/opt/nvr --wildcards *.cab
		_nvr_create_link=1
	elif [ -f ${UPDATEPKG_DIR}/nvr_app.tar.gz ]; then
		/bin/rm -rf /usr/msb 2>/dev/null; mkdir /usr/msb
		cd /
		/bin/tar zxf ${UPDATEPKG_DIR}/nvr_app.tar.gz --exclude *.cab
		[ -d /mnt/ext/opt/nvr ] || /bin/mkdir -p ${_nvr_target}
		/bin/tar zxvf ${UPDATEPKG_DIR}/nvr_app.tar.gz -C /mnt/ext/opt/nvr --wildcards *.cab
		_nvr_create_link=1
	fi
	if [ "x${_nvr_create_link}" = "x1" ]; then
		/bin/rm -f /home/httpd/cgi-bin/NNVRVMon.cab 2>/dev/null
		/bin/rm -f /home/httpd/cgi-bin/QNAP*.cab 2>/dev/null
		/bin/ln -sf ${_nvr_target}/home/httpd/cgi-bin/NNVRVMon.cab /home/httpd/cgi-bin/NNVRVMon.cab
		/bin/ln -sf ${_nvr_target}/home/httpd/cgi-bin/QNAPG726.cab /home/httpd/cgi-bin/QNAPG726.cab
		/bin/ln -sf ${_nvr_target}/home/httpd/cgi-bin/QNAPQ264.cab /home/httpd/cgi-bin/QNAPQ264.cab
		/bin/ln -sf ${_nvr_target}/home/httpd/cgi-bin/QNAPQIVG.cab /home/httpd/cgi-bin/QNAPQIVG.cab
		/bin/ln -sf ${_nvr_target}/home/httpd/cgi-bin/QNAPQMP4.cab /home/httpd/cgi-bin/QNAPQMP4.cab
		/bin/ln -sf ${_nvr_target}/home/httpd/cgi-bin/QNAPQVivoTek.cab /home/httpd/cgi-bin/QNAPQVivoTek.cab
	fi
	[ ! -f /home/httpd/ajax_obj/js/qnapui.js_bak ] || /bin/cp /home/httpd/ajax_obj/js/qnapui.js_bak /home/httpd/ajax_obj/js/qnapui.js
	[ ! -f /home/httpd/cgi-bin/html/login.html_bak ] || /bin/cp /home/httpd/cgi-bin/html/login.html_bak /home/httpd/cgi-bin/html/login.html
	[ ! -f /home/httpd/cgi-bin/html/login_2.html_bak ] || /bin/cp /home/httpd/cgi-bin/html/login_2.html_bak /home/httpd/cgi-bin/html/login_2.html
fi

if [ -f /home/httpd/ajax_obj/js/qnapui.js ] && [ -f /home/httpd/cgi-bin/html/login.html ]; then
	[ -f /home/httpd/ajax_obj/js/qnapui.js_bak ] || /bin/cp /home/httpd/ajax_obj/js/qnapui.js /home/httpd/ajax_obj/js/qnapui.js_bak
	[ -f /home/httpd/cgi-bin/html/login.html_bak ] || /bin/cp /home/httpd/cgi-bin/html/login.html /home/httpd/cgi-bin/html/login.html_bak
	[ -f /home/httpd/cgi-bin/html/login_2.html_bak ] || /bin/cp /home/httpd/cgi-bin/html/login_2.html /home/httpd/cgi-bin/html/login_2.html_bak
	/bin/sed -i 's/\/nssindex.html/\/cgi-bin\/camera_view.cgi/g' /home/httpd/ajax_obj/js/qnapui.js 2>/dev/null
	/bin/sed -i 's/\/nssindex.html/\/cgi-bin\/camera_view.cgi/g' /home/httpd/cgi-bin/html/login.html 2>/dev/null
	/bin/sed -i 's/\/nssindex.html/\/cgi-bin\/camera_view.cgi/g' /home/httpd/cgi-bin/html/login_2.html 2>/dev/null
fi

if [ "$SUPPORT_NVR" == "yes" ]; then
	/bin/mkdir /play
	for i in 1 2 3 4 5 6 7 8
	do
    		/bin/mkdir /play/ch$i
    		/bin/mkdir /play/ch$i/record_nvr
    		/bin/ln -s $NVR_ROOTFS/data/record_nvr/channel$i /play/ch$i/record_nvr/channel$i
    		/bin/mkdir /play/ch$i/record_nvr_unk
    		/bin/ln -s $NVR_ROOTFS/data/record_nvr_unk/channel$i $/play/ch$i/record_nvr_unk/channel$i
    		/bin/mkdir /play/ch$i/record_nvr_alarm
    		/bin/ln -s $NVR_ROOTFS/data/record_nvr_alarm/channel$i /play/ch$i/record_nvr_alarm/channel$i
	done
#ln -s $NVR_ROOTFS/data /data
#ln -s /data /home/httpd/record
#ln -s /data /home/httpd/cgi-bin/record
	if [ -e /home/httpd/cgi-bin/record ]; then
		/bin/rm /home/httpd/cgi-bin/record
	fi
	/bin/ln -s $NVR_ROOTFS/share/$RecordingsShare /home/httpd/cgi-bin/record

	if [ -e /home/httpd/record ]; then
		/bin/rm /home/httpd/record
	fi
	/bin/ln -s $NVR_ROOTFS/share/$RecordingsShare /home/httpd/record

	if [ ! -e /etc/config/nvrd.xml ]; then
		/bin/cp -a /etc/default_config/nvrd.xml /etc/config/
	fi

	if [ ! -e /etc/config/nvrstat.xml ]; then
		/bin/cp -a /etc/default_config/nvrstat.xml /etc/config/
	fi
	if [ ! -e /var/.nvr_quota_info ]; then
		/bin/mkdir /var/.nvr_quota_info
	fi

	if [ -e /share/$RecordingsShare/record_nvr_tmp ]; then
    		/bin/rm -rf /share/$RecordingsShare/record_nvr_tmp
	fi
	if [ -e /share/$RecordingsShare/record_nvr_unk ]; then
    		/bin/rm -rf /share/$RecordingsShare/record_nvr_unk
	fi

	if [ -e /share/$RecordingsShare/record_nvr_pre ]; then
    		/bin/rm -rf /share/$RecordingsShare/record_nvr_pre
	fi

	/usr/bin/qcmd XMLCR
	/usr/bin/qcmd umsid_info
	/usr/bin/qcmd DUSG
fi

}


HIDDEN_CONF_FILE=/mnt/HDA_ROOT/.conf
NVR_HIDDEN_LOG_DIR=/mnt/HDA_ROOT/.sqdatabase
NVR_VS_LOG_ROOT_DIR=/home/sqdatabase
NVR_VS_LOG_DIR=$NVR_ROOTFS$NVR_VS_LOG_ROOT_DIR
NVR_LOG_DATABASE=nvrevent.db
NVR_ORG_LOG_DATABASE=.nvrevent.db


create_nvr_log_database()
{
        # create nvr log symbolic link
        if [ "$SUPPORT_NVR" == "yes" ]; then
                if [ -f $HIDDEN_CONF_FILE ]; then
                        #check log dir
                        if [ ! -d $NVR_HIDDEN_LOG_DIR ]; then
                                echo $NVR_HIDDEN_LOG_DIR not create, create $NVR_HIDDEN_LOG_DIR... [OK]
                                /bin/mkdir -p $NVR_HIDDEN_LOG_DIR
                        fi
                        # create NVR_VS_LOG_DIR
                        if [ ! -f $NVR_HIDDEN_LOG_DIR/$NVR_LOG_DATABASE ]; then
                                echo sqdatabase file not found, create symboloc link $NVR_VS_LOG_DIR... [OK]
                                /bin/cp $NVR_VS_LOG_DIR/$NVR_ORG_LOG_DATABASE $NVR_HIDDEN_LOG_DIR/$NVR_LOG_DATABASE
                                /bin/rm -rf $NVR_VS_LOG_DIR
                                /bin/ln -sf $NVR_HIDDEN_LOG_DIR $NVR_VS_LOG_DIR
                        else
                                # NVR_VS_LOG_DIR link not exist
                                echo log hidden config directory exist, create symboloc link $NVR_VS_LOG_DIR... [OK]
                                /bin/rm -rf $NVR_VS_LOG_DIR
                                /bin/ln -sf $NVR_HIDDEN_LOG_DIR $NVR_VS_LOG_DIR
                        fi
                else
			/bin/mv $NVR_VS_LOG_DIR/$NVR_ORG_LOG_DATABASE $NVR_VS_LOG_DIR/$NVR_LOG_DATABASE
                        echo check no HD exist... [OK]
                fi
                if [ "$NVR_ROOTFS" != "" ]; then
                        if [ ! -e $NVR_VS_LOG_ROOT_DIR ]; then
                                /bin/rm -rf $NVR_VS_LOG_ROOT_DIR
                                /bin/ln -s $NVR_VS_LOG_DIR $NVR_VS_LOG_ROOT_DIR
                        fi
                fi
        fi
}
case "$1" in
    init)
		touch /home/httpd/cgi-bin/http_stream_start.cgi
		touch /home/httpd/cgi-bin/http_stream_stop.cgi
		chmod +x /home/httpd/cgi-bin/http_stream_start.cgi
		chmod +x /home/httpd/cgi-bin/http_stream_stop.cgi

		[ -f /usr/bin/nvrd ] && create_nvr_log_database
	;;
    start)
	# only mount if no previous mount
	APP_RUNTIME_CONF="/var/.application.conf"
	NSS_Hide=`/sbin/getcfg DISABLE survielance -d 0 -f $APP_RUNTIME_CONF`
	if [ "x$NSS_Hide" = "x1" ]; then
		/sbin/setcfg NVR Enable FALSE
	fi
	if [ `/sbin/getcfg NVR Enable -u -d FALSE` = TRUE ]; then
			# dont't do reentry control here!, do it in nvrd
			[ -f /usr/bin/nvrd ] || init_nvrd
			/usr/bin/qcmd XMLCR
			/usr/bin/sysadm init
			/usr/bin/sysadm start
			create_nvr_log_database
			echo -n "Starting nvrd services:" 
			/bin/kill -HUP `pidof proftpd`
			/bin/touch /tmp/.nvrd_alive
			/sbin/daemon_mgr.nvr evtLogd start "/usr/bin/evtLogd"
			/sbin/daemon_mgr.nvr nvrd start "/usr/bin/nvrd  2>/dev/null"
			/sbin/daemon_mgr.nvr qplayd start "/usr/bin/qplayd  2>/dev/null"
			# start http streaming here
			count=30;
			while [ $count -gt 0 ]; do
				${WGET} -4 -N http://127.0.0.1:`/sbin/getcfg System "Web Access Port" -d 8080`/cgi-bin/http_stream_start.cgi >& /dev/null
				if [ -e /home/httpd/cgi-bin/getstream.cgi ]; then
					count=0
				else
        				/bin/sleep 1
					count=$(($count-1));
				fi
			done
			echo -n " nvrd"
			echo "."
	fi
	/etc/init.d/refresh_nvr_crontab.sh
	/etc/init.d/ftp.sh reconfig
	;;
    stop)
	echo -n "Shutting down nvrd services:" 
	# stop http streaming here
	count=30;
	while [ $count -gt 0 ]; do
		${WGET} -4 -N http://127.0.0.1:`/sbin/getcfg System "Web Access Port" -d 8080`/cgi-bin/http_stream_stop.cgi >& /dev/null
		if [ -e /home/httpd/cgi-bin/getstream.cgi ]; then
        		/bin/sleep 1
			count=$(($count-1));
		else
			count=0
		fi
	done
	/sbin/daemon_mgr.nvr qplayd stop /usr/bin/qplayd
	/sbin/daemon_mgr.nvr nvrd stop /usr/bin/nvrd
	echo -n " nvrd"
	echo "."
	count=30;
	while [ $count -gt 0 ] && [ "`pidof nvrd`" != "" ]; do
        /bin/sleep 1
	count=$(($count-1));
	done
	if [ $count -gt 0 ]; then
		/bin/kill -9 `pidof nvrd` > /dev/null 2> /dev/null
	fi
	/sbin/daemon_mgr.nvr evtLogd stop /usr/bin/evtLogd
	if [ "`pidof evtLogd`" != "" ]; then
	/bin/kill -9 `pidof evtLogd` > /dev/null 2> /dev/null
	fi
	/etc/init.d/refresh_nvr_crontab.sh
	/usr/bin/sysadm stop
	/usr/bin/sysadm uninit
	;;
    restart)
	$0 stop
	$0 start
	;;	
    *)
        echo "Usage: /etc/init.d/nvrd {start|stop|restart}"
        exit 1
esac

exit 0
