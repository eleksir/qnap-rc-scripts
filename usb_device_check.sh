#!/bin/sh
#
# Copyright @ 2010 QNAP Systems Inc.
#
# This script use to check the inserted usb devices
#
# Version       1.0.0
#
RT2870_Vid=(050d 07d1 148f 14b2 7392)
RT2870_Pid=(815c 3c09 2770 3c25 7717)
RT3070_Vid=(083a 0b05 1737 7392 0411 0586 0586 07d1 07b8 07d1)
RT3070_Pid=(a701 1784 0077 7711 015d 341a 341e 3c16 3071 3c0a)
RT5370_Vid=(2001 2001)
RT5370_Pid=(3c19 3c15)
RT3572_Vid=(13b1 1737)
RT3572_Pid=(002f 0079)
RTL8712_Vid=(050d 050d 0b05 0586 07d1 0bda)
RTL8712_Pid=(845a 945a 1786 341f 3303 8172)
RTL8192CU_Vid=(0bda)
RTL8192CU_Pid=(8176)

WIRELESS_CHECK_TEMP="/tmp/wireless_check_tmp"	
/usr/sbin/lsusb | /bin/awk '{print $6}' > $WIRELESS_CHECK_TEMP

        while read LINE
        do	
		#echo $LINE 
		usb_idVendor=`echo $LINE | cut -f 1 -d ':'`
		usb_idProduct=`echo $LINE | cut -f 2 -d ':'`
		for (( i=0; i<${#RT2870_Vid[@]}; i=i+1 ))
		do
		        if [ "${RT2870_Vid[$i]}" = "$usb_idVendor" ] && [ "${RT2870_Pid[$i]}" = "$usb_idProduct" ]; then
       		 	        #echo ${RT2870_Vid[$i]} ${RT2870_Pid[$i]} "MATCH!!!" >> /tmp/usbdebug
		                DEVNAME="WIFI_RT2870"
				/etc/init.d/wireless_modules.sh start $DEVNAME
	                	break;
	        	fi
		done

		for (( i=0; i<${#RT3070_Vid[@]}; i=i+1 ))
		do
		        if [ "${RT3070_Vid[$i]}" = "$usb_idVendor" ] && [ "${RT3070_Pid[$i]}" = "$usb_idProduct" ]; then
	        	        #echo ${RT3070_Vid[$i]} ${RT3070_Pid[$i]} "MATCH!!!" >> /tmp/usbdebug
                		DEVNAME="WIFI_RT3070"
				/etc/init.d/wireless_modules.sh start $DEVNAME
	        	        break;
		        fi
		done

                for (( i=0; i<${#RT5370_Vid[@]}; i=i+1 ))
                do
                        if [ "${RT5370_Vid[$i]}" = "$usb_idVendor" ] && [ "${RT5370_Pid[$i]}" = "$usb_idProduct" ]; then
                                #echo ${RT5370_Vid[$i]} ${RT5370_Pid[$i]} "MATCH!!!" >> /tmp/usbdebug
                                DEVNAME="WIFI_RT5370"
                                /etc/init.d/wireless_modules.sh start $DEVNAME
                                break;
                        fi
                done

                for (( i=0; i<${#RT3572_Vid[@]}; i=i+1 ))
                do
                        if [ "${RT3572_Vid[$i]}" = "$usb_idVendor" ] && [ "${RT3572_Pid[$i]}" = "$usb_idProduct" ]; then
                                #echo ${RT3572_Vid[$i]} ${RT3572_Pid[$i]} "MATCH!!!" >> /tmp/usbdebug
                                DEVNAME="WIFI_RT3572"
                                /etc/init.d/wireless_modules.sh start $DEVNAME
                                break;
                        fi
                done

                for (( i=0; i<${#RTL8192CU_Vid[@]}; i=i+1 ))
                do
                        if [ "${RTL8192CU_Vid[$i]}" = "$usb_idVendor" ] && [ "${RTL8192CU_Pid[$i]}" = "$usb_idProduct" ]; then
                                #echo ${RTL8192CU_Vid[$i]} ${RTL8192CU_Pid[$i]} "MATCH!!!" >> /tmp/usbdebug
                                DEVNAME="WIFI_RTL8192CU"
                                /etc/init.d/wireless_modules.sh start $DEVNAME
                                break;
                        fi
                done

		/bin/uname -a | /bin/grep x86_64
		if [ $? != 0 ]; then
		for (( i=0; i<${#RTL8712_Vid[@]}; i=i+1 ))
		do
		        if [ "${RTL8712_Vid[$i]}" = "$usb_idVendor" ] && [ "${RTL8712_Pid[$i]}" = "$usb_idProduct" ]; then
       		 	        #echo ${RTL8712_Vid[$i]} ${RTL8712_Pid[$i]} "MATCH!!!" >> /tmp/usbdebug
		         	DEVNAME="WIFI_RTL8712"
				/etc/init.d/wireless_modules.sh start $DEVNAME
	                	break;
        		fi
		done
		fi		

        done < $WIRELESS_CHECK_TEMP

