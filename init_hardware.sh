#!/bin/sh
mknod /dev/rtcmon c 10 81
mknod /dev/watchdog c 10 130 
mknod -m 666 /dev/fuse c 10 229

[ "x$COMPRT4LCD" != "xyes" ] && [ -f /sbin/lcd_tool ] && /sbin/lcd_tool -1 "Loading Driver" -2 "Please Wait..."
if [ -f /lib/modules/misc/usbcore.ko ]; then
	[ ! -f /lib/modules/misc/usb-common.ko ] || /sbin/insmod /lib/modules/misc/usb-common.ko 2>>/dev/null
	[ ! -f /lib/modules/misc/usbcore.ko ] || /sbin/insmod /lib/modules/misc/usbcore.ko 2>>/dev/null
	[ ! -f /lib/modules/misc/ehci-hcd.ko ] || /sbin/insmod /lib/modules/misc/ehci-hcd.ko 2>>/dev/null
	[ ! -f /lib/modules/misc/uhci-hcd.ko ] || /sbin/insmod /lib/modules/misc/uhci-hcd.ko 2>>/dev/null
	/bin/mount -t usbfs none /proc/bus/usb 2>>/dev/null
fi
#ARM add model
[ ! -f /lib/modules/others/fuse.ko ] || /sbin/insmod /lib/modules/others/fuse.ko
[ ! -f /lib/modules/others/quota_tree.ko ] || /sbin/insmod /lib/modules/others/quota_tree.ko
[ ! -f /lib/modules/others/quota_v1.ko ] || /sbin/insmod /lib/modules/others/quota_v1.ko
[ ! -f /lib/modules/others/quota_v2.ko ] || /sbin/insmod /lib/modules/others/quota_v2.ko
[ ! -f /lib/modules/others/llc.ko ] || /sbin/insmod /lib/modules/others/llc.ko
[ ! -f /lib/modules/others/psnap.ko ] || /sbin/insmod /lib/modules/others/psnap.ko
[ ! -f /lib/modules/others/appletalk.ko ] || /sbin/insmod /lib/modules/others/appletalk.ko
[ ! -f /lib/modules/others/fat.ko ] || /sbin/insmod /lib/modules/others/fat.ko
[ ! -f /lib/modules/others/vfat.ko ] || /sbin/insmod /lib/modules/others/vfat.ko
[ ! -f /lib/modules/others/nls_cp437.ko ] || /sbin/insmod /lib/modules/others/nls_cp437.ko
[ ! -f /lib/modules/others/nls_cp850.ko ] || /sbin/insmod /lib/modules/others/nls_cp850.ko
[ ! -f /lib/modules/others/nls_iso8859-2.ko ] || /sbin/insmod /lib/modules/others/nls_iso8859-2.ko
[ ! -f /lib/modules/others/nls_utf8.ko ] || /sbin/insmod /lib/modules/others/nls_utf8.ko
[ ! -f /lib/modules/others/autofs.ko ] || /sbin/insmod /lib/modules/others/autofs.ko
[ ! -f /lib/modules/misc/linear.ko ] || /sbin/insmod /lib/modules/misc/linear.ko
[ ! -f /lib/modules/misc/raid0.ko ] || /sbin/insmod /lib/modules/misc/raid0.ko
[ ! -f /lib/modules/misc/raid10.ko ] || /sbin/insmod /lib/modules/misc/raid10.ko
#---- RAID 6 ----
[ ! -f /lib/modules/misc/xor.ko ] || /sbin/insmod /lib/modules/misc/xor.ko
[ ! -f /lib/modules/misc/raid6_pq.ko ] || /sbin/insmod /lib/modules/misc/raid6_pq.ko
[ ! -f /lib/modules/misc/async_tx.ko ] || /sbin/insmod /lib/modules/misc/async_tx.ko
[ ! -f /lib/modules/misc/async_memcpy.ko ] || /sbin/insmod /lib/modules/misc/async_memcpy.ko
[ ! -f /lib/modules/misc/async_xor.ko ] || /sbin/insmod /lib/modules/misc/async_xor.ko
[ ! -f /lib/modules/misc/async_pq.ko ] || /sbin/insmod /lib/modules/misc/async_pq.ko
[ ! -f /lib/modules/misc/async_raid6_recov.ko ] || /sbin/insmod /lib/modules/misc/async_raid6_recov.ko
[ ! -f /lib/modules/misc/raid456.ko ] || /sbin/insmod /lib/modules/misc/raid456.ko
#------------
[ ! -f /lib/modules/misc/dm-mod.ko ] || /sbin/insmod /lib/modules/misc/dm-mod.ko
[ ! -f /lib/modules/misc/dm-crypt.ko ] || /sbin/insmod /lib/modules/misc/dm-crypt.ko
[ ! -f /lib/modules/misc/aes_generic.ko ] || /sbin/insmod /lib/modules/misc/aes_generic.ko
[ ! -f /lib/modules/misc/sha1_generic.ko ] || /sbin/insmod /lib/modules/misc/sha1_generic.ko
[ ! -f /lib/modules/misc/sha256_generic.ko ] || /sbin/insmod /lib/modules/misc/sha256_generic.ko
[ ! -f /lib/modules/misc/sha512_generic.ko ] || /sbin/insmod /lib/modules/misc/sha512_generic.ko
[ ! -f /lib/modules/misc/mv_cesa.ko ] || /sbin/insmod /lib/modules/misc/mv_cesa.ko
if [ -f /lib/modules/misc/cryptodev.ko ]; then
	/sbin/insmod /lib/modules/misc/cryptodev.ko
	_maj=`/bin/cat /sys/class/misc/crypto/dev 2>/dev/null | /bin/cut -d ":" -f 1`
	_min=`/bin/cat /sys/class/misc/crypto/dev 2>/dev/null | /bin/cut -d ":" -f 2`
	if [ ! -z "$_maj" ] && [ ! -z "$_min" ]; then
		/bin/mknod -m 0666 /dev/crypto c "$_maj" "$_min"
	fi
fi
#-------------
[ ! -f /lib/modules/misc/usb-storage.ko ] || /sbin/insmod /lib/modules/misc/usb-storage.ko 2>>/dev/null
[ ! -f /lib/modules/misc/usbhid.ko ] || /sbin/insmod /lib/modules/misc/usbhid.ko
[ ! -f /lib/modules/misc/rtcmod.ko ] || /sbin/insmod /lib/modules/misc/rtcmod.ko
[ ! -f /lib/modules/misc/iTCO_vendor_support.ko ] || /sbin/insmod /lib/modules/misc/iTCO_vendor_support.ko
[ ! -f /lib/modules/misc/iTCO_wdt.ko ] || /sbin/insmod /lib/modules/misc/iTCO_wdt.ko heartbeat=120
[ ! -f /lib/modules/misc/usblp.ko ] || /sbin/insmod /lib/modules/misc/usblp.ko
[ ! -f /lib/modules/misc/atl1e.ko ] || /sbin/insmod /lib/modules/misc/atl1e.ko
[ ! -f /lib/modules/misc/e1000e.ko ] || /sbin/insmod /lib/modules/misc/e1000e.ko
[ ! -f /lib/modules/misc/sysinfo.ko ] || /sbin/insmod /lib/modules/misc/sysinfo.ko
[ ! -f /lib/modules/misc/jbd2.ko ] || /sbin/insmod /lib/modules/misc/jbd2.ko
[ ! -f /lib/modules/misc/ext4.ko ] || /sbin/insmod /lib/modules/misc/ext4.ko
[ ! -f /lib/modules/misc/ufsd.ko ] || /sbin/insmod /lib/modules/misc/ufsd.ko
[ ! -f /lib/modules/others/isofs.ko ] || /sbin/insmod /lib/modules/others/isofs.ko
[ ! -f /lib/modules/others/udf.ko ] || /sbin/insmod /lib/modules/others/udf.ko
[ ! -f /lib/modules/others/sunrpc.ko ] || /sbin/insmod /lib/modules/others/sunrpc.ko
[ ! -f /lib/modules/others/lockd.ko ] || /sbin/insmod /lib/modules/others/lockd.ko
[ ! -f /lib/modules/others/nfs_acl.ko ] || /sbin/insmod /lib/modules/others/nfs_acl.ko
[ ! -f /lib/modules/others/auth_rpcgss.ko ] || /sbin/insmod /lib/modules/others/auth_rpcgss.ko
[ ! -f /lib/modules/others/nfs.ko ] || /sbin/insmod /lib/modules/others/nfs.ko
[ ! -f /lib/modules/others/exportfs.ko ] || /sbin/insmod /lib/modules/others/exportfs.ko
[ ! -f /lib/modules/others/nfsd.ko ] || /sbin/insmod /lib/modules/others/nfsd.ko
[ ! -f /lib/modules/others/cifs.ko ] || /sbin/insmod /lib/modules/others/cifs.ko
[ ! -f /lib/modules/misc/ehci-hcd.ko ] || /sbin/insmod /lib/modules/misc/ehci-hcd.ko 2>>/dev/null
#[ ! -f /lib/modules/misc/uas.ko ] || /sbin/insmod /lib/modules/misc/uas.ko 2>>/dev/null
[ ! -f /lib/modules/misc/xhci.ko ] || /sbin/insmod /lib/modules/misc/xhci.ko
[ ! -f /lib/modules/misc/xhci-hcd.ko ] || /sbin/insmod /lib/modules/misc/xhci-hcd.ko
[ ! -f /lib/modules/misc/etxhci-hcd.ko ] || /sbin/insmod /lib/modules/misc/etxhci-hcd.ko
[ ! -f /lib/modules/others/smbfs.ko ] || /sbin/insmod /lib/modules/others/smbfs.ko
[ ! -f /lib/modules/misc/fnotify.ko ] || /sbin/insmod /lib/modules/misc/fnotify.ko
[ ! -f /usr/local/modules/lib80211.ko ] || /sbin/insmod /usr/local/modules/lib80211.ko
[ ! -f /usr/local/modules/cfg80211.ko ] || /sbin/insmod /usr/local/modules/cfg80211.ko
[ ! -f /usr/local/modules/mac80211.ko ] || /sbin/insmod /usr/local/modules/mac80211.ko
[ ! -f /usr/local/modules/hmac.ko ] || /sbin/insmod /usr/local/modules/hmac.ko
[ ! -f /usr/local/modules/ecb.ko ] || /sbin/insmod /usr/local/modules/ecb.ko
[ ! -f /usr/local/modules/md4.ko ] || /sbin/insmod /usr/local/modules/md4.ko
if [ -f /sbin/qwatchdogd ]; then
        BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf 2>/dev/null`
        if [ "x${BOOT_CONF}" = "xTS-NASARM" ]; then
            /sbin/daemon_mgr qwatchdogd start "/sbin/qwatchdogd -t 1 &"
            if [ -x /sbin/hal_app ]; then
                /sbin/hal_app --se_sys_set_pic_wdt enc_sys_id=root,mode=0
            else                
                /sbin/pic_raw 103 # disable pic watch dog
            fi                
        else
            /sbin/daemon_mgr qwatchdogd start "/sbin/qwatchdogd -t 5 &"
        fi
fi

#SOUND
[ ! -f /lib/modules/others/soundcore.ko ] || insmod /lib/modules/others/soundcore.ko
[ ! -f /lib/modules/others/snd.ko ] || insmod /lib/modules/others/snd.ko
[ ! -f /lib/modules/others/snd-timer.ko ] || insmod /lib/modules/others/snd-timer.ko
[ ! -f /lib/modules/others/snd-seq-device.ko ] || insmod /lib/modules/others/snd-seq-device.ko
[ ! -f /lib/modules/others/snd-page-alloc.ko ] || insmod /lib/modules/others/snd-page-alloc.ko
[ ! -f /lib/modules/others/snd-rawmidi.ko ] || insmod /lib/modules/others/snd-rawmidi.ko
[ ! -f /lib/modules/others/snd-seq.ko ] || insmod /lib/modules/others/snd-seq.ko
[ ! -f /lib/modules/others/snd-seq-midi-event.ko ] || insmod /lib/modules/others/snd-seq-midi-event.ko
[ ! -f /lib/modules/others/snd-seq-midi.ko ] || insmod /lib/modules/others/snd-seq-midi.ko
[ ! -f /lib/modules/others/snd-seq-oss.ko ] || insmod /lib/modules/others/snd-seq-oss.ko
[ ! -f /lib/modules/others/snd-pcm.ko ] || insmod /lib/modules/others/snd-pcm.ko
[ ! -f /lib/modules/others/snd-mixer-oss.ko ] || insmod /lib/modules/others/snd-mixer-oss.ko
[ ! -f /lib/modules/others/snd-pcm-oss.ko ] || insmod /lib/modules/others/snd-pcm-oss.ko
[ ! -f /lib/modules/others/snd-hwdep.ko ] || insmod /lib/modules/others/snd-hwdep.ko
[ ! -f /lib/modules/others/snd-usb-lib.ko ] || insmod /lib/modules/others/snd-usb-lib.ko
[ ! -f /lib/modules/others/snd-usbmidi-lib.ko ] || insmod /lib/modules/others/snd-usbmidi-lib.ko

#hd station
[ ! -f /lib/modules/others/hid-logitech.ko ] || insmod /lib/modules/others/hid-logitech.ko
[ ! -f /lib/modules/others/hid-logitech-dj.ko ] || insmod /lib/modules/others/hid-logitech-dj.ko
[ ! -f /lib/modules/others/snd-hda-codec.ko ] || insmod /lib/modules/others/snd-hda-codec.ko
[ ! -f /lib/modules/others/snd-hda-codec-analog.ko ] || insmod /lib/modules/others/snd-hda-codec-analog.ko
[ ! -f /lib/modules/others/snd-hda-codec-hdmi.ko ] || insmod /lib/modules/others/snd-hda-codec-hdmi.ko
if [ -f /lib/modules/others/snd-hda-intel.ko ]; then
	insmod /lib/modules/others/snd-hda-intel.ko
	[ ! -f /lib/modules/others/snd-usb-audio.ko ] || insmod /lib/modules/others/snd-usb-audio.ko qnap_start_index=1
else
	[ ! -f /lib/modules/others/snd-usb-audio.ko ] || insmod /lib/modules/others/snd-usb-audio.ko
fi

[ ! -f /lib/modules/others/snd-usb-caiaq.ko ] || insmod /lib/modules/others/snd-usb-caiaq.ko

[ ! -f /lib/modules/others/drm_kms_helper.ko ] || insmod /lib/modules/others/drm_kms_helper.ko
[ ! -f /lib/modules/others/i915.ko ] || insmod /lib/modules/others/i915.ko

[ ! -f /lib/modules/others/v4l2-int-device.ko ] || insmod /lib/modules/others/v4l2-int-device.ko
[ ! -f /lib/modules/others/v4l1-compat.ko ] || insmod /lib/modules/others/v4l1-compat.ko
[ ! -f /lib/modules/others/v4l2-compat-ioctl32.ko ] || insmod /lib/modules/others/v4l2-compat-ioctl32.ko
[ ! -f /lib/modules/others/videodev.ko ] || insmod /lib/modules/others/videodev.ko
[ ! -f /lib/modules/others/v4l2-common.ko ] || insmod /lib/modules/others/v4l2-common.ko
[ ! -f /lib/modules/others/videobuf2-core.ko ] || insmod /lib/modules/others/videobuf2-core.ko
[ ! -f /lib/modules/others/videobuf2-memops.ko ] || insmod /lib/modules/others/videobuf2-memops.ko
[ ! -f /lib/modules/others/videobuf2-vmalloc.ko ] || insmod /lib/modules/others/videobuf2-vmalloc.ko
[ ! -f /lib/modules/others/uvcvideo.ko ] || insmod /lib/modules/others/uvcvideo.ko

[ -d /dev/snd ] || /bin/mkdir -m 777 /dev/snd
[ -c /dev/snd/seq ] || /bin/mknod -m 666 /dev/snd/seq c 116 1
[ -c /dev/snd/timer ] || /bin/mknod -m 666 /dev/snd/timer c 116 33
for (( i=0; i<5; i=i+1 ))
do
	[ -c /dev/dsp${i} ] || /bin/mknod -m 666 /dev/dsp${i} c 14 `expr 3 + ${i} \* 16`
	[ -c /dev/audio${i} ] || /bin/mknod -m 666 /dev/audio${i} c 14 `expr 4 + ${i} \* 16`
	[ -c /dev/mixer${i} ] || /bin/mknod -m 666 /dev/mixer${i} c 14 `expr ${i} \* 16`
	[ -c /dev/video${i} ] || /bin/mknod -m 666 /dev/video${i} c 81 ${i}
	[ -c /dev/snd/controlC${i} ] || /bin/mknod -m 666 /dev/snd/controlC${i} c 116 `expr 0 + ${i} \* 32`
	[ -c /dev/snd/pcmC${i}D0p ] || /bin/mknod -m 666 /dev/snd/pcmC${i}D0p c 116 `expr 16 + ${i} \* 32`
	[ -c /dev/snd/pcmC${i}D0c ] || /bin/mknod -m 666 /dev/snd/pcmC${i}D0c c 116 `expr 24 + ${i} \* 32`
done
/bin/ln -sf dsp0 /dev/dsp
/bin/ln -sf audio0 /dev/audio
/bin/ln -sf mixer0 /dev/mixer
/bin/ln -sf video0 /dev/video

[ -c /dev/fb0 ] || /bin/mknod -m 666 /dev/fb0 c 29 0
[ -c /dev/fb1 ] || /bin/mknod -m 666 /dev/fb1 c 29 1

#/sbin/insmod /lib/modules/misc/sysinfo.ko
# Added by Ricky - for TS-101M
#insmod /lib/modules/misc/sd_mod.ko
#insmod /lib/modules/misc/sg.ko
#if [ -f /lib/modules/misc/usb-storage.ko ]; then
#	insmod /lib/modules/misc/usb-storage.ko
#else 
#	insmod /lib/modules/usb-storage.ko
#fi
#insmod /lib/modules/misc/fat.ko
#insmod /lib/modules/misc/msdos.ko
#insmod /lib/modules/misc/vfat.ko
#insmod /lib/modules/misc/ntfs.ko

# for ntfs-3g
[ ! -f /lib/modules/misc/fuse.ko ] || /sbin/insmod /lib/modules/misc/fuse.ko

if [ -f /usr/local/lib/dm-ddsnap.ko ]; then
	for i in a b c d e f g h
	do
		/sbin/mksdnod -p 7 sd$i > /dev/null
	done
	j=16
	while [ $j -le 30 ] ; do
		/bin/mknod /dev/md$j b 9 $j
	        j=$(($j+1))
	done
fi
[ ! -f /usr/local/bin/vconfig ] || [ ! -f /usr/local/modules/8021q.ko ] || /sbin/insmod /usr/local/modules/8021q.ko

# scan network card on PCIE bus
/etc/init.d/check_lan_port.sh

