#!/bin/sh

EXE_CLOUD3P="/usr/sbin/cloud3p"
DIR_VAULT_CONF="/etc/config/vaultServices"
SZF_VAULT_CONF="vaultServices.config"
SZP_VAULT_CONF_DEF="/etc/default_config/vaultServices/vaultServices.config"

start() {
	if [ -f "$EXE_CLOUD3P" ]; then
		echo "Startup all cloud related services..."
		[ -d "$DIR_VAULT_CONF" ] || mkdir "$DIR_VAULT_CONF"

		if [ ! -f "$DIR_VAULT_CONF/$SZF_VAULT_CONF" ] && [ -f "$SZP_VAULT_CONF_DEF" ]; then
			/bin/cp -p "$SZP_VAULT_CONF_DEF" "$DIR_VAULT_CONF/$SZF_VAULT_CONF"
		fi

		"$EXE_CLOUD3P" --start
	fi
}

stop(){
	if [ -f "$EXE_CLOUD3P" ]; then
		echo "Shutting down all cloud related services..."
		"$EXE_CLOUD3P" --stop
	fi
}

case "$1" in
	"start")
		start
		;;

	"stop")
		stop
		;;

	"restart")
		stop
		sleep 1
		start
	;;

	*)
		echo "Usage: /etc/init.d/cloud3p.sh {start|stop|restart}"
		exit 1
esac

exit 0
