#!/bin/sh
AVAHI_SRC=/mnt/ext/opt/avahi0630
DBUS_DAEMON=/usr/sbin/dbus-daemon
AVAHI_DAEMON=/usr/sbin/avahi-daemon
AVAHI_CONF_DIR=/etc/avahi/services
AVAHI_TM_CONF_FILE=${AVAHI_CONF_DIR}/timemachine.service
MYCLOUDNAS_CONF="/etc/config/qnapddns.conf"
Project_Name=`/sbin/getcfg Project Name -f /var/default`
/sbin/test -f ${AVAHI_DAEMON} || exit 0


set_avahi_conf()
{
	/bin/rm -rf ${AVAHI_CONF_DIR}/web.service \
				${AVAHI_CONF_DIR}/samba.service \
				${AVAHI_CONF_DIR}/afp.service \
				${AVAHI_CONF_DIR}/ssh.service \
				${AVAHI_CONF_DIR}/sftp-ssh.service \
				${AVAHI_CONF_DIR}/https.service \
				${AVAHI_CONF_DIR}/upnp.service \
				${AVAHI_CONF_DIR}/itunes.service \
				${AVAHI_CONF_DIR}/printer.service \
				${AVAHI_CONF_DIR}/qmobile.service \
				${AVAHI_CONF_DIR}/qdiscover.service \
				${AVAHI_CONF_DIR}/csco.service \
				${AVAHI_CONF_DIR}/ftp.service >&/dev/null

	MAC_ADDR=`/sbin/getcfg "MAC" "eth0" -f "/var/MacAddress" | /bin/tr '[a-z]' '[A-Z]'`
	WEB_PORT=`/sbin/getcfg System "Web Access Port" -d 8080`
	HTTPS_PORT=`/sbin/getcfg "Stunnel" "Port" -d "443"`
	HOSTNAME=`/sbin/getcfg System "Server Name"`
	MYCLOUDNAS_NAME=`/sbin/getcfg "QNAP DDNS Service" "Host Name" -f "${MYCLOUDNAS_CONF}"`
	MYCLOUDNAS_DN=`/sbin/getcfg "QNAP DDNS Service" "Domain Name Server" -f "${MYCLOUDNAS_CONF}"`
	FW_VER=`/sbin/getcfg "System" "Version" -d --`
	FW_BN=`/sbin/getcfg System "Build Number" -d --`
	SN=`/sbin/get_hwsn`
	DISPLAY_MODEL=`/sbin/get_display_name`
	MODEL=`/sbin/getcfg "System" "Model" -d NAS`
	
	if [ "x$HOSTNAME" = "x" ]; then
		/sbin/gen_hostname
		/bin/sync
		HOSTNAME=`/sbin/getcfg System "Server Name"`
	fi

	if [ `/sbin/getcfg "Bonjour Service" Web -u -d 1` = 1 ]; then
		BROWSENAME=`/sbin/getcfg "Bonjour Service" "Web Service Name" -d "$HOSTNAME"`
/bin/cat > ${AVAHI_CONF_DIR}/web.service <<__EOF__
<?xml version="1.0" standalone='no'?><!--*-nxml-*-->
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
<name replace-wildcards="yes">$BROWSENAME</name>
<service>
<type>_http._tcp</type>
<port>${WEB_PORT}</port>
<txt-record>path=/</txt-record>
</service>
</service-group>
__EOF__
	fi

	if [ `/sbin/getcfg "Bonjour Service" SAMBA -u -d 1` = 1 ]; then
		BROWSENAME=`/sbin/getcfg "Bonjour Service" "SAMBA Service Name" -d "$HOSTNAME(SMB)"`
/bin/cat > ${AVAHI_CONF_DIR}/samba.service <<__EOF__
<?xml version="1.0" standalone='no'?><!--*-nxml-*-->
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
<name replace-wildcards="yes">$BROWSENAME</name>
<service>
<type>_smb._tcp</type>
<port>445</port>
</service>
</service-group>
__EOF__
	fi

	if [ `/sbin/getcfg "Bonjour Service" AFP -u -d 0` = 1 ]; then
		BROWSENAME=`/sbin/getcfg "Bonjour Service" "AFP Service Name" -d "$HOSTNAME(AFP)"`
/bin/cat > ${AVAHI_CONF_DIR}/afp.service <<__EOF__
<?xml version="1.0" standalone='no'?><!--*-nxml-*-->
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
<name replace-wildcards="yes">$BROWSENAME</name>
<service>
<type>_afpovertcp._tcp</type>
<port>548</port>
</service>
<service>
<type>_device-info._tcp</type>
<port>0</port>
<txt-record>model=Mac</txt-record>
</service>
</service-group>
__EOF__
	fi

	if [ `/sbin/getcfg "Bonjour Service" SSH -u -d 0` = 1 ]; then
		BROWSENAME=`/sbin/getcfg "Bonjour Service" "SSH Service Name"`
		SSH_PORT=`/sbin/getcfg LOGIN "SSH Port"`
/bin/cat > ${AVAHI_CONF_DIR}/ssh.service <<__EOF__
<?xml version="1.0" standalone='no'?><!--*-nxml-*-->
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
<name replace-wildcards="yes">$BROWSENAME</name>
<service>
<type>_ssh._tcp</type>
<port>${SSH_PORT}</port>
</service>
</service-group>
__EOF__
	fi

	if [ `/sbin/getcfg "Bonjour Service" FTP -u -d 0` = 1 ]; then
		BROWSENAME=`/sbin/getcfg "Bonjour Service" "FTP Service Name"`
		FTP_PORT=`/sbin/getcfg FTP Port`
/bin/cat > ${AVAHI_CONF_DIR}/ftp.service <<__EOF__
<?xml version="1.0" standalone='no'?><!--*-nxml-*-->
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
<name replace-wildcards="yes">$BROWSENAME</name>
<service>
<type>_ftp._tcp</type>
<port>${FTP_PORT}</port>
<txt-record>model=path=/</txt-record>
</service>
</service-group>
__EOF__
	fi

	if [ `/sbin/getcfg "Bonjour Service" HTTPS -u -d 0` = 1 ]; then
		BROWSENAME=`/sbin/getcfg "Bonjour Service" "HTTPS Service Name"`
/bin/cat > ${AVAHI_CONF_DIR}/https.service <<__EOF__
<?xml version="1.0" standalone='no'?><!--*-nxml-*-->
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
<name replace-wildcards="yes">$BROWSENAME</name>
<service>
<type>_https._tcp</type>
<port>${HTTPS_PORT}</port>
<txt-record>model=path=/</txt-record>
</service>
</service-group>
__EOF__
	fi

	if [ `/sbin/getcfg "Bonjour Service" UPNP -u -d 0` = 1 ]; then
		BROWSENAME=`/sbin/getcfg "Bonjour Service" "UPNP Service Name" -d "$HOSTNAME(DLNA)"`
		UPNP_PORT=`/sbin/getcfg "TwonkyMedia" "Port" -d 9000`
/bin/cat > ${AVAHI_CONF_DIR}/upnp.service <<__EOF__
<?xml version="1.0" standalone='no'?><!--*-nxml-*-->
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
<name replace-wildcards="yes">$BROWSENAME</name>
<service>
<type>_upnp._tcp</type>
<port>${UPNP_PORT}</port>
</service>
</service-group>
__EOF__
	fi

	if [ `/sbin/getcfg "Bonjour Service" "QMobile" -u -d 1` = 1 ]; then
		BROWSENAME=`/sbin/getcfg "Bonjour Service" "QMobile Service Name" -d "$HOSTNAME(Apps)"`
		QMOBILE_PORT=`/sbin/getcfg "QMobile" "Port" -d 3986`
/bin/cat > ${AVAHI_CONF_DIR}/qmobile.service <<__EOF__
<?xml version="1.0" standalone='no'?><!--*-nxml-*-->
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
<name replace-wildcards="yes">$BROWSENAME</name>
<service>
<type>_qmobile._tcp</type>
<port>${QMOBILE_PORT}</port>
</service>
</service-group>
__EOF__
	fi

	if [ `/sbin/getcfg "Bonjour Service" QNAP -u -d 1` = 1 ]; then
		BROWSENAME=`/sbin/getcfg "Bonjour Service" QNAP -d "$HOSTNAME"`
		atype="http";
		aport=${WEB_PORT};
		if [ `/sbin/getcfg "Stunnel" "Enable" -u -d 1` = 1  -a `/sbin/getcfg "System" "Force SSL" -u -d 0` = 1 ]; then
			atype="https"
			aport=${HTTPS_PORT}
		fi
		qTXT="accessType=${atype},accessPort=${aport},model=${MODEL},displayModel=${DISPLAY_MODEL},fwVer=${FW_VER},fwBuildNum=${FW_BN}"
		if [ x != x"$SN" ]; then
			qTXT="${qTXT},serialNum=${SN}"
		fi
		if [ x != x"$MYCLOUDNAS_NAME" -a x != x"$MYCLOUDNAS_DN" ]; then
			qTXT="${qTXT},myCloudNASName=${MYCLOUDNAS_NAME},myCloudNASDomain=${MYCLOUDNAS_DN}"
		fi
		
/bin/cat > ${AVAHI_CONF_DIR}/qdiscover.service <<__EOF__
<?xml version="1.0" standalone='no'?><!--*-nxml-*-->
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
<name replace-wildcards="yes">$BROWSENAME</name>
<service>
<type>_qdiscover._tcp</type>
<port>${WEB_PORT}</port>
<txt-record>$qTXT</txt-record>
</service>
</service-group>
__EOF__
	fi
	
	if [ `/sbin/getcfg iTune Enable -u -d FALSE` = TRUE ]; then
		_iTunes_ID=`/sbin/get_hwsn`
		if [ $? != 0 ]; then
			_iTunes_ID=`/sbin/uuidgen | /bin/cut -c 1-8`
		fi
		if [ "x`/sbin/getcfg iTune "Password Enabled" -u -d 0`" = "x1" ]; then
			export _Passowrd=true
		else
			export _Passowrd=false
		fi
/bin/cat > ${AVAHI_CONF_DIR}/itunes.service <<__EOF__
<?xml version="1.0" standalone='no'?>
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
<name replace-wildcards="yes">$HOSTNAME(iTunes)</name>
<service>
<type>_daap._tcp</type>
<port>3689</port>
<txt-record>txtvers=1</txt-record>
<txt-record>Machine Name=$HOSTNAME(iTunes)</txt-record>
<txt-record>Database ID=${_iTunes_ID}</txt-record>
<txt-record>iTSh Version=131073</txt-record>
<txt-record>Version=196610</txt-record>
<txt-record>Passowrd=${_Passowrd}</txt-record>
</service>
</service-group>

__EOF__
	fi

	if [ `/sbin/getcfg "Bonjour Service" CSCO -u -d 0` = 1 ]; then
		service_name=`/sbin/getcfg "Bonjour Service" "Web Service Name" -d $HOSTNAME`"(csco-sb)"
		/sbin/setcfg "Bonjour Service" "CSCO Service Name" $service_name
		model_name=`/sbin/getcfg "System" "Model"`
		devicedescr=`/bin/echo "$model_name" | /bin/tr -d '[a-z][A-Z]'`
		[ ${model_name} = "NSS324R" ] && devicedescr="324R"
		fmVersion=`/sbin/getcfg "System" "Version"`
		hdVersion=`/sbin/get_hwver`
		[ ${model_name} = "NSS324R" ] && hdVersion="1.0"
		vid=`/bin/echo "${hdVersion} 10" | awk '{ printf("%g", $1 * $2);}'`
		vid=$(($vid - 10 + 1 ))
		if [ ${vid} -lt 10 ]; then
			vid="0$vid"
		fi
		MACAddress=`/sbin/getcfg "MAC" "eth0" -f "/var/MacAddress" | /bin/tr '[a-z]' '[A-Z]' | /bin/tr -d ':'`
		serialNo=`/sbin/get_hwsn`
		hostname=`/bin/hostname`
		mdf_id=""
		if [ ${model_name} = "NSS322" ]; then
			mdf_id="282984440"
		elif [ ${model_name} = "NSS324" ]; then
			mdf_id="282984441"
		elif [ ${model_name} = "NSS326" ]; then
			mdf_id="282984448"
		elif [ ${model_name} = "NSS324R" ]; then
			mdf_id="282984441"
		fi
		accesstype="noaccess";
		if [ `/sbin/getcfg "Stunnel" "Enable" -u -d 1` = 1 ]; then
			if [ "x$accesstype" = "xnoaccess" ]; then
				accesstype="https"
			else
				accesstype="$accesstype, https"
			fi
		fi
		if [ `/sbin/getcfg "System" "Force SSL" -u -d 0` = 0 ]; then
			if [ "x$accesstype" = "xnoaccess" ]; then
				accesstype="http"
			else
				accesstype="$accesstype, http"
			fi
		else
			if [ "x$accesstype" = "xnoaccess" ]; then
				accesstype="http"
			fi
		fi
		if [ `/sbin/getcfg "LOGIN" "SSH Enable" -u -d TRUE` = TRUE ]; then
			if [ "x$accesstype" = "xnoaccess" ]; then
				accesstype="ssh"
			else
				accesstype="$accesstype, ssh"
			fi
		fi
		if [ `/sbin/getcfg "LOGIN" "TELNET Enable" -u -d FALSE` = TRUE ]; then
			if [ "x$accesstype" = "xnoaccess" ]; then
				accesstype="telnet"
			else
				accesstype="$accesstype, telnet"
			fi
		fi


/bin/cat > ${AVAHI_CONF_DIR}/csco.service <<__EOF__
<?xml version="1.0" standalone='no'?>
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
<name replace-wildcards="yes">%h(csco)</name>
<service>
<type>_csco-sb._tcp</type>
<port>${WEB_PORT}</port>
<txt-record>version=1.0</txt-record>
<txt-record>model=${model_name}</txt-record>
<txt-record>deviceType=NAS</txt-record>
<txt-record>deviceDescr=Cisco NSS ${devicedescr} Smart Storage</txt-record>
<txt-record>fmVersion=$fmVersion</txt-record>
<txt-record>PIDVID=${model_name} V${vid}</txt-record>
<txt-record>MACAddress=${MACAddress}</txt-record>
<txt-record>serialNo=${serialNo}</txt-record>
<txt-record>hostname=${hostname}</txt-record>
<txt-record>MDFID=${mdf_id}</txt-record>
<txt-record>accessType=${accesstype}</txt-record>
</service>
</service-group>

__EOF__
	fi

for i in 1 2 3 4 5 6 7; do
[ $i -eq 1 ] && i=""
	if [ `/sbin/getcfg "Bonjour Service" "PRINTER$i" -u -d 0` = 1 ]; then
		local phy_exist=`/sbin/getcfg "Printers" "Phy exist$i"`
		[ "x$phy_exist" = "x" ] || [ $phy_exist -eq 0 ] && continue
		local status=`/sbin/getcfg "Printers" "Status$i"`
		[ $status -eq 4 ] && continue
		local service_name=`/sbin/getcfg "Bonjour Service" "PRINTER Service Name$i"`
		local printer_port=`/bin/cat /etc/config/cups/cupsd.conf|/bin/grep "Listen"|/bin/cut -d':' -f 2|/usr/bin/head -n 1`
		local printer_name=`/sbin/getcfg "Printers" "Name$i"`
		local manufacture=`/sbin/getcfg "Printers" "Manufacture$i"`
		local model=`/sbin/getcfg "Printers" "Model$i"`
/bin/cat > ${AVAHI_CONF_DIR}/printer.service <<__EOF__
<?xml version="1.0" standalone='no'?>
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
<name replace-wildcards="yes">%h(Airprint)</name>
<service>
<type>_ipp._tcp</type>
<subtype>_universal._sub._ipp._tcp</subtype>
<port>$printer_port</port>
<txt-record>txtvers=1</txt-record>
<txt-record>qtotal=1</txt-record>
<txt-record>priority=0</txt-record>
<txt-record>rp=printers/$printer_name</txt-record>
<txt-record>ty=$manufacture $model</txt-record>
<txt-record>note=$manufacture $model</txt-record>
<txt-record>product=(GPL Ghostscript)</txt-record>
<txt-record>pdl=application/pdf,application/postscript,application/vnd.cups-raster,application/octet-stream,image/png</txt-record>
<txt-record>URF=none</txt-record>
</service>
</service-group>

__EOF__
	fi
done

}

dbus_start()
{
	if [ -L /var/run/dbus ] || [ -d ${AVAHI_SRC}/var/run/dbus ]; then
		/bin/rm -rf /var/run/dbus
		/bin/rm -rf ${AVAHI_SRC}/var/run/dbus
	fi

	DBUS_PID=`/bin/ps | /bin/grep dbus | /bin/grep -v grep | /bin/awk '{print $1}'`

	if [ ! -z "$DBUS_PID" ]; then
		/bin/echo "Dbus is still running ... restart dbus ..."
		kill -9 $DBUS_PID
	fi

	/bin/mkdir -p ${AVAHI_SRC}/var/run/dbus
	/bin/ln -sf ${AVAHI_SRC}/var/run/dbus /var/run/dbus

	/bin/echo -n "Starting Dbus ... "
	${DBUS_DAEMON} --system
	/bin/sleep 1
	/bin/kill -HUP `/bin/cat /var/run/dbus/pid`
	/bin/sleep 1
	/bin/echo "Dbus Started"
}

dbus_stop()
{
	DBUS_PID=`/bin/ps | /bin/grep dbus | /bin/grep -v grep | /bin/awk '{print $1}'`

	if [ -z "$DBUS_PID" ]; then
		/bin/echo "Dbus is not running ..."
	else
		/bin/echo -n "Stopping Dbus ... "
		/bin/kill -9 $DBUS_PID
		/bin/sleep 1
		/bin/echo "Stopped."
	fi

	/bin/rm -rf /var/run/dbus
	/bin/rm -rf $AVAHI_SRC/var/run/dbus
}


PUBLIC_SHARE=`/sbin/getcfg SHARE_DEF defPublic -d Public -f /etc/config/def_share.info`

#Determine BASE installation location according to smb.conf
BASE=
publicdir=`/sbin/getcfg $PUBLIC_SHARE path -f /etc/config/smb.conf`
if [ ! -z $publicdir ] && [ -d $publicdir ];then
	publicdirp1=`/bin/echo $publicdir | /bin/cut -d "/" -f 2`
	publicdirp2=`/bin/echo $publicdir | /bin/cut -d "/" -f 3`
	publicdirp3=`/bin/echo $publicdir | /bin/cut -d "/" -f 4`
	if [ ! -z $publicdirp1 ] && [ ! -z $publicdirp2 ] && [ ! -z $publicdirp3 ]; then
		[ -d "/${publicdirp1}/${publicdirp2}/${PUBLIC_SHARE}" ] && BASE="/${publicdirp1}/${publicdirp2}"
	fi
fi

#Determine BASE installation location by checking where the Public folder is.
if [ -z $BASE ]; then
	for datadirtest in /share/HDA_DATA /share/HDB_DATA /share/HDC_DATA /share/HDD_DATA /share/MD0_DATA; do
		[ -d $datadirtest/$PUBLIC_SHARE ] && BASE="/${publicdirp1}/${publicdirp2}"
	done
fi

if [ -z $BASE ] ; then
	echo "The Public share not found."
	exit 1
fi


case "$1" in
	start)
		if [ -f /tmp/avahi0630.lock ]; then
			/bin/kill -INT `/bin/ps | /bin/grep avahi-daemon | /bin/grep -v grep | /bin/grep running | /bin/awk '{print $1}'` 2>/dev/null 1>/dev/null
			/bin/rm -rf /tmp/avahi0630.lock
		fi
		
		if [ -f /etc/config/timemachine.conf ]; then
			cp /etc/config/timemachine.conf /etc/config/timemachine.conf-pre_avahi
		fi
		[ -f ${AVAHI_DAEMON} ] || exit 1
		#/etc/init.d/bonjour.sh stop
		/bin/echo "Try to shutting down AVAHI services first"
		/bin/kill -INT `/bin/ps | /bin/grep avahi-daemon | /bin/grep -v grep | /bin/grep running | /bin/awk '{print $1}'` 2>/dev/null 1>/dev/null
		/bin/rm -rf /var/run/avahi-daemon
		/bin/rm -rf ${AVAHI_SRC}/var/run/avahi-daemon
		mkdir -p ${AVAHI_SRC}/var/run/avahi-daemon
		ln -sf ${AVAHI_SRC}/var/run/avahi-daemon /var/run/avahi-daemon

		/etc/init.d/timemachine.sh stop
		/bin/sleep 3

		# create the lock file 
		touch /tmp/avahi0630.lock
		
		# starting Dbus
		dbus_start
		
		/sbin/ldconfig
		
		#starting AVAHI
		set_avahi_conf

		if [ "x"`/sbin/getcfg TimeMachine Enabled -d FALSE` = "xTRUE" ]; then
			/etc/init.d/timemachine.sh start
		fi
		/bin/sleep 2

		#${AVAHI_DAEMON} --debug
		${AVAHI_DAEMON} -D
		#/bin/sleep 3
		#CURRENT_AVAHI_PID=`/bin/ps | /bin/grep avahi-daemon | /bin/grep -v grep | /bin/grep running | /bin/awk '{print $1}'`
		#if [ -z "$CURRENT_AVAHI_PID" ] ; then
			#/bin/echo "Error: Avahi failed to start."
			#/sbin/write_log "Error: Avahi failed to start." 1
		#fi
		;;
	stop)
		/bin/echo "Stop Avahi"
		if [ ! -f /tmp/avahi0630.lock ]; then
			/bin/echo "Avahi hasn't been enabled or started ..."
			exit 0
		fi

		/bin/echo -n "Shutting down services: "
		/bin/kill -INT `/bin/ps | /bin/grep avahi-daemon | /bin/grep -v grep | /bin/grep running | /bin/awk '{print $1}'` 2>/dev/null 1>/dev/null

		/bin/rm -rf /var/run/avahi-daemon/pid
		/bin/rm -rf /var/run/avahi-daemon/socket
		/etc/init.d/timemachine.sh stop

		#Stopping Dbus
		dbus_stop
		
		/bin/sleep 1
		
		/bin/rm -rf /etc/config/timemachine.conf-pre_avahi
		
		/bin/rm -rf /tmp/avahi0630.lock
		;;
	restart)
		$0 stop
		$0 start
		;;
	reload)
		CURRENT_AVAHI_PID=`/bin/ps | /bin/grep avahi-daemon | /bin/grep "local]" | /bin/awk '{print $1}'`
		if [ ! -z "$CURRENT_AVAHI_PID" ]; then
			if [ -f /var/lock/avahi_set_conf.lock ]; then
				exit 0;
			fi
			touch /var/lock/avahi_set_conf.lock
			set_avahi_conf
			/bin/kill -HUP $CURRENT_AVAHI_PID
			rm /var/lock/avahi_set_conf.lock
		fi
		;;
	*)
		/bin/echo "Usage: $0 {start|stop|restart}"
		exit 1
esac

exit 0
