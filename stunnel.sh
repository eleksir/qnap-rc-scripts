#!/bin/sh
# For Administration
# default port: 6000

STUNNEL_CONF_DIR=/etc/config/stunnel
STUNNEL_CONF_DEFAULT=/etc/default_config/stunnel
STUNNEL_KEY=/etc/config/stunnel/backup.key
STUNNEL_CERT=/etc/config/stunnel/backup.cert
STUNNEL_KEY_DEFAULT=/etc/default_config/stunnel/backup.key
STUNNEL_CERT_DEFAULT=/etc/default_config/stunnel/backup.cert
STUNNEL_PEM=/etc/config/stunnel/stunnel.pem
STUNNEL_PEM_DEFAULT=/etc/default_config/stunnel/stunnel.pem
STUNNEL_DOWNLOAD_PATH=/home/httpd
DOWNLOAD_PATH=/home/httpd/cgi-bin/download
STUNNEL_CONF=/etc/stunnel/stunnel.conf

test -f /usr/sbin/stunnel || exit 0
project_name=`/sbin/getcfg Project Name -d QNAP -f /var/default`

prepare_cert_key()
{
	if [ ! -f $STUNNEL_CERT ] && [ -f $STUNNEL_CERT_DEFAULT ]; then
		/bin/cp $STUNNEL_CERT_DEFAULT $STUNNEL_CERT
	fi
	
	if [ ! -f $STUNNEL_KEY ] && [ -f $STUNNEL_KEY_DEFAULT ]; then
		/bin/cp $STUNNEL_KEY_DEFAULT $STUNNEL_KEY
	fi
	
	if [ -f $STUNNEL_CERT ] && [ -f $STUNNEL_KEY ]; then
		#avaiable to download default SSL key
		[ x`/sbin/getcfg "SSL Import" "default certificate"` = xno ] || /sbin/setcfg "SSL Import" "default certificate" no
	fi
}

case "$1" in
    start)
	echo -n "Starting stunnel services: "
	STUNNEL_PORT=`/sbin/getcfg "Stunnel" "Port" -d "443"`
	WFM_ENABLE_DEFINED_PORT=`/sbin/getcfg "WebFS" "Enable_Defined_Port" -d "FALSE"`
	WFM_DEFINED_PORT=`/sbin/getcfg "WebFS" "Port" -d "8008"`
	WFM_DEFINED_SSL_PORT=`/sbin/getcfg "WebFS" "SSL_Port" -d "8090"`
	
	prepare_cert_key
	
	[ `/sbin/getcfg "Stunnel" "Enable" -d "1"` = "0" ] && exit 1
	
	if [ ! -d ${STUNNEL_CONF_DIR} ]; then
		[ ! -f ${STUNNEL_CONF_DIR} ] || /bin/rm ${STUNNEL_CONF_DIR}
		/bin/mkdir ${STUNNEL_CONF_DIR}
		[ -d ${STUNNEL_CONF_DEFAULT} ] && /bin/cp ${STUNNEL_CONF_DEFAULT}/* ${STUNNEL_CONF_DIR}
	else
		if [ `/sbin/getcfg "SSL Import" "default certificate" -d "yes"` = "yes" ]; then
			/bin/cmp ${STUNNEL_PEM_DEFAULT} ${STUNNEL_PEM} 1>/dev/null 2>/dev/null
			if [ $? != 0 ]; then
				[ -f ${STUNNEL_PEM_DEFAULT} ] && /bin/cp -f ${STUNNEL_PEM_DEFAULT} ${STUNNEL_PEM}
			fi
		fi
	fi
	
	/bin/grep "Some security enhancements" ${STUNNEL_CONF} 1>>/dev/null 2>>/dev/null
	[ $? = 0 ] && /bin/sed -i 's/^.*Some security enhancements.*$/ciphers = ALL:!SSLv2:!LOW:!EXPORT40:@STRENGTH/' ${STUNNEL_CONF}
	/bin/sed -i 's/^ciphers.*$/ciphers = ALL:!SSLv2:!LOW:!EXPORT40:@STRENGTH/' ${STUNNEL_CONF}
	if [ `/sbin/getcfg "Network" "IPV6" -u -d "FALSE"` = "TRUE" ]; then
		/sbin/setcfg "https" "accept" ":::${STUNNEL_PORT}" -f "${STUNNEL_CONF}"
		if [ $WFM_ENABLE_DEFINED_PORT = "TRUE" ]; then
			/sbin/setcfg "wfm" "accept" ":::${WFM_DEFINED_SSL_PORT}" -f "${STUNNEL_CONF}"
		fi
	else
		/sbin/setcfg "https" "accept" "${STUNNEL_PORT}" -f "${STUNNEL_CONF}"
		if [ $WFM_ENABLE_DEFINED_PORT = "TRUE" ]; then
			/sbin/setcfg "wfm" "accept" "${WFM_DEFINED_SSL_PORT}" -f "${STUNNEL_CONF}"
		fi
	fi
	/sbin/setcfg "https" "TIMEOUTclose" "0" -f "${STUNNEL_CONF}"
	/sbin/setcfg "https" "TIMEOUTconnect" "2" -f "${STUNNEL_CONF}"
		
	if [ $WFM_ENABLE_DEFINED_PORT = "TRUE" ]; then
		/sbin/setcfg "wfm" "connect" "127.0.0.1:${WFM_DEFINED_PORT}" -f "${STUNNEL_CONF}"
		/sbin/setcfg "wfm" "TIMEOUTclose" "0" -f "${STUNNEL_CONF}"
		/sbin/setcfg "wfm" "TIMEOUTconnect" "2" -f "${STUNNEL_CONF}"
	else
		/sbin/rmcfg "wfm" -f "${STUNNEL_CONF}" 2>/dev/null
	fi
	/sbin/setcfg "" debug 0 -f "${STUNNEL_CONF}"
	/bin/touch /var/qfunc/stunnel.enable
	/sbin/daemon_mgr stunnel start \
		"/usr/sbin/stunnel ${STUNNEL_CONF} 2>/dev/null 1>/dev/null"
	echo -n " stunnel"
	echo "."
	;;
    stop)
	echo -n "Shutting down stunnel services:" 
	/sbin/daemon_mgr stunnel stop /usr/sbin/stunnel
	/bin/kill -9 `pidof stunnel`
	/bin/rm -f /var/qfunc/stunnel.enable
	echo -n " stunnel"
	echo "."
	;;
    restart)
	$0 stop
	/bin/sleep 2
	$0 start
	;;	
    prepare_cert_key)
	prepare_cert_key
	;;	
    *)
        echo "Usage: /etc/init.d/stunnel {start|stop|restart|prepare_cert_key}"
        exit 1
esac

exit 0
