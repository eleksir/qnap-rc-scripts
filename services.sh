#!/bin/sh

NSS_ENABLED=`/sbin/getcfg NVR "NSS Enabled" -d no`

SERVICES="recycled.sh mysqld.sh atalk.sh ftp.sh bt_scheduler.sh btd.sh StartMediaService.sh ImRd.sh init_iTune.sh twonkymedia.sh Qthttpd.sh crond.sh nfs smb.sh lunportman.sh iscsitrgt.sh nvrd.sh snmp rsyslog.sh qsyncman.sh iso_mount.sh init_mozyclient.sh antivirus.sh ldap_server.sh vpn_pptp.sh vpn_openvpn.sh cloud3p.sh"
SERVICES_STOP="cloud3p.sh vpn_openvpn.sh vpn_pptp.sh ldap_server.sh antivirus.sh init_mozyclient.sh iso_mount.sh qsyncman.sh rsyslog.sh snmp nvrd.sh lunportman.sh iscsitrgt.sh smb.sh nfs crond.sh Qthttpd.sh twonkymedia.sh init_iTune.sh ImRd.sh StartMediaService.sh bt_scheduler.sh btd.sh ftp.sh atalk.sh mysqld.sh recycled.sh"

Other_Services()
{
	if [ -z "$1" ] || ([ "$1" != start ] && [ "$1" != stop ]); then
		return
	fi
	#service list
	[ -f /etc/init.d/init_symform.sh ] && /etc/init.d/init_symform.sh $1
}

case "$1" in
    start)
	echo -n "Start services: "
	[ ! -f "/tmp/quick_format.flag" ] || exit 0
	for i in $SERVICES
	do
		if [ -f /etc/init.d/$i ]; then
			echo -n "$i "
			if [ "x$i" = "xnvrd.sh" ]; then
#echo "`date`" "begin service.sh start $i" >> /tmp/nvrd_dbg
				if [ "x${NSS_ENABLED}" = "xyes" ]; then
					if [ "`/etc/init.d/nvrd.sh started`" == "1" ]; then
						/etc/init.d/$i _start 2>/dev/null 1>/dev/null
					fi
				else
					/etc/init.d/$i start 2>/dev/null 1>/dev/null
				fi
#echo "`date`" "begin service.sh start $i" >> /tmp/nvrd_dbg
			else
				/etc/init.d/$i start 2>/dev/null 1>/dev/null
			fi
		fi
	done
	echo .
	$0 qpkg_start
	Other_Services start
	;;
    stop)
	Other_Services stop
	$0 qpkg_stop
	echo -n "Stop service: "
	for i in $SERVICES_STOP
	do
		if [ -f /etc/init.d/$i ]; then
			echo -n "$i "
			if [ "x$i" = "xnvrd.sh" ]; then
#echo "`date`" "begin service.sh stop $i" >> /tmp/nvrd_dbg
				if [ "x${NSS_ENABLED}" = "xyes" ]; then
					if [ "`/etc/init.d/nvrd.sh started`" == "1" ]; then
						/etc/init.d/$i _stop 2>/dev/null 1>/dev/null
					fi
				else
					/etc/init.d/$i stop 2>/dev/null 1>/dev/null
				fi
#echo "`date`" "end service.sh stop $i" >> /tmp/nvrd_dbg
			else
				/etc/init.d/$i stop 2>/dev/null 1>/dev/null
			fi
		fi
	done
	echo .
	. /etc/init.d/check_nss2
	if [ ! -f "/tmp/quick_format.flag" ] && [ "x$_NSS2_SUPPORT" = "xyes" ]; then 
		/etc/init.d/thttpd.sh restart 2>/dev/null 1>/dev/null
	fi
	;;

    qpkg_start)
	echo -n "Start qpkg service: "
	for i in /etc/rcS.d/QS??* ;do
		[ -f "$i" ] || continue
		case "$i" in
			*.sh)
				# Source shell script for speed.
				(
					trap - INT QUIT TSTP
					set start
					. $i
				)
				;;
			*)
				# No sh extension, so fork subprocess.
				[ -f $i ] && $i start
				;;
		esac
	done
	echo .
	;;
    qpkg_stop)
	echo -n "Stop qpkg service: "
	for i in /etc/rcK.d/QK??* ;do
		[ -f "$i" ] || continue
		case "$i" in
			*.sh)
			# Source shell script for speed.
			(
				trap - INT QUIT TSTP
				set stop
				. $i
			)
			;;
		*)
			# No sh extension, so fork subprocess.
			[ -f $i ] && $i stop
			;;
		esac
	done
	echo .
	;;
    restart)
	$0 stop
	$0 start
	;;	
    *)
        echo "Usage: /etc/init.d/services.sh {start|stop|restart}"
        exit 1
esac

exit 0
