#!/bin/sh

MNT_POINT="/mnt/ext"
ROOT_PART="/mnt/HDA_ROOT"
UPDATEPKG_DIR="${ROOT_PART}/update_pkg"
PHP_INI="/etc/config/php.ini"
APP_RUNTIME_CONF="/var/.application.conf"
QPKG_CONF="/etc/config/qpkg.conf"
DEF_QPKG_CONF="/etc/default_config/qpkg.conf"
MultimediaShare=`/sbin/getcfg SHARE_DEF defMultimedia -d Qmultimedia -f /etc/config/def_share.info`
MSV2_SUPPORT=`/sbin/getcfg "QPHOTO" "MSV2_SUPPORT" -d FALSE -f /etc/default_config/uLinux.conf`
ThumbnailGenerator="/mnt/ext/opt/MSV2/api/background_conversion.php"
RUNTIME_ALARM_INFO="/tmp/.alarm_info.conf"
BACKUP_ALARM_INFO="/share/${MultimediaShare}/.@__thumb/.alarm_info.conf"
UPNP_ROOT="/mnt/QUPNP"
BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf 2>/dev/null`
QPHOTO_Hide=`/sbin/getcfg DISABLE qmultimedia -d 0 -f $APP_RUNTIME_CONF`
MS_Hide=`/sbin/getcfg DISABLE musics -d 0 -f $APP_RUNTIME_CONF`
PS_Hide=`/sbin/getcfg DISABLE photos -d 0 -f $APP_RUNTIME_CONF`
MMSHARE_PATH=`/sbin/getcfg ${MultimediaShare} path -d /ERROR -f /etc/config/smb.conf`
DEMO_SITE_SUPPORT=`/sbin/getcfg DEMO_SITE_SUPPORT Enable -u -d FALSE -f $APP_RUNTIME_CONF`
MEDIALIB_DBCMD="/usr/local/medialibrary/bin/mymediadbcmd"
MEDIALIB_CONF="/etc/config/medialibrary.conf"
PHOTO_SCAND=photo_scand
. /etc/init.d/functions

make_qpkg_base(){
FindDefVol
_ret=$?
if [ $_ret != 0 ]; then
	echo "Default volume not found."
	[ "x$1" = "xstop" ] || exit 1
fi
QPKG_ROOT="${DEF_VOLMP}/.qpkg"
[ -d "${QPKG_ROOT}" ] || /bin/mkdir -p "${QPKG_ROOT}"
/bin/chmod 777 "${QPKG_ROOT}"
}

function check_audio_dev()
{
	for (( i=0; i<5; i=i+1 ))
	do
		if [ -f /proc/asound/card${i}/pcm0p/sub0/info ]; then
			/bin/grep PLAYBACK /proc/asound/card${i}/pcm0p/sub0/info 1>>/dev/null 2>>/dev/null
			if [ $? -eq 0 ]; then
				/bin/ln -sf audio${i} /dev/audio
				/bin/ln -sf mixer${i} /dev/mixer
				/bin/ln -sf dsp${i} /dev/dsp
				break;
			fi
		fi
	done
}

function set_def_qpkg_info()
{
	[ "x$1" != x ] || return 1
	MY_QPKG_Name=`/sbin/getcfg "$1" Name -u -d X -f $QPKG_CONF`
	if [ xX != x"$MY_QPKG_Name" ]; then
		if [ x`/sbin/getcfg "$1" Enable -u -d TRUE -f $QPKG_CONF` = xFALSE ]; then
			MY_QPKG_Enable="FALSE"
		else
			MY_QPKG_Enable="TRUE"
		fi
	else
		MY_QPKG_Enable="`/sbin/getcfg "$1" Enable -u -d FALSE -f $DEF_QPKG_CONF`"
	fi
	if [ -f $DEF_QPKG_CONF ] && [ xX != x`/sbin/getcfg "$1" Name -u -d X -f $DEF_QPKG_CONF` ]; then
		[ -f $QPKG_CONF ] || /bin/touch $QPKG_CONF
		/sbin/setcfg -f $QPKG_CONF "$1" Name "`/sbin/getcfg -f $DEF_QPKG_CONF "$1" Name`"
		/sbin/setcfg -f $QPKG_CONF "$1" Version "`/sbin/getcfg -f $DEF_QPKG_CONF "$1" Version`"
		if [ "$1" = "MusicStation" ] && [ "x$MS_Hide" = "x1" ]; then
			/sbin/setcfg -f $QPKG_CONF "$1" Enable FALSE
		elif [ "$1" = "PhotoStation" ] && [ "x$PS_Hide" = "x1" ]; then
			/sbin/setcfg -f $QPKG_CONF "$1" Enable FALSE
		else
			/sbin/setcfg -f $QPKG_CONF "$1" Enable "$MY_QPKG_Enable"
		fi
		/sbin/setcfg -f $QPKG_CONF "$1" QPKG_File "`/sbin/getcfg -f $DEF_QPKG_CONF "$1" QPKG_File`"
		/sbin/setcfg -f $QPKG_CONF "$1" Date "`/sbin/getcfg -f $DEF_QPKG_CONF "$1" Date`"
		/sbin/setcfg -f $QPKG_CONF "$1" Shell "`/sbin/getcfg -f $DEF_QPKG_CONF "$1" Shell`"
		/sbin/setcfg -f $QPKG_CONF "$1" Install_Path "`/sbin/getcfg -f $DEF_QPKG_CONF "$1" Install_Path`"
		/sbin/setcfg -f $QPKG_CONF "$1" WebUI "`/sbin/getcfg -f $DEF_QPKG_CONF "$1" WebUI`"
		/sbin/setcfg -f $QPKG_CONF "$1" Author "`/sbin/getcfg -f $DEF_QPKG_CONF "$1" Author`"
		/sbin/setcfg -f $QPKG_CONF "$1" Sys_App "`/sbin/getcfg -f $DEF_QPKG_CONF "$1" Sys_App -d 0`"
	fi
	/bin/touch "/tmp/.${1}.updated"
	return 0
}

function mmscan_schedule()
{
	BG_HOUR=`/sbin/getcfg "QPHOTO" "BG_HOUR" -d 3`
	BG_MINUTE=`/sbin/getcfg "QPHOTO" "BG_MINUTE" -u -d 0`
	/bin/grep ImR_all  /etc/config/crontab 1>>/dev/null 2>>/dev/null
	ret=$?
	if [ $ret -eq 0 ]; then
		/bin/sed -i '/ImR_all/d' /etc/config/crontab
	fi
	/bin/grep "ImRd" /etc/config/crontab 1>>/dev/null 2>>/dev/null
	if [ $? -ne 0 ]; then
		if [ $BG_HOUR -ge 0 ]; then
			/bin/echo "$BG_MINUTE $BG_HOUR * * * /etc/init.d/ImRd.sh bgThGen" >> /etc/config/crontab
		fi
	else
		if [ $BG_HOUR -ge 0 ]; then
			/bin/sed -i 's/^.*.ImRd.*$/'"$BG_MINUTE $BG_HOUR * * * \/etc\/init.d\/ImRd.sh bgThGen"'/g' /etc/config/crontab
		else
			/bin/sed -i '/ImRd.sh/d' /etc/config/crontab
		fi
	fi
	/bin/pidof crond 1>>/dev/null 2>>/dev/null
	[ $? -ne 0 ] || /usr/bin/crontab /etc/config/crontab
}

function enable_medialib()
{
	if [ "x$1" = "xstop" ]; then
		/etc/init.d/StartMediaService.sh stop 1>/dev/null 2>/dev/null
		return 0
	fi
	/bin/pidof mymediadbserver >/dev/null 2>&1
	if [ $? != 0 ] && [ -x /etc/init.d/StartMediaService.sh ]; then
		if [ ! -f $PACKAGE_PATH/bin/mymediadbserver ] && [ -f /mnt/HDA_ROOT/update_pkg/MusicStation/medialibrary.tgz ]; then
			/bin/rm -rf ${MNT_POINT}/opt/medialibrary 2>/dev/null
			/bin/tar zxf /mnt/HDA_ROOT/update_pkg/MusicStation/medialibrary.tgz -C ${MNT_POINT}/opt/ 2>/dev/null
			/bin/rm -rf /usr/local/medialibrary 2>/dev/null
			/bin/ln -sf ${MNT_POINT}/opt/medialibrary /usr/local/medialibrary
		fi
		/etc/init.d/StartMediaService.sh start 1>/dev/null 2>/dev/null
	fi
	ln -sf ../init.d/StartMediaService.sh /etc/rcK.d/K03MediaService

}

function musicstation()
{
	MS_QPKG_Ver=`/sbin/getcfg MusicStation Version -d 2.0 -f $QPKG_CONF`
	UseSysAccount=`/sbin/getcfg MUSICSTATION UseSysAccount -u -d x -f /etc/config/uLinux.conf`
	if [ "$MS_QPKG_Ver" \< "2.1" ] && [ ! -f ${UPDATEPKG_DIR}/musicstation.tgz ]; then
		/sbin/rmcfg MusicStation -f $QPKG_CONF 2>/dev/null
		/sbin/setcfg MUSICSTATION Enable FALSE
		[ $1 != "start" ] || return 1
	fi
	MS_Enable=`/sbin/getcfg MUSICSTATION Enable -u -d TRUE`
	if [ "x$MS_Hide" = "x1" ] && [ x"$MS_Enable" = x"TRUE" ]; then
		/sbin/setcfg MUSICSTATION Enable FALSE
		MS_Enable="FALSE"
	fi
	MS_QPKG=`/sbin/getcfg MusicStation QPKG -u -d FALSE -f $APP_RUNTIME_CONF`
	MS_QPKG_Name=`/sbin/getcfg MusicStation Name -u -d X -f $QPKG_CONF`
	if [ ! -f ${UPDATEPKG_DIR}/.MusicStation_removed ] && [ x"$MS_QPKG" = xTRUE ] && [ x"$MS_QPKG_Name" = xX ]; then
		set_def_qpkg_info MusicStation
	elif [ ! -f ${UPDATEPKG_DIR}/.MusicStation_removed ] && [ ! -f "/tmp/.MusicStation.updated" ] && [ ! -d ${UPDATEPKG_DIR}/MusicStation ] && [ x"$MS_QPKG" = xTRUE ]; then
		set_def_qpkg_info MusicStation
	fi
	if [ x"$MS_QPKG" = xTRUE ] && [ x"$MS_QPKG_Name" != xX ]; then
		#use QPKG
		MS_QPKG_Enable=`/sbin/getcfg MusicStation Enable -u -d FALSE -f $QPKG_CONF`
		if [ "x$MS_Hide" = "x1" ] && [ x"$MS_QPKG_Enable" = x"TRUE" ]; then
			[ -f ${UPDATEPKG_DIR}/.MusicStation_removed ] || /sbin/setcfg MusicStation Enable FALSE -f $QPKG_CONF
			MS_QPKG_Enable="FALSE"
		fi
		if [ ! -z "$MS_QPKG_Enable" ] && [ x"$MS_Enable" != x"$MS_QPKG_Enable" ]; then
			MS_Enable=$MS_QPKG_Enable
			/sbin/setcfg MUSICSTATION Enable "$MS_QPKG_Enable"
		fi
	fi
	if [ $1 = "start" ]; then
		if [ x"$MS_QPKG" = xTRUE ] && [ -f ${UPDATEPKG_DIR}/.MusicStation_removed ]; then
			[ ! -f ${UPDATEPKG_DIR}/musicstation.tgz ] || /bin/rm -f ${UPDATEPKG_DIR}/musicstation.tgz
			return 1
		fi
		if [ -f ${UPDATEPKG_DIR}/musicstation.tgz ]; then
			/bin/tar zxf ${UPDATEPKG_DIR}/musicstation.tgz -C "${QPKG_ROOT}"
			/bin/rm -rf ${MNT_POINT}/opt/musicstation 2>/dev/null
			/bin/ln -sf "${QPKG_ROOT}/musicstation" ${MNT_POINT}/opt/
		fi
		if [ ! -f ${UPDATEPKG_DIR}/musicstation.tgz ] && [ ! -d /mnt/ext/opt/musicstation/api ]; then
			/sbin/rmcfg MusicStation -f $QPKG_CONF 2>/dev/null
			/sbin/setcfg MUSICSTATION Enable FALSE
			return 1
		fi
		/bin/sed -i 's/QRINFOVAL_USESYSACCOUNT/'"${UseSysAccount}"'/g' /mnt/ext/opt/musicstation/api/libs/inc_common.php
		if [ -f ${MNT_POINT}/opt/musicstation/api/radioclass.php ] && [ -d /mnt/ext/opt/MSV2/api ]; then
			/bin/cp ${MNT_POINT}/opt/musicstation/api/radioclass.php /mnt/ext/opt/MSV2/api/
		fi
		[ -d /share/${MultimediaShare}/.@__thumb ] || /bin/mkdir /share/${MultimediaShare}/.@__thumb
		[ -d /share/${MultimediaShare}/.@__thumb ] && /bin/chmod 777 /share/${MultimediaShare}/.@__thumb
		[ -d ${MNT_POINT}/opt/musicstation/sound ] && /bin/cp -af ${MNT_POINT}/opt/musicstation/sound /share/${MultimediaShare}/.@__thumb/
		[ -f /share/${MultimediaShare}/.@__thumb/MS.db ] && /bin/chmod 666 /share/${MultimediaShare}/.@__thumb/MS.db
		if [ $MS_Enable = TRUE ]; then
			enable_medialib start
			/sbin/setcfg MultimediaStation USBSound TRUE -f $APP_RUNTIME_CONF
			/sbin/setcfg MusicStation Enable TRUE -f $APP_RUNTIME_CONF
			[ -d $UPNP_ROOT ] || /bin/mkdir -m 777 $UPNP_ROOT
			/bin/grep "$UPNP_ROOT" /proc/mounts 1>/dev/null 2>/dev/null
			[ $? = 0 ] || _djmounted=FALSE
			if [ o${_djmounted} = oFALSE ]; then
				for i in `/bin/pidof djmount`
				do
					/bin/kill -9 $i
					sleep 1
				done
				if [ "x${BOOT_CONF}" = "xTS-NASARM" ]; then
					[ -f /usr/local/bin/djmount ] && /usr/local/bin/djmount -o "allow_other,max_item=256,iocharset=UTF-8" $UPNP_ROOT
				else
					[ -f /usr/local/bin/djmount ] && /usr/local/bin/djmount -o "allow_other,max_item=512,iocharset=UTF-8" $UPNP_ROOT
				fi
			fi
			[ -f /usr/local/sbin/speaker.sh ] || /bin/ln -sf ${MNT_POINT}/opt/musicstation/speaker.sh /usr/local/sbin/
			[ -f /usr/bin/aumix ] || /bin/ln -sf /usr/local/sbin/aumix /usr/bin/
			IMPORTFLAG=`/sbin/getcfg OPTIONS IMPORTFLAG -d 0 -f ${MEDIALIB_CONF}`
			if [ "$IMPORTFLAG" = "0" ] && [ -x /usr/local/apache/bin/php ] && [ -f /mnt/ext/opt/musicstation/api/playlist_upgrade.php ]; then
				/usr/local/apache/bin/php /mnt/ext/opt/musicstation/api/playlist_upgrade.php
				/sbin/setcfg OPTIONS IMPORTFLAG 1 -f ${MEDIALIB_CONF}
			fi
			[ x0 = x`/sbin/getcfg OPTIONS INOTIFY -d 0 -f ${MEDIALIB_CONF}` ] || ${MEDIALIB_DBCMD} inotifyStart 1>>/dev/null 2>>/dev/null
			if [ x1 = x`/sbin/getcfg OPTIONS DAILYBUILD -d 0 -f ${MEDIALIB_CONF}` ]; then
				${MEDIALIB_DBCMD} SchedulBuild `/sbin/getcfg OPTIONS DAILYBUILD_MM -d 0 -f ${MEDIALIB_CONF}` `/sbin/getcfg OPTIONS DAILYBUILD_HH -d 0 -f ${MEDIALIB_CONF}`
			fi
		else
			[ -d ${MNT_POINT}/opt/musicstation ] || /bin/mkdir ${MNT_POINT}/opt/musicstation
			#/bin/rm -rf ${MNT_POINT}/opt/musicstation/* 2>>/dev/null
			/bin/echo "The service is not enabled." > ${MNT_POINT}/opt/musicstation/index.php
			[ ! -f ${MNT_POINT}/opt/musicstation/error.htm ] || /bin/cp ${MNT_POINT}/opt/musicstation/error.htm ${MNT_POINT}/opt/musicstation/index.php
			enable_medialib stop
		fi
	fi
	if [ $1 = "stop" ]; then	
		/bin/umount "$UPNP_ROOT" 1>>/dev/null 2>>/dev/null
		sleep 1
		for i in `/bin/pidof djmount`
		do
			/bin/kill -9 $i
			sleep 1
		done
		/sbin/setcfg MultimediaStation USBSound TRUE -f $APP_RUNTIME_CONF
		/sbin/setcfg MusicStation Enable FALSE -f $APP_RUNTIME_CONF
		[ -d ${MNT_POINT}/opt/musicstation ] || /bin/mkdir ${MNT_POINT}/opt/musicstation
		if [ $MS_Enable = TRUE ]; then
			/bin/echo "The service is not activated." > ${MNT_POINT}/opt/musicstation/index.php
			[ ! -f ${MNT_POINT}/opt/musicstation/error.htm ] || /bin/cp ${MNT_POINT}/opt/musicstation/error.htm ${MNT_POINT}/opt/musicstation/index.php
		else
			/bin/echo "The service is not enabled." > ${MNT_POINT}/opt/musicstation/index.php
			[ ! -f ${MNT_POINT}/opt/musicstation/error.htm ] || /bin/cp ${MNT_POINT}/opt/musicstation/error.htm ${MNT_POINT}/opt/musicstation/index.php
		fi
		[ ! -f ${MNT_POINT}/opt/musicstation/api/libs/inc_common.php ] || /bin/rm -f ${MNT_POINT}/opt/musicstation/api/libs/inc_common.php
		[ ! -f ${MNT_POINT}/opt/musicstation/api/libs/inc_common.php.disabled ] || /bin/cp  ${MNT_POINT}/opt/musicstation/api/libs/inc_common.php.disabled ${MNT_POINT}/opt/musicstation/api/libs/inc_common.php
		#${MEDIALIB_DBCMD} DelSchedulBuild 1>>/dev/null 2>>/dev/null
		/bin/sed -i "/^.*mymediadbcmd.*buildall/d" /etc/config/crontab
		/bin/sed -i "/^.*mymediadbcmd.*stopbuild/d" /etc/config/crontab
		/bin/pidof crond 1>>/dev/null 2>>/dev/null
		[ $? -ne 0 ] || /usr/bin/crontab /etc/config/crontab
		${MEDIALIB_DBCMD} inotifyStop 1>>/dev/null 2>>/dev/null
		${MEDIALIB_DBCMD} stopbuildall 1>>/dev/null 2>>/dev/null
		enable_medialib stop
	fi
}

function photo_scan()
{
	case "$1" in
	start)
		/bin/kill `pidof photo_scand.sh` 2>/dev/null
		/etc/init.d/photo_scand.sh 2>/dev/null &
		;;
	stop)
		/sbin/daemon_mgr $PHOTO_SCAND stop "/sbin/photo_scand"
		/bin/kill `pidof photo_scand.sh` 2>/dev/null
		sleep 1
		;;
	*)
		echo "do nothing"
		;;
	esac
	
}

function photostation2()
{
	PS_Enable=`/sbin/getcfg PHOTOSTATION Enable -u -d TRUE`
	UseSysAccount=`/sbin/getcfg PHOTOSTATION UseSysAccount -u -d x -f /etc/config/uLinux.conf`
	if [ "x$PS_Hide" = "x1" ] && [ x"$PS_Enable" = x"TRUE" ]; then
		/sbin/setcfg PHOTOSTATION Enable FALSE
		PS_Enable="FALSE"
	fi
	PS_QPKG=`/sbin/getcfg PhotoStation QPKG -u -d FALSE -f $APP_RUNTIME_CONF`
	PS_QPKG_Name=`/sbin/getcfg PhotoStation Name -u -d X -f $QPKG_CONF`
	if [ ! -f ${UPDATEPKG_DIR}/.PhotoStation_removed ] && [ x"$PS_QPKG" = xTRUE ] && [ x"$PS_QPKG_Name" = xX ]; then
		set_def_qpkg_info PhotoStation
	elif [ ! -f ${UPDATEPKG_DIR}/.PhotoStation_removed ] && [ ! -f "/tmp/.PhotoStation.updated" ] && [ ! -d ${UPDATEPKG_DIR}/PhotoStation ] && [ x"$PS_QPKG" = xTRUE ]; then
		set_def_qpkg_info PhotoStation
	fi
	if [ x"$PS_QPKG" = xTRUE ] && [ x"$PS_QPKG_Name" != xX ]; then
		#use QPKG
		PS_QPKG_Enable=`/sbin/getcfg PhotoStation Enable -u -d FALSE -f $QPKG_CONF`
		if [ "x$PS_Hide" = "x1" ] && [ x"$PS_QPKG_Enable" = x"TRUE" ]; then
			[ -f ${UPDATEPKG_DIR}/.PhotoStation_removed ] || /sbin/setcfg PhotoStation Enable FALSE -f $QPKG_CONF
			PS_QPKG_Enable="FALSE"
		fi
		if [ ! -z "$PS_QPKG_Enable" ] && [ x"$PS_Enable" != x"$PS_QPKG_Enable" ]; then
			PS_Enable=$PS_QPKG_Enable
			/sbin/setcfg PHOTOSTATION Enable "$PS_QPKG_Enable"
		fi
	fi
	if [ $1 = "start" ]; then
		if [ ! -d "${MMSHARE_PATH}" ]; then
			echo "${MultimediaShare} directory is not ready."
			exit 1
		fi
		if [ x"$PS_QPKG" = xTRUE ] && [ -f ${UPDATEPKG_DIR}/.PhotoStation_removed ]; then
			[ ! -f ${UPDATEPKG_DIR}/photostation2.tgz ] || /bin/rm -f ${UPDATEPKG_DIR}/photostation2.tgz
			return 1
		fi
		if [ -f ${UPDATEPKG_DIR}/photostation2.tgz ]; then
			/bin/tar zxf ${UPDATEPKG_DIR}/photostation2.tgz -C "${QPKG_ROOT}" 2>>/dev/null
			/bin/rm -rf ${MNT_POINT}/opt/photostation2 2>/dev/null
			/bin/ln -sf "${QPKG_ROOT}/photostation2" ${MNT_POINT}/opt/
		fi
		if [ ! -f ${UPDATEPKG_DIR}/photostation2.tgz ] && [ ! -d /mnt/ext/opt/photostation2/api ]; then
			/sbin/rmcfg PhotoStation -f $QPKG_CONF 2>/dev/null
			/sbin/setcfg PHOTOSTATION Enable FALSE
			return 1
		fi
		[ -d ${MNT_POINT}/opt/photostation2/Samples ] || /bin/ln -sf ${MNT_POINT}/opt/MSV2/Samples ${MNT_POINT}/opt/photostation2/Samples
		[ ! -f /mnt/ext/opt/photostation2/api/fileclass.php ] || /bin/cp /mnt/ext/opt/photostation2/api/fileclass.php /mnt/ext/opt/MSV2/api/fileclass.php
		if [ -d /share/${MultimediaShare}/.@__thumb ]; then
			if [ ! -f "/share/${MultimediaShare}/.@__thumb/PS_data.db" ]; then
				/bin/cp /mnt/ext/opt/photostation2/api/libs/PS_data_basic.db "/share/${MultimediaShare}/.@__thumb/PS_data.db"
				chown httpdusr "/share/${MultimediaShare}/.@__thumb/PS_data.db"
			fi
			chmod 666 "/share/${MultimediaShare}/.@__thumb/PS_data.db"
			[ ! -f "/share/${MultimediaShare}/.@__thumb/MS_data.db" ] || chmod 666 "/share/${MultimediaShare}/.@__thumb/MS_data.db"
		fi
		/bin/sed -i 's/QINFOVAL_PLATFORM/'"${BOOT_CONF}"'/g' ${MNT_POINT}/opt/photostation2/api/libs/inc_common.php
		/bin/sed -i 's/QINFOVAL_DEMO_SITE/'"${DEMO_SITE_SUPPORT}"'/g' ${MNT_POINT}/opt/photostation2/api/libs/inc_common.php
		FFmpegExtra=`/sbin/getcfg MultimediaStation FFmpegExtra -u -d FALSE -f $APP_RUNTIME_CONF`
		/bin/sed -i 's/QINFOVAL_MS_EXTRA/'"${FFmpegExtra}"'/g' ${MNT_POINT}/opt/photostation2/api/libs/inc_common.php
		/bin/sed -i 's/QINFOVAL_MS_FILE_ROOT/'"\/share\/${MultimediaShare}"'/g' ${MNT_POINT}/opt/photostation2/api/libs/inc_common.php
		/bin/sed -i 's/QRINFOVAL_USESYSACCOUNT/'"${UseSysAccount}"'/g' /mnt/ext/opt/photostation2/api/libs/inc_common.php
		if [ $PS_Enable != TRUE ]; then
			[ -d ${MNT_POINT}/opt/photostation2 ] || /bin/mkdir ${MNT_POINT}/opt/photostation2
			/bin/echo "Photo Station is not enabled." > ${MNT_POINT}/opt/photostation2/index.php
			[ ! -f ${MNT_POINT}/opt/photostation2/disabled_page.html ] || /bin/cp ${MNT_POINT}/opt/photostation2/disabled_page.html ${MNT_POINT}/opt/photostation2/index.php
		else
			photo_scan start
			/sbin/setcfg PhotoStation Enable TRUE -f $APP_RUNTIME_CONF
		fi
		[ -f /tmp/geoip.xml ] && /bin/ln -sf /tmp/geoip.xml ${MNT_POINT}/opt/photostation2/geoip.xml
	fi
	if [ $1 = "stop" ]; then
		photo_scan stop
		/sbin/setcfg PhotoStation Enable FALSE -f $APP_RUNTIME_CONF
		[ -d ${MNT_POINT}/opt/photostation2 ] || /bin/mkdir ${MNT_POINT}/opt/photostation2
		if [ $PS_Enable = TRUE ]; then
			/bin/echo "The service is not activated." > ${MNT_POINT}/opt/photostation2/index.php
			[ ! -f ${MNT_POINT}/opt/photostation2/disabled_page.html ] || /bin/cp ${MNT_POINT}/opt/photostation2/disabled_page.html ${MNT_POINT}/opt/photostation2/index.php
		else
			/bin/echo "The service is not enabled." > ${MNT_POINT}/opt/photostation2/index.php
			[ ! -f ${MNT_POINT}/opt/photostation2/disabled_page.html ] || /bin/cp ${MNT_POINT}/opt/photostation2/disabled_page.html ${MNT_POINT}/opt/photostation2/index.php
		fi
		[ ! -f ${MNT_POINT}/opt/photostation2/api/libs/inc_common.php ] || /bin/rm -f ${MNT_POINT}/opt/photostation2/api/libs/inc_common.php
		[ ! -f ${MNT_POINT}/opt/photostation2/api/libs/inc_common.php.disabled ] || /bin/cp  ${MNT_POINT}/opt/photostation2/api/libs/inc_common.php.disabled ${MNT_POINT}/opt/photostation2/api/libs/inc_common.php
	fi
}

function mmstation(){
	QPHOTO_Enable=`/sbin/getcfg QPHOTO Enable -u -d TRUE` #MSV2
	if [ "x$QPHOTO_Hide" = "x1" ] && [ x"$QPHOTO_Enable" = x"TRUE" ]; then
		/sbin/setcfg QPHOTO Enable FALSE
		QPHOTO_Enable="FALSE"
	fi
	if [ $1 = "start" ]; then
		if [ $QPHOTO_Enable = TRUE ]; then
			if [ x${MSV2_SUPPORT} = xTRUE ]; then
				if [ x${MSV2_SUPPORT} = xTRUE ] && [ -f ${UPDATEPKG_DIR}/MSV2.tgz ]; then
					cd ${MNT_POINT}/opt/
					/bin/tar zxf ${UPDATEPKG_DIR}/MSV2.tgz MSV2/index.php
				fi
				[ -f /sbin/flv_convertd ] && /sbin/daemon_mgr flv_convertd start "/sbin/flv_convertd -d"
				index_file=index`/sbin/getcfg System "Build Number" -f /etc/default_config/uLinux.conf`
				if [ -f ${MNT_POINT}/opt/MSV2/index.swf ] || [ -f "${MNT_POINT}/opt/MSV2/${index_file}.swf" ]; then
					/bin/grep INDEXSWFFILENAME ${MNT_POINT}/opt/MSV2/index.php 1>>/dev/null 2>>/dev/null
					if [ $? -eq 0 ]; then
						/bin/sed -i 's/INDEXSWFFILENAME/'"${index_file}"'/g' ${MNT_POINT}/opt/MSV2/index.php
						[ -f ${MNT_POINT}/opt/MSV2/index.swf ] && /bin/rm -f ${MNT_POINT}/opt/MSV2/index20*.swf
						/bin/mv ${MNT_POINT}/opt/MSV2/index.swf "${MNT_POINT}/opt/MSV2/${index_file}.swf" 2>/dev/null
					fi
				fi
				/bin/rm -rf /home/httpd/cgi-bin/Qmultimedia/* 2>>/dev/null
				/bin/rm -rf /home/httpd/Qmultimedia/* 2>>/dev/null
				[ -d /home/httpd/Qmultimedia/ ] && /bin/echo "Page not found." > /home/httpd/Qmultimedia/index.html
			fi
			echo "."
		else
			if [ x${MSV2_SUPPORT} = xTRUE ]; then
				[ -d ${MNT_POINT}/opt/MSV2 ] || /bin/mkdir ${MNT_POINT}/opt/MSV2
			#	/bin/rm -rf ${MNT_POINT}/opt/MSV2/* 2>>/dev/null
				/bin/echo "Multimedia Station is not enabled." > ${MNT_POINT}/opt/MSV2/index.php
			fi
		fi
	fi #end of start
	if [ $1 = "stop" ]; then
		if [ -f /sbin/flv_convertd ]; then
			/sbin/daemon_mgr flv_convertd stop "/sbin/flv_convertd -d"
			[ ! -f /var/lock/flv_convertd ] || /bin/kill -9 `/bin/cat /var/lock/flv_convertd` 2>/dev/null
			[ ! -f /var/lock/flv_convertd ] || /bin/rm -f /var/lock/flv_convertd
		fi
		/bin/echo "The service is not enabled." > ${MNT_POINT}/opt/MSV2/index.php
	fi
}

make_qpkg_base "$1"
case "$1" in
	start)
		echo -n "Starting media stations: " 
		check_audio_dev
		if [ ! -f /tmp/geoip.xml ]; then
			/bin/touch /tmp/geoip.xml
			[ -f /mnt/ext/opt/photostation2/geoip.xml ] && /bin/cp /mnt/ext/opt/photostation2/geoip.xml /tmp/geoip.xml
			/sbin/curl -4 --retry 2 --connect-timeout 30 -m 30 http://www.mycloudnas.com/geoip/getMyGeoIP.php -o /tmp/geoip.xml 2>/dev/null &
		fi

		if [ ! -d "${MMSHARE_PATH}" ]; then
			echo "${MultimediaShare} directory is not ready."
			exit 1
		else
			/bin/chmod 777 "${MMSHARE_PATH}"
		fi

		mmscan_schedule
		
		if [ x${MSV2_SUPPORT} = xTRUE ] && [ -f ${UPDATEPKG_DIR}/MSV2.tgz ]; then
			/bin/tar zxf ${UPDATEPKG_DIR}/MSV2.tgz -C "${QPKG_ROOT}"
			/bin/rm -rf ${MNT_POINT}/opt/MSV2 2>/dev/null
			/bin/ln -sf "${QPKG_ROOT}/MSV2" ${MNT_POINT}/opt/
		fi
		if [ -d "${MNT_POINT}/opt/MSV2" ]; then
			[ -d /share/${MultimediaShare}/.@__thumb/image_frame ] || /bin/mkdir -p /share/${MultimediaShare}/.@__thumb/image_frame
			/bin/chmod 777 /share/${MultimediaShare}/.@__thumb
			/bin/chmod 777 /share/${MultimediaShare}/.@__thumb/image_frame
			[ -f /share/${MultimediaShare}/.@__thumb/MS.db ] && /bin/chmod 666 /share/${MultimediaShare}/.@__thumb/MS.db
			[ ! -f "/share/${MultimediaShare}/.@__thumb/MS_data.db" ] || chmod 666 "/share/${MultimediaShare}/.@__thumb/MS_data.db"
			[ ! -f "/share/${MultimediaShare}/.@__thumb/PS_data.db" ] || chmod 666 "/share/${MultimediaShare}/.@__thumb/PS_data.db"
			[ -d ${MNT_POINT}/opt/MSV2/image_frame ] && /bin/cp -a ${MNT_POINT}/opt/MSV2/image_frame/*.png /share/${MultimediaShare}/.@__thumb/image_frame/
			/bin/chmod 666 /share/${MultimediaShare}/.@__thumb/image_frame/*.png
			if [ ! -f /share/${MultimediaShare}/.@__thumb/install.lock ]; then
				content=`/bin/ls "/share/${MultimediaShare}/"`
				if [ -z "${content}" ] && [ -d ${MNT_POINT}/opt/MSV2/Samples ]; then
					/bin/cp -a ${MNT_POINT}/opt/MSV2/Samples /share/${MultimediaShare}/
					[ ! -d /share/${MultimediaShare}/Samples/thumb ] || /bin/rm -rf /share/${MultimediaShare}/Samples/thumb
					/bin/chmod 777 /share/${MultimediaShare}/Samples
					/bin/chmod 666 /share/${MultimediaShare}/Samples/*.jpg
					if [ -x /usr/local/apache/bin/php ]; then
						[ -f ${MNT_POINT}/opt/MSV2/init_samples.php ] && /usr/local/apache/bin/php -c $PHP_INI ${MNT_POINT}/opt/MSV2/init_samples.php
						[ $? = 0 ] && /bin/touch /share/${MultimediaShare}/.@__thumb/install.lock
					fi
				else
					/bin/touch /share/${MultimediaShare}/.@__thumb/install.lock
				fi
			fi
			[ ! -f /share/${MultimediaShare}/.@__thumb/install.lock ] || /bin/rm -f ${MNT_POINT}/opt/MSV2/init_samples.php
		fi
		[ ! -f ${MNT_POINT}/opt/MSV2/speaker.sh ] || /bin/ln -sf ${MNT_POINT}/opt/MSV2/speaker.sh /usr/local/sbin/
		[ -f /usr/bin/aumix ] || /bin/ln -sf /usr/local/sbin/aumix /usr/bin/
		[ -f "${BACKUP_ALARM_INFO}" ] && /bin/cp -af "${BACKUP_ALARM_INFO}" "${RUNTIME_ALARM_INFO}"
		[ -f /sbin/lpb_scheduler ] && /sbin/daemon_mgr lpb_scheduler start "/sbin/lpb_scheduler -d"
		[ ! -f /tmp/.2genth.ing ] || /bin/rm -f /tmp/.2genth.ing 2>/dev/null
		[ -f /sbin/genthd ] && /sbin/daemon_mgr genthd start "/sbin/genthd&"
		
		if [ -x ${MNT_POINT}/opt/MSV2/bin/dcraw ] && [ ! -x /usr/local/sbin/dcraw ]; then
			/bin/ln -sf ${MNT_POINT}/opt/MSV2/bin/dcraw /usr/local/sbin/
		fi
		echo -n "Multimedia Station, "
		mmstation start
		echo -n "Music Station, "
		musicstation start
		echo -n "Photo Station. "
		photostation2 start
		echo
		;;
	stop)
		echo -n "Shutting down media stations: "
			if [ x${MSV2_SUPPORT} = xTRUE ] && [ -f /sbin/flv_convertd ]; then
				[ -f /sbin/lpb_scheduler ] && /sbin/daemon_mgr lpb_scheduler stop "/sbin/lpb_scheduler -d"
				[ ! -f /var/lock/flv_convertd.lpb_scheduler ] || /bin/kill -9 `/bin/cat /var/lock/flv_convertd.lpb_scheduler` 2>/dev/null
				[ ! -f /var/lock/flv_convertd.lpb_scheduler ] || /bin/rm -f /var/lock/flv_convertd.lpb_scheduler
				[ -f /sbin/genthd ] && /sbin/daemon_mgr genthd stop "/sbin/genthd"
				[ -f "${RUNTIME_ALARM_INFO}" ] && /bin/cp -af "${RUNTIME_ALARM_INFO}" "${BACKUP_ALARM_INFO}"
				#/bin/sed -i '/ImRd.sh/d' /etc/config/crontab
				#/bin/pidof crond 1>>/dev/null 2>>/dev/null
				#[ $? -ne 0 ] || /usr/bin/crontab /etc/config/crontab
			fi
			if [ -d "/share/${MultimediaShare}/.@__thumb/magick_tmpdir" ]; then
				/bin/chmod 777 "/share/${MultimediaShare}/.@__thumb/magick_tmpdir"
				/bin/rm -rf /share/${MultimediaShare}/.@__thumb/magick_tmpdir/*
			fi
			echo -n "Multimedia Station, "
			mmstation stop
			echo -n "Music Station, "
			musicstation stop
			echo -n "Photo Station. "
			photostation2 stop
			/bin/kill `pidof inotifywait` 2>/dev/null
			echo
		;;
	restart)
		$0 stop
		$0 start
		;;
	notify)
	# notify daemon that a new job is the queue
		#/bin/kill -USR1 `pidof ImRd`
		;;
	bgThGen)
		trgtFolder="/share/${MultimediaShare}"
		if [ ! -z "${2}" ]; then
			[ -d "${2}" ] || exit 1
			trgtFolder="${2}"
		fi
		QPHOTO_Enable=`/sbin/getcfg QPHOTO Enable -u -d TRUE`
		[ x"$QPHOTO_Enable" = x"TRUE" ] || ThumbnailGenerator="/mnt/ext/opt/photostation2/api/background_conversion.php"
		if [ x${MSV2_SUPPORT} = xTRUE ]; then
			[ -f $ThumbnailGenerator ] || exit 1
			ret=1
			for i in `/bin/pidof php`
			do
                /bin/cat /proc/$i/cmdline | /bin/grep "$ThumbnailGenerator" 1>>/dev/null 2>>/dev/null
                [ $? -eq 0 ] && ret=0 && break
			done
		else
			/bin/ps | /bin/grep "ImR_all" | /bin/grep "soft" 1>>/dev/null 2>>/dev/null
			ret=$?
		fi

		if [ $ret -eq 0 ]; then
			/bin/echo "bgThGen is running."
			exit 1
		fi
		/bin/echo "ImRd bgThGen start."
		[ ! -f "/tmp/.bgThGen.last" ] || /bin/rm -f "/tmp/.bgThGen.last"
		if [ x${MSV2_SUPPORT} = xTRUE ]; then
			j=1
			while [ $j -le 10 ] ; do
				ret=1
				for i in `/bin/pidof php`
				do
					/bin/cat /proc/$i/cmdline | /bin/grep "$ThumbnailGenerator" 1>>/dev/null 2>>/dev/null
					[ $? -eq 0 ] && ret=0 && break
				done
				[ $ret = 0 ] || /usr/local/apache/bin/php -c $PHP_INI -d memory_limit=128M $ThumbnailGenerator "${trgtFolder}" 1000
				j=$(($j+1))
				[ -f "/tmp/.bgThGen.last" ] || break
			done
		else
			/usr/local/sbin/ImR_all -soft /Qmultimedia 1>>/dev/null 2>>/dev/null
		fi
		/bin/echo "bgThGen finished."
		;;
	musicstation)
		musicstation $2
		;;
	photostation2)
		photostation2 $2
		;;
	mmstation)
		mmstation $2
		;;
	mmscan_schedule)
		mmscan_schedule
		;;
	*)
		echo "Usage: /etc/init.d/$0 {start|stop|restart}"
		exit 1
esac
[ -d /home/httpd/cgi-bin/apps ] && /sbin/user_cmd -1 /etc/qos.ui.sm.conf > /home/httpd/cgi-bin/apps/start.json
exit 0
