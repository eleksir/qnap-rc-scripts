#!/bin/sh
FBDISK_MODULE=/lib/modules/misc/fbdisk.ko
module_fbdisk="fbdisk"
device_fbdisk="fbdisk"
module_fbsnap="fbsnap"
device_fbsnap="fbsnap"
max_device=256


start_fbdisk()
{
    /sbin/insmod $FBDISK_MODULE
    major_fbdisk=`/bin/awk "\\$2==\"$module_fbdisk\" {print \\$1}" /proc/devices`
    major_fbdisk=$(($major_fbdisk+0))
    major_fbsnap=`/bin/awk "\\$2==\"$module_fbsnap\" {print \\$1}" /proc/devices`
    major_fbsnap=$(($major_fbsnap+0))
    if [ $major_fbdisk -gt 0 ];
    then
    	i=0
    	while [ "$i" -ne ${max_device} ]; do
		/bin/mknod /dev/${device_fbdisk}${i} b $major_fbdisk $i
		/bin/mknod /dev/${device_fbsnap}${i} b $major_fbsnap $i
		/bin/chmod 444 /dev/${device_fbsnap}${i}
		i=$(($i+1))
    	done
    fi
}

stop_fbdisk()
{
    /sbin/rmmod $FBDISK_MODULE
    i=0
    while [ "$i" -ne ${max_device} ]; do
		/bin/rm -f /dev/${device_fbdisk}${i}
		/bin/rm -f /dev/${device_fbsnap}${i}
		i=$(($i+1))
    done
}

# check if fbdisk.ko exist
[ -f $FBDISK_MODULE ] || exit 0

case "$1" in
	start)
		start_fbdisk;
		;;
	stop)
		stop_fbdisk;
		;;
	restart)
		stop_fbdisk;
		sleep 1
		start_fbdisk;
		;;
	*)
		echo "Usage: %0 (start|stop|restart)"
		exit 1
esac

exit 0

