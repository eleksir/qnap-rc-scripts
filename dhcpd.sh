#!/bin/sh
# dhcpd         This shell script takes care of starting and stopping
#               dhcpd.
#
# description: dhcpd provide access to Dynamic Host Control Protocol.

BOND_NIC=`/sbin/getcfg "Network" "BONDING NICs" -d "0"`
NIC_NUM=`/sbin/getcfg "Network" "Interface Number" -d "2"`
# See how we were called.
case "$1" in
  start)
	# Stop daemons
	/bin/kill -HUP `/bin/pidof dhcpd`
	# Start daemons
	/usr/sbin/create_dhcpd_conf > /dev/null
	
	echo -n "Starting dhcpd: "
	if [ `/sbin/getcfg "DHCP Server" "Enable" -u -d "FALSE"` = FALSE ]
	then
		echo disabled
		exit 0
	fi
	DEV=`/sbin/getcfg "DHCP Server" "Interface" -d "eth0"`
	
	vlan_enable=`/sbin/getcfg $DEV vlan_enable -d "FALSE"`
	if [ "$vlan_enable" = "TRUE" ]; then
		vlan_id=`/sbin/getcfg $DEV vlan_id -d ""`
		if [ "x$vlan_id" != "x" ]; then
			DEV="$DEV.$vlan_id"
		fi
	fi

	[ -d /etc/config/dhcp ] || /bin/mkdir /etc/config/dhcp;
	[ -d /var/state ] || /bin/mkdir /var/state
	if [ ! -L /var/state/dhcp ]; then
		/bin/ln -sf /etc/config/dhcp /var/state/dhcp
		/bin/touch /var/state/dhcp/dhcpd.leases
	fi
	/sbin/daemon_mgr dhcpd start "/usr/local/sbin/dhcpd -q $DEV 2>/dev/null 1>/dev/null"
	echo -n "dhcpd on $DEV"
	echo "."
	touch /var/lock/subsys/dhcpd
	
#	echo -n "ddns update"
#	/etc/init.d/ddns_update.sh > /dev/null
#	echo "."
	
	
	;;
  stop)
	# Stop daemons.
	echo -n "Shutting down dhcpd: "
	/sbin/daemon_mgr dhcpd stop /usr/local/sbin/dhcpd
	#kill -HUP `/bin/pidof dhcpd`
#	local pid
	pid=`/bin/pidof dhcpd`
	if [ ! -z $pid ]; then
		kill $pid
	fi
	echo -n "dhcpd"
	echo "."
	rm -f /var/lock/subsys/dhcpd
	;;
  restart)
	$0 start
	RETVAL=$?
	;;
  *)
	echo "Usage: dhcpd { [start]|stop|[restart] }"
	exit 1
esac

exit 0
