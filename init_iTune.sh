#!/bin/sh

CONF_FILE=/etc/config/mt-daapd.conf
PLAY_LIST_FILE=/etc/config/mt-daapd.playlist
DB_DIR="/mnt/HDA_ROOT/.db_dir"
DB_PATH="${DB_DIR}/songs.gdb"
NEW_DB_PATH="${DB_DIR}/songs3.db"
LOCK_FILE=/var/lock/subsys/daapd
MultimediaShare=`/sbin/getcfg SHARE_DEF defMultimedia -d Qmultimedia -f /etc/config/def_share.info`
SHARE_PATH=/share/${MultimediaShare}
ITUNES_WEB=`/sbin/getcfg iTune "Web" -d FALSE`
SERVER="`/sbin/getcfg \"System\" \"Server Name\"`(iTunes)"
HOSTNAME=`/sbin/getcfg System "Server Name" -d NAS`
project_name=`/sbin/getcfg Project Name -f /var/default`
if [ "x$project_name" = "xAthens" ]; then
	service_name=`/sbin/getcfg "Bonjour Service" "Web Service Name" -d $HOSTNAME`
	SERVER="${service_name}(iTunes)"
fi
APP_RUNTIME_CONF="/var/.application.conf"
iTunes_Enable=`/sbin/getcfg iTune Enable -u -d FALSE`

function check_MultimediaShare()
{
	if [ ! -d ${SHARE_PATH} ]; then
		[ -d "/share/Multimedia" ] && SHARE_PATH="/share/Multimedia"
		[ -d "/share/Qmultimedia" ] && SHARE_PATH="/share/Qmultimedia"
	fi
}

function _install()
{
	if [ ! -f "/mnt/ext/opt/mt-daapd/mt-daapd" ]; then
		[ ! -d "/mnt/ext/opt/mt-daapd" ] || /bin/rm -rf /mnt/ext/opt/mt-daapd
		[ -d /mnt/ext/opt ] || mkdir -p /mnt/ext/opt
		if [ -f "/mnt/HDA_ROOT/update_pkg/mt-daapd.tgz" ]; then
			/bin/tar -xf /mnt/HDA_ROOT/update_pkg/mt-daapd.tgz -C /mnt/ext/opt
			/bin/ln -sf /mnt/ext/opt/mt-daapd /usr/local/mt-daapd
			/bin/ln -sf /mnt/ext/opt/mt-daapd/mt-daapd /usr/sbin/mt-daapd
		else
			/bin/echo "mt-daapd.tgz does not exist"
			exit 1
		fi
		[ -f /etc/config/mt-daapd.conf ] || /bin/cp /mnt/ext/opt/mt-daapd/mt-daapd.conf ${CONF_FILE}
	fi
	# check config file
	check_MultimediaShare
	[ -f ${CONF_FILE} ] || /bin/cp /mnt/ext/opt/mt-daapd/mt-daapd.conf ${CONF_FILE} 2>>/dev/null
	if [ -f ${CONF_FILE} ]; then
		RUNAS=`/sbin/getcfg general runas -d null -f ${CONF_FILE}`
		if [ "x$RUNAS" != xadmin ]; then
			[ -f /mnt/ext/opt/mt-daapd/mt-daapd.conf ] || /bin/echo "Default mt-daapd.conf not found."
			/bin/cp -f /mnt/ext/opt/mt-daapd/mt-daapd.conf ${CONF_FILE} 2>>/dev/null
			[ -f ${CONF_FILE} ] || exit 1
			/sbin/setcfg general mp3_dir $SHARE_PATH -f ${CONF_FILE}
		fi
		/sbin/setcfg general servername "${SERVER}" -f ${CONF_FILE}
		if [ "x${ITUNES_WEB}" = "xFALSE" ]; then
			/sbin/setcfg general web_root "/dev/null"  -f ${CONF_FILE}
		else
			/sbin/setcfg general web_root "/usr/local/mt-daapd/admin-root" -f ${CONF_FILE}
		fi
		/sbin/getcfg general mp3_dir -d null -f ${CONF_FILE} | /bin/grep "${SHARE_PATH}" 1>/dev/null 2>/dev/null
		if [ $? != 0 ]; then
			/sbin/getcfg general mp3_dir -d null -f ${CONF_FILE} | /bin/grep "/share/Multimedia" 1>/dev/null 2>/dev/null
			_retM=$?
			/sbin/getcfg general mp3_dir -d null -f ${CONF_FILE} | /bin/grep "/share/Qmultimedia" 1>/dev/null 2>/dev/null
			_retQ=$?
			if [ $_retM = 0 ] || [ $_retQ = 0 ]; then
				/sbin/setcfg general mp3_dir $SHARE_PATH -f ${CONF_FILE}
			fi
		fi
	fi
	/bin/grep HDA_ROOT /proc/mounts 1>/dev/null 2>&1
	[ $? = 0 ] || exit 1
	[ -d ${DB_DIR} ] || /bin/mkdir ${DB_DIR}
}

stop()
{
	/bin/kill -INT `/bin/pidof mt-daapd`
	/bin/sleep 1
	[ ! -f ${DB_PATH} ] || /bin/rm -rf ${DB_PATH}
	/bin/rm -f ${LOCK_FILE} 2>/dev/null 1>/dev/null
	/bin/kill -9 `/bin/pidof mt-daapd`
}

reset()
{
	/bin/rm ${CONF_FILE} -f 1>/dev/null 2>/dev/null
	/sbin/setcfg iTune Web FALSE
	/sbin/setcfg iTune "Web Password" admin
	/sbin/setcfg iTune "Password Enabled" 0
	/sbin/setcfg iTune "Password" ""
	$0 start
}

iTunes_Hide=`/sbin/getcfg DISABLE iTunes -d 0 -f $APP_RUNTIME_CONF`
if [ "x$iTunes_Hide" = "x1" ]; then
	/sbin/setcfg iTune Enable FALSE
	iTunes_Enable="FALSE"
fi

case "$1" in
  start)
	[ ! -f ${PLAY_LIST_FILE} ] || /bin/rm -f ${PLAY_LIST_FILE}
	_install
	if [ "x$iTunes_Enable" != "xTRUE" ]; then
		echo "Starting iTune Services: disabled."
	    stop
		exit 0
	fi
        echo -n "Starting iTunes services:"
	[ -f ${LOCK_FILE} ] && stop
	if [ ! -f ${CONF_FILE} ]; then
		/bin/echo "${CONF_FILE} not found."
		exit 1
	fi
	/bin/ln -sf ${CONF_FILE} /etc/mt-daapd.conf
	/usr/sbin/mt-daapd -m -c /etc/mt-daapd.conf 2>/dev/null 1>/dev/null
	if [ -f /tmp/.mt_daapd.err ]; then
		_pid=`/bin/pidof mt-daapd`
		[ x$_pid = x ] || /bin/kill -9 $_pid  2>/dev/null
		[ ! -d ${DB_DIR} ] || /bin/rm -rf ${DB_DIR}/* 2>/dev/null 1>/dev/null
		/bin/rm -f /tmp/.mt_daapd.err 2>/dev/null
		/usr/sbin/mt-daapd -m -c /etc/mt-daapd.conf 2>/dev/null 1>/dev/null
	fi
	/etc/init.d/bonjour.sh reload 2>/dev/null 1>/dev/null
	/bin/touch ${LOCK_FILE}
	;;
  stop)
	echo -n "Shutting down iTunes services:"
	stop
	;;
  reset)
	echo -n "Reset iTunes services:"
	reset;
	;;
  restart)
	echo "Restarting iTunes services:"
	$0 stop
	$0 start
	echo "done."
	;;
  *)
	echo "Usage: init_iTune {start|stop|restart|reset}"
	exit 1
esac


