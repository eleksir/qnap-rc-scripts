#!/bin/sh

OPENSSL="/usr/bin/openssl"

case "$1" in
	sign)
		$OPENSSL dgst -sha1 < $2 > /tmp/hash
		$OPENSSL rsautl -sign -inkey $4 -keyform PEM -in hash > $3
		exit 0
		;;
	verify)
		hash=`$OPENSSL dgst -sha1 < $2`
		verified=`$OPENSSL rsautl -verify -inkey $4 -keyform PEM -certin -in $3`
		if [ "${verified}" = "${hash}" ]; then
			echo "Verified"
			exit 0
		else
			echo "Invalid"
			exit 1
		fi
		;;
	*)
		echo "Usage: openssl_signature {sign|verify}\n"
		echo "sign: openssl_signature sign [file path] [signature path] [private key] "
		echo "verify: openssl_signature verify [file path] [signature path] [public key] "		
		exit 1
		;;
esac

exit 0