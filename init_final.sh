#!/bin/sh
. /etc/init.d/functions
ROOT_PART="/mnt/HDA_ROOT"
UPDATEPKG_DIR="${ROOT_PART}/update_pkg"
FLASH_TMP="/flashfs_tmp"
# check if the HAL subsystem exist
if [ -x /sbin/hal_app ]; then
    BOOT_DEV=$(/sbin/hal_app --get_boot_pd port_id=0)
else
    BOOT_DEV="/dev/sdx"
fi
FLASH_RFS1=${BOOT_DEV}2
FLASH_RFS2=${BOOT_DEV}3
BOOT_CONF=`/bin/cat /etc/config/BOOT.conf`

_init_spool()
{
	local volume=`/sbin/getcfg Public path -f /etc/smb.conf | cut -d '/' -f 3`
	local spool_dir="/share/$volume/.spool"

	[ -d $spool_dir ] || /bin/mkdir "$spool_dir" && /bin/chmod 777 $spool_dir
	[ -L "/var/spool" ] && /bin/rm -f "/var/spool" && /bin/ln -s "$spool_dir" "/var/spool"

	# init cups spool
	if [ -f /usr/sbin/cupsd ]; then
		[ -d "$spool_dir/cups" ] && /bin/rm -fr "$spool_dir/cups"
	fi
}

check_sum()
{
	if [ ! -f $1 ] || [ ! -f $2 ];then
		echo "${1} or ${2} not found."
		return 1
	fi
	local file_cksum=`/sbin/cksum $1`
	local file_crc=`echo $file_cksum | cut -d ' ' -f 1`
	local file_size=`echo $file_cksum | cut -d ' ' -f 2`

	local cksum_cksum=`cat $2`
	local cksum_crc=`echo $cksum_cksum | cut -d ' ' -f 1`
	local cksum_size=`echo $cksum_cksum | cut -d ' ' -f 2`

	if [ $file_crc = $cksum_crc ] && [ $file_size = $cksum_size ]; then
		return 0
	fi
	return 1
}
rsync_startup() {
    rsync_support=`/sbin/getcfg System "Rsync Support" -u`
    if [ "x${rsync_support}" = "x" ]; then
		/sbin/setcfg System "Rsync Support" TRUE
    fi
#    [ -f /etc/config/rsyncd.conf ] || /etc/init.d/rsyncd_srv.sh
#    [ -f /etc/config/rsync_schedule.conf ] || echo "" > /etc/config/rsync_schedule.conf
#
#    if [ `/sbin/getcfg System "Rsync Support" -u -d FALSE` = TRUE ]; then
#		echo "Starting rsync services: rsync."
#		/sbin/daemon_mgr rsync stop "/sbin/rsync --daemon"
#		if [ `/sbin/getcfg "System" "Rsync Model" -u -d QNAP` = NORMAL ]; then
#			#/sbin/daemon_mgr rsync start "/usr/bin/rsync --daemon --sever-mode=0"
#			/usr/bin/rsyncd --daemon --sever-mode=0
#		else
#			#/sbin/daemon_mgr rsync start "/usr/bin/rsync --daemon --sever-mode=1"
#			/usr/bin/rsyncd --daemon --sever-mode=1
#		fi
#    fi
	/etc/init.d/rsyncd.sh start
}

[ -f /sbin/hd_util ] && /sbin/daemon_mgr hd_util stop "/sbin/hd_util &"
[ -f /sbin/hd_util ] && /sbin/daemon_mgr hd_util start "/sbin/hd_util &"
[ -f /sbin/hotswap ] && /sbin/daemon_mgr hotswap stop "/sbin/hotswap &"
[ -f /sbin/hotswap ] && /sbin/daemon_mgr hotswap start "/sbin/hotswap &"
[ -f /sbin/qsmartd ] && /sbin/daemon_mgr qsmartd stop "/sbin/qsmartd -d"
[ -f /sbin/qsmartd ] && /sbin/daemon_mgr qsmartd start "/sbin/qsmartd -d"
[ -f /sbin/gen_bandwidth ] && /sbin/daemon_mgr gen_bandwidth start "/sbin/gen_bandwidth -r -i 5 &"
[ -f /etc/init.d/klogd.sh ] && /sbin/daemon_mgr klogd.sh start "/etc/init.d/klogd.sh start &"
[ x`/bin/pidof bcclient` = x ] || /bin/kill -USR1 `/bin/pidof bcclient`
rsync_startup

#start syslog server
[ -f /etc/init.d/rsyslog.sh ] && /etc/init.d/rsyslog.sh start

#add for online users
/sbin/initsem 2>/dev/null 1>/dev/null

# add for Buzzer of warning
BUZZER="/root/.buzzer_warnning.conf"
echo "[Misc]" >> $BUZZER
if [ `/sbin/getcfg "Misc" "Buzzer Warning Enable" -u -d TRUE` = TRUE ]; then
	/sbin/setcfg "Misc" "Buzzer Warning Enable" TRUE -f $BUZZER
else
	/sbin/setcfg "Misc" "Buzzer Warning Enable" FALSE -f $BUZZER
fi

/sbin/gen_exports >/etc/config/nfssetting
/etc/init.d/ddns_update.sh

#prepare default ssl cert/key for download
/etc/init.d/stunnel.sh prepare_cert_key

#unlook usb copy function
/sbin/setcfg "usb copy" "lock flag" 0 -f "/etc/config/uLinux.conf"

#init qpkg
[ -d ${UPDATEPKG_DIR} ] || /bin/mkdir ${UPDATEPKG_DIR}
/bin/mount | /bin/grep HDA_ROOT 1>>/dev/null 2>>/dev/null
if [ $? = 0 ] && [ "x${BOOT_CONF}" = "xTS-NASX86" ]; then
	if [ `/sbin/getcfg "QWEB" "Enable" -d 0` = 0 ]; then
		/etc/init.d/Qthttpd.sh start	# to extract apache/php
	fi
	if [ `/sbin/getcfg MySQL Enable -u -d FALSE` = FALSE ]; then
		/etc/init.d/mysqld.sh start	# to extract mysql
	fi
	[ -d $FLASH_TMP ] || /bin/mkdir $FLASH_TMP
	/bin/mount $FLASH_RFS1 $FLASH_TMP -o ro
	check_sum "$FLASH_TMP/boot/qpkg.tar" "$FLASH_TMP/boot/qpkg.tar.cksum"
	if [ $? = 0 ]; then
		/bin/tar xf $FLASH_TMP/boot/qpkg.tar -C ${UPDATEPKG_DIR}
		/bin/umount $FLASH_RFS1
	else
		/bin/umount $FLASH_RFS1
		/bin/mount $FLASH_RFS2 $FLASH_TMP -o ro
		check_sum "$FLASH_TMP/boot/qpkg.tar" "$FLASH_TMP/boot/qpkg.tar.cksum"
		[ $? = 0 ] && /bin/tar xf $FLASH_TMP/boot/qpkg.tar -C ${UPDATEPKG_DIR}
		/bin/umount $FLASH_RFS2
	fi
	/bin/rmdir $FLASH_TMP
	if [ -f ${UPDATEPKG_DIR}/ffmpeg.tgz ]; then
		/etc/init.d/installtgz.sh ffmpeg ${UPDATEPKG_DIR}/ffmpeg.tgz
		/etc/init.d/installtgz.sh ImageMagick ${UPDATEPKG_DIR}/ImageMagick.tgz
	fi
	/etc/init.d/Qthttpd.sh restart
	/etc/init.d/ImRd.sh restart
	
	/etc/init.d/installtgz.sh jsLib ${UPDATEPKG_DIR}/jsLib.tgz
	/etc/init.d/installtgz.sh language ${UPDATEPKG_DIR}/language.tgz &
	/etc/init.d/installtgz.sh vim ${UPDATEPKG_DIR}/vim.tgz 
	/etc/init.d/installtgz.sh samba ${UPDATEPKG_DIR}/samba.tgz 
	/etc/init.d/installtgz.sh radius ${UPDATEPKG_DIR}/radius.tgz &
	/etc/init.d/installtgz.sh printer ${UPDATEPKG_DIR}/printer.tgz 
	/etc/init.d/installtgz.sh vaultServices ${UPDATEPKG_DIR}/vaultServices.tgz 
	/etc/init.d/installtgz.sh WFM2 ${UPDATEPKG_DIR}/WFM2.tgz &
	/etc/init.d/installtgz.sh DSv3 ${UPDATEPKG_DIR}/DSv3.tgz
	/etc/init.d/installtgz.sh vpnpptp ${UPDATEPKG_DIR}/vpnpptp.tgz 
	/etc/init.d/installtgz.sh vpnopenvpn ${UPDATEPKG_DIR}/vpnopenvpn.tgz
	/etc/init.d/installtgz.sh ldap_server ${UPDATEPKG_DIR}/ldap_server.tgz
	/etc/init.d/installtgz.sh avahi0630 ${UPDATEPKG_DIR}/avahi0630.tgz
	if [ -f ${UPDATEPKG_DIR}/medialibrary.tgz ]; then
		/etc/init.d/installtgz.sh medialibrary ${UPDATEPKG_DIR}/medialibrary.tgz
	fi
	
	/bin/touch ${UPDATEPKG_DIR}/update_qpkg_f
	[ ! -f /etc/init.d/init_mozyclient.sh ] || /etc/init.d/init_mozyclient.sh start
	/etc/init.d/smb.sh restart
	/etc/init.d/init_qpkg.sh start
fi

# init lunportman
if [ -f /etc/init.d/lunportman.sh ]; then
	/etc/init.d/lunportman.sh start 2>/dev/null 1>/dev/null
fi

# init qsyncman
if [ -f /etc/init.d/qsyncman.sh ]; then
	/etc/init.d/qsyncman.sh start 2>/dev/null 1>/dev/null
fi
/etc/init.d/update_def_share.sh &
if [ -x "/sbin/appriv" ]; then
        if [ ! -f /etc/config/nas_priv.db  -o ! -s /etc/config/nas_priv.db ]; then
                /sbin/appriv -i
        else
                /sbin/appriv -g &
        fi
fi
[ x`/sbin/getcfg QPHOTO UseSysAccount` != x ] || /sbin/setcfg QPHOTO UseSysAccount 1
[ x`/sbin/getcfg MUSICSTATION UseSysAccount` != x ] || /sbin/setcfg MUSICSTATION UseSysAccount 1
[ x`/sbin/getcfg PHOTOSTATION UseSysAccount` != x ] || /sbin/setcfg PHOTOSTATION UseSysAccount 1
#start media library services
if [ -f /usr/local/medialibrary/bin/mymediadbserver ]; then
	/etc/init.d/StartMediaService.sh start
fi
[ -x /etc/init.d/twonkymedia.sh ] && ( /etc/init.d/twonkymedia.sh restart 2>/dev/null 1>/dev/null & ) && sleep 3

_init_spool
[ -f /etc/init.d/printer.sh ] && /etc/init.d/printer.sh start 2>/dev/null 1>/dev/null
[ -f /etc/init.d/cupsd.sh ] && /etc/init.d/cupsd.sh start &> /dev/null
[ -f /etc/init.d/antivirus.sh ] && /etc/init.d/antivirus.sh init &> /dev/null
[ -f /etc/init.d/ldap_server.sh ] && /etc/init.d/ldap_server.sh init &> /dev/null
[ x`/sbin/getcfg BTDownload addonEnable -u -d FALSE` = xTRUE ] || /sbin/setcfg BTDownload addonEnable TRUE
[ ! -f /etc/init.d/btd.sh ] || /etc/init.d/btd.sh restart &> /dev/null

. /etc/init.d/check_nss2
[ "x$_NSS2_SUPPORT" = "xyes" ] && /bin/cp -f /etc/init.d/nvr_svc.sh /etc/init.d/nvrd.sh 2>/dev/null

/bin/touch ${UPDATEPKG_DIR}/.Squid_required
[ -x ${UPDATEPKG_DIR}/Squid.bin ] && ( ${UPDATEPKG_DIR}/Squid.bin 2>/dev/null 1>/dev/null & ) && sleep 3 && /bin/rm -f ${UPDATEPKG_DIR}/.Squid_required
