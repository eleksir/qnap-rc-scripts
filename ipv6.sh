#!/bin/sh
# Copyright @ 2009 QNAP Systems Inc.

GETCFG="/sbin/getcfg"
SETCFG="/sbin/setcfg"

IPV6=`$GETCFG "Network" "IPV6" -u -d "FALSE"`
BOND_SUP=`$GETCFG "Network" "BONDING Support" -u -d "FALSE"`
RETVAL=0
BOND_NIC=`$GETCFG "Network" "BONDING NICs" -d "0"`
NIC_NUM=`$GETCFG "Network" "Interface Number" -d "2"`
ADV_BOND_SUPPORT=`$GETCFG "Network" "Advanced Bonding Support" -d "FALSE"`
GROUP_NUM=`$GETCFG "Network Group" "Group Count" -d $NIC_NUM`

set_ipv6_dns()
{
  [ "$IPV6" = "TRUE" ] || exit 1
  /bin/sed -i '/:/d' /etc/resolv.conf
	local haveflg="0"
  for i in 1 2
  do
    local ipv6_dns=`/sbin/getcfg "Network" "IPV6 Domain Name Server $i" -d "::"`
    /bin/grep "$ipv6_dns" /etc/resolv.conf 2>/dev/null 1>/dev/null
    if [ $? != 0 ]; then
      if [ "$ipv6_dns" != "::" ]; then
        /bin/echo "nameserver $ipv6_dns" >> /etc/resolv.conf
        haveflg=1
      fi
    fi
  done
  if [ "x$haveflg" = "x1" ]; then
    /bin/sed -i '/0.0.0.0/d' /etc/resolv.conf
  fi
}

init_dhcpv6()
{
  /sbin/lsmod | /bin/grep -sq ipv6
  if [ $? -ne 0 ]; then
    [ ! -f /lib/modules/others/ipv6.ko ] || /sbin/insmod /lib/modules/others/ipv6.ko
    /bin/sleep 1
    for (( i=0; i<$NIC_NUM; i=i+1 ))
    do
      [ ! -f /proc/sys/net/ipv6/conf/eth$i/accept_ra ] || /bin/echo "0" > /proc/sys/net/ipv6/conf/eth$i/accept_ra
    done
    [ ! -f /proc/sys/net/ipv6/conf/all/accept_ra ] || /bin/echo 0 > /proc/sys/net/ipv6/conf/all/accept_ra
    [ ! -f /proc/sys/net/ipv6/conf/default/accept_ra ] || /bin/echo 0 > /proc/sys/net/ipv6/conf/default/accept_ra
  fi
  /bin/echo 0 > /proc/sys/net/ipv6/conf/all/disable_ipv6
  [ -d /etc/config/dhcpv6 ] || /bin/mkdir -p /etc/config/dhcpv6
  [ -d /var/lib/dhcpv6 ] || /bin/mkdir -p /var/lib/dhcpv6
  [ -d /var/run/dhcpv6 ] || /bin/mkdir -p /var/run/dhcpv6
  [ -d /var/db ] || /bin/mkdir /var/db
}

enable_autoconf()
{
  #default is enabled
  if [ `$GETCFG "${1}" "Enable Autoconf" -u -d "TRUE"` = "TRUE" ]; then
    [ -f /proc/sys/net/ipv6/conf/${1}/autoconf ] && /bin/echo 1 > /proc/sys/net/ipv6/conf/${1}/autoconf
    [ -f /proc/sys/net/ipv6/conf/${1}/accept_ra ] && /bin/echo 1 > /proc/sys/net/ipv6/conf/${1}/accept_ra
  else
    [ -f /proc/sys/net/ipv6/conf/${1}/autoconf ] && /bin/echo 0 > /proc/sys/net/ipv6/conf/${1}/autoconf
    [ -f /proc/sys/net/ipv6/conf/${1}/accept_ra ] && /bin/echo 0 > /proc/sys/net/ipv6/conf/${1}/accept_ra
  fi
}

set_static_ipv6()
{
  #default is enabled
  if [ `$GETCFG "${1}" "Enable Autoconf" -u -d "TRUE"` = "FALSE" ]; then
    IPV6ADDR=`$GETCFG "${1}" "IPV6 Address" -d "2001:0:1:2::5000/64"`
    /sbin/ifconfig "${1}" inet6 add "$IPV6ADDR"
    GATEWAY=`$GETCFG "${1}" "IPV6 Gateway" -d "::/0"`
    [ "${GATEWAY%%/*}" = "::" ] || /sbin/route -A inet6 add "2000::/3" gw "${GATEWAY%%/*}" dev "${1}"
  fi
}

start()
{
  [ "$IPV6" = "TRUE" ] || exit 1
  [ -f "/lib/modules/others/ipv6.ko" ] || exit 1
  init_dhcpv6
  if [ "x$ADV_BOND_SUPPORT" = "xTRUE" ]; then
    for (( i=0; i<$GROUP_NUM; i=i+1 ))
    do
      local net_interface=`$GETCFG "Network Group" "Group $i" -d ""`
      if [ "x${net_interface}" != "x" ]; then
          enable_autoconf ${net_interface}
          [ `$GETCFG "${net_interface}" "Use DHCPV6" -u -d "FALSE"` = "TRUE" ] && /sbin/dhcp6c -p /var/run/dhcpv6/dhcp6c_${net_interface}.pid ${net_interface}
          set_static_ipv6 ${net_interface}
      fi
    done
  else
    if [ "$NIC_NUM" = "4" ]; then
      if [ "$BOND_SUP" = "TRUE" ]; then
        if [ "$BOND_NIC" = "0" ]; then
          enable_autoconf bond0
          if [ `$GETCFG "bond0" "Use DHCPV6" -u -d "FALSE"` = "TRUE" ]; then
            /sbin/dhcp6c bond0
          fi
          set_static_ipv6 bond0
          enable_autoconf eth2
          enable_autoconf eth3
          [ `$GETCFG "eth2" "Use DHCPV6" -u -d "FALSE"` = "TRUE" ] && /sbin/dhcp6c -p /var/run/dhcpv6/dhcp6c_eth2.pid eth2
          [ `$GETCFG "eth3" "Use DHCPV6" -u -d "FALSE"` = "TRUE" ] && /sbin/dhcp6c -p /var/run/dhcpv6/dhcp6c_eth3.pid eth3
          set_static_ipv6 eth2
          set_static_ipv6 eth3
        elif [ "$BOND_NIC" = "1" ]; then
          enable_autoconf bond1
          if [ `$GETCFG "bond1" "Use DHCPV6" -u -d "FALSE"` = "TRUE" ]; then
            /sbin/dhcp6c bond1
          fi
          set_static_ipv6 bond1
          enable_autoconf eth0
          enable_autoconf eth1
          [ `$GETCFG "eth0" "Use DHCPV6" -u -d "FALSE"` = "TRUE" ] && /sbin/dhcp6c -p /var/run/dhcpv6/dhcp6c_eth0.pid eth0
          [ `$GETCFG "eth1" "Use DHCPV6" -u -d "FALSE"` = "TRUE" ] && /sbin/dhcp6c -p /var/run/dhcpv6/dhcp6c_eth1.pid eth1
          set_static_ipv6 eth0
          set_static_ipv6 eth1
        elif [ "$BOND_NIC" = "2" ]; then
          enable_autoconf bond0
          if [ `$GETCFG "bond0" "Use DHCPV6" -u -d "FALSE"` = "TRUE" ]; then
            /sbin/dhcp6c bond0
          fi
          set_static_ipv6 bond0
          enable_autoconf bond1
          if [ `$GETCFG "bond1" "Use DHCPV6" -u -d "FALSE"` = "TRUE" ]; then
          /sbin/dhcp6c bond1
          fi
          set_static_ipv6 bond1
        elif [ "$BOND_NIC" = "3" ]; then
          enable_autoconf bond0
          if [ `$GETCFG "bond0" "Use DHCPV6" -u -d "FALSE"` = "TRUE" ]; then
            /sbin/dhcp6c bond0
          fi
          set_static_ipv6 bond0
        fi
      else
        enable_autoconf eth0
        enable_autoconf eth1
        enable_autoconf eth2
        enable_autoconf eth3
        [ `$GETCFG "eth0" "Use DHCPV6" -u -d "FALSE"` = "TRUE" ] && /sbin/dhcp6c -p /var/run/dhcpv6/dhcp6c_eth0.pid eth0
        [ `$GETCFG "eth1" "Use DHCPV6" -u -d "FALSE"` = "TRUE" ] && /sbin/dhcp6c -p /var/run/dhcpv6/dhcp6c_eth1.pid eth1
        [ `$GETCFG "eth2" "Use DHCPV6" -u -d "FALSE"` = "TRUE" ] && /sbin/dhcp6c -p /var/run/dhcpv6/dhcp6c_eth2.pid eth2
        [ `$GETCFG "eth3" "Use DHCPV6" -u -d "FALSE"` = "TRUE" ] && /sbin/dhcp6c -p /var/run/dhcpv6/dhcp6c_eth3.pid eth3
        set_static_ipv6 eth0
        set_static_ipv6 eth1
        set_static_ipv6 eth2
        set_static_ipv6 eth3
      fi
    else
      if [ "$BOND_SUP" = "TRUE" ]; then
        enable_autoconf bond0
        if [ `$GETCFG "bond0" "Use DHCPV6" -u -d "FALSE"` = "TRUE" ]; then
          /sbin/dhcp6c bond0
        fi
        set_static_ipv6 bond0
      else
        enable_autoconf eth0
        enable_autoconf eth1
        [ `$GETCFG "eth0" "Use DHCPV6" -u -d "FALSE"` = "TRUE" ] && /sbin/dhcp6c -p /var/run/dhcpv6/dhcp6c_eth0.pid eth0
        [ `$GETCFG "eth1" "Use DHCPV6" -u -d "FALSE"` = "TRUE" ] && /sbin/dhcp6c -p /var/run/dhcpv6/dhcp6c_eth1.pid eth1
        set_static_ipv6 eth0
        set_static_ipv6 eth1
      fi
    fi
  fi
  set_ipv6_dns
  /etc/init.d/radvd.sh start
  [ -d /sys/kernel/config/target/iscsi ] && /sbin/iscsi_util --ip_change
}

stop()
{
  /etc/init.d/radvd.sh stop
  if [ "x$ADV_BOND_SUPPORT" = "xTRUE" ]; then
    for (( i=0; i<$GROUP_NUM; i=i+1 ))
    do
      local net_interface=`$GETCFG "Network Group" "Group $i" -d ""`
      if [ "x${net_interface}" != "x" ]; then
        enable_autoconf ${net_interface}
      fi
    done
  else
    if [ "$NIC_NUM" = "4" ]; then  
      if [ "$BOND_SUP" = "TRUE" ]; then
        if [ "$BOND_NIC" = "0" ]; then
          enable_autoconf bond0
          enable_autoconf eth2
          enable_autoconf eth3
        elif [ "$BOND_NIC" = "1" ]; then
          enable_autoconf bond1
          enable_autoconf eth0
          enable_autoconf eth1        
        elif [ "$BOND_NIC" = "2" ]; then
          enable_autoconf bond0
          enable_autoconf bond1
        elif [ "$BOND_NIC" = "3" ]; then
          enable_autoconf bond0
        fi
      else
        enable_autoconf eth0
        enable_autoconf eth1
        enable_autoconf eth2
        enable_autoconf eth3
      fi
    else
      if [ "$BOND_SUP" = "TRUE" ]; then
        enable_autoconf bond0
      else
        enable_autoconf eth0
        enable_autoconf eth1
      fi
    fi
  fi
  
  pid=`/bin/pidof dhcp6c`
  if [ ! -z "$pid" ]; then 
    /bin/kill $pid
    /bin/sleep 2
  fi
  pid=`/bin/pidof dhcp6c`
  [ ! -z "$pid" ] && /bin/kill -9 $pid
  #/bin/echo "To disable IPv6 needs to restart the system."
}

restart() 
{
  stop
  /bin/sleep 1
  start
  /bin/sleep 1
}

case "$1" in
start)
  stop 
  /bin/sleep 1
  start

  ;;

stop)
  stop
  ;;

restart)
  restart
  ;;

setdns)
  set_ipv6_dns
  ;;

init)
  [ "$IPV6" = "TRUE" ] || exit 1
  [ -f "/lib/modules/others/ipv6.ko" ] || exit 1
  init_dhcpv6
  ;;
*)
  /bin/echo $"Usage: $0 {start|stop|restart|setdns|init}"
  exit 1
esac

exit 0
