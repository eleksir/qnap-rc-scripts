#! /bin/sh

if [ ! -d "$1" ]; then
	mkdir "$1"
fi

if [ ! -d "$2" ]; then
	mkdir "$2"
fi

if [ ! -d "$3" ]; then
	mkdir "$3"
fi

if [ ! -d "$4" ]; then
	mkdir "$4"
fi

if [ ! -d "$5" ]; then
	mkdir "$5"
fi

if [ ! -d "$6" ]; then
	mkdir "$6"
fi

if [ ! -e /usr/bin/cut ]; then
	cp cut "$7/usr/bin"
fi
