#!/bin/sh
GETCFG="/sbin/getcfg"
NET="/usr/local/samba/bin/net"

quiet="$1"
ads_register_dns() {
	_ads="$(${GETCFG} global security -d USER -f /etc/config/smb.conf)"

	if [ "${_ads}" == "ADS" ]; then
		_reg_dns="$(${GETCFG} Samba "register dns" -d FALSE)"

		if [ "${_reg_dns}" == "TRUE" ]; then
			try=0

			while [ $try -lt 3 ]; do
				"${NET}" ads dns register -P
				if [ $? == 0 ]; then
					[ "$quiet" == "" ] && /sbin/write_log "[Microsoft Networking] The NAS has been registered successfully in DNS" 4
					break
				fi
				/bin/sleep 1
				let "try++"
			done

			if [ $try -ge 3 ]; then
				/sbin/write_log "[Microsoft Networking] The NAS registration in DNS failed" 2
			fi
		fi
	fi
}

ads_register_dns

