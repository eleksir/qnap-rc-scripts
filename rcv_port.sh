#!/bin/sh

RCV_PORT="/proc/tsinfo/sk_rcv_port"

if [ ! -f $RCV_PORT ]; then
    exit 0
fi

case "$1" in
    start)
	echo -n "Start rcv_port: "
	PORT="set 139 445 20"
	echo "$PORT" > $RCV_PORT
	echo -n $PORT
	echo .
	;;
    stop)
	echo -n "Stop rcv_port: "
	PORT="set"
	echo "$PORT" > $RCV_PORT
	echo -n $PORT
	echo .
	;;

    restart)
	$0 stop
	$0 start
	;;	
    *)
        echo "Usage: /etc/init.d/rcv_port.sh {start|stop|restart}"
        exit 1
esac

exit 0
