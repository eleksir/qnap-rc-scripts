#!/bin/sh
func="$1"
opt="$2"
MIN_FSIZE=200 #minimum config file size required
. /etc/init.d/functions

#1. Check conifg in uLinux
backup_en=`/sbin/getcfg BackupConfig Enable -d null`
backup_files=`/sbin/getcfg BackupConfig Files -d "/etc/config/*"`
backup_max=`/sbin/getcfg BackupConfig "Max config" -d 10`
if [ "x$backup_en" = "xFALSE" ] && [ -z "$func" ]; then
	echo "Backup func is Disabled"
	exit 0
fi
	
#2. Determine BASE installation location according to smb.conf
FindDefVol
_ret=$?
if [ $_ret != 0 ]; then
	echo "Default volume not found."
	exit 1
fi

CONFIG_DIR="$DEF_VOLMP/.@backup_config"
[ -d  "$CONFIG_DIR" ]|| /bin/mkdir "$CONFIG_DIR"

if [ "x$func" = "xrestore" ] && [ ! -z "$opt" ]; then
	ret=0
	[ ! -z "$opt" ] || exit 1
	cd $CONFIG_DIR
	/bin/ls -r | grep "^0_" | while read archive
	do
		[ -f $archive ] || continue
		restore_f=`tar -tf "$archive" 2>>/dev/null | grep "$opt"`
		if [ $? = 0 ]; then
			[ ! -f "/$restore_f" ] || /bin/cp "/$restore_f" "/$restore_f".replaced
			/bin/tar -f $archive -x "$restore_f"
			f_size=`/usr/bin/du -b "$restore_f" | /bin/awk '{print $1}'`
			if [ -z "$f_size" ] || [ "$f_size" -lt $MIN_FSIZE ]; then
				/bin/rm -rf "$restore_f" 2>>/dev/null
				continue
			fi
			/bin/tar -f $archive -C / -x "$restore_f"
			ret=$?
			break
		fi
	done
	exit $ret
fi

#3. file rotate
/bin/ls -r $CONFIG_DIR | while read config_name
do
	echo "$config_name" | grep -q "_*\.tar\.gz"
	[ "$?" = "1" ]&& continue
	index=${config_name%%_*}
	postfix_name=${config_name#*_}
	let "index++"
	if [ $index -ge $backup_max ]; then
		#echo "remove $CONFIG_DIR/$config_name"
		/bin/rm -f "$CONFIG_DIR/$config_name"
		continue
	fi
	let "index%=backup_max"
	/bin/mv "$CONFIG_DIR/$config_name" "$CONFIG_DIR/${index}_${postfix_name}"
done

#4. Backup config
	tar_name=`date +%Y%m%d_%H%M`
	[ "x$backup_files" != "x" ]&& /bin/tar --no-recursion -czpf $CONFIG_DIR/0_$tar_name.tar.gz $backup_files > /dev/null 2>&1 
