#!/bin/sh
pidnum=`/bin/pidof opentftpd`
TFTP_ENABLE=`/sbin/getcfg "TFTP Server" "Enable" -d FALSE`
if [ "${TFTP_ENABLE}" = "FALSE" ]; then
	/sbin/setcfg "TFTP Server" "Enable" "FALSE"
	/sbin/setcfg "TFTP Server" "PORT" "69"
	/sbin/setcfg "TFTP Server" "Root Folder" "Public"
	/sbin/setcfg "TFTP Server" "Root Subfolder" ""
	/sbin/setcfg "TFTP Server" "Log Folder" "Public"
        /sbin/setcfg "TFTP Server" "Log Subfolder" ""
	/sbin/setcfg "TFTP Server" "Allowed mode" "0"
fi

# Start opentftp:
opentftpd_start() {
	##### Modify line below for location of executive and other files #####
	/usr/sbin/create_tftpd_conf > /dev/null
	TFTP_LOG=`/sbin/getcfg "TFTP Server" "Save Log" -d 0`
	if [ "x${TFTP_LOG}" = "x1" ]; then
		TFTP_SHARE_FOLDER=`/sbin/getcfg "TFTP Server" "Log Folder" -d "Public"`
		TFTP_LOG_FOLDER=`/sbin/getcfg "${TFTP_SHARE_FOLDER}" "path" -f /etc/config/smb.conf`
		TFTP_SUB_FOLDER=`/sbin/getcfg "TFTP Server" "Log Subfolder" -d ""`
		/usr/sbin/opentftpd -i /etc/opentftpd.ini -l ${TFTP_LOG_FOLDER}/${TFTP_SUB_FOLDER}/opentftpd.log
	else
		/usr/sbin/opentftpd -i /etc/opentftpd.ini
	fi
	echo "Server opentftpd started"
}

# Stop opentftp:
opentftpd_stop() {
	kill $pidnum
	i=0
	while [ ${i} -lt 5 ]
	do
		pidnum=`/bin/pidof opentftpd`
		if [ -z "$pidnum" ]; then
			break
		fi
		sleep 1
		i=`/usr/bin/expr ${i} + 1`
	done
	pidnum=`/bin/pidof opentftpd`
	[ -z "$pidnum" ] || kill -9 $pidnum
	echo "Server opentftp stopped"
}

case "$1" in
    'start')
	if [ -z "$pidnum" ]; then
		opentftpd_start
		RETVAL=0
	else
		echo "Server opentftp is already running - Try restart"
		RETVAL=1
	fi
	;;
    'stop')
	if [ -z "$pidnum" ]; then
		RETVAL=1
	else
		opentftpd_stop
		RETVAL=0
	fi
	;;
    'restart')
	if [ -z "$pidnum" ]; then
		echo "Server opentftp is not running"
		opentftpd_start
	else
		opentftpd_stop
		opentftpd_start
	fi
	RETVAL=0
	;;
    'status')
	if [ -z "$pidnum" ]; then
		echo "Server opentftp is not running"
		RETVAL=1
	else
		echo "Server opentftp is running - Pid : $pidnum"
		RETVAL=0
	fi
	;;
    *)
	echo "Usage $0 { start | stop | restart | status }"
	RETVAL=1
	;;
esac
exit $RETVAL

# Enjoy!
