#!/bin/sh
# For Qweb and Qphoto
# default port: 80

BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf 2>/dev/null`
ROOT_PART="/mnt/HDA_ROOT"
QWEB_DEFAULT_INDEX_HTML="/home/httpd/help/eng/help_qweb.html"
QWEB_DEFAULT_INDEX_PHP="/home/httpd/help/eng/help_qweb.php"
WebShare=`/sbin/getcfg SHARE_DEF defWeb -d Qweb -f /etc/config/def_share.info`
QWEB_SLINK="/home/Qhttpd/${WebShare}"
QWEB_FOLDER="/share/${WebShare}"
QWEB_INDEX_HTML="$QWEB_SLINK/index.html"
QWEB_INDEX_HTM="$QWEB_SLINK/index.htm"
QWEB_INDEX_PHP="$QWEB_SLINK/index.php"
QEACCELERATOR_TMPLINK="/tmp/.eaccelerator.tmp"
QEACCELERATOR_TMPDIR="/.eaccelerator.tmp"
MNT_POINT="/mnt/ext"
APACHE_SOURCE="${MNT_POINT}/opt/source/apache_php5.tgz"
APACHE_DIR="/usr/local/apache"
APACHE_CONF_DIR="/etc/config/apache"
UPDATEPKG_DIR="${ROOT_PART}/update_pkg"
PHP_INI="/etc/config/php.ini"
APACHE_CONF="/etc/config/apache/apache.conf"
APP_RUNTIME_CONF="/var/.application.conf"
QPKG_CONF="/etc/config/qpkg.conf"
SSMTP_CONF_DIR=/etc/config/ssmtp
SSMTP_CONF_DEFAULT=/etc/default_config/ssmtp
CMS_ENABLE=`/sbin/getcfg CMS Enable -d FALSE -f /etc/config/uLinux.conf`
PGSQL_ENABLE=`/sbin/getcfg PgSQL Enable -d FALSE -f /etc/config/uLinux.conf`

ZEND_SOURCE="${UPDATEPKG_DIR}/Zend.tgz"
DEF_MEMORY_LIMIT=256
[ "x${BOOT_CONF}" = "xTS-NASX86" ] || DEF_MEMORY_LIMIT=128
DEF_POST_MAX_SIZE=2047
DEF_UPLOAD_MAX_FILESIZE=2047
MSV2_SUPPORT=`/sbin/getcfg "QPHOTO" "MSV2_SUPPORT" -d "FALSE" -f /etc/default_config/uLinux.conf`
PHP_SESSION_PATH=""
CustomLog=`/sbin/getcfg QWEB CustomLog -u -d FALSE`
ramUse=`/bin/df  | grep "/dev/ram" | /bin/awk '{print $5}'`
. /etc/init.d/functions
make_php_sess_base(){
	FindDefVol
	_ret=$?
	if [ $_ret != 0 ]; then
		return 1
	fi
	[ -d "${DEF_VOLMP}/.php_session" ] || /bin/mkdir -p "${DEF_VOLMP}/.php_session"
	/bin/chmod 777 "${DEF_VOLMP}/.php_session"
	PHP_SESSION_PATH="${DEF_VOLMP}/.php_session"
	[ -d "${DEF_VOLMP}/${WebShare}" ] && /bin/chmod 777 "${DEF_VOLMP}/${WebShare}"
}

init_qweb()
{
	if [ ! -f $QWEB_INDEX_HTML ] && [ ! -f $QWEB_INDEX_HTM ] && [ ! -f $QWEB_INDEX_PHP ]; then
		/bin/cp "$QWEB_DEFAULT_INDEX_PHP" "$QWEB_INDEX_PHP"
	fi
}

check_qweb_link()
{
	if [ `/sbin/getcfg "QWEB" "Enable" -d 1` = 1 ]; then
		if [ -d $QWEB_FOLDER ]; then
			if [ ! -e $QWEB_SLINK ]; then
				ln -s $QWEB_FOLDER $QWEB_SLINK
			elif [ ! -L $QWEB_SLINK ]; then
			#shouldn't come to this place
				rm -rf $QWEB_SLINK
				ln -s $QWEB_FOLDER $QWEB_SLINK
			fi
			init_qweb
		fi
	fi
}
clean_apache_log()
{
	/bin/rm	-f ${APACHE_DIR}/logs/* 1>>/dev/null 2>>/dev/null
	/bin/rm	-rf ${APACHE_DIR}/logs/ssl_scache 1>>/dev/null 2>>/dev/null
	/bin/mkdir ${APACHE_DIR}/logs/ssl_scache
	[ $? = 0 ] && echo "Apache logs have been cleaned."
	/bin/grep "Qthttpd.sh" /etc/config/crontab 1>/dev/null 2>/dev/null
	if [ $? = 0 ]; then
		/bin/sed -i '/Qthttpd/d' /etc/config/crontab
		/usr/bin/crontab /etc/config/crontab
	fi
	/bin/sync
	return 0
}

set_config()
{
	MEMORY_LIMIT=`/sbin/getcfg PHP memory_limit -d 0 -f $PHP_INI | /bin/cut -d 'M' -f 1`
	POST_MAX_SIZE=`/sbin/getcfg PHP post_max_size -d 0 -f $PHP_INI | /bin/cut -d 'M' -f 1`
	UPLOAD_MAX_FILESIZE=`/sbin/getcfg PHP upload_max_filesize -d 0 -f $PHP_INI | /bin/cut -d 'M' -f 1`
	sendmailp=`/sbin/getcfg "mail function" "sendmail_path" -f $PHP_INI`
	[ "x${sendmailp}" != x ] || /sbin/setcfg "mail function" sendmail_path "/usr/sbin/sendmail -t -i" -f $PHP_INI
	[ x`/sbin/getcfg MySQL "mysql.default_socket" -f $PHP_INI` != x ] || /sbin/setcfg MySQL "mysql.default_socket" \"/tmp/mysql.sock\" -f $PHP_INI
	[ x`/sbin/getcfg eaccelerator "eaccelerator.shm_size" -f $PHP_INI -d 16` = x32 ] || /sbin/setcfg eaccelerator eaccelerator.shm_size 32 -f $PHP_INI
	[ x`/sbin/getcfg "PHP" "include_path" -f $PHP_INI` != "\".:/etc/config/php\"" ] || /sbin/setcfg PHP include_path "\".:/etc/config/php\"" -f	$PHP_INI
	[ x`/sbin/getcfg "PHP" "upload_tmp_dir" -f $PHP_INI` = "x${QWEB_FOLDER}" ] || /sbin/setcfg "PHP" "upload_tmp_dir" "${QWEB_FOLDER}" -f $PHP_INI
	[ x`/sbin/getcfg "Session" "session.save_path" -f $PHP_INI` = "x${PHP_SESSION_PATH}" ] || /sbin/setcfg "Session" "session.save_path" "${PHP_SESSION_PATH}" -f $PHP_INI
	if [ "x${MSV2_SUPPORT}" = xTRUE ] && [ -f /usr/bin/ffmpeg ]; then
		/bin/grep ffmpeg.so ${PHP_INI} 1>>/dev/null 2>>/dev/null
		[ $? != 0 ] || /bin/sed -i "/extension = ffmpeg.so/d" ${PHP_INI} 2>/dev/null 1>/dev/null
		/bin/grep "^Include" ${APACHE_CONF} | /bin/grep "apache-msv2.conf" 1>>/dev/null 2>>/dev/null
		if [ $? != 0 ] && [ -f /etc/config/apache/extra/apache-msv2.conf ]; then
			/bin/echo "" >> ${APACHE_CONF}
			/bin/echo "Include /etc/config/apache/extra/apache-msv2.conf" >> ${APACHE_CONF}
		fi
		[ -f /etc/config/apache/extra/apache-msv2.conf ] || /bin/sed -i "/^Include.*apache-msv2.conf/d" ${APACHE_CONF} 2>/dev/null 1>/dev/null
		/bin/grep "^Include" ${APACHE_CONF} | /bin/grep "apache-musicstation.conf" 1>>/dev/null 2>>/dev/null
		if [ $? != 0 ] && [ -f /etc/config/apache/extra/apache-musicstation.conf ]; then
			/bin/echo "Include /etc/config/apache/extra/apache-musicstation.conf" >> ${APACHE_CONF}
		fi

		/bin/grep "^Include" ${APACHE_CONF} | /bin/grep "apache-photo.conf" 1>>/dev/null 2>>/dev/null
		if [ $? != 0 ] && [ -f /etc/config/apache/extra/apache-photo.conf ]; then
			/bin/echo "Include /etc/config/apache/extra/apache-photo.conf" >> ${APACHE_CONF}
		fi

		if [ -f /usr/local/apache/modules/imagick.so ]; then
			[ x`/sbin/getcfg Imagick extension -f ${PHP_INI}` = "ximagick.so" ] || /sbin/setcfg Imagick extension "imagick.so" -f ${PHP_INI}
		else
			/bin/grep "\[Imagick\]" ${PHP_INI} 1>>/dev/null 2>>/dev/null
			[ $? != 0 ] || /sbin/setcfg -e Imagick extension -f ${PHP_INI}
		fi
		#[ x`/sbin/getcfg eaccelerator extension -f ${PHP_INI}` = x ] || /sbin/setcfg -e eaccelerator extension -f ${PHP_INI}
		[ $MEMORY_LIMIT -le $DEF_MEMORY_LIMIT ] || /sbin/setcfg PHP memory_limit "$DEF_MEMORY_LIMIT"M -f $PHP_INI
		[ $POST_MAX_SIZE -le $DEF_POST_MAX_SIZE ] || /sbin/setcfg PHP post_max_size "$DEF_POST_MAX_SIZE"M -f $PHP_INI
		[ $UPLOAD_MAX_FILESIZE -le $DEF_UPLOAD_MAX_FILESIZE ] || /sbin/setcfg PHP upload_max_filesize "$DEF_UPLOAD_MAX_FILESIZE"M -f $PHP_INI
	else
		/sbin/setcfg -e Imagick extension -f ${PHP_INI}
		/bin/sed -i "/extension = ffmpeg.so/d" ${PHP_INI} 2>/dev/null 1>/dev/null
		/bin/sed -i "/^Include.*apache-msv2.conf/d" ${APACHE_CONF} 2>/dev/null 1>/dev/null
		/bin/sed -i "/^Include.*apache-musicstation.conf/d" ${APACHE_CONF} 2>/dev/null 1>/dev/null
		/bin/sed -i "/^Include.*apache-photo.conf/d" ${APACHE_CONF} 2>/dev/null 1>/dev/null
		[ x`/sbin/getcfg eaccelerator extension -f ${PHP_INI}` = x"eaccelerator.so" ] || /sbin/setcfg eaccelerator extension "eaccelerator.so" -f ${PHP_INI}
	fi
	VS_QPKG=`/sbin/getcfg VideoStation QPKG -u -d FALSE -f $APP_RUNTIME_CONF`
	VS_QPKG_Name=`/sbin/getcfg VideoStation Name -u -d X -f $QPKG_CONF`
	if [ x"$VS_QPKG" = xTRUE ]; then
		/bin/grep "^Include" ${APACHE_CONF} | /bin/grep "apache-video.conf" 1>>/dev/null 2>>/dev/null
		if [ $? != 0 ] && [ -f /etc/config/apache/extra/apache-video.conf ]; then
			/bin/echo "" >> ${APACHE_CONF}
			/bin/echo "Include /etc/config/apache/extra/apache-video.conf" >> ${APACHE_CONF}
		fi
	else
		/bin/sed -i "/^Include.*apache-video.conf/d" ${APACHE_CONF} 2>/dev/null 1>/dev/null
	fi
	if [ "x${PHP_ZEND_SUPPORT}" = "xTRUE" ]; then
		/bin/grep Zend ${PHP_INI} 2>/dev/null 1>/dev/null
		if [ $? != 0 ]; then
			/sbin/setcfg Zend zend_extension_manager.optimizer /usr/local/apache/modules/Zend/lib/Optimizer-3.3.0 -f ${PHP_INI}
			/sbin/setcfg Zend zend_extension_manager.optimizer_ts /usr/local/apache/modules/Zend/lib/Optimizer_TS-3.3.0 -f ${PHP_INI}
			/sbin/setcfg Zend zend_optimizer.version 3.3.0a -f ${PHP_INI}
			/sbin/setcfg Zend zend_extension /usr/local/apache/modules/Zend/lib/ZendExtensionManager.so -f ${PHP_INI}
			/sbin/setcfg Zend zend_extension_ts /usr/local/apache/modules/Zend/lib/ZendExtensionManager_TS.so -f ${PHP_INI}
		fi
	else
		/bin/grep Zend ${PHP_INI} 2>/dev/null 1>/dev/null
		if [ $? = 0 ]; then
			[ -x /sbin/rmcfg ] && /sbin/rmcfg Zend -f ${PHP_INI}
		fi
	fi
	
	if [ -f ${APACHE_CONF} ]; then
		/bin/grep "^DocumentRoot \"/share/${WebShare}\"" ${APACHE_CONF} 1>/dev/null 2>/dev/null
		[ $? = 0 ] || /bin/sed -i 's/^DocumentRoot.*$/'"DocumentRoot \"\/share\/${WebShare}\""'/g' ${APACHE_CONF}
		/bin/grep "^<Directory \"/share/${WebShare}\">" ${APACHE_CONF} 1>/dev/null 2>/dev/null
		[ $? = 0 ] || /bin/sed -i 's/^<Directory \"\/share\/Qweb.*$/'"<Directory \"\/share\/${WebShare}\">"'/' ${APACHE_CONF}
		if [ "x${CMS_ENABLE}" != xTRUE ]; then 
		if [ "x${BOOT_CONF}" = "xTS-NASARM" ]; then
			/bin/grep "^MinSpareServers 2" ${APACHE_CONF} 1>/dev/null 2>/dev/null
			[ $? = 0 ] || /bin/sed -i 's/^MinSpareServers.*$/'"MinSpareServers 2"'/g' ${APACHE_CONF}
			/bin/grep "^MaxSpareServers 5" ${APACHE_CONF} 1>/dev/null 2>/dev/null
			[ $? = 0 ] || /bin/sed -i 's/^MaxSpareServers.*$/'"MaxSpareServers 5"'/g' ${APACHE_CONF}
			/bin/grep "^StartServers 2" ${APACHE_CONF} 1>/dev/null 2>/dev/null
			[ $? = 0 ] || /bin/sed -i 's/^StartServers.*$/'"StartServers 2"'/g' ${APACHE_CONF}
			/bin/grep "^MaxClients 10" ${APACHE_CONF} 1>/dev/null 2>/dev/null
			[ $? = 0 ] || /bin/sed -i 's/^MaxClients.*$/'"MaxClients 10"'/g' ${APACHE_CONF}
			/bin/grep "^MaxRequestsPerChild 30" ${APACHE_CONF} 1>/dev/null 2>/dev/null
			[ $? = 0 ] || /bin/sed -i 's/^MaxRequestsPerChild.*$/'"MaxRequestsPerChild 30"'/g' ${APACHE_CONF}
		else
			maxClient=`/bin/grep MaxClients ${APACHE_CONF} | /bin/cut -d ' ' -f 2`
			if [ ! -z "$maxClient" ] && [ $maxClient -gt 50 ]; then
				/bin/sed -i 's/^MaxClients.*$/'"MaxClients 50"'/g' ${APACHE_CONF}
			fi
		fi
		if [ "x${CustomLog}" != xTRUE ]; then
			/bin/grep "^LogLevel" ${APACHE_CONF} | /bin/grep crit 1>>/dev/null 2>>/dev/null
			[ $? = 0 ] || /bin/sed -i 's/^LogLevel.*$/'"LogLevel crit"'/g' ${APACHE_CONF}
			/bin/grep "CustomLog" ${APACHE_CONF} 1>>/dev/null 2>>/dev/null
			[ $? != 0 ] || /bin/sed -i '/CustomLog/d' ${APACHE_CONF}
			/bin/grep "ErrorLog" ${APACHE_CONF} | /bin/grep "/dev/null" 1>>/dev/null 2>>/dev/null
			[ $? = 0 ] || /bin/sed -i 's/^ErrorLog.*$/'"ErrorLog \/dev\/null"'/g' ${APACHE_CONF}
		fi
		fi
	fi

	/bin/grep TraceEnable ${APACHE_CONF} 1>>/dev/null 2>>/dev/null
	if [ $? != 0 ]; then
		/bin/sed -i 's/^DocumentRoot/ServerTokens Prod\nTraceEnable off\nDocumentRoot/' ${APACHE_CONF}
		/bin/sed -i '/^.*UserDir public_html/d' ${APACHE_CONF}
	fi
	if [ -f ${APACHE_CONF_DIR}/extra/apache-ssl.conf ]; then
		/bin/grep "^DocumentRoot \"/share/${WebShare}\"" ${APACHE_CONF_DIR}/extra/apache-ssl.conf 1>/dev/null 2>/dev/null
		[ $? = 0 ] || /bin/sed -i 's/^DocumentRoot.*$/'"DocumentRoot \"\/share\/${WebShare}\""'/g' ${APACHE_CONF_DIR}/extra/apache-ssl.conf
		/bin/grep "^SSLCipherSuite ALL:\!SSLv2:\!LOW:\!EXPORT40:@STRENGTH" ${APACHE_CONF_DIR}/extra/apache-ssl.conf 1>/dev/null 2>/dev/null
		[ $? = 0 ] || /bin/sed -i 's/^SSLCipherSuite.*$/SSLCipherSuite ALL:!SSLv2:!LOW:!EXPORT40:@STRENGTH/' ${APACHE_CONF_DIR}/extra/apache-ssl.conf
		if [ x`/sbin/getcfg QWEB Enable_SSL -d 1` = x1 ]; then
			ssl_port=`/sbin/getcfg QWEB SSL_Port -d 8081`
			/bin/grep "^Listen ${ssl_port}$" ${APACHE_CONF_DIR}/extra/apache-ssl.conf 1>>/dev/null 2>>/dev/null
			[ $? = 0 ] || /bin/sed -i 's/^Listen .*$/'"Listen ${ssl_port}"'/g' ${APACHE_CONF_DIR}/extra/apache-ssl.conf
			/bin/grep "^<VirtualHost _default_:${ssl_port}>" ${APACHE_CONF_DIR}/extra/apache-ssl.conf 1>>/dev/null 2>>/dev/null
			[ $? = 0 ] || /bin/sed -i 's/^<VirtualHost _default_:.*$/'"<VirtualHost _default_:${ssl_port}>"'/g' ${APACHE_CONF_DIR}/extra/apache-ssl.conf
			/bin/grep "^Include" ${APACHE_CONF} | /bin/grep "apache-ssl.conf" 1>>/dev/null 2>>/dev/null
			if [ $? != 0 ]; then
				/bin/echo "" >> ${APACHE_CONF}
				/bin/echo "Include ${APACHE_CONF_DIR}/extra/apache-ssl.conf" >> ${APACHE_CONF}
			fi
		else
			/bin/grep "^Include" ${APACHE_CONF} | /bin/grep "apache-ssl.conf" 1>>/dev/null 2>>/dev/null
			[ $? != 0 ] || /bin/sed -i "/^Include.*apache-ssl.conf/d" ${APACHE_CONF} 2>/dev/null 1>/dev/null
		fi
	fi
	if [ -f ${APACHE_CONF_DIR}/extra/apache-dav.conf ]; then
		if [ x`/sbin/getcfg QWEB "Enable_WebDAV" -d 0` = x1 ]; then
			/bin/grep "^Include" ${APACHE_CONF} | /bin/grep "apache-dav.conf" 1>>/dev/null 2>>/dev/null
			if [ $? != 0 ]; then
				/bin/echo "" >> ${APACHE_CONF}
				/bin/echo "Include ${APACHE_CONF_DIR}/extra/apache-dav.conf" >> ${APACHE_CONF}
			fi
		else
			/bin/grep "^Include" ${APACHE_CONF} | /bin/grep "apache-dav.conf" 1>>/dev/null 2>>/dev/null
			[ $? != 0 ] || /bin/sed -i "/^Include.*apache-dav.conf/d" ${APACHE_CONF} 2>/dev/null 1>/dev/null
		fi
	fi
}

prepare_apache(){
	
	[ -d ${QEACCELERATOR_TMPDIR} ] || /bin/mkdir ${QEACCELERATOR_TMPDIR}
	#if [ "x${MSV2_SUPPORT}" != xTRUE ]; then
		/bin/mount | /bin/grep "${QEACCELERATOR_TMPDIR}"
		[ $? = 0 ] || /bin/mount -t tmpfs tmpfs ${QEACCELERATOR_TMPDIR} -o size=32M
	#fi
	/bin/chmod 777 ${QEACCELERATOR_TMPDIR}
	/bin/rm -rf ${QEACCELERATOR_TMPLINK} 1>>/dev/null 2>>/dev/null
	/bin/ln -sf ${QEACCELERATOR_TMPDIR} ${QEACCELERATOR_TMPLINK}
	if [ -f ${UPDATEPKG_DIR}/apache_php5.tgz ];then
		[ ! -f $APACHE_SOURCE ] || /bin/rm -f $APACHE_SOURCE
		APACHE_SOURCE="${UPDATEPKG_DIR}/apache_php5.tgz"
	fi			
	if [ ! -f ${APACHE_DIR}/bin/apachectl ] && [ -f $APACHE_SOURCE ]; then
		/bin/rm ${APACHE_DIR}/conf 1>>/dev/null 2>>/dev/null
		/bin/tar xzf $APACHE_SOURCE -C ${MNT_POINT}/opt
		/bin/ln -sf ${MNT_POINT}/opt/apache ${APACHE_DIR}
		/bin/sync
	fi
	if [ ! -d ${APACHE_CONF_DIR} ];then
			/bin/rm -rf ${APACHE_CONF_DIR} 2>/dev/null
			[ -d ${APACHE_DIR}/conf ] && /bin/cp -a ${APACHE_DIR}/conf ${APACHE_CONF_DIR}
	fi
	if [ "x${PHP_ZEND_SUPPORT}" = "xTRUE" ]; then
		if [ ! -d /usr/local/apache/modules/Zend ]; then
			/bin/tar xzf ${ZEND_SOURCE} -C ${MNT_POINT}/opt/apache/modules
		fi
	fi
	if [ -d ${APACHE_CONF_DIR} ] && [ -f ${APACHE_DIR}/bin/apachectl ]; then
		/usr/bin/readlink ${APACHE_DIR}/conf | grep config 1>>/dev/null 2>>/dev/null
		if [ $? != 0 ] && [ -d ${APACHE_CONF_DIR} ];then
			/bin/rm -rf ${APACHE_DIR}/conf
			/bin/ln -sf ${APACHE_CONF_DIR} ${APACHE_DIR}/conf
		fi
		#remove alias of Qmultimedia
		/bin/grep "Alias /Qmultimedia/" ${APACHE_CONF} 1>>/dev/null 2>>/dev/null
		if [ $? = 0 ]; then 
			/bin/grep -v "Alias /Qmultimedia/" ${APACHE_CONF} > ${APACHE_CONF}.tmp
			/bin/cp ${APACHE_CONF}.tmp ${APACHE_CONF}
		fi
		#copy extra apache conf files
		[ -d ${APACHE_CONF_DIR}/extra ] || /bin/mkdir ${APACHE_CONF_DIR}/extra
		[ -f ${APACHE_CONF_DIR}/extra/apache-dav.conf ] || /bin/cp /etc/default_config/apache-dav.conf ${APACHE_CONF_DIR}/extra/		
		[ -f ${APACHE_CONF_DIR}/extra/apache-ssl.conf ] || /bin/cp /etc/default_config/apache-ssl.conf ${APACHE_CONF_DIR}/extra/
		#use default apache-xxx.conf
		[ -f /etc/default_config/apache-msv2.conf ] && /bin/cp /etc/default_config/apache-msv2.conf ${APACHE_CONF_DIR}/extra/
		[ -f /etc/default_config/apache-musicstation.conf ] && /bin/cp /etc/default_config/apache-musicstation.conf ${APACHE_CONF_DIR}/extra/
		[ -f /etc/default_config/apache-photo.conf ] && /bin/cp /etc/default_config/apache-photo.conf ${APACHE_CONF_DIR}/extra/
		[ -f /etc/default_config/apache-video.conf ] && /bin/cp /etc/default_config/apache-video.conf ${APACHE_CONF_DIR}/extra/
		make_php_sess_base
		set_config
	fi
	if [ -f $APACHE_SOURCE ] && [ -d ${APACHE_CONF_DIR} ];then
		if [ ! -f ${APACHE_CONF_DIR}/magic ]; then
			/bin/tar xzf $APACHE_SOURCE -C /tmp apache/conf/magic
			/bin/cp /tmp/apache/conf/magic ${APACHE_CONF_DIR}
			/bin/rm -rf /tmp/apache
		fi
		/bin/grep "x-chrome-extension" ${APACHE_CONF_DIR}/mime.types 1>>/dev/null 2>>/dev/null
		if [ $? != 0 ] || [ ! -f ${APACHE_CONF_DIR}/mime.types ]; then
			/bin/tar xzf $APACHE_SOURCE -C /tmp apache/conf/mime.types
			/bin/cp /tmp/apache/conf/mime.types ${APACHE_CONF_DIR}
			/bin/rm -rf /tmp/apache
		fi
	fi
	/sbin/httpd_init
	if [ "x${CMS_ENABLE}" = xTRUE ]; then 
		if [ ! -d "/hd" ]; then
			volume="HDA_DATA"
			/usr/bin/readlink "/share/Public" 1>>/dev/null 2>>/dev/null;
			if [ $? = 0 ] && [ -d "/share/Public" ]; then
				volume_test=`/sbin/getcfg Public path -f /etc/smb.conf | cut -d '/' -f 3`
				[ "x${volume_test}" = "x" ] || volume=${volume_test}
			fi
			/bin/ln -sf /share/${volume} /hd
		fi
		[  -d "/hd/data" ] || /bin/mkdir -p -m 700 "/hd/data" 
		[  -d "/hd/logs" ] || /bin/mkdir -p -m 700 "/hd/logs" 
		[  -d "/hd/fcgi" ] || /bin/mkdir -p -m 700 "/hd/fcgi" 
		/bin/chown httpdusr "/hd/fcgi"
		if [ ! -d /fcgi ]; then
			/bin/ln -s /hd/fcgi /fcgi
		fi
		[  -d "/hd/notify" ] || /bin/mkdir -p -m 700 "/hd/notify" 
		[  -d "/hd/backup" ] || /bin/mkdir -p -m 700 "/hd/backup" 
		[  -d "/hd/data/interval" ] || /bin/mkdir -p -m 700 "/hd/data/interval" 
		[  -d "/hd/data/profile" ] || /bin/mkdir -p -m 700 "/hd/data/profile" 
		[  -d "/hd/data/rule" ] || /bin/mkdir -p -m 700 "/hd/data/rule" 
		[  -d "/hd/data/emap" ] || /bin/mkdir -p -m 700 "/hd/data/emap" 
		[  -d "/hd/data/bookmark" ] || /bin/mkdir -p -m 700 "/hd/data/bookmark" 
		[  -d "/hd/logs/system" ] || /bin/mkdir -p -m 700 "/hd/logs/system" 
		[  -d "/hd/logs/event" ] || /bin/mkdir -p -m 700 "/hd/logs/event" 
		[  -d "/hd/logs/service" ] || /bin/mkdir -p -m 700 "/hd/logs/service" 
		[  -d "/hd/logs/applica" ] || /bin/mkdir -p -m 700 "/hd/logs/applica" 
		/bin/echo " " > /hd/logs/debug
	fi
}

test -f /usr/local/sbin/Qthttpd || exit 0
[ -f ${ZEND_SOURCE} ] || /sbin/setcfg QWEB PHP_ZEND_SUPPORT FALSE -f /etc/default_config/uLinux.conf
PHP_ZEND_SUPPORT=`/sbin/getcfg QWEB PHP_ZEND_SUPPORT -d FALSE -f /etc/default_config/uLinux.conf`
[ "x${CMS_ENABLE}" = xTRUE ] && export LD_LIBRARY_PATH="/usr/local/apache/lib/apr-util-1"

case "$1" in
    start)
	if [ ! -z "$ramUse" ] && [ "$ramUse" = "100%" ]; then
		/bin/echo "Out-of-disk-space on ramdisk"
		/sbin/write_log "Out-of-disk-space on ramdisk. If restarting the server does not solve the problem please contact support for further assistance." 1
		exit 1
	fi
	prepare_apache
	echo -n "Starting Qthttpd services:"
	Web_Access_Port=`/sbin/getcfg System "Web Access Port" -d 8080`
	if [ `/sbin/getcfg "QWEB" "Enable" -d 1` = 0 ] && [ ${Web_Access_Port} != 80 ]; then
		[ -d "/home/Qhttpd/RSS" ] || /bin/ln -sf /home/httpd/RSS /home/Qhttpd/RSS
		[ -d "/home/Qhttpd/ajax_obj" ] || /bin/ln -sf /home/httpd/ajax_obj /home/Qhttpd/ajax_obj
		[ ! -f /home/Qhttpd/index.html ] || /bin/sed -i 's/+\":\"+.*+/'"+\":\"+${Web_Access_Port}+"'/' /home/Qhttpd/index.html
		/sbin/daemon_mgr Qthttpd start \
			"/usr/local/sbin/Qthttpd -p 80 -nor -nos -u admin -d /home/Qhttpd -c '**.*'"
		echo -n " Qthttpd"
		touch /var/lock/subsys/Qthttpd
		echo "."
	else
		check_qweb_link
		if [ ! -d ${SSMTP_CONF_DIR} ]; then
			[ ! -f ${SSMTP_CONF_DIR} ] || /bin/rm ${SSMTP_CONF_DIR}
			/bin/mkdir ${SSMTP_CONF_DIR}
			[ -d ${SSMTP_CONF_DEFAULT} ] && /bin/cp ${SSMTP_CONF_DEFAULT}/* ${SSMTP_CONF_DIR}
		fi
	# Start Apache
		if [ ! -d $QWEB_FOLDER ]; then
			#/sbin/write_log "Default share ${WebShare} is not found. Apache httpd start failed." 2
			/bin/echo "Default share ${WebShare} is not found. Apache httpd start failed."
			exit 1
		fi 
		if [ -d ${APACHE_CONF_DIR} ] && [ -f ${APACHE_DIR}/bin/apachectl ]; then
			clean_apache_log
			[ $? = 0 ] || echo "Some error occurred in clean_apache_log."
			if [ "x${CMS_ENABLE}" != xTRUE ]; then 
				/sbin/daemon_mgr Qthttpd stop /usr/local/sbin/Qthttpd
				/sbin/daemon_mgr apache start "${APACHE_DIR}/bin/apachectl start 1>/dev/null 2>/dev/null </dev/null"
				#${APACHE_DIR}/bin/apachectl start 1>/dev/null 2>/dev/null </dev/null
			else
				if [ -f /usr/local/cmsbin/cmsmsghandler ]; then
					/bin/pidof cmsmsghandler 1>>/dev/null 2>>/dev/null
					if [ $? = 0 ]; then
						/sbin/daemon_mgr cmsmsghandler stop "/usr/local/cmsbin/cmsmsghandler"
						/bin/sleep 10
						/bin/pidof cmsmsghandler 1>>/dev/null 2>>/dev/null
						if [ $? = 0 ]; then
							/bin/kill -9 `/bin/pidof cmsmsghandler` >& /dev/null
							/bin/sleep 10
						fi
					fi
				fi
				[ "x${PGSQL_ENABLE}" = xTRUE ] && [ -f /etc/init.d/pgsqld.sh ] && /etc/init.d/pgsqld.sh start 1>/dev/null 2>/dev/null 
				/bin/sleep 5
				[ -f /usr/local/cmsbin/dbgtrm ] && /usr/local/cmsbin/dbgtrm  1>/dev/null 2>/dev/null &
				[ -f /usr/local/cmsbin/initlog ] && /usr/local/cmsbin/initlog  1>/dev/null 2>/dev/null
				[ -f /usr/local/cmsbin/configapacheldap.sh ] && /usr/local/cmsbin/configapacheldap.sh  1>/dev/null 2>/dev/null
				/bin/sleep 3
				/sbin/daemon_mgr apache start "${APACHE_DIR}/bin/apachectl start >& /tmp/apalog </dev/null"
				/bin/sleep 3
				[ -f /usr/local/cmsbin/cmsmsghandler ] && /sbin/daemon_mgr cmsmsghandler start "/usr/local/cmsbin/cmsmsghandler  1>/dev/null 2>/dev/null &"
			fi
			j=0
			while [ $j -le 10 ]; do
				/bin/pidof apache 1>>/dev/null 2>>/dev/null
				if [ $? = 0 ]; then
					/bin/renice -2 `/bin/pidof apache` 1>>/dev/null 2>>/dev/null
					break
				fi
				sleep 1
				j=$(($j+1))
			done
		fi
	fi
	;;
    stop)
	echo -n "Shutting down Qthttpd services:" 
	/sbin/daemon_mgr Qthttpd stop /usr/local/sbin/Qthttpd
	rm -f /var/lock/subsys/Qthttpd
	echo -n " Qthttpd"
	echo "."
	# Stop Apache
	# due to daemon_mgr only kill one process of apache.....
	${APACHE_DIR}/bin/apachectl stop
	/bin/sleep 1
	/sbin/daemon_mgr apache stop "${APACHE_DIR}/bin/apachectl stop"
	[ $? = 0 ] && echo "Apache httpd is stopped."
	sleep 3
	/bin/pidof apache 1>>/dev/null 2>>/dev/null
	if [ $? = 0 ]; then
		/bin/kill `/bin/pidof apache`
		/bin/sleep 3
		/bin/pidof apache 1>>/dev/null 2>>/dev/null
		[ $? != 0 ] || /bin/kill -9 `/bin/pidof apache`
	fi
	if [ "x${CMS_ENABLE}" = xTRUE ]; then 
		/bin/pidof cmsmsghandler 1>>/dev/null 2>>/dev/null
		if [ $? = 0 ]; then
			/sbin/daemon_mgr cmsmsghandler stop "/usr/local/cmsbin/cmsmsghandler"
			/bin/sleep 3
			/bin/pidof cmsmsghandler 1>>/dev/null 2>>/dev/null
			if [ $? = 0 ]; then
				/bin/kill -9 `/bin/pidof cmsmsghandler` >& /dev/null
				/bin/sleep 3
			fi
		fi
		/bin/pidof dbgtrm 1>>/dev/null 2>>/dev/null
		if [ $? = 0 ]; then
			/bin/kill -9 `/bin/pidof dbgtrm` >& /dev/null
			/bin/sleep 3
		fi
		[ "x${PGSQL_ENABLE}" = xTRUE ] && [ -f /etc/init.d/pgsqld.sh ] && /etc/init.d/pgsqld.sh stop 1>/dev/null 2>/dev/null 
	fi
	/bin/umount ${QEACCELERATOR_TMPDIR} 1>>/dev/null 2>>/dev/null
	make_php_sess_base
	[ ! -d "${PHP_SESSION_PATH}" ] || /bin/rm -rf "${PHP_SESSION_PATH}"/*
	#ms/ps might not exist after downgrade
	if [ ! -z "$ramUse" ] && [ "$ramUse" = "100%" ]; then
		/bin/echo "Out-of-disk-space on ramdisk"
		/sbin/write_log "Out-of-disk-space on ramdisk. If restarting the server does not solve the problem please contact support for further assistance." 1
		exit 1
	fi
	/bin/cp "${APACHE_CONF}" "${APACHE_CONF}.tmp"
	/bin/sed -i "/^Include.*apache-msv2.conf/d" "${APACHE_CONF}.tmp" 2>/dev/null 1>/dev/null
	/bin/sed -i "/^Include.*apache-musicstation.conf/d" "${APACHE_CONF}.tmp" 2>/dev/null 1>/dev/null
	/bin/sed -i "/^Include.*apache-photo.conf/d" "${APACHE_CONF}.tmp" 2>/dev/null 1>/dev/null
	/bin/sed -i "/^Include.*apache-video.conf/d" "${APACHE_CONF}.tmp" 2>/dev/null 1>/dev/null
	[ -f /bin/awk ] && /bin/awk -F"xxxxxx" -v OFS="\n" ' $1=$1 ' ${APACHE_CONF}.tmp > ${APACHE_CONF}

	;;
    restart)
	$0 stop
	/bin/sleep 3
	$0 start
	;;
    reload)
	check_qweb_link
	/bin/kill -HUP `pidof Qthttpd`
	;;
    *)
        echo "Usage: /etc/init.d/$0 {start|stop|restart}"
        exit 1
esac

exit 0
