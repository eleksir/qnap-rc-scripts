#!/bin/sh

model_name=`/bin/cat /etc/config/BOOT.conf`
if [ "x$model_name" == "xTS-NASARM" ]; then
        partition="U-Boot Config"
        proc_mtd="/proc/mtd"
        base=0
        mtdno=`/bin/cat $proc_mtd | /bin/grep "$partition" | /bin/cut -d':' -f 1 | /bin/sed 's/mtd//g'`
        ubootcfg_r="/sbin/ubootcfg -b 0 -o - -f /dev/mtdblock${mtdno}"
        ubootcfg_w="/sbin/ubootcfg -b 0 -i - -f /dev/mtdblock${mtdno}"
        getucfg="/sbin/getmtd_ubootcfg.sh"
        setucfg="/sbin/setmtd_ubootcfg.sh"
        delucfg="/sbin/delmtd_ubootcfg.sh"
        chgbootcmd="cp.l 0xf8200000 0x800000 0x80000;cp.l 0xf8400000 0xa00000 0x240000;bootm 0x800000"
        cmd=`$ubootcfg_r | /bin/grep ^bootcmd= | /bin/cut -d '=' -f 2 | /bin/cut -d ' ' -f 1`

        if [ "x$cmd" == "xcp.b" ]; then
                $ubootcfg_r | sed "/^bootcmd=/ c bootcmd=$chgbootcmd" | $ubootcfg_w
                echo "bootcmd change ok"
        else
                echo "bootcmd check ok"
        fi
	# add pic watch dog timer 5 min, time out reboot
	chgbootcmd="uart1 0x68;cp.l 0xf8200000 0x800000 0x80000;cp.l 0xf8400000 0xa00000 0x240000;bootm 0x800000"
	cmd=`$ubootcfg_r | /bin/grep ^bootcmd= | /bin/cut -d '=' -f 2 | /bin/cut -d ' ' -f 1`
	if [ "x$cmd" == "xcp.l" ]; then
                $ubootcfg_r | sed "/^bootcmd=/ c bootcmd=$chgbootcmd" | $ubootcfg_w
                echo "bootcmd change ok"
        fi
fi

