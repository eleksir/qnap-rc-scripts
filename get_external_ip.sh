#!/bin/ash
#
# Script for getting an external WAN IP
# in my test, can't use below URL "http://www.whatismyipaddress.com" "http://ipid.shat.net" "http://www.showmyip.com"

#URLS="http://checkip.dyndns.org http://whatismyip.com http://www.edpsciences.com/htbin/ipaddress"
URLS="http://checkip.dyndns.org http://monip.net/ http://forum.qnap.com/mywanip.php"
Project_Name=`/sbin/getcfg Project Name -d null -f /var/default`
WGET=/bin/wget-s
TMP_FILE=/tmp/get_external_ip

[ -f ${WGET} ] || /bin/ln -sf /usr/bin/wget-s ${WGET} 1>/dev/null 2>/dev/null

#To prevent wget will not work
#${WGET} -T 1 -t 1 ftp://127.0.0.1 1>/dev/null 2>/dev/null

i=1;
for URL in $URLS
do
	${WGET} -4 -O ${TMP_FILE} -o /dev/null -T 19 -t 1 "${URL}" 2>/dev/null 1>/dev/null
	RET1=$?
	IP=`/bin/cat ${TMP_FILE} | /bin/tr -cs '[0-9\.]' '\012' | /bin/awk -F'.' 'NF==4 && $1>0 && $1<256 && $2<256 && $3<256 && $4<256 && !/\.\./' | /bin/uniq`
	RET2=$?
	if [ "x${IP}" != "x" ]; then
		if [ ${RET1} = 0 ] && [ ${RET2} = 0 ]; then
			echo $IP
			rm ${TMP_FILE}
			exit 0
		fi
	fi
done
exit 1
