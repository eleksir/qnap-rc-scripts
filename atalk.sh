#!/bin/sh
# chkconfig: - 91 35
# description: This package enables Linux to talk to Macintosh computers via the \
#              AppleTalk networking protocol. It includes a daemon to allow Linux \
#              to act as a file server over EtherTalk or IP for Mac's.
# processname: atalkd
# pidfile: /var/run/atalkd.pid
# config: /usr/local/etc/metatalk/*

ATALK_PATH=/usr/local/etc/netatalk
ZONE=`/sbin/getcfg Appletalk "Default Zone" -d "*"`

BOND_SUP=`/sbin/getcfg "Network" "BONDING Support" -d "FALSE"`
if [ "$BOND_SUP" = "TRUE" ]; then
	DEV="bond0"
else
	DEV="eth0"
fi

/bin/echo $DEV -zone "$ZONE" > $ATALK_PATH/atalkd.conf
# Source function library.
#. /etc/init.d/functions

# Source networking configuration.
#. /etc/sysconfig/network

# Source Appletalk configuration
. ${ATALK_PATH}/netatalk.conf

# Check that networking is up.
#[ ${NETWORKING} = "no" ] && exit 0

start() {
	if [ `/sbin/getcfg Appletalk Enable -u -d TRUE` = FALSE ]
	then
	    echo "Starting AppleTalk Services: disabled."
	    exit 0
	fi
	#if [ `/sbin/getcfg Appletalk Version -d 0` != 2.1 ]
	#then
	#	/sbin/_remove
	#	/sbin/setcfg Appletalk Version "2.1"
	#fi
	quota=`/sbin/getcfg TimeMachine Size -d 0 -f /etc/config/usr_quota.conf 2>/dev/null`
	if [ "$quota" != "0" ]; then
		/sbin/setcfg TimeMachine Size 0 -f /etc/config/usr_quota.conf > /dev/null 2>&1
		TMUID=`/bin/grep ^TimeMachine: /etc/passwd | /bin/awk -F ':' '{printf $3}'`
		QFMT=`/sbin/getcfg System "Quota Type" -d vfsold`
		if [ "$TMUID" != "" ]; then
			/sbin/setquota -u -F $QFMT $TMUID 0 0 0 0 -a > /dev/null 2>&1
		fi
	fi
	/sbin/mkafpconf > /dev/null 2>&1
	echo -n "Starting AppleTalk services: "
	/sbin/daemon_mgr afpd start "/usr/local/sbin/afpd -F /etc/afp.conf -p /var/afpd3.pid"
}
  
stop() {
	echo -n "Shutting down AppleTalk services: "
	/sbin/daemon_mgr afpd stop /usr/local/sbin/afpd
	/bin/sleep 1
	/bin/pidof afpd 1>>/dev/null 2>>/dev/null
	[ $? != 0 ] || /bin/kill -TERM `/bin/pidof afpd`
	echo ""
}


restart() {
	stop
	start
}

reload()
{
	[ -f /var/afpd3.pid ] && kill -HUP `cat /var/afpd3.pid 2>/dev/null`
}

case "$1" in
    start)
	start	
	;;
    stop)
	stop
	;;
    restart)
	restart
	;;
    reload)
	reload
	;;
    condrestart)
    [ -f /var/afpd3.pid ] && restart || : 
     ;;
    *)
	echo $"Usage: $0 {start|stop|restart|condrestart}"
	exit 1
esac

exit 0
