#/bin/sh
# Copyright @ 2009 QNAP Systems Inc.

GETCFG="/sbin/getcfg"
SETCFG="/sbin/setcfg"
DAEMON_MGR="/sbin/daemon_mgr"
volume_test="HDA_DATA"
volume="HDA_DATA"

CONFIG_FILE="/etc/rsyslog.conf"
MAILDIR="/var/syslog_maildir"
ret_val=0

init_settings()
{
	[ -d ${MAILDIR} ] || /bin/mkdir -p ${MAILDIR}
	/bin/mount | /bin/grep "${MAILDIR}"
	[ $? = 0 ] || /bin/mount -t tmpfs tmpfs ${MAILDIR} -o size=8M
	/bin/chmod 777 ${MAILDIR}
	
	/sbin/rsyslog_util init
	ret_val=$?
	[ -d "/tmp/rsyslog" ] || /bin/mkdir "/tmp/rsyslog"
}

start()
{
	[ -f "/usr/sbin/rsyslogd" ] || exit 1
	if [ `$GETCFG "Global" "Enable" -d "False" -f "/etc/config/syslog_server.conf"` != "TRUE" ]; then
		/bin/echo "rsyslogd is not Enabled."
		exit 1
	fi
	
	SyslogShareName=`$GETCFG "Global" "log file share" -d "Download" -f "/etc/config/syslog_server.conf"`
	SyslogSharePath=`$GETCFG "${SyslogShareName}" "path" -f "/etc/config/smb.conf"`
	if [ "x${SyslogSharePath}" = x ] || [ ! -d "${SyslogSharePath}" ]; then
		#/sbin/write_log "[Syslog Server] Log file path does not exist." 2
		/bin/echo "[Syslog Server] Log file path does not exist."
		exit 1
	fi
	SyslogFilePath=`$GETCFG "Global" "log file path" -d "" -f "/etc/config/syslog_server.conf"`
	if [ "x${SyslogFilePath}" != x ] && [ ! -f "${SyslogSharePath}/${SyslogFilePath}" ]; then
		/bin/echo "${SyslogSharePath}/${SyslogFilePath} does not exist."
		/bin/mkdir -p "${SyslogSharePath}/${SyslogFilePath}"
		/bin/rm -rf "${SyslogSharePath}/${SyslogFilePath}"
	fi
	
	if [ -f "/var/run/rsyslogd.pid" ]; then
		/bin/echo "rsyslogd aleardy running."
		exit 1
	fi
	/bin/echo $"Starting rsyslog: "
	init_settings
	
	if [ ${ret_val} != 0 ]; then
		/bin/echo "rsyslogd can't init."
		exit 1
	fi
	
	$DAEMON_MGR rsyslogd start "/usr/sbin/rsyslogd -f $CONFIG_FILE -c4 -M /usr/local/lib/rsyslog/" 
	$DAEMON_MGR syslog_maild start "/sbin/syslog_maild -d"
}

stop()
{
	/bin/echo $"Shutting down rsyslog: "

	$DAEMON_MGR rsyslogd stop "/usr/sbin/rsyslogd -f $CONFIG_FILE -c4 -M /usr/local/lib/rsyslog/"
	/bin/sleep 2
	if [ -f "/var/run/rsyslogd.pid" ]; then
		/bin/kill `/bin/cat /var/run/rsyslogd.pid` 1>>/dev/null 2>>/dev/null
		/bin/kill `/sbin/pidof rsyslogd` 1>>/dev/null 2>>/dev/null
		/bin/sleep 2
		/bin/rm -f /var/run/rsyslogd.pid
	fi
	$DAEMON_MGR syslog_maild stop "/sbin/syslog_maild -d"
	/bin/kill `/bin/pidof syslog_maild` 1>>/dev/null 2>>/dev/null
}

restart()
{
	stop
	/bin/sleep 1
	start
}

reload()
{
	/bin/echo $"Reloading rsyslog: "
	if [ -f "/var/run/rsyslogd.pid" ]; then
		/sbin/rsyslog_util init
		/bin/sleep 1
		/bin/kill -SIGHUP `/bin/cat /var/run/rsyslogd.pid`
	fi
}

case "$1" in
start)
	start
	;;

stop)
	stop
	;;

restart)
	restart
	;;

reload)
	reload
	;;
	
*)
	/bin/echo $"Usage: $0 {start|stop|restart|reload}"
	exit 1
esac

exit 0
