#!/bin/sh

IDMAP_MAX=4
NET=/usr/local/samba/bin/net
IDMAP_TDB=/usr/local/samba/var/locks/winbindd_idmap.tdb

volume_test=`/sbin/getcfg Public path -f /etc/smb.conf | cut -d '/' -f 3`
[ "x${volume_test}" = "x" ] || volume=${volume_test}
LOCATION=/share/${volume}/.idmap
if [ ! -d ${LOCATION} ]; then
	/bin/mkdir ${LOCATION}
fi
PREFIX1=$LOCATION/.idmap.dump
PREFIX2=$LOCATION/.idmap.leave

check_ADS()
{
	/bin/grep "security = ADS" /etc/config/smb.conf 2>/dev/null 1>/dev/null
	if [ $? != 0 ]; then
		echo "ADS off"
		exit 1
	fi
}

case "$1" in
	leave_dump)
		check_ADS
		_latest_no=`/sbin/getcfg Samba "idmap leave" -d 0`
		if [ "x$_latest_no" = "x$IDMAP_MAX" ]; then
			_latest_no=1
		else
			_latest_no=`/usr/bin/expr $_latest_no + 1`
		fi
		/sbin/setcfg Samba "idmap leave" $_latest_no
		$NET idmap dump $IDMAP_TDB > $PREFIX2$_latest_no
	;;
	dump)
		check_ADS
		_latest_no=`/sbin/getcfg Samba "idmap dump" -d 0`
		if [ "x$_latest_no" = "x$IDMAP_MAX" ]; then
			_latest_no=1
		else
			_latest_no=`/usr/bin/expr $_latest_no + 1`
		fi
		/sbin/setcfg Samba "idmap dump" $_latest_no
		$NET idmap dump $IDMAP_TDB > $PREFIX1$_latest_no
	;;
	restore)
		if [ "x$2" = "x" ]; then
			echo $0 restore [idmap_file]
			exit 1
		fi
		$NET idmap restore < $2
		/bin/rm -f /usr/local/samba/var/locks/gencache.tdb 2>/dev/null 1>/dev/null
		/etc/init.d/winbind restart 2>/dev/null 1>/dev/null
	;;
	list)
		echo PATH = [$LOCATION]
		/bin/ls -al $LOCATION
	;;
	*)
	echo "Usage: $0 {leave_dump|dump|restore|list}"
        exit 1
esac

exit 0
