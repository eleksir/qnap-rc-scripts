#!/bin/sh

LIBTRASH="/etc/config/libtrash.conf"
DEF_LIBTRASH="/etc/default_config/libtrash.conf"

case "$1" in
    start)
	if [ ! -f ${LIBTRASH} ]; then
		/bin/cp -fp ${DEF_LIBTRASH} ${LIBTRASH} 1>>/dev/null 2>>/dev/null
	fi

	if [ `/sbin/getcfg "Network Recycle Bin" Enable -u -d FALSE` = TRUE ]; then
        	echo -n "Starting recycled services:"

		/sbin/getcfg "Network Recycle Bin 1" public -f /etc/config/smb.conf
                result=$?
                if [ $result -eq 0 ]
                then
                        /sbin/setcfg "Network Recycle Bin 1" "veto files" "" -f /etc/config/smb.conf
                fi
                /sbin/getcfg "Network Recycle Bin 2" public -f /etc/config/smb.conf
                result=$?
                if [ $result -eq 0 ]
                then
                        /sbin/setcfg "Network Recycle Bin 2" "veto files" "" -f /etc/config/smb.conf
                fi
		/sbin/getcfg "Network Recycle Bin 3" public -f /etc/config/smb.conf
                result=$?
                if [ $result -eq 0 ]
                then
                        /sbin/setcfg "Network Recycle Bin 3" "veto files" "" -f /etc/config/smb.conf
                fi
                /sbin/getcfg "Network Recycle Bin 4" public -f /etc/config/smb.conf
                result=$?
                if [ $result -eq 0 ]
                then
                        /sbin/setcfg "Network Recycle Bin 4" "veto files" "" -f /etc/config/smb.conf
                fi
		/sbin/getcfg "Network Recycle Bin 5" public -f /etc/config/smb.conf
                result=$?
                if [ $result -eq 0 ]
                then
                        /sbin/setcfg "Network Recycle Bin 5" "veto files" "" -f /etc/config/smb.conf
                fi
		/sbin/getcfg "Network Recycle Bin 6" public -f /etc/config/smb.conf
                result=$?
                if [ $result -eq 0 ]
                then
                        /sbin/setcfg "Network Recycle Bin 6" "veto files" "" -f /etc/config/smb.conf
                fi
                /sbin/getcfg "Network Recycle Bin 7" public -f /etc/config/smb.conf
                result=$?
                if [ $result -eq 0 ]
                then
                        /sbin/setcfg "Network Recycle Bin 7" "veto files" "" -f /etc/config/smb.conf
                fi
                /sbin/getcfg "Network Recycle Bin 8" public -f /etc/config/smb.conf
                result=$?
                if [ $result -eq 0 ]
                then
                        /sbin/setcfg "Network Recycle Bin 8" "veto files" "" -f /etc/config/smb.conf
                fi
		#/usr/bin/killall -SIGHUP smbd
		[ x`/sbin/getcfg Misc Configured -u -d FALSE` = xTRUE ] && /etc/init.d/smb.sh restart 1>>/dev/null 2>>/dev/null
                #/sbin/daemon_mgr recycled start "/sbin/recycled"
		[ -f /sbin/gpiod ] && /bin/kill -USR1 `pidof gpiod`
		if [ `/sbin/getcfg AppleTalk Enable` = TRUE ] && [ -f /tmp/.boot_done ]; then
			echo -n "restart atalk...."
			/etc/init.d/atalk.sh restart 1>>/dev/null 2>>/dev/null
		fi
                echo -n " recycled"
                echo "."
	fi
        ;;
    stop)
        echo -n "Shutting down recycled services:"
		[ -f /sbin/gpiod ] && /bin/kill -USR1 `pidof gpiod`
        #/sbin/daemon_mgr recycled stop /sbin/recycled
        echo -n " recycled"
        echo "."
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    *)
        echo "Usage: /etc/init.d/recycled {start|stop|restart}"
        exit 1
esac

exit 0
