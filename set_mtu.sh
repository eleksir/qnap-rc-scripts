#!/bin/sh
GETCFG="/sbin/getcfg"
SETCFG="/sbin/setcfg"
BOND_SUP=`$GETCFG "Network" "BONDING Support" -d "FALSE"`
IF_NUM=`$GETCFG "Network" "Interface Number" -d "1"`

set_mtu()
{
        /bin/sleep 7
        local MTU=`$GETCFG eth0 MTU -d 1500`
        /bin/echo "[eth0]" > /tmp/NowSpeed
        /sbin/eth_util eth0 >> /tmp/NowSpeed
        local SPEED=`$GETCFG eth0 "Speed of eth0" -f /tmp/NowSpeed`
        if [ "$SPEED" = "1000" ]; then
                /sbin/ifconfig eth0 mtu $MTU
        else
                /sbin/ifconfig eth0 mtu 1500
                if [ ! "$MTU" = "1500" ]; then
                        $SETCFG eth0 MTU 1500
                        /sbin/write_log "Jumbo Frame is disabled because the connection speed is not Gigabit." 1
                fi
        fi
}


set_mtu_2lan()
{
/bin/sleep 12
if [ "$BOND_SUP" = "TRUE" ]; then
	local MTU=`$GETCFG bond0 MTU -d 1500`
	local STATUS1=`/bin/cat /sys/class/net/eth0/operstate`
	local STATUS2=`/bin/cat /sys/class/net/eth1/operstate`
	if [ "$STATUS1" = "up" ]; then
		/bin/echo "[eth0]" > /tmp/NowSpeed
		/sbin/eth_util eth0 >> /tmp/NowSpeed
		local SPEED=`$GETCFG eth0 "Speed of eth0" -f /tmp/NowSpeed`
	elif [ "$STATUS2" = "up" ]; then
		/bin/echo "[eth1]" > /tmp/NowSpeed
		/sbin/eth_util eth1 >> /tmp/NowSpeed
		local SPEED=`$GETCFG eth1 "Speed of eth1" -f /tmp/NowSpeed`
	else
		local SPEED=`$GETCFG bond0 Speed -d "auto"`
		/bin/echo "[Lan 1] $STATUS1 [Lan2] $STATUS2 [bond0 Speed] $SPEED"
		if [ "$SPEED" = "auto" ]; then
			SPEED="1000"
		fi
	fi
	if [ "$SPEED" = "1000" ]; then
		/sbin/ifconfig bond0 mtu $MTU
	else
		/sbin/ifconfig bond0 mtu 1500
	fi
	if [ ! "$SPEED" = "1000" ] && [ ! "$MTU" = "1500" ]; then
		`$SETCFG bond0 MTU 1500`
		/sbin/write_log "Jumbo Frame is disabled because the connection speed is not Gigabit." 1
	fi
else
	local STATUS=`/bin/cat /sys/class/net/eth0/operstate`
	if [ "$STATUS" = "up" ]; then
		local MTU=`$GETCFG eth0 MTU -d 1500`
		/bin/echo "[eth0]" > /tmp/NowSpeed
		/sbin/eth_util eth0 >> /tmp/NowSpeed
		local SPEED=`$GETCFG eth0 "Speed of eth0" -f /tmp/NowSpeed`
		if [ "$SPEED" = "1000" ]; then
			/sbin/ifconfig eth0 mtu $MTU
		else
			/sbin/ifconfig eth0 mtu 1500
		fi
		if [ ! "$SPEED" = "1000" ] && [ ! "$MTU" = "1500" ]; then
			`$SETCFG eth0 MTU 1500`
			/sbin/write_log "LAN 1 Jumbo Frame is disabled because the connection speed is not Gigabit." 1
		fi
	fi
	STATUS=`/bin/cat /sys/class/net/eth1/operstate`
	if [ "$STATUS" = "up" ]; then
		MTU=`$GETCFG eth1 MTU -d 1500`
		/bin/echo "[eth1]" >> /tmp/NowSpeed
		/sbin/eth_util eth1 >> /tmp/NowSpeed
		SPEED=`$GETCFG eth1 "Speed of eth1" -f /tmp/NowSpeed`
		if [ "$SPEED" = "1000" ]; then
			/sbin/ifconfig eth1 mtu $MTU
		else
			/sbin/ifconfig eth1 mtu 1500
		fi
		if [ ! "$SPEED" = "1000" ] && [ ! "$MTU" = "1500" ]; then
			`$SETCFG eth1 MTU 1500`
			/sbin/write_log "LAN 2 Jumbo Frame is disabled because the connection speed is not Gigabit." 1
		fi
	fi
fi
}

set_mtu_4lan()
{
                /bin/sleep 12
                if [ "$BOND_SUP" = "TRUE" ]; then
                        local MTU=`$GETCFG bond0 MTU -d 1500`
                        local MTU1=`$GETCFG bond1 MTU -d 1500`
                        local STATUS1=`/bin/cat /sys/class/net/eth0/operstate`
                        local STATUS2=`/bin/cat /sys/class/net/eth1/operstate`
                        local STATUS3=`/bin/cat /sys/class/net/eth2/operstate`
                        local STATUS4=`/bin/cat /sys/class/net/eth3/operstate`
                        /bin/rm /tmp/NowSpeed
                        if [ "$BOND_NIC" = "0" ]; then
                                if [ "$STATUS1" = "up" ]; then
                                        /bin/echo "[eth0]" >> /tmp/NowSpeed
                                        /sbin/eth_util eth0 >> /tmp/NowSpeed
                                        local SPEED=`$GETCFG eth0 "Speed of eth0" -f /tmp/NowSpeed`
                                elif [ "$STATUS2" = "up" ]; then
                                        /bin/echo "[eth1]" >> /tmp/NowSpeed
                                        /sbin/eth_util eth1 >> /tmp/NowSpeed
                                        local SPEED=`$GETCFG eth1 "Speed of eth1" -f /tmp/NowSpeed`
                                else
                                        local SPEED=`$GETCFG bond0 Speed -d "auto"`
                                        /bin/echo "[Lan 1] $STATUS1 [Lan2] $STATUS2 [bond0 Speed] $SPEED"
                                        if [ "$SPEED" = "auto" ]; then
                                                SPEED="1000"
                                        fi
                                fi
                                if [ "$SPEED" = "100" ]; then
                                        /sbin/ifconfig bond0 mtu 1500
                                else
                                        /sbin/ifconfig bond0 mtu $MTU
                                fi
                                if [ "$SPEED" = "100" ] && [ ! "$MTU" = "1500" ]; then
                                        `$SETCFG bond0 MTU 1500`
                                        /sbin/write_log "Jumbo Frame is disabled because the connection speed is not Gigabit." 1
                                fi
                                if [ "$NIC_NUM" = "4" ]; then
                                        STATUS=`/bin/cat /sys/class/net/eth2/operstate`
                                        if [ "$STATUS" = "up" ]; then
                                                MTU=`$GETCFG eth2 MTU -d 1500`
                                                /bin/echo "[eth2]" >> /tmp/NowSpeed
                                                /sbin/eth_util eth2 >> /tmp/NowSpeed
                                                SPEED=`$GETCFG eth2 "Speed of eth2" -f /tmp/NowSpeed`
                                                if [ "$SPEED" = "100" ]; then
                                                        /sbin/ifconfig eth2 mtu 1500
                                                else
                                                        /sbin/ifconfig eth2 mtu $MTU
                                                fi
                                                if [ "$SPEED" = "100" ] && [ ! "$MTU" = "1500" ]; then
                                                        `$SETCFG eth2 MTU 1500`
                                                        /sbin/write_log "LAN 3 Jumbo Frame is disabled because the connection speed is not Gigabit." 1
                                                fi
                                        fi
                                        STATUS=`/bin/cat /sys/class/net/eth3/operstate`
                                        if [ "$STATUS" = "up" ]; then
                                                MTU=`$GETCFG eth3 MTU -d 1500`
                                                /bin/echo "[eth3]" >> /tmp/NowSpeed
                                                /sbin/eth_util eth3 >> /tmp/NowSpeed
                                                SPEED=`$GETCFG eth3 "Speed of eth3" -f /tmp/NowSpeed`
                                                if [ "$SPEED" = "100" ]; then
                                                        /sbin/ifconfig eth3 mtu 1500
                                                else
                                                        /sbin/ifconfig eth3 mtu $MTU
                                                fi
                                                if [ "$SPEED" = "100" ] && [ ! "$MTU" = "1500" ]; then
                                                        `$SETCFG eth1 MTU 1500`
                                                        /sbin/write_log "LAN 4 Jumbo Frame is disabled because the connection speed is not Gigabit." 1
                                                fi
                                        fi
                                fi
                        elif [ "$BOND_NIC" = "1" ]; then
                                STATUS=`/bin/cat /sys/class/net/eth0/operstate`
                                /bin/rm /tmp/NowSpeed
                                if [ "$STATUS" = "up" ]; then
                                        MTU=`$GETCFG eth0 MTU -d 1500`
                                        /bin/echo "[eth0]" >> /tmp/NowSpeed
                                        /sbin/eth_util eth0 >> /tmp/NowSpeed
                                        SPEED=`$GETCFG eth0 "Speed of eth0" -f /tmp/NowSpeed`
                                        if [ "$SPEED" = "100" ]; then
                                                /sbin/ifconfig eth0 mtu 1500
                                        else
                                                /sbin/ifconfig eth0 mtu $MTU
                                        fi
                                        if [ "$SPEED" = "100" ] && [ ! "$MTU" = "1500" ]; then
                                                `$SETCFG eth0 MTU 1500`
                                                /sbin/write_log "LAN 1 Jumbo Frame is disabled because the connection speed is not Gigabit." 1
                                        fi
                                fi
                                STATUS=`/bin/cat /sys/class/net/eth1/operstate`
                                if [ "$STATUS" = "up" ]; then
                                        MTU=`$GETCFG eth1 MTU -d 1500`
                                        /bin/echo "[eth1]" >> /tmp/NowSpeed
                                        /sbin/eth_util eth1 >> /tmp/NowSpeed
                                        SPEED=`$GETCFG eth1 "Speed of eth1" -f /tmp/NowSpeed`
                                        if [ "$SPEED" = "100" ]; then
                                                /sbin/ifconfig eth1 mtu 1500
                                        else
                                                /sbin/ifconfig eth1 mtu $MTU
                                        fi
                                        if [ "$SPEED" = "100" ] && [ ! "$MTU" = "1500" ]; then
                                                `$SETCFG eth1 MTU 1500`
                                                /sbin/write_log "LAN 2 Jumbo Frame is disabled because the connection speed is not Gigabit." 1
                                        fi
                                fi
                                MTU=`$GETCFG bond0 MTU -d 1500`
                                if [ "$STATUS3" = "up" ]; then
                                        /bin/echo "[eth2]" >> /tmp/NowSpeed
                                        /sbin/eth_util eth2 >> /tmp/NowSpeed
                                        local SPEED=`$GETCFG eth2 "Speed of eth2" -f /tmp/NowSpeed`
                                elif [ "$STATUS4" = "up" ]; then
                                        /bin/echo "[eth3]" >> /tmp/NowSpeed
                                        /sbin/eth_util eth3 >> /tmp/NowSpeed
                                        local SPEED=`$GETCFG eth3 "Speed of eth3" -f /tmp/NowSpeed`
                                else
                                        local SPEED=`$GETCFG bond0 Speed -d "auto"`
                                        /bin/echo "[Lan 3] $STATUS3 [Lan4] $STATUS4 [bond0 Speed] $SPEED"
                                        if [ "$SPEED" = "auto" ]; then
                                                SPEED="1000"
                                        fi
                                fi
                                if [ "$SPEED" = "100" ]; then
                                        /sbin/ifconfig bond0 mtu 1500
                                else
                                        /sbin/ifconfig bond0 mtu $MTU
                                fi
                                if [ "$SPEED" = "100" ] && [ ! "$MTU" = "1500" ]; then
                                        `$SETCFG bond0 MTU 1500`
                                        /sbin/write_log "Jumbo Frame is disabled because the connection speed is not Gigabit." 1
                                fi
                        elif [ "$BOND_NIC" = "2" ]; then
                                if [ "$STATUS1" = "up" ]; then
                                        /bin/echo "[eth0]" >> /tmp/NowSpeed
                                        /sbin/eth_util eth0 >> /tmp/NowSpeed
                                        local SPEED=`$GETCFG eth0 "Speed of eth0" -f /tmp/NowSpeed`
                                elif [ "$STATUS2" = "up" ]; then
                                        /bin/echo "[eth1]" >> /tmp/NowSpeed
                                        /sbin/eth_util eth1 >> /tmp/NowSpeed
                                        local SPEED=`$GETCFG eth1 "Speed of eth1" -f /tmp/NowSpeed`
                                else
                                        local SPEED=`$GETCFG bond0 Speed -d "auto"`
                                        /bin/echo "[Lan 1] $STATUS1 [Lan2] $STATUS2 [bond0 Speed] $SPEED"
                                        if [ "$SPEED" = "auto" ]; then
                                                SPEED="1000"
                                        fi
                                fi
                                if [ "$SPEED" = "100" ]; then
                                        /sbin/ifconfig bond0 mtu 1500
                                else
                                        /sbin/ifconfig bond0 mtu $MTU
                                fi
                                if [ "$SPEED" = "100" ] && [ ! "$MTU" = "1500" ]; then
                                        `$SETCFG bond0 MTU 1500`
                                        /sbin/write_log "Jumbo Frame is disabled because the connection speed is not Gigabit." 1
                                fi

                                if [ "$STATUS3" = "up" ]; then
                                        /bin/echo "[eth2]" >> /tmp/NowSpeed
                                        /sbin/eth_util eth2 >> /tmp/NowSpeed
                                        local SPEED=`$GETCFG eth2 "Speed of eth2" -f /tmp/NowSpeed`
                                elif [ "$STATUS4" = "up" ]; then
                                        /bin/echo "[eth3]" >> /tmp/NowSpeed
                                        /sbin/eth_util eth3 >> /tmp/NowSpeed
                                        local SPEED=`$GETCFG eth3 "Speed of eth3" -f /tmp/NowSpeed`
                                else
                                        local SPEED=`$GETCFG bond0 Speed -d "auto"`
                                        /bin/echo "[Lan 3] $STATUS3 [Lan4] $STATUS4 [bond1 Speed] $SPEED"
                                        if [ "$SPEED" = "auto" ]; then
                                                SPEED="1000"
                                        fi
                                fi
                                if [ "$SPEED" = "100" ]; then
                                        /sbin/ifconfig bond1 mtu 1500
                                else
                                        /sbin/ifconfig bond1 mtu $MTU1
                                fi
                                if [ "$SPEED" = "100" ] && [ ! "$MTU1" = "1500" ]; then
                                        `$SETCFG bond1 MTU 1500`
                                        /sbin/write_log "Jumbo Frame is disabled because the connection speed is not Gigabit." 1
                                fi
                        elif [ "$BOND_NIC" = "3" ]; then
                                if [ "$STATUS1" = "up" ]; then
                                        /bin/echo "[eth0]" >> /tmp/NowSpeed
                                        /sbin/eth_util eth0 >> /tmp/NowSpeed
                                        local SPEED=`$GETCFG eth0 "Speed of eth0" -f /tmp/NowSpeed`
                                elif [ "$STATUS2" = "up" ]; then
                                        /bin/echo "[eth1]" > /tmp/NowSpeed
                                        /sbin/eth_util eth1 >> /tmp/NowSpeed
                                        local SPEED=`$GETCFG eth1 "Speed of eth1" -f /tmp/NowSpeed`
                                elif [ "$STATUS3" = "up" ]; then
                                        /bin/echo "[eth2]" > /tmp/NowSpeed
                                        /sbin/eth_util eth2 >> /tmp/NowSpeed
                                        local SPEED=`$GETCFG eth2 "Speed of eth2" -f /tmp/NowSpeed`
                                elif [ "$STATUS4" = "up" ]; then
                                        /bin/echo "[eth3]" > /tmp/NowSpeed
                                        /sbin/eth_util eth3 >> /tmp/NowSpeed
                                        local SPEED=`$GETCFG eth3 "Speed of eth3" -f /tmp/NowSpeed`
                                else
                                        local SPEED=`$GETCFG bond0 Speed -d "auto"`
                                        /bin/echo "[Lan 1] $STATUS1 [Lan2] $STATUS2 [bond0 Speed] $SPEED"
                                        if [ "$SPEED" = "auto" ]; then
                                                SPEED="1000"
                                        fi
                                fi
                                if [ "$SPEED" = "100" ]; then
                                        /sbin/ifconfig bond0 mtu 1500
                                else
                                        /sbin/ifconfig bond0 mtu $MTU
                                fi
                                if [ "$SPEED" = "100" ] && [ ! "$MTU" = "1500" ]; then
                                        `$SETCFG bond0 MTU 1500`
                                        /sbin/write_log "Jumbo Frame is disabled because the connection speed is not Gigabit." 1
                                fi
                        else
                                /bin/echo "network error: BONDING NICs unmatched"
                        fi

                else
                        local STATUS=`/bin/cat /sys/class/net/eth0/operstate`
                        /bin/rm /tmp/NowSpeed
                        if [ "$STATUS" = "up" ]; then
                                MTU=`$GETCFG eth0 MTU -d 1500`
                                /bin/echo "[eth0]" >> /tmp/NowSpeed
                                /sbin/eth_util eth0 >> /tmp/NowSpeed
                                SPEED=`$GETCFG eth0 "Speed of eth0" -f /tmp/NowSpeed`
                                if [ "$SPEED" = "100" ]; then
                                        /sbin/ifconfig eth0 mtu 1500
                                else
                                        /sbin/ifconfig eth0 mtu $MTU
                                fi
                                if [ "$SPEED" = "100" ] && [ ! "$MTU" = "1500" ]; then
                                        `$SETCFG eth0 MTU 1500`
                                        /sbin/write_log "LAN 1 Jumbo Frame is disabled because the connection speed is not Gigabit." 1
                                fi
                        fi
                        STATUS=`/bin/cat /sys/class/net/eth1/operstate`
                        if [ "$STATUS" = "up" ]; then
                                MTU=`$GETCFG eth1 MTU -d 1500`
                                /bin/echo "[eth1]" >> /tmp/NowSpeed
                                /sbin/eth_util eth1 >> /tmp/NowSpeed
                                SPEED=`$GETCFG eth1 "Speed of eth1" -f /tmp/NowSpeed`
                                if [ "$SPEED" = "100" ]; then
                                        /sbin/ifconfig eth1 mtu 1500
                                else
                                        /sbin/ifconfig eth1 mtu $MTU
                                fi
                                if [ "$SPEED" = "100" ] && [ ! "$MTU" = "1500" ]; then
                                        `$SETCFG eth1 MTU 1500`
                                        /sbin/write_log "LAN 2 Jumbo Frame is disabled because the connection speed is not Gigabit." 1
                                fi
                        fi
                        if [ "$NIC_NUM" = "4" ]; then
                                STATUS=`/bin/cat /sys/class/net/eth2/operstate`
                                if [ "$STATUS" = "up" ]; then
                                        MTU=`$GETCFG eth2 MTU -d 1500`
                                        /bin/echo "[eth2]" >> /tmp/NowSpeed
                                        /sbin/eth_util eth2 >> /tmp/NowSpeed
                                        SPEED=`$GETCFG eth2 "Speed of eth2" -f /tmp/NowSpeed`
                                        if [ "$SPEED" = "100" ]; then
                                                /sbin/ifconfig eth2 mtu 1500
                                        else
                                                /sbin/ifconfig eth2 mtu $MTU
                                        fi
                                        if [ "$SPEED" = "100" ] && [ ! "$MTU" = "1500" ]; then
                                                `$SETCFG eth2 MTU 1500`
                                                /sbin/write_log "LAN 3 Jumbo Frame is disabled because the connection speed is not Gigabit." 1
                                        fi
                                fi
                                STATUS=`/bin/cat /sys/class/net/eth3/operstate`
                                if [ "$STATUS" = "up" ]; then
                                        MTU=`$GETCFG eth3 MTU -d 1500`
                                        /bin/echo "[eth3]" >> /tmp/NowSpeed
                                        /sbin/eth_util eth3 >> /tmp/NowSpeed
                                        SPEED=`$GETCFG eth3 "Speed of eth3" -f /tmp/NowSpeed`
                                        if [ "$SPEED" = "100" ]; then
                                                /sbin/ifconfig eth3 mtu 1500
                                        else
                                                /sbin/ifconfig eth3 mtu $MTU
                                        fi
                                        if [ "$SPEED" = "100" ] && [ ! "$MTU" = "1500" ]; then
                                                `$SETCFG eth1 MTU 1500`
                                                /sbin/write_log "LAN 4 Jumbo Frame is disabled because the connection speed is not Gigabit." 1
                                        fi
                                fi
                        fi
                fi
}

case $IF_NUM in
1)
	set_mtu
        ;;
2)
	set_mtu_2lan
        ;;
4)
	set_mtu_4lan
	;;
*)
	echo "IF_NUM = $IF_NUM ,not support"
        exit 1
esac

exit 0
