#!/bin/sh
GETCFG="/sbin/getcfg"
SETCFG="/sbin/setcfg"
DEFAULT_NIC_NUM=`$GETCFG "Network" "Interface Number" -f /etc/default_config/uLinux.conf`
CONFIG_NIC_NUM=`$GETCFG "Network" "Interface Number"`
ADV_BOND_SUPPORT=`$GETCFG "Network" "Advanced Bonding Support" -u -d "FALSE"`
LAN_CARD_10G=(808610fb 8086151c 19a20710 80861528)
LAN_CARD_1G=(808610c9 808610a7)
no_extra_lan_card="TRUE"
LAN_CARD_10G_V2=(8086:10FB 8086:151C 19A2:0710 8086:1528)
LAN_CARD_1G_V2=(8086:10C9 8086:10A7)
EXTRA_LAN_CARD_PORT_NUM=0
have_10G_extra_lan_card="FALSE"
have_1G_extra_lan_card="FALSE"
MAX_EXTRA_LAN_CARD_PORT_NUM=8

# Patch for build-in 4Lan TS-879U(Ivy Bridge)
Model_NameX=`/bin/cat /etc/default_config/uLinux.conf | grep "^Model =" | cut -d ' ' -f3 2>/dev/null`
if [ "x${Model_NameX}" = "xTS-1279" ] || [ "x${Model_NameX}" = "xTS-879U" ] || [ "x${Model_NameX}" = "xTS-1279U" ] || [ "x${Model_NameX}" = "xTS-1279-G" ] || [ "x${Model_NameX}" = "xTS-879U-G" ] || [ "x${Model_NameX}" = "xTS-1279U-G" ]; then
    if [ -e /proc/tsinfo/hw_version ];then
        Board=$(cat /proc/tsinfo/hw_version | cut -d ' ' -f2)
        if [ $((Board & 2)) -eq 2 ];then
            NIC_NUM=4
            `$SETCFG "Network" "Interface Number" "$NIC_NUM"
            `$SETCFG "Network" "Interface Number" "$NIC_NUM" -f /etc/default_config/uLinux.conf
        fi
    fi
fi
DEFAULT_NIC_NUM=`$GETCFG "Network" "Interface Number" -f /etc/default_config/uLinux.conf`
CONFIG_NIC_NUM=`$GETCFG "Network" "Interface Number"`

for (( i=0; i<${#LAN_CARD_10G[@]}; i=i+1 ))
do
	/bin/cat /proc/bus/pci/devices | grep ${LAN_CARD_10G[$i]} 2>/dev/null 1>/dev/null
	if [ $? = 0 ]; then
		have_10G_extra_lan_card="TRUE"
		pic_raw 99
		break;
	else
		pic_raw 100
	fi
done

for (( i=0; i<${#LAN_CARD_1G[@]}; i=i+1 ))
do
    /bin/cat /proc/bus/pci/devices | grep ${LAN_CARD_1G[$i]} 2>/dev/null 1>/dev/null
    if [ $? = 0 ]; then
        have_1G_extra_lan_card="TRUE"
        break;
    fi
done

if [ $have_10G_extra_lan_card = "TRUE" ]; then
	[ ! -f /lib/modules/misc/mdio.ko ] || /sbin/insmod /lib/modules/misc/mdio.ko
	[ ! -f /lib/modules/misc/ixgbe.ko ] || /sbin/insmod /lib/modules/misc/ixgbe.ko
	[ ! -f /lib/modules/misc/be2net.ko ] || /sbin/insmod /lib/modules/misc/be2net.ko
	/sbin/setcfg Samba SO_SNDBUF 2621440 -f /etc/default_config/uLinux.conf
	/sbin/setcfg Samba SO_RCVBUF 2621440 -f /etc/default_config/uLinux.conf
	no_extra_lan_card="FALSE"
	/bin/touch /tmp/option_10G
	pic_raw 99
else
	pic_raw 100
fi

if [ $have_1G_extra_lan_card = "TRUE" ]; then
	[ ! -f /lib/modules/misc/igb.ko ] || /sbin/insmod /lib/modules/misc/igb.ko
	no_extra_lan_card="FALSE"
	/bin/touch /tmp/option_1G
fi

for (( i=$DEFAULT_NIC_NUM; i<$MAX_EXTRA_LAN_CARD_PORT_NUM; i=i+1 ))
do
	if [ -d /sys/class/net/eth$i ]; then
		for (( j=0; j<${#LAN_CARD_10G_V2[@]}; j=j+1 ))
		do
			/bin/cat /sys/class/net/eth$i/device/uevent | grep ${LAN_CARD_10G_V2[$j]} 2>/dev/null 1>/dev/null
			if [ $? = 0 ]; then
				/bin/touch /var/option_extra_lan
				$SETCFG "eth$i" "max speed" "10000" -f "/var/option_extra_lan"
				
				EXTRA_LAN_CARD_PORT_NUM=$EXTRA_LAN_CARD_PORT_NUM+1
				break;
			fi
		done
		
		for (( j=0; j<${#LAN_CARD_1G_V2[@]}; j=j+1 ))
		do
			/bin/cat /sys/class/net/eth$i/device/uevent | grep ${LAN_CARD_1G_V2[$j]} 2>/dev/null 1>/dev/null
			if [ $? = 0 ]; then
				/bin/touch /var/option_extra_lan
				$SETCFG "eth$i" "max speed" "1000" -f "/var/option_extra_lan"
				EXTRA_LAN_CARD_PORT_NUM=$EXTRA_LAN_CARD_PORT_NUM+1
				break;
			fi
		done
	fi
done

if [ $no_extra_lan_card = "TRUE" ]; then
	if [ $DEFAULT_NIC_NUM != $CONFIG_NIC_NUM ]; then
		/bin/rm /var/option_extra_lan
		/bin/rm /tmp/option_1G
		/bin/rm /tmp/option_10G
	fi
else
	lan_port_num=$[$DEFAULT_NIC_NUM + $EXTRA_LAN_CARD_PORT_NUM]
	$SETCFG "Network" "Interface Number" "$lan_port_num"
	$SETCFG -f /etc/default_config/uLinux.conf "Network" "Interface Number" "$lan_port_num"
fi

$SETCFG -f /etc/default_config/uLinux.conf "Network" "Advanced Bonding Support" "TRUE"
[ -f /etc/init.d/network.sh_adv_bonding ] && /bin/cp /etc/init.d/network.sh_adv_bonding /etc/init.d/network.sh
