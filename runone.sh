#!/bin/sh

# run all script in /etc/config/runone
# after finish, rm all files in /etc/config/runone
for i in /etc/config/runone/* ;do
	case "$i" in
		*.sh)
			echo run [$i]
			$i
			/bin/rm $i
			;;
		*)
			echo "[$i] do nothing"
			;;
	esac
done

