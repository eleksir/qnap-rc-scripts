#!/bin/sh
# For Administration
# default port: 8008
WFM_THTTPD_PATH="/usr/local/sbin/wfm_thttpd"
ENABLE_WFM=`/sbin/getcfg WebFS Enable`
ENABLE_WFM_PORT=`/sbin/getcfg WebFS Enable_Defined_Port -d "FALSE"`
WFM_PORT=`/sbin/getcfg WebFS Port -d "8008"`
WFM_SSLPORT=`/sbin/getcfg WebFS SSL_Port`
RUNNING_FILE="/var/lock/subsys/wfm_thttpd"
APP_RUNTIME_CONF="/var/.application.conf"
PortSettingSupport=`/sbin/getcfg WebFS PortSettingSupport -d TRUE -f ${APP_RUNTIME_CONF}`

if [ "$PortSettingSupport" = "FALSE" ]; then
	[ "$ENABLE_WFM_PORT" = "FALSE" ] || /sbin/setcfg WebFS Enable_Defined_Port "FALSE"
fi
if [ "$ENABLE_WFM" = "FALSE" ]; then
	[ ! -f /home/httpd/cgi-bin/filemanager.html ] || /bin/mv /home/httpd/cgi-bin/filemanager.html /home/filemanager.html
	exit 1
fi

case "$1" in
    start)
	echo -n "Starting wfm_thttpd services:" 
	if [ "$ENABLE_WFM_PORT" = "TRUE" ]; then
		${WFM_THTTPD_PATH} -p ${WFM_PORT} -nor -nos -u admin -d /home/httpd/cgi-bin/filemanager -c '**.*'
	else
		echo " no enable defined port."
		exit 1
	fi
	echo -n " wfm_thttpd"
	/bin/touch $RUNNING_FILE
	echo "."
	[ -f /home/httpd/cgi-bin/filemanager/utilRequest.cgi ] && `/home/httpd/cgi-bin/filemanager/utilRequest.cgi check_sharelink_db 1>>/dev/null 2>>/dev/null`
	if [ $? = 1 ]; then
		/bin/echo "rm share_link.db"
		/bin/rm -rf /etc/config/share_link.db
	fi
	;;
    stop)
	echo -n "Shutting down wfm_thttpd services:" 
	/bin/rm -f $RUNNING_FILE
	pid=`/bin/ps |/bin/grep wfm_thttpd`
	[ $? = 0 ] && /usr/bin/killall wfm_thttpd 2> /dev/null
	echo -n " wfm_thttpd"
	echo "."
	;;
    restart)
	$0 stop
	$0 start
	;;	
    *)
        echo "Usage: /etc/init.d/wfm_thttpd {start|stop|restart}"
        exit 1
esac

exit 0
