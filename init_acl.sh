#!/bin/sh

if [ `/sbin/getcfg System "Init ACL" -d 0` == 1 ] && [ `/sbin/getcfg System "ACL Enable" -d FALSE` == TRUE ]; then
	/sbin/init_acl 1>/dev/null 2>/dev/null
	/sbin/setcfg System "Init ACL" 0
fi

