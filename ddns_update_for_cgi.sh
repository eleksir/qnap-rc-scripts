#!/bin/sh
# ddns_update.sh         This shell script takes care of ddns updating
#
# description: ez_ipupdate provide register domain name from special server.

echo -n "Starting ddns update: "
rm /tmp/ddns.result -f

DDNS_TMP_FILE="/var/ddns_tmp.conf"

check_ddns_temp_file()
{
	if [ ! -f ${DDNS_TMP_FILE} ]; then
		/bin/echo "${DDNS_TMP_FILE} is not exist"
		/bin/echo "[DDNS]" > ${DDNS_TMP_FILE}
		/bin/echo "Support = "`/sbin/getcfg "DDNS" "Support" -u -d "TRUE"` >> ${DDNS_TMP_FILE}
		/bin/echo "Enable = "`/sbin/getcfg "DDNS" "Enable" -u -d "FALSE"` >> ${DDNS_TMP_FILE}
		/bin/echo "Server Type = "`/sbin/getcfg "DDNS" "Server Type" -u -d "0"` >> ${DDNS_TMP_FILE}
		/bin/echo "Config File = "`/sbin/getcfg "DDNS" "Config File" -d "/etc/ddns_update.conf"` >> ${DDNS_TMP_FILE}
		/bin/echo "Check External IP = "`/sbin/getcfg "DDNS" "Check External IP" -d "0"` >> ${DDNS_TMP_FILE}
		/bin/echo "User Name = "`/sbin/getcfg "DDNS" "User Name" -d ""` >> ${DDNS_TMP_FILE}
		/bin/echo "Password = "`/sbin/getcfg "DDNS" "Password" -d ""` >> ${DDNS_TMP_FILE}
		/bin/echo "Host Name = "`/sbin/getcfg "DDNS" "Host Name" -d ""` >> ${DDNS_TMP_FILE}
		/bin/echo "DDNS Type = "`/sbin/getcfg "DDNS" "DDNS Type" -d "0"` >> ${DDNS_TMP_FILE}
	fi
}

check_ddns_temp_file

[ -f /usr/sbin/noip2c ] || ln -sf /usr/sbin/noip2 /usr/sbin/noip2c
		
if [ `/sbin/getcfg "DDNS" "Support" -u -d "TRUE" -f ${DDNS_TMP_FILE}` = TRUE ]
then
	if [ `/sbin/getcfg "DDNS" "Enable" -u -d "FALSE" -f ${DDNS_TMP_FILE}` = FALSE ]
	then
		echo "disabled."
		exit 0
	fi
	
	if [ `/sbin/getcfg "DDNS" "Server Type" -u -d "0" -f ${DDNS_TMP_FILE}` != 5 ]
	then
		/usr/sbin/create_ddns_conf > /dev/null
		result=$?
		if [ $result -ne 0 ] 
		then
			echo "ERROR."
			exit 1
		fi

		config_file=`/sbin/getcfg "DDNS" "Config File" -d "/etc/ddns_update.conf" -f ${DDNS_TMP_FILE}`
		/usr/sbin/ez-ipupdate -c $config_file 2>/dev/null
		result=$?
	else
		if [ ! -f /etc/config/no-ip2.conf ]; then
			username=`/sbin/getcfg "DDNS" "User Name" -d "" -f ${DDNS_TMP_FILE}`
			password=`/sbin/getcfg "DDNS" "Password" -d "" -f ${DDNS_TMP_FILE}`
			hostname=`/sbin/getcfg "DDNS" "Host Name" -d "" -f ${DDNS_TMP_FILE}`
			externalIP=`/sbin/getcfg "DDNS" "Check External IP" -d "5" -f ${DDNS_TMP_FILE}`
			if [ $externalIP = 0 ]; then
				externalIP=5
			fi
			/usr/sbin/noip2c -C -u ${username} -p ${password} -n ${hostname} -U ${externalIP}
		fi
		if [ -f /etc/config/no-ip2.conf ]; then
			/sbin/daemon_mgr noip2 start /usr/sbin/noip2 1>/dev/null 2>/dev/null
			result=0
			echo "DDNS was updated successfully"
		else
			/sbin/setcfg "DDNS" "Enable" "FALSE"
			/sbin/setcfg "DDNS" "Enable" "FALSE" -f ${DDNS_TMP_FILE}
			/sbin/write_log "Enabling Dynamic DNS Service failed due to noip2 execution failure." 2
			result=1
		fi
		if [ $result -ne 0 ];then
			echo $result > /tmp/ddns.result
		else
			echo "DDNS was updated successfully"
		fi
		echo -n "noip2"
		echo "."
		touch /var/lock/subsys/noip2
		exit $result
	fi

	if [ $result -ne 0 ]
	then
		echo $result > /tmp/ddns.result
#		/sbin/write_log "Failed to update DDNS." 2
	else
		echo "DDNS was updated successfully"
#		/sbin/write_log "DDNS was updated successfully." 4
	fi
#update record
        /sbin/setcfg "DDNS" "Update Server Response" ${result} -f ${DDNS_TMP_FILE}
	[ -f /sbin/set_ipupatetime ] && /sbin/set_ipupatetime "1"

	echo -n "ez-ipupdate"
	echo "."
	touch /var/lock/subsys/ez-ipupdate
	exit $result
else
	echo "Un-support"
fi

exit 1
