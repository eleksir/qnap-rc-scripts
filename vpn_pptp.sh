#!/bin/sh

PPTPD=/usr/sbin/pptpd
PIDFILE="/var/run/pptpd.pid"
PPTPD_CONF="/etc/ppp/pptpd.conf"
OPTIONS_PPTPD="/etc/ppp/options.pptpd"
VPN_CONF="/etc/config/vpn.conf"

DBG="nodebug"
if [ "x$2" == "xdebug" ]; then
	DBG="debug"
fi

init_pptp_modules()
{
	/bin/echo "init insmod and mknod"
	[ -c /dev/ppp ] || /bin/mknod --mode=644 /dev/ppp c 108 0
	[ -d /etc/config/ppp ] || mkdir -p /etc/config/ppp
	[ -L /etc/ppp ] || /bin/ln -sf /etc/config/ppp /etc/ppp
	/bin/echo "1" > /proc/sys/net/ipv4/ip_forward
	[ -L /etc/ppp/ip-up ] || /bin/ln -sf /sbin/nasutil /etc/ppp/ip-up
	[ -L /etc/ppp/ip-down ] || /bin/ln -sf /sbin/nasutil /etc/ppp/ip-down
	[ -L /usr/lib/libsmbpw.so ] || /bin/ln -sf /usr/lib/libsmbpw.1.3.so /usr/lib/libsmbpw.so
	
	/sbin/lsmod | grep ppp_generic 1>>/dev/null 2>>/dev/null
	if [ $? != 0 ]; then
		/bin/echo "insmod pptp and pppd modules"
		for i in sha1_generic.ko arc4.ko ecb.ko slhc.ko crc-ccitt.ko ppp_generic.ko ppp_async.ko ppp_mppe.ko \
			bsd_comp.ko ppp_deflate.ko
		do
			[ ! -f /lib/modules/others/vpn/$i ] || /sbin/insmod /lib/modules/others/vpn/$i
		done
	fi
}

init_iptable_modules()
{
	[ -L /usr/lib/libxtables.so.7 ] || /bin/ln -sf /usr/lib/libxtables.so.7.0.0 /usr/lib/libxtables.so.7
	[ -L /usr/lib/libxtables.so ] || /bin/ln -sf /usr/lib/libxtables.so.7.0.0 /usr/lib/libxtables.so
	[ -L /usr/lib/libip4tc.so.0 ] || /bin/ln -sf /usr/lib/libip4tc.so.0.0.0 /usr/lib/libip4tc.so.0
	[ -L /usr/lib/libip4tc.so ] || /bin/ln -sf /usr/lib/libip4tc.so.0.0.0 /usr/lib/libip4tc.so
	[ -L /usr/lib/libip6tc.so.0 ] || /bin/ln -sf /usr/lib/libip6tc.so.0.0.0 /usr/lib/libip6tc.so.0
	[ -L /usr/lib/libip6tc.so ] || /bin/ln -sf /usr/lib/libip6tc.so.0.0.0 /usr/lib/libip6tc.so
	/sbin/lsmod | grep x_tables 1>>/dev/null 2>>/dev/null
	if [ $? != 0 ]; then
		/bin/echo "insmod iptable modules"
		for i in x_tables.ko nf_conntrack.ko xt_tcpudp.ko nf_defrag_ipv4.ko nf_conntrack_ipv4.ko nf_nat.ko \
			ip_tables.ko iptable_nat.ko iptable_filter.ko ipt_MASQUERADE.ko ipt_REDIRECT.ko 
		do
			[ ! -f /lib/modules/others/vpn/$i ] || /sbin/insmod /lib/modules/others/vpn/$i
		done
	fi
}

remove_pptp_modules()
{
	for i in ppp_deflate bsd_comp ppp_mppe ppp_async ppp_generic crc-ccitt slhc 
	do
		/sbin/rmmod $i
	done
}

remove_iptable_modules()
{
	for i in ipt_REDIRECT ipt_MASQUERADE iptable_filter iptable_nat ip_tables nf_nat nf_conntrack_ipv4 nf_defrag_ipv4 \
		xt_tcpudp nf_conntrack x_tables
	do
		/sbin/rmmod $i
	done
}

start()
{
	if [ ! -f ${PPTPD} ]; then
		/bin/echo "pptpd is not exit."
		exit 1
	fi
	
	if [ ! -f /usr/sbin/pppd ]; then
		/bin/echo "pppd is not exit."
		exit 1
	fi
	
	if [ ! -L /etc/rcK.d/K47vpn_pptp ]; then
		/bin/ln -sf ../init.d/vpn_pptp.sh /etc/rcK.d/K47vpn_pptp
	fi

	if [ -f ${PIDFILE} ]; then
		/bin/echo "pptpd is aleady run."
		exit 1
	fi
	
	if [ `/sbin/getcfg "PPTP" "Enable" -u -d "FALSE" -f "${VPN_CONF}"` = "TRUE" ]; then
		/bin/echo "pptpd start."
		init_pptp_modules
		init_iptable_modules
		/sbin/vpn_util init_pptp ${DBG}
		/sbin/daemon_mgr pptpd start "${PPTPD} -c ${PPTPD_CONF} -o ${OPTIONS_PPTPD} -p ${PIDFILE}"
		LOCALIP=`grep "^localip" ${PPTPD_CONF} | cut -d' ' -f 2`
		INTERFACE=`grep "^bcrelay" ${PPTPD_CONF} | cut -d' ' -f 2`
		/bin/echo "LOCALIP = ${LOCALIP}    INTERFACE = ${INTERFACE}"
		/sbin/iptables -t nat -A POSTROUTING -s ${LOCALIP}/24 -o ${INTERFACE} -j MASQUERADE
	else
		/bin/echo "pptpd is disabled." 
	fi
}

stop()
{
	/bin/echo "pptpd stop."
	LOCALIP=`grep "^localip" ${PPTPD_CONF} | cut -d' ' -f 2`
	INTERFACE=`grep "^bcrelay" ${PPTPD_CONF} | cut -d' ' -f 2`
	/bin/echo "LOCALIP = ${LOCALIP}    INTERFACE = ${INTERFACE}"
	/sbin/iptables -t nat -D POSTROUTING -s ${LOCALIP}/24 -o ${INTERFACE} -j MASQUERADE
	
	/bin/kill -9 `/bin/pidof pppd` 1>>/dev/null 2>&1
	/bin/kill -9 `/bin/pidof bcrelay` 1>>/dev/null 2>&1
	/sbin/daemon_mgr pptpd stop "${PPTPD} -c ${PPTPD_CONF} -o ${OPTIONS_PPTPD} -p ${PIDFILE}"
	/bin/kill -9 `/bin/cat ${PIDFILE}` 1>>/dev/null 2>&1
	/bin/rm -f ${PIDFILE} 1>>/dev/null 2>&1
	/bin/rm -f /var/log/pptp_online_user 1>>/dev/null 2>&1
}

reload()
{
	if [ `/sbin/getcfg "PPTP" "Enable" -u -d "FALSE" -f "${VPN_CONF}"` = "FALSE" ]; then
		/bin/echo "pptpd is disabled." 
		exit 1;
	fi
	/bin/echo "pptpd stop."
	/bin/kill -9 `/bin/pidof bcrelay` 1>>/dev/null 2>&1
	/sbin/daemon_mgr pptpd stop "${PPTPD} -c ${PPTPD_CONF} -o ${OPTIONS_PPTPD} -p ${PIDFILE}"
	/bin/kill -9 `/bin/cat ${PIDFILE}` 1>>/dev/null 2>&1
	/bin/rm -f ${PIDFILE} 1>>/dev/null 2>&1
	LOCALIP=`grep "^localip" ${PPTPD_CONF} | cut -d' ' -f 2`
	INTERFACE=`grep "^bcrelay" ${PPTPD_CONF} | cut -d' ' -f 2`
	/bin/echo "LOCALIP = ${LOCALIP}    INTERFACE = ${INTERFACE}"
	/sbin/iptables -t nat -D POSTROUTING -s ${LOCALIP}/24 -o ${INTERFACE} -j MASQUERADE
	
	/sbin/vpn_util init_pptp ${DBG}
	/sbin/daemon_mgr pptpd start "${PPTPD} -c ${PPTPD_CONF} -o ${OPTIONS_PPTPD} -p ${PIDFILE}"
	LOCALIP=`grep "^localip" ${PPTPD_CONF} | cut -d' ' -f 2`
	INTERFACE=`grep "^bcrelay" ${PPTPD_CONF} | cut -d' ' -f 2`
	/bin/echo "LOCALIP = ${LOCALIP}    INTERFACE = ${INTERFACE}"
	/sbin/iptables -t nat -A POSTROUTING -s ${LOCALIP}/24 -o ${INTERFACE} -j MASQUERADE
}

restart()
{
	stop
	sleep 1
	start
}

case "$1" in
    start)
		start
		;;
    stop)
		stop
		remove_pptp_modules
		;;
    restart)
		restart
		;;
	init)
		/bin/echo "pptpd init."
		init_pptp_modules
		init_iptable_modules
		/sbin/vpn_util init_pptp
		;;
	reload)
		reload
		;;
    *)
        echo "Usage: /etc/init.d/$0 {start|stop|restart|reload}"
        exit 1
esac

exit 0
