#!/bin/sh
#
# initialize MAC address
#

MOUNTPOINT="/tmp/config"
ETH0_MAC_ADDR="/ETH0.MAC_ADDR"
DEFAULT_MAC_ADDR="00:00:00:00:05:09"
GETCFG="/sbin/getcfg"
SETCFG="/sbin/setcfg"
NIC_NUM=`$GETCFG "Network" "Interface Number" -d "2"`

#only use for ARM && PPC
get_mac_address()
{
	local mac_addr
	local ret

	mac_addr=`/sbin/iface_get_mac $1 ${DEFAULT_MAC_ADDR}`
	ret=$?

	if [ ${mac_addr} = ${DEFAULT_MAC_ADDR} ]; then
		mac_addr=`/sbin/get_mac`
	fi

	/sbin/ifconfig $1 down
	/sbin/ifconfig $1 hw ether $mac_addr
	/sbin/ifconfig $1 up

	if [ $ret -eq 0 ]; then
		[ $? != 0 ] || /bin/echo "MAC_ADDR $mac_addr" 
	else
		/sbin/ifconfig $1 hw ether $mac_addr
		/bin/echo "MAC address not found. Set default MAC Addr $mac_addr" 
	fi
}

swap_two_nics()
{
    /sbin/ifconfig eth0 down
    /sbin/ifconfig eth1 down
    /bin/ip link set dev eth0 name not_eth0
    /bin/ip link set dev eth1 name eth0
    /bin/ip link set dev not_eth0 name eth1
    /sbin/ifconfig eth0 up
    /sbin/ifconfig eth1 up
}
swap_four_nics()
{
    /sbin/ifconfig eth1 down
    /sbin/ifconfig eth2 down
    /sbin/ifconfig eth3 down
    /bin/ip link set dev eth1 name not_eth1
    /bin/ip link set dev eth2 name eth1
    /bin/ip link set dev eth3 name eth2
    /bin/ip link set dev not_eth1 name eth3
    /sbin/ifconfig eth1 up
    /sbin/ifconfig eth2 up
    /sbin/ifconfig eth3 up
}

BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf 2>/dev/null`

if [ "x${BOOT_CONF}" = "xTS-NASARM" ] && [ ! -x /sbin/hal_app ]; then
    /bin/touch /sbin/mac_sync
    /bin/ln -sf /sbin/config_util /sbin/mac_sync
    /sbin/mac_sync
    /bin/rm /sbin/mac_sync
fi

if [ ! -x /sbin/hal_app ];then
    # rename eth0 eth1
    Model_Name=`/bin/cat /etc/default_config/Model_Name.conf 2>/dev/null`
    if [ "x${Model_Name}" = "xTS-509" ] || [ "x${Model_Name}" = "xTS-419" ] || [ "x${Model_Name}" = "xTS-809" ] || [ "x${Model_Name}" = "xTS-259" ] || [ "x${Model_Name}" = "xTS-459" ] || [ "x${Model_Name}" = "xTS-559" ] || [ "x${Model_Name}" = "xTS-659" ] || [ "x${Model_Name}" = "xTS-859" ] || [ "x${Model_Name}" = "xTS-269" ] || [ "x${Model_Name}" = "xTS-469" ] || [ "x${Model_Name}" = "xTS-569" ] || [ "x${Model_Name}" = "xTS-669" ] || [ "x${Model_Name}" = "xTS-869" ] || [ "x${Model_Name}" = "xTS-1269" ];then
	    CPU_MODEL=""
	    _nic_num=`/sbin/getcfg Network "Interface Number" -d 2`

        [ ! -f /proc/tsinfo/cpu_model ] || CPU_MODEL=`/bin/cat /proc/tsinfo/cpu_model | cut -d ' ' -f 1 2>/dev/null`
        if [ "x${CPU_MODEL}" = "x88F6282" ] || [ "x${_nic_num}" = "x1" ] ;then
                echo "nothing" >/dev/null
        else
		    /sbin/ifconfig eth0 down
    		/sbin/ifconfig eth1 down
	    	/bin/ip link set dev eth0 name not_eth0
		    /bin/ip link set dev eth1 name eth0
    		/bin/ip link set dev not_eth0 name eth1
	    	/sbin/ifconfig eth0 up
		    /sbin/ifconfig eth1 up
	    fi
    fi

    Model_NameX=`/bin/cat /etc/default_config/uLinux.conf | grep "^Model =" | cut -d ' ' -f3 2>/dev/null`
    if [ "x${Model_NameX}" = "xTS-1279" ] || [ "x${Model_NameX}" = "xTS-879U" ] || [ "x${Model_NameX}" = "xTS-1279U" ] || [ "x${Model_NameX}" = "xTS-1279-G" ] || [ "x${Model_NameX}" = "xTS-879U-G" ] || [ "x${Model_NameX}" = "xTS-1279U-G" ]; then
        if [ -e /proc/tsinfo/hw_version ];then
            Board=$(cat /proc/tsinfo/hw_version | cut -d ' ' -f2)
            if [ $((Board & 2)) -eq 2 ];then
                swap_four_nics
            else
                swap_two_nics
            fi
        else
            swap_two_nics
        fi
    else
        if [ "x${Model_NameX}" = "xTS-1679U" ] || [ "x${Model_NameX}" = "xTS-1679U-G" ]; then
            swap_four_nics
        fi
    fi
fi

MAC_FILE="/var/MacAddress"
if [ "x${BOOT_CONF}" = "xTS-NASARM" ] && [ ! -x /sbin/hal_app ]; then
	/bin/echo "[MAC]" > ${MAC_FILE}
	mac_addr=`/sbin/iface_get_mac eth0 ${DEFAULT_MAC_ADDR}`
	if [ ${mac_addr} = ${DEFAULT_MAC_ADDR} ]; then
		mac_addr=`/sbin/get_mac`
	fi
	/bin/echo "eth0 = $mac_addr" >> ${MAC_FILE}
	if [ "x${Model_Name}" = "xTS-419" ]; then
		mac_addr=`/sbin/iface_get_mac eth1 ${DEFAULT_MAC_ADDR}`
		/bin/echo "eth1 = $mac_addr" >> ${MAC_FILE}
	fi
else
    #set mac address to /var/MacAddress
    /bin/echo "[MAC]" > ${MAC_FILE}
    if_num=0
    while [ $if_num -lt $NIC_NUM ]
    do
        if [ "x${BOOT_CONF}" = "xTS-NASARM" ] && [ -x /sbin/hal_app ]; then
            mac_addr=`/sbin/hal_app --se_sys_get_mac obj_index=${if_num}`
            if [ $? -ne 0 ] || [ "x${mac_addr}" = "x" ]; then
                mac_addr=${DEFAULT_MAC_ADDR}
            fi
            /sbin/ifconfig eth${if_num} down
            /sbin/ifconfig eth${if_num} hw ether ${mac_addr}
            /sbin/ifconfig eth${if_num} up
        fi
        mac_addr=`/bin/cat /sys/class/net/eth${if_num}/address`
        /bin/echo "eth${if_num} $mac_addr"
        /bin/echo "eth${if_num} = $mac_addr" >> ${MAC_FILE}
        if_num=$((${if_num} + 1))
    done
fi

[ -d $MOUNTPOINT ] || /bin/mkdir $MOUNTPOINT

# setup lo
/sbin/ifconfig lo 127.0.0.1 up 2> /dev/null 1> /dev/null
/sbin/route add -net 127.0.0.0 netmask 255.0.0.0 lo 2> /dev/null 1> /dev/null

if [ "x${BOOT_CONF}" = "xTS-NASARM" ] && [ ! -x /sbin/hal_app ]; then
	/sbin/ifconfig eth0 > /dev/null 2>&1
	[ $? = 0 ] && get_mac_address eth0
	/sbin/ifconfig eth1 > /dev/null 2>&1
	[ $? = 0 ] && get_mac_address eth1
fi

#ldconfig
/sbin/ldconfig 1>/dev/null 2>&1



