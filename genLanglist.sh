#!/bin/sh
#This shell script will creat support language list accroding the language file's exist.

_LANG_FOLDER=(/home/httpd/cgi-bin/inc/)
_LANG_FILE=(/home/httpd/cgi-bin/inc/lang_eng.js /home/httpd/cgi-bin/inc/lang_chs.js /home/httpd/cgi-bin/inc/lang_cht.js /home/httpd/cgi-bin/inc/lang_cze.js /home/httpd/cgi-bin/inc/lang_dan.js /home/httpd/cgi-bin/inc/lang_ger.js /home/httpd/cgi-bin/inc/lang_spa.js /home/httpd/cgi-bin/inc/lang_fre.js /home/httpd/cgi-bin/inc/lang_ita.js /home/httpd/cgi-bin/inc/lang_jpn.js /home/httpd/cgi-bin/inc/lang_kor.js /home/httpd/cgi-bin/inc/lang_nor.js /home/httpd/cgi-bin/inc/lang_pol.js /home/httpd/cgi-bin/inc/lang_rus.js /home/httpd/cgi-bin/inc/lang_fin.js /home/httpd/cgi-bin/inc/lang_swe.js /home/httpd/cgi-bin/inc/lang_dut.js /home/httpd/cgi-bin/inc/lang_es-mex.js /home/httpd/cgi-bin/inc/lang_tur.js /home/httpd/cgi-bin/inc/lang_tha.js /home/httpd/cgi-bin/inc/lang_por.js /home/httpd/cgi-bin/inc/lang_hun.js /home/httpd/cgi-bin/inc/lang_grk.js /home/httpd/cgi-bin/inc/lang_rom.js)
_QUICK_LANG_FILE=(/home/httpd/cgi-bin/quick/inc/lang_eng.js /home/httpd/cgi-bin/quick/inc/lang_chs.js /home/httpd/cgi-bin/quick/inc/lang_cht.js /home/httpd/cgi-bin/quick/inc/lang_cze.js /home/httpd/cgi-bin/quick/inc/lang_dan.js /home/httpd/cgi-bin/quick/inc/lang_ger.js /home/httpd/cgi-bin/quick/inc/lang_spa.js /home/httpd/cgi-bin/quick/inc/lang_fre.js /home/httpd/cgi-bin/quick/inc/lang_ita.js /home/httpd/cgi-bin/quick/inc/lang_jpn.js /home/httpd/cgi-bin/quick/inc/lang_kor.js /home/httpd/cgi-bin/quick/inc/lang_nor.js /home/httpd/cgi-bin/quick/inc/lang_pol.js /home/httpd/cgi-bin/quick/inc/lang_rus.js /home/httpd/cgi-bin/quick/inc/lang_fin.js /home/httpd/cgi-bin/quick/inc/lang_swe.js /home/httpd/cgi-bin/quick/inc/lang_dut.js /home/httpd/cgi-bin/quick/inc/lang_tha.js /home/httpd/cgi-bin/quick/inc/lang_por.js /home/httpd/cgi-bin/quick/inc/lang_hun.js /home/httpd/cgi-bin/quick/inc/lang_grk.js /home/httpd/cgi-bin/quick/inc/lang_rom.js)
_code=(\'en\' \'zh_CN\' \'zh_TW\' \'cz\' \'da\' \'de\' \'es\' \'fr\' \'it\' \'ja\' \'ko\' \'no\' \'pl\' \'ru\' \'fi\' \'sv_SE\' \'dut\' \'es\' \'tur\' \'tha\' \'por\' \'hun\' \'grk\' \'rom\' )
_display_name=("'English'" "'简体中文'" "'繁體中文'" "'Czech'" "'Dansk'" "'Deutsch'" "'Español'" "'Français'" "'Italiano'" "'日本語'" "'한글'" "'Norsk'" "'Polski'" "'Русский'" "'Suomi'" "'Svenska'" "'Nederlands'" "'Español mexicano'" "'Turk dili'" "'ไทย'" "'Português'" "'Magyar'" "'Ελληνικά'" "'Român'")
_charset=(\'ascii\' \'utf-8\' \'UTF-8\' \'UTF-8\' \'utf-8\' \'utf-8\' \'utf-8\' \'utf-8\' \'utf-8\' \'utf-8\' \'utf-8\' \'utf-8\' \'utf-8\' \'utf-8\' \'utf-8\' \'utf-8\' \'utf-8\' \'utf-8\' \'utf-8\' \'utf-8\' \'utf-8\' \'utf-8\' \'utf-8\' \'utf-8\')
_cookie_name=(\'ENG\' \'SCH\' \'TCH\' \'CZE\' \'DAN\' \'GER\' \'SPA\' \'FRE\' \'ITA\' \'JPN\' \'KOR\' \'NOR\' \'POL\' \'RUS\' \'FIN\' \'SWE\' \'DUT\' \'ESM\' \'TUR\' \'THA\' \'POR\' \'HUN\' \'GRK\' \'ROM\')
_file_name=(\'eng\' \'chs\' \'cht\' \'cze\' \'dan\' \'ger\' \'spa\' \'fre\' \'ita\' \'jpn\' \'kor\' \'nor\' \'pol\' \'rus\' \'fin\' \'swe\' \'dut\' \'es-mex\' \'tur\' \'tha\' \'por\' \'hun\' \'grk\' \'rom\')
_output_file=/home/httpd/ajax_obj/extjs/languages.js

str=""
element_count=${#_LANG_FILE[@]}
index=0

#lang_folder=1 : /home/httpd/cgi-bin/inc/ exist 
#lang_folder=0 : /home/httpd/cgi-bin/inc/ do not exist 
test -e ${_LANG_FILE[0]} &&  lang_folder=1 ||  lang_folder=0

if [ ${lang_folder} -eq 1 ]; then
	while [ "$index" -lt "$element_count" ]; do
		if [ -f ${_LANG_FILE[$index]} ]; then
				firstline="["${_code[$index]}","${_display_name[$index]}","${_charset[$index]}","${_cookie_name[$index]}","${_file_name[$index]}"],"
				str=${str}${firstline}
		fi
		index=`/usr/bin/expr $index + 1`
	done
else	
	while [ "$index" -lt "${#_QUICK_LANG_FILE[@]}" ]; do
		if [  -f ${_QUICK_LANG_FILE[$index]}  ]; then
				firstline="["${_code[$index]}","${_display_name[$index]}","${_charset[$index]}","${_cookie_name[$index]}","${_file_name[$index]}"],"
				str=${str}${firstline}
		fi
		index=`/usr/bin/expr $index + 1`
	done
fi
count_str=${#str}
restult=''
#echo ${str}
if [ "$count_str" -gt 0 ]; then
	result=${str::$count_str -1 }
fi
#Display and output to $_output_file
echo "Ext.namespace('Ext.exampledata');" > $_output_file
echo "Ext.exampledata.languages = ["$result"];" >> $_output_file



