#!/bin/sh

PACKAGE_PATH=/usr/local/medialibrary
export MYMEDIADB_PATH=$PACKAGE_PATH/bin
export LD_LIBRARY_PATH=$PACKAGE_PATH/lib:$LD_LIBRARY_PATH
MULTIMEDIA_PATH=`/sbin/getcfg -f /etc/config/smb.conf "Multimedia" "path"`
if [ "x${MULTIMEDIA_PATH}" = "x" ]; then
        MULTIMEDIA_PATH=`/sbin/getcfg -f /etc/config/smb.conf "Qmultimedia" "path"`
fi
[ -f /etc/config/medialibrary.conf ] || /bin/touch /etc/config/medialibrary.conf
[ x`/sbin/getcfg OPTIONS INOTIFY -d x -f /etc/config/medialibrary.conf` != xx ] || /sbin/setcfg OPTIONS INOTIFY 1 -f /etc/config/medialibrary.conf

case "$1" in
	start)
		if [ "x${MULTIMEDIA_PATH}" = "x" ]; then
        		echo "I cannot find multimedia folder"
        		exit 0
		fi
		MOUNT_PATH=`dirname $MULTIMEDIA_PATH`
		MULTIMEDIA_NAME=`basename $MULTIMEDIA_PATH`
		[ -x $PACKAGE_PATH/bin/mymediadbserver ] || exit 1
		/bin/pidof mymediadbserver >/dev/null 2>&1
		[ $? != 0 ] || exit 1
		if [ -f $PACKAGE_PATH/bin/PriMyDB ]; then
			[ -d /tmp/.system ] || /bin/mkdir /tmp/.system
			[ -f /tmp/.system/PriMyDB ] || /usr/bin/install -m 777 $PACKAGE_PATH/bin/PriMyDB /tmp/.system/
		fi
		$PACKAGE_PATH/bin/mymediadbserver -d >/dev/null 2>&1
		sleep 2
		$PACKAGE_PATH/bin/mymediadbcmd adddisk $MOUNT_PATH >/dev/null 2>&1
		$PACKAGE_PATH/bin/mymediadbcmd addfolder $MOUNT_PATH $MULTIMEDIA_NAME 1 >/dev/null 2>&1
		[ -f $PACKAGE_PATH/bin/InstallMusicSamples.sh ] && $PACKAGE_PATH/bin/InstallMusicSamples.sh
		INOTIFY=`/sbin/getcfg OPTIONS INOTIFY -d 0 -f /etc/config/medialibrary.conf`
		if [ "$INOTIFY" = "1" ]; then
			$PACKAGE_PATH/bin/mymediadbcmd buildfolder $MOUNT_PATH $MULTIMEDIA_NAME >/dev/null 2>&1
		fi
		sleep 2
		$PACKAGE_PATH/bin/myidbserver -d >/dev/null 2>&1
		INOTIFY=`/sbin/getcfg OPTIONS INOTIFY -d 0 -f /etc/config/medialibrary.conf`
		if [ "$INOTIFY" = "1" ]; then
			$PACKAGE_PATH/bin/mymediadbcmd inotifyStart >/dev/null 2>&1
		fi
		CODEPAGE=`/sbin/getcfg OPTIONS CODEPAGE -d -1 -f /etc/config/medialibrary.conf`
		if [ ! "$CODEPAGE" = "-1" ]; then
			$PACKAGE_PATH/bin/mymediadbcmd codepage $CODEPAGE >/dev/null 2>&1
		fi
		;;
		
	stop)
		killall myidbserver
		killall mymediadbserver
		sleep 1
		;;

	restart)
		$0 stop
		$0 start
		;;
		
	*)
		echo "Usage: /etc/init.d/StrarMediaService.sh {start|stop|restart}"
		exit 1
esac
exit 0	



