#!/bin/sh

#. /etc/init.d/check_nss2

ROOT_PART="/mnt/HDA_ROOT"
BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf 2>/dev/null`
# check if the HAL subsystem exist
if [ -x /sbin/hal_app ]; then
    BOOT_DEV=$(/sbin/hal_app --get_boot_pd port_id=0)
    EXT_ROOT="/dev/md13"
elif [ "x${BOOT_CONF}" = "xTS-NASARM" ]; then
    BOOT_DEV="/dev/mtdblock"
    EXT_ROOT="/dev/sda4"
else
    BOOT_DEV="/dev/sdx"
    EXT_ROOT="/dev/sda4"
fi

if [ "x${BOOT_CONF}" = "xTS-NASARM" ]; then
    DEV_NAS_CONFIG=${BOOT_DEV}5
else    
    DEV_NAS_CONFIG=${BOOT_DEV}6
fi
TMP_CONFIG="/tmp/nasconfig_tmp"
DEFAULT_CFG_FILE="/etc/config/uLinux.conf"
NASCONFIG_FS="/opt/nasconfig_fs.img.tgz"
SWAP_DEVICE=/dev/md`/sbin/getcfg Storage "Disk Drive Number"`
restore_flag1=0
FLASH_UBOOT="/dev/mtdblock0"
FLASH_KERNEL="/dev/mtdblock1"
DEV_NAS_CONFIG_SIZE_MIN=32


NSS_ENABLED=`/sbin/getcfg "NVR" "NSS Enabled" -d "no"`

_init_spool()
{
	local volume=`/sbin/getcfg Public path -f /etc/smb.conf | cut -d '/' -f 3`
	local spool_dir="/share/$volume/.spool"
	/bin/mount | /bin/grep $volume
	if [ $? != 0 ]; then
		spool_dir="/mnt/HDA_ROOT/.cups"
	fi

	[ -d $spool_dir ] || /bin/mkdir "$spool_dir" && /bin/chmod 777 $spool_dir
	/bin/rm -f "/var/spool"
	/bin/ln -s "$spool_dir" "/var/spool"

	# init cups spool
	if [ -f /usr/sbin/cupsd ]; then
		[ -d "$spool_dir/cups" ] && /bin/rm -fr "$spool_dir/cups"
	fi
}

check_sum()
{
	local file_cksum=`/sbin/cksum $1`
	local file_crc=`echo $file_cksum | cut -d ' ' -f 1`
	local file_size=`echo $file_cksum | cut -d ' ' -f 2`

	local cksum_cksum=`cat $2`
	local cksum_crc=`echo $cksum_cksum | cut -d ' ' -f 1`
	local cksum_size=`echo $cksum_cksum | cut -d ' ' -f 2`

	if [ ! -f $1 ] || [ ! -f $2 ]; then
		[ ! -f $1 ] && echo "$1 not found."
		[ ! -f $2 ] && echo "$2 not found."
		return 1
 	fi

	if [ $file_crc = $cksum_crc ] && [ $file_size = $cksum_size ]; then
		return 0
	fi
	return 1
}

update_flash_config(){
	[ -f ${ROOT_PART}/.config/Model_Name.conf ] || return 1
	echo "Update config files"
	[ -d $TMP_CONFIG ] || /bin/mkdir $TMP_CONFIG
	/bin/mount $DEV_NAS_CONFIG -t ext2 $TMP_CONFIG
	if [ $? = 0 ]; then
		DEV_NAS_CONFIG_FREE_SIZE=`/bin/df -k | /bin/grep "$DEV_NAS_CONFIG" | /bin/awk '{print $4}'`
		if [ ${DEV_NAS_CONFIG_FREE_SIZE} -le ${DEV_NAS_CONFIG_SIZE_MIN} ]; then
			/bin/umount $DEV_NAS_CONFIG
			/bin/echo "The available space in flash rom is not enough."
			return 1
		fi
		/bin/grep "\[END_FLAG\]" $DEFAULT_CFG_FILE 1>>/dev/null 2>>/dev/null
		if [ $? = 0 ]; then
			DEFAULT_CFG_FILE_SIZE=`/usr/bin/du -k $DEFAULT_CFG_FILE | /bin/awk '{print $1}'`
			[ ${DEV_NAS_CONFIG_FREE_SIZE} -gt ${DEFAULT_CFG_FILE_SIZE} ] && /bin/cp $DEFAULT_CFG_FILE $TMP_CONFIG
		fi
		if [ ! -f ${TMP_CONFIG}/system.map.key ]; then
			/sbin/uuidgen > ${TMP_CONFIG}/system.map.key
			/bin/cp ${TMP_CONFIG}/system.map.key /etc/config/system.map.key
		fi
		smbconf_cksum_val=0
		flash_smbconf_cksum_val=1
		if [ -f /etc/config/smb.conf ]; then
			DEV_NAS_CONFIG_FREE_SIZE=`/bin/df -k | /bin/grep "$DEV_NAS_CONFIG" | /bin/awk '{print $4}'`
			SMBCONF_SIZE=`/usr/bin/du -k /etc/config/smb.conf | /bin/awk '{print $1}'`
			if [ -z $DEV_NAS_CONFIG_FREE_SIZE ] || [ -z $SMBCONF_SIZE ] || [ $SMBCONF_SIZE -le 0 ] || [ ${DEV_NAS_CONFIG_FREE_SIZE} -le ${SMBCONF_SIZE} ]; then
				/bin/umount $DEV_NAS_CONFIG
				/bin/echo "The available space in flash rom is not enough."
				return 1
			fi
			smbconf_cksum_val=`/sbin/cksum /etc/config/smb.conf | /bin/cut -d ' ' -f 1`
			[ -f ${TMP_CONFIG}/smb.conf ] && flash_smbconf_cksum_val=`/sbin/cksum ${TMP_CONFIG}/smb.conf | /bin/cut -d ' ' -f 1`
			if [ $smbconf_cksum_val != $flash_smbconf_cksum_val ]; then
				/bin/cp /etc/config/smb.conf ${TMP_CONFIG}/smb.conf
				flash_smbconf_cksum_val=`/sbin/cksum ${TMP_CONFIG}/smb.conf | /bin/cut -d ' ' -f 1`
				/bin/echo $flash_smbconf_cksum_val > ${TMP_CONFIG}/smb.conf.cksum
				/bin/echo $flash_smbconf_cksum_val > /etc/config/smb.conf.cksum
				/bin/echo "Configuration files in the flash rom is updated."
			fi
		fi
		system_map_uuid=`/bin/cat ${TMP_CONFIG}/system.map.key`
		system_map_uuid_hd=`/bin/cat /etc/config/system.map.key`
		if [ "x$system_map_uuid" != x ] && [ "x$system_map_uuid" != "x$system_map_uuid_hd" ]; then
			DEV_NAS_CONFIG_FREE_SIZE=`/bin/df -k | /bin/grep "$DEV_NAS_CONFIG" | /bin/awk '{print $4}'`
			[ ${DEV_NAS_CONFIG_FREE_SIZE} -gt ${DEV_NAS_CONFIG_SIZE_MIN} ] && /bin/cp ${TMP_CONFIG}/system.map.key /etc/config/system.map.key
		fi
		/bin/sync
		/bin/umount $DEV_NAS_CONFIG
	fi
}

check_config(){
	echo "Check config files...start"
	[ -d $TMP_CONFIG ] || /bin/mkdir $TMP_CONFIG
	/bin/mount -t ext2 $DEV_NAS_CONFIG $TMP_CONFIG
	if [ $? = 0 ]; then
#		/bin/grep "\[END_FLAG\]" $DEFAULT_CFG_FILE 1>>/dev/null 2>>/dev/null
#		if [ $? != 0 ]; then
#			echo "Restore uLinux.conf..."
#			restore_flag1=1
#			/bin/grep "\[END_FLAG\]" $TMP_CONFIG/uLinux.conf 1>>/dev/null 2>>/dev/null
#			if [ $? = 0 ];then
#				/bin/cp $TMP_CONFIG/uLinux.conf $DEFAULT_CFG_FILE
#			else
#				/bin/cp $DEGAULT_CFG_DIR/uLinux.conf $DEFAULT_CFG_FILE
#			fi
#		fi

		if [ -f ${ROOT_PART}/.config/Model_Name.conf ]; then
		smbconf_cksum_val=0
		flash_smbconf_cksum_val=1
		flash_smbconf_cksum_ver=2
		system_map_uuid=`/bin/cat ${TMP_CONFIG}/system.map.key`
		system_map_uuid_hd=`/bin/cat /etc/config/system.map.key`
		SMBCONF_SIZE=`/usr/bin/du -k /etc/config/smb.conf | /bin/awk '{print $1}'`
		ROOT_PART_FREE_SIZE=`/bin/df -k | /bin/grep "$ROOT_PART" | /bin/awk '{print $4}'`
		if [ -z $SMBCONF_SIZE ] || [ -z $ROOT_PART_FREE_SIZE ] || [ $SMBCONF_SIZE -le 0 ] || [ $ROOT_PART_FREE_SIZE -le $SMBCONF_SIZE ]; then
			/bin/umount $DEV_NAS_CONFIG
			/bin/echo "Configuration file recovery failed."
			return 1
		fi
		if [ "x$system_map_uuid" != x ] && [ "x$system_map_uuid" = "x$system_map_uuid_hd" ]; then
			if [ -f /etc/config/smb.conf ] && [ -f /etc/config/smb.conf.cksum ]; then
				smbconf_cksum_val=`/sbin/cksum /etc/config/smb.conf | /bin/cut -d ' ' -f 1`
				smbconf_cksum_ver=`/bin/cat ${TMP_CONFIG}/smb.conf.cksum`
				if [ -n $smbconf_cksum_ver ] && [ $smbconf_cksum_ver = $smbconf_cksum_val ]; then
					/bin/umount $DEV_NAS_CONFIG
					/bin/echo "No need to do recovery."
					return 0
				fi
			fi	
			if [ -f ${TMP_CONFIG}/smb.conf ] && [ -f ${TMP_CONFIG}/smb.conf.cksum ]; then
				flash_smbconf_cksum_val=`/sbin/cksum ${TMP_CONFIG}/smb.conf | /bin/cut -d ' ' -f 1`
				[ -f /etc/config/smb.conf ] && smbconf_cksum_val=`/sbin/cksum /etc/config/smb.conf | /bin/cut -d ' ' -f 1`
				flash_smbconf_cksum_ver=`/bin/cat ${TMP_CONFIG}/smb.conf.cksum`
				if [ $smbconf_cksum_val != $flash_smbconf_cksum_val ] && [ $flash_smbconf_cksum_ver = $flash_smbconf_cksum_val ]; then
					#/bin/cp ${TMP_CONFIG}/smb.conf /etc/config/smb.conf
					#/bin/cp ${TMP_CONFIG}/smb.conf.cksum /etc/config/smb.conf.cksum
					restore_flag1=1
				fi
			fi
		fi
		if [ ! -f ${TMP_CONFIG}/smb.conf ] && [ ! -f ${TMP_CONFIG}/smb.conf.cksum ] && [ -f /etc/config/smb.conf ]; then
			DEV_NAS_CONFIG_FREE_SIZE=`/bin/df -k | /bin/grep "$DEV_NAS_CONFIG" | /bin/awk '{print $4}'`
			freesize_need=$(($SMBCONF_SIZE+$DEV_NAS_CONFIG_SIZE_MIN))
			if [ ! -z $DEV_NAS_CONFIG_FREE_SIZE ] && [ $DEV_NAS_CONFIG_FREE_SIZE -gt $freesize_need ]; then 
				smbconf_cksum_val=`/sbin/cksum /etc/config/smb.conf | /bin/cut -d ' ' -f 1`
				/bin/cp /etc/config/smb.conf ${TMP_CONFIG}/smb.conf
				/bin/echo $smbconf_cksum_val > ${TMP_CONFIG}/smb.conf.cksum
				if [ -f ${TMP_CONFIG}/system.map.key ]; then
					/bin/cp ${TMP_CONFIG}/system.map.key /etc/config/system.map.key
				else
					/sbin/uuidgen > ${TMP_CONFIG}/system.map.key
					/bin/cp ${TMP_CONFIG}/system.map.key /etc/config/system.map.key
				fi
			fi			
		fi
		if [ ! -f /etc/config/smb.conf.cksum ]; then
			smbconf_cksum_val=`/sbin/cksum /etc/config/smb.conf | /bin/cut -d ' ' -f 1`
			/bin/echo $smbconf_cksum_val > /etc/config/smb.conf.cksum
		fi
		fi #end of Model_Name.conf
		/bin/sync
		/bin/umount $DEV_NAS_CONFIG
		echo "Check config files...end"
	else
		echo "Mount $DEV_NAS_CONFIG failed...try to recover it..."
		[ -f ${NASCONFIG_FS} ] && echo "${NASCONFIG_FS} is found."
		[ -f ${NASCONFIG_FS} ] && /bin/tar xzf ${NASCONFIG_FS} -O > $DEV_NAS_CONFIG
	fi
	
	#Restore config logs
	if [ $restore_flag1 = 1 ]; then
		echo 
		#/sbin/write_log "System configuration file(s) recovered." 2
	fi

}

stop_all_dev(){
	MD_DEVICES="md0 md1 md2 md3 md4 md5"
	HD_DEVICES="sda3 sdb3 sdc3 sdd3 sde3 sdf3 sdg3 sdh3 sdi3 sdj3 sdk3 sdkl3 sdz3"
	/sbin/qwatchdogd -d
	if [ -f /tmp/ups_stop_service ]; then
		/bin/ps | grep "/etc/rcK.d/" | grep -v grep >/dev/null
		[ $? != 0 ] || /usr/sbin/killall5
	else
		/usr/sbin/killall5
	fi
	/sbin/qwatchdogd &
	echo "umount and stop encrypt devices."
	for i in $MD_DEVICES
	do
		if [ `/sbin/getcfg Storage "Encrypt_${i}" -d "FALSE"` = "TRUE" ]; then
			/bin/umount /dev/mapper/${i} 1>>/dev/null 2>>/dev/null
			if [ "$?" = "0" ] && [ -f /sbin/cryptsetup ]; then
				 /sbin/cryptsetup luksClose ${i} 1>>/dev/null 2>>/dev/null
				 [ "$?" = "0" ] && /sbin/mdadm -S /dev/${i}
			fi
		else
			/bin/cat /proc/mounts | /bin/grep ${i} | /bin/grep "ro,"
			if [ $? = 0 ]; then
				readonly_flag=1
			else
				readonly_flag=0
			fi
			/bin/umount /dev/${i} 1>>/dev/null 2>>/dev/null
			if [ "$?" = "0" ]; then
				/bin/sleep 1
				if [ $readonly_flag = 0 ]; then
					/sbin/mdadm -S /dev/${i}
					[ "$?" = "0" ] || echo "Cannot stop raid device /dev/${i}."
				else
					echo "/dev/${i} is set read-only."
				fi
			fi		
		fi
	done
	for i in $HD_DEVICES
	do
		if [ `/sbin/getcfg Storage "Encrypt_${i}" -d "FALSE"` = "TRUE" ]; then
			/bin/umount /dev/mapper/${i} 1>>/dev/null 2>>/dev/null
			if [ "$?" = "0" ] && [ -f /sbin/cryptsetup ]; then
				 /sbin/cryptsetup luksClose ${i} 1>>/dev/null 2>>/dev/null
			fi
		else
			/bin/umount /dev/${i} 1>>/dev/null 2>>/dev/null
		fi
	done
}

PUBLIC_SHARE=Public
# Use around 5TB (5000000M)
USE_THRESHOLD=5000000
# Free around 5G (5000M)
AVAIL_THRESHOLD=5000 

preload_group_block_bitmap()
{
    /bin/echo "group block bitmap usage checking..."
    public_dir=$(/sbin/getcfg $PUBLIC_SHARE path -f /etc/config/smb.conf)
    if [ ! -z $public_dir ] && [ -d $public_dir ]; then
        public_mnt_path=$(/usr/bin/dirname $public_dir)
        /bin/echo "find default mount path $public_mnt_path"
        if [ ! -z $public_mnt_path ] && [ -d $public_mnt_path ]; then
            use_size="$(/bin/df -m | /bin/awk -v mnt_path=$public_mnt_path '{if ($6 == mnt_path) print $3}')"
            avail_size="$(/bin/df -m | /bin/awk -v mnt_path=$public_mnt_path '{if ($6 == mnt_path) print $4}')"
            if [ X != X$use_size ] && [ X != X$avail_size ]; then
                if [ $use_size -gt $USE_THRESHOLD ] && [ $avail_size -gt $AVAIL_THRESHOLD ]; then
                    /bin/echo "$USE_THRESHOLD/$AVAIL_THRESHOLD exceeds threshold, try to preload them."
                    /bin/dd if=/dev/zero of=$public_mnt_path/.group_bitmap_preload bs=1M count=1024
                    /bin/rm -f $public_mnt_path/.group_bitmap_preload
                fi
            fi
        fi
    fi
}

case "$1" in
  start)
	echo "Starting Disk initialized:"
	[ -f /sbin/lcd_tool ] && /sbin/lcd_tool -1 "Mount Volume" -2 "Please Wait..."
	check_config
	/bin/rm /etc/config/usb_share_mapping -f
	/bin/touch /etc/config/usb_share_mapping
	/bin/touch /etc/default_config/usb_share_mapping
	/bin/touch /var/lock/add_ext_dev
	/bin/rm /etc/config/esata_share_mapping -f
        /bin/touch /etc/config/esata_share_mapping
        /bin/touch /etc/default_config/esata_share_mapping
	/bin/touch /etc/config/usb_share_setting
	/bin/touch /etc/default_config/usb_share_setting
	# Clear QRAID1 status to avoid system shutdown abnormal.
	/sbin/setcfg QRAID1 "device name" ""
	if [ -f /sbin/storage_boot_init ]; then
		/sbin/storage_boot_init 2
	fi
    # preload FS group block bitmap 
    preload_group_block_bitmap
	/bin/touch /tmp/init.1
	# Check external disks.
	if [ -f /sbin/storage_boot_init ]; then
		/sbin/storage_boot_init 6 &
	fi
	[ -f /sbin/hotswap ] && /sbin/daemon_mgr hotswap stop "/sbin/hotswap &"
	/bin/sleep 1
	[ -f /sbin/hotswap ] && /sbin/daemon_mgr hotswap start "/sbin/hotswap &" &
	/bin/touch /var/ledvalue
	/bin/touch /tmp/check.status
	/bin/touch /tmp/format.status
#---u-boot & uImage_bak update---	
if [ "x${BOOT_CONF}" = "xTS-NASARM" ]; then
	if [ -f $ROOT_PART/update_pkg/uImage_bak ]; then
		echo "Update Kernel..."
		check_sum "$ROOT_PART/update_pkg/uImage_bak" "$ROOT_PART/update_pkg/uImage_bak.cksum"
		if [ $? = 0 ]; then
			/bin/dd if=$ROOT_PART/update_pkg/uImage_bak of=${FLASH_KERNEL}
			echo "Kernel is updated."
		else
			echo "Kernel update error."
		fi
		/bin/rm -f $ROOT_PART/update_pkg/uImage_bak*
	fi
	#Reboot after u-boot is updated.
	if [ -f $ROOT_PART/update_pkg/u-boot.bin ]; then
		echo "Update U-boot..."
		check_sum "$ROOT_PART/update_pkg/u-boot.bin" "$ROOT_PART/update_pkg/u-boot.bin.cksum"
		if [ $? = 0 ]; then
			/bin/dd if=$ROOT_PART/update_pkg/u-boot.bin of=${FLASH_UBOOT}
			echo "u-boot is updated."
		else
			echo "u-boot update error."
		fi
		/bin/rm -f $ROOT_PART/update_pkg/u-boot.bin*
		echo "Reboot..."
		/sbin/reboot
	fi
fi
	[ -f /sbin/qsmartd ] && ( /sbin/daemon_mgr qsmartd start "/sbin/qsmartd -d" & ) && sleep 1
#	for TS119 TS219 TS419
	if [ -f /etc/config/quick_update.flag ]; then
		/sbin/setcfg "Misc" "Configured" "TRUE"
		rm -f /etc/config/quick_update.flag
	fi
	if [ "x${NSS_ENABLED}" = "xyes" ]; then
		cp -v /etc/init.d/nvr_svc.sh /etc/init.d/nvrd.sh
	else
		touch /home/httpd/cgi-bin/data/lang_tools.js
		if [ ! -e /home/httpd/cgi-bin/surv/lang ]; then
			mkdir -p /home/httpd/cgi-bin/surv/lang
		fi
		touch /home/httpd/cgi-bin/surv/lang/lang_tools.js

	fi
	_init_spool
	standby_enable=`/sbin/getcfg Misc "Disk StandBy Timeout Enable" -d FALSE`
	[ "x${standby_enable}" = "xFALSE" ] && /sbin/hdsuspend -a -t 0
	/sbin/gen_next_alarm
#Write BOOT to crontab for RR postpone.
	/sbin/gen_next_alarm 2 -c
	;;
  stop)
  [ -f /tmp/donotrecover ] || update_flash_config
	echo "Unmount ISO shares"
        /etc/init.d/iso_mount.sh stop
	/sbin/daemon_mgr klogd.sh stop "/etc/init.d/klogd.sh stop" &
	/etc/init.d/klogd.sh stop
	if [ -f /sbin/storage_boot_init ]; then
		/sbin/storage_boot_init 7
	fi
	[ -f /sbin/hotswap ] && /sbin/daemon_mgr hotswap stop "/sbin/hotswap -d" &
	/sbin/daemon_mgr bcclient stop "/sbin/bcclient"
	/sbin/daemon_mgr rsync stop "/sbin/rsync --daemon" &
	[ -f /sbin/hd_util ] && /sbin/daemon_mgr hd_util stop "/sbin/hd_util -d" &
	[ -f /sbin/qsmartd ] && /bin/kill -HUP `/bin/pidof qsmartd`
	[ -f /sbin/qsmartd ] && /sbin/daemon_mgr qsmartd stop "/sbin/qsmartd -d" &
	[ -f /sbin/qsmart ] && /sbin/qsmart -c -d 1 1>/dev/null 2>/dev/null
	[ -f /sbin/qsmart ] && /sbin/qsmart -c -d 2 1>/dev/null 2>/dev/null
	[ -f /sbin/qsmart ] && /sbin/qsmart -c -d 3 1>/dev/null 2>/dev/null
	[ -f /sbin/qsmart ] && /sbin/qsmart -c -d 4 1>/dev/null 2>/dev/null
	#set power recovery mode to power off if mode is last state
# check if the HAL subsystem exist
if [ -x /sbin/hal_app ]; then
    /sbin/hal_event --se_shutdown
else
    Internal_Model=`/sbin/getcfg System "Internal Model"`
    if [ "x$Internal_Model" != "xTS-879" ] && [ "x$Internal_Model" != "xTS-1079" ] && [ "x$Internal_Model" != "xTS-1279" ] && [ "x$Internal_Model" != "xTS-1679" ] ;then
        Power_Recovery_Mode=`/sbin/getcfg Misc Power_Recovery_Mode -d 0`;
	    case "$Power_Recovery_Mode" in
    	    0)
              #LAST STATE
              /sbin/pic_raw 68
              echo "last state mode"
              ;;
	    esac
    fi
fi    
	/bin/kill -9 `/bin/pidof daemon_mgr`
	/bin/kill -9 `/bin/pidof utelnetd`
	/bin/kill -9 `/bin/pidof proftpd`
	/bin/kill -9 `/bin/pidof rsyncd`
	/bin/kill -9 `/bin/pidof cupsd`
	if [ -f /tmp/ups_stop_service ]; then
		/sbin/daemon_mgr lcdmond stop /sbin/lcdmond 1>>/dev/null 2>>/dev/null
		/bin/sleep 1
		[ -f /sbin/lcd_tool ] && /sbin/lcd_tool -f 1>>/dev/null 2>>/dev/null
	fi
# check if the HAL subsystem exist
if [ -x /sbin/hal_app ]; then
    /sbin/hal_event --clear_ipc
    /sbin/storage_util --data_umount
else
	stop_all_dev
fi    
	[ -f /sbin/umount_all_extdev ] && /sbin/umount_all_extdev
	/bin/umount /dev/md9
	if [ "$?" = "0" ]; then
		/bin/sleep 1
		/sbin/mdadm -S /dev/md9
		[ "$?" = "0" ] || echo "Cannot stop raid device /dev/md9."
	fi
	/sbin/swapoff ${SWAP_DEVICE}
	[ "$?" = "0" ] && /sbin/mdadm -S ${SWAP_DEVICE}
	/bin/sleep 3
	/bin/umount $EXT_ROOT
	[ "$?" = "0" ] || /bin/echo "init_disk.sh: Umount $EXT_ROOT failed."
	/sbin/mdadm -S $EXT_ROOT
	[ "$?" = "0" ] || /bin/echo "Cannot stop raid device $EXT_ROOT."
#Turn off HDD LED
# check if the HAL subsystem exist
if [ ! -x /sbin/hal_app ]; then
	for i in 0 1 2 3 4 5; do
		[ ! -f /sys/class/scsi_host/host$i/em_message ] || /bin/echo 0x0 > /sys/class/scsi_host/host$i/em_message && sync
	done
else
	# reboot all SAS enclosure if enclosure fw has been upgrade
        if [ -f /etc/enclosure_upgrade.info ]; then
		/sbin/hal_app --se_reboot enc_id=-1
        fi
fi    
	;;
  restart)
        echo "Restarting Disk services:"
        $0 stop
        $0 start
        echo "done."
        ;;
  update_flash_config)
		#update_flash_config
		echo
	;;
	check_config)
		check_config
	;;
  init_spool)
	_init_spool
	;;
  *)
        echo "Usage: init_disk {start|stop|restart}"
        exit 1
esac
