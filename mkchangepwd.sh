#!/bin/sh

if [ -z "$1" ] || [ -z "$2" ]; then
	echo $0 [username] [password]
fi

OUT_FILE="/etc/config/runone/changepwd.sh"
USER="$1"
PWD="$2"

if [ ! -d "/etc/config/runone" ]; then
	echo "mkdir /etc/config/runone"
	/bin/mkdir "/etc/config/runone"
fi

echo "#!/bin/sh" > "$OUT_FILE"
echo "" >> "$OUT_FILE"
echo "/usr/bin/passwd -p $PWD $USER" >> "$OUT_FILE"
echo "/usr/local/samba/bin/smbpasswd $USER $PWD" >> "$OUT_FILE"

/bin/chmod 755 "$OUT_FILE"
