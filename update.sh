#!/bin/sh
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

UPDATE_FOLDER="/mnt/update"
UPDATEPKG_DIR="/mnt/HDA_ROOT/update_pkg"
PIC_RAW="/sbin/pic_raw"
SET_STATUS_LED="/sbin/set_status_led"
UPDATE_PROCESS="/tmp/update_process"
UPDATE_FW_PROCESS="/tmp/update_fx_process"
RAIDMNG_STATUS="/tmp/raidmng.status"
BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf 2>/dev/null`
USING_RAM1=0
FUNC_SWITCH_CONF="/etc/config/.funcSwitch.conf"
APP_RUNTIME_CONF="/var/.application.conf"
UPDATE_LOCKED=6

begin_led()
{
# check if the HAL subsystem exist
    if [ -x /sbin/hal_app ]; then
        /sbin/hal_app --status_led_set enc_id=0,event=64,status=1
        /sbin/hal_app --power_led_blink enc_id=0,enable=1
    else
    	${SET_STATUS_LED} 64 1; ${PIC_RAW} 76
    fi    	
}
end_led()
{
# check if the HAL subsystem exist
    if [ -x /sbin/hal_app ]; then
        /sbin/hal_app --status_led_set enc_id=0,event=64,status=0
        /sbin/hal_app --power_led enc_id=0,enable=1
    else
    	${SET_STATUS_LED} 64 0; ${PIC_RAW} 77
    fi
}
do_beep()
{

	if [ x`/sbin/getcfg Misc "Buzzer Quiet Enable"` != "xTRUE" ]; then
# check if the HAL subsystem exist
        if [ -x /sbin/hal_app ]; then
    		/sbin/hal_app --se_buzzer enc_id=0,mode=1
            sleep 1
    		/sbin/hal_app --se_buzzer enc_id=0,mode=1
            sleep 1
    		/sbin/hal_app --se_buzzer enc_id=0,mode=1
        else
    		${PIC_RAW} 81; sleep 1; ${PIC_RAW} 81; sleep 1; ${PIC_RAW} 81
        fi    		
	fi
}

remove_update()
{
		/bin/rm -rf ${UPDATE_FOLDER}/*
}

get_new_ver()
{
	local filename=$path_name
	set -- `/usr/bin/du -b "$filename"`
	local pos=`expr $1 - 48`
	/bin/dd if=$filename of=${UPDATE_FOLDER}/newver skip=$pos bs=1 count=16
	[ $? = 0 ] && cat ${UPDATE_FOLDER}/newver || echo ""
}

md5="0"
cksum="0"
project_name=`/sbin/getcfg Project Name -d QNAP -f /var/default`

get_md5_cksum()
{
	if [ "x$project_name" = "xAthens" ]; then
		[ -f /bin/md5sum ] || /bin/ln -sf /bin/busybox /bin/md5sum
		md5=`/bin/md5sum $path_name | /bin/cut -d ' ' -f 1`
		echo "md5=${md5}"
	else
		cksum=`/sbin/cksum $path_name | /bin/cut -d ' ' -f 1`
		echo "cksum=${cksum}"
	fi
}

set_md5_cksum()
{
	if [ "x$project_name" = "xAthens" ]; then
		/sbin/setsum md5 ${md5}
	else
		/sbin/setsum cksum ${cksum}
	fi
}

check_hidden_feature()
{
if [ $1 \> $2 ]; then
	echo "Downgrade from $1 to $2"
	if [ "x1" = "x`/sbin/getcfg OPTION HomeFeature -d 0 -f $APP_RUNTIME_CONF`" ]; then
		/sbin/setcfg DISABLE qdownload 1 -f $APP_RUNTIME_CONF
		/sbin/setcfg DISABLE musics 1 -f $APP_RUNTIME_CONF
		/sbin/setcfg DISABLE photos 1 -f $APP_RUNTIME_CONF
		/sbin/setcfg DISABLE qmultimedia 1 -f $APP_RUNTIME_CONF
		/sbin/setcfg DISABLE iTunes 1 -f $APP_RUNTIME_CONF
		/sbin/setcfg DISABLE upnp 1 -f $APP_RUNTIME_CONF
		/sbin/setcfg DISABLE survielance 1 -f $APP_RUNTIME_CONF
		/sbin/setcfg DISABLE HomeFeature 1 -f $APP_RUNTIME_CONF
		if [ -f $FUNC_SWITCH_CONF ]; then
			/sbin/setcfg DISABLE qdownload 1 -f $FUNC_SWITCH_CONF
			/sbin/setcfg DISABLE musics 1 -f $FUNC_SWITCH_CONF
			/sbin/setcfg DISABLE photos 1 -f $FUNC_SWITCH_CONF
			/sbin/setcfg DISABLE qmultimedia 1 -f $FUNC_SWITCH_CONF
			/sbin/setcfg DISABLE iTunes 1 -f $FUNC_SWITCH_CONF
			/sbin/setcfg DISABLE upnp 1 -f $FUNC_SWITCH_CONF
			/sbin/setcfg DISABLE survielance 1 -f $FUNC_SWITCH_CONF
			/sbin/setcfg DISABLE HomeFeature 1 -f $FUNC_SWITCH_CONF
		fi
		/sbin/setcfg OPTION HomeFeature 0 -f $APP_RUNTIME_CONF
		echo "HomeFeature disabled."
	fi
fi
}
do_exit()
{
	local ret=0
	local old_ver=`/sbin/getcfg System Version`
	local new_ver=`/bin/cat ${UPDATE_FOLDER}/newver`

	if [ "x${BOOT_CONF}" = "xTS-NASX86" ] && [ "x$USING_RAM1" = "x1" ]; then
		/bin/umount /mnt/ram1 2>/dev/null 1>/dev/null
	fi
	if [ $1 != $UPDATE_LOCKED ]; then
		/sbin/lock_system_update unlock
	fi

	case $1 in
		0)#normal exit
			set_md5_cksum
			ret=0
			[ ! -z $new_ver ] && [ ! -z $old_ver ] && check_hidden_feature "$old_ver" "$new_ver"
			[ ! -z $new_ver ]	\
			&& /sbin/write_log "[Firmware Upgrade] System updated successfully from $old_ver to $new_ver." 4			\
			|| /sbin/write_log "[Firmware Upgrade] System updated successfully." 4
			;;
		1)#list tar failed
			echo -1 > ${UPDATE_PROCESS}
			echo -1 > ${UPDATE_FW_PROCESS}
			ret=1
			[ ! -z $new_ver ]	\
			&& /sbin/write_log "[Firmware Upgrade] System update failed." 1				\
			|| /sbin/write_log "[Firmware Upgrade] System update failed." 1
			;;
		2)#extract tar failed
			echo -2 > ${UPDATE_PROCESS}
			echo -2 > ${UPDATE_FW_PROCESS}
			ret=1
			[ ! -z $new_ver ]	\
			&& /sbin/write_log "[Firmware Upgrade] System update failed." 1				\
			|| /sbin/write_log "[Firmware Upgrade] System update failed." 1
			;;
		$UPDATE_LOCKED)#update is locked
			echo -$UPDATE_LOCKED > ${UPDATE_PROCESS}
			echo -$UPDATE_LOCKED > ${UPDATE_FW_PROCESS}
			ret=1
			/sbin/write_log "[Firmware Upgrade] System update failed. Maybe system is rebooting or another update is processing" 1
			;;
		*)
			echo -5 > ${UPDATE_PROCESS}
			echo -5 > ${UPDATE_FW_PROCESS}
			ret=1
			[ ! -z $new_ver ]	\
			&& /sbin/write_log "[Firmware Upgrade] System update failed." 1				\
			|| /sbin/write_log "[Firmware Upgrade] System update failed." 1
			;;
	esac

	if [ $ret != 0 ]; then
		remove_update
	fi
	/sbin/setcfg -e System "QPKG CENTER XML"
	grep VOLUME ${RAIDMNG_STATUS} 1>>/dev/null 2>>/dev/null
	if [ $? = 0 ]; then
		grep VOLUME ${RAIDMNG_STATUS} | grep "\-1" 1>>/dev/null 2>>/dev/null
		[ $? != 0 ] && exit $ret
	fi

	
	end_led
	exit $ret
}

remove_pre_pkg()
{
	if [ -f "${UPDATEPKG_DIR}/antivirus.tgz" ]; then
		/etc/init.d/antivirus.sh stop 2>>/dev/null
		/bin/rm -f "${UPDATEPKG_DIR}/antivirus.tgz"
	fi
}

begin_led
/sbin/lock_system_update lock
if [ $? != 0 ]; then
	do_exit $UPDATE_LOCKED
fi
path_name=$1
if [ -f $path_name ]; then
	get_md5_cksum
	_tgz=${path_name}.tgz
	if [ "x${BOOT_CONF}" = "xTS-NASX86" ]; then
		_imgsize=`/usr/bin/du -k $path_name | /bin/cut -f 1`
		_imgsize=`/usr/bin/expr $_imgsize \* 2`
		/sbin/mke2fs -m0 /dev/ram1 2>/dev/null 1>/dev/null
		[ -d /mnt/ram1 ] || /bin/mkdir /mnt/ram1 > /dev/null
		/bin/mount /dev/ram1 /mnt/ram1 2>/dev/null 1>/dev/null
		if [ $? = 0 ]; then
			_ram1size=`/bin/df -k | /bin/grep "/dev/ram1" | /bin/awk '{print $4}'`
			if [ ! -z ${_ram1size} ] && [ ${_ram1size} -gt ${_imgsize} ]; then
				_tgz=/mnt/ram1/update_image.tgz
				USING_RAM1=1
			else
				/bin/umount /mnt/ram1 2>/dev/null 1>/dev/null
			fi
		fi
	fi
	#remove existing package
	#remove_pre_pkg
	/sbin/PC1 d QNAPNASVERSION4 $path_name ${_tgz}; \
	get_new_ver
	/sbin/degrade_check $path_name
	if [ $? = 0 ]; then
		do_exit 3
	fi
	/bin/rm -f $path_name
	/bin/tar tzf ${_tgz}; [ $? != 0 ] && do_exit 1;
	/bin/tar xzf ${_tgz} -C ${UPDATE_FOLDER}; [ $? != 0 ] && do_exit 2; \
	/bin/rm -f ${_tgz}
	if [ "x${BOOT_CONF}" = "xTS-NASX86" ] && [ "x$USING_RAM1" = "x1" ]; then
		/bin/umount /mnt/ram1 2>/dev/null 1>/dev/null
	fi
	[ -f ${UPDATE_FOLDER}/update_img.sh ] && /bin/mv ${UPDATE_FOLDER}/update_img.sh /tmp/update_img.sh

	if [ -f /tmp/update_img.sh ]; then
		/tmp/update_img.sh $path_name
	else
		/etc/init.d/update_img.sh $path_name
	fi
	if [ $? = 0 ]; then
#		do_beep
#		do_beep_check_flag
		do_exit 0
	elif [ `/bin/cat $UPDATE_PROCESS`x != -3x ]; then
		do_exit 5
	fi
else
	do_exit 1
fi
