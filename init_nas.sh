#!/bin/sh
ROOT_PART="/mnt/HDA_ROOT"
MOUNTPOINT="/tmp/config"
BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf 2>/dev/null`
# check if the HAL subsystem exist
if [ -x /sbin/hal_app ]; then
    BOOT_DEV=$(/sbin/hal_app --get_boot_pd port_id=0)
elif [ "x${BOOT_CONF}" = "xTS-NASARM" ]; then
	BOOT_DEV="/dev/mtdblock"
else
    BOOT_DEV="/dev/sdx"
fi

if [ "x${BOOT_CONF}" = "xTS-NASARM" ]; then
    DEV_NAS_CONFIG=${BOOT_DEV}5
else
    DEV_NAS_CONFIG=${BOOT_DEV}6
fi
BOOTCOUNT="$MOUNTPOINT/BOOT_COUNT"
EXPR="/usr/bin/expr"
UPDATEPKG_DIR="${ROOT_PART}/update_pkg"
CMS_ENABLE=`/sbin/getcfg CMS Enable -d FALSE -f /etc/config/uLinux.conf`

if [ "`/sbin/getcfg -f /etc/default_config/uLinux.conf NVR Support -d FALSE`" == "TRUE" ]; then
    SUPPORT_NVR=yes
else
    SUPPORT_NVR=no
fi

#Look usb copy function in start QNAS
/sbin/setcfg "usb copy" "lock flag" 1 -f "/etc/config/uLinux.conf"

rsync_startup() {
    rsync_support=`/sbin/getcfg System "Rsync Support" -u`
    if [ "x${rsync_support}" = "x" ]; then
		/sbin/setcfg System "Rsync Support" TRUE
    fi
    /etc/init.d/rsyncd.sh start &
}

check_def_share() {
	defRecordingsShare=`/sbin/getcfg SHARE_DEF defRecordings -d Qrecordings -f /etc/default_config/def_share.info`
	[ -f /etc/config/def_share.info ] && /bin/cp /etc/config/def_share.info /etc/default_config/def_share.info
	[ -f /etc/config/def_share.info ] || /bin/touch /etc/config/def_share.info
	PublicShare=`/sbin/getcfg SHARE_DEF defPublic -d Public -f /etc/config/def_share.info`
	UsbShare=`/sbin/getcfg SHARE_DEF defUsb -d Qusb -f /etc/config/def_share.info`
	WebShare=`/sbin/getcfg SHARE_DEF defWeb -d Qweb -f /etc/config/def_share.info`
	MultimediaShare=`/sbin/getcfg SHARE_DEF defMultimedia -d Qmultimedia -f /etc/config/def_share.info`
	DownloadShare=`/sbin/getcfg SHARE_DEF defDownload -d Qdownload -f /etc/config/def_share.info`
	RecordingsShare=`/sbin/getcfg SHARE_DEF defRecordings -d Qrecordings -f /etc/config/def_share.info`

    if [ "x$SUPPORT_NVR" = "xyes" ]; then
		if [ x$RecordingsShare = x"none" ]; then
			/sbin/setcfg SHARE_DEF defRecordings "${defRecordingsShare}" -f /etc/default_config/def_share.info
			/sbin/setcfg SHARE_DEF defRecordings "${defRecordingsShare}" -f /etc/config/def_share.info
		fi
        if [ `/sbin/getcfg NVR "Enable" -u -d FALSE` = TRUE ]; then
                DEF_SHARE="${PublicShare} ${UsbShare} ${WebShare} ${MultimediaShare} ${DownloadShare} ${RecordingsShare}"
        else
                DEF_SHARE="${PublicShare} ${UsbShare} ${WebShare} ${MultimediaShare} ${DownloadShare}"
        fi
    else
		if [ x$RecordingsShare != x"none" ]; then
			/sbin/setcfg SHARE_DEF defRecordings none -f /etc/default_config/def_share.info
			/sbin/setcfg SHARE_DEF defRecordings none -f /etc/config/def_share.info			
		fi
        DEF_SHARE="${PublicShare} ${UsbShare} ${WebShare} ${MultimediaShare} ${DownloadShare}"
    fi

    noshare=""
    cnt=0
    nofolder=""
    fcnt=0

    [ -z "`/bin/df | /bin/grep /share`" ] && return

    for i in $DEF_SHARE
    do
 			Dir_Path=`/sbin/getcfg $i path -d "none" -f /etc/config/smb.conf`  
 			echo "$Dir_Path"
			if [ "$i" != "none" ] && [ ! -d $Dir_Path ]; then
	   	 [ $cnt = 0 ] && noshare="$i" || noshare="$noshare, $i"
	   	 cnt=`expr $cnt + 1`
			fi 
		done

    for j in $DEF_SHARE
    do
			if [ "$i" != "none" ] && [ ! -d "/share/${j}" ]; then
	    	[ $fcnt = 0 ] && nofolder="$j" || nofolder="$nofolder, $j"
				fcnt=`expr $fcnt + 1`
			fi 
    done


    if [ $cnt -gt 0 ]; then
	[ $cnt = 1 ] \
	&& msg="Default network share($noshare) does not exist. Restore default network shares or format your disk volume." \
	|| msg="Default network shares($noshare) do not exist. Restore default network shares or format your disk volume."
	echo "Warning: $msg"
	/sbin/write_log "$msg" 2
    fi

    if [ $fcnt -gt 0 ]; then
	[ $fcnt = 1 ] \
	&& msg="Folder($nofolder) for default network shares does not exist. Restore default network shares or format your disk volume." \
	|| msg="Folders($nofolder) for default network shares do not exist. Restore default network shares or format your disk volume."
	echo "Warning: $msg"
	/sbin/write_log "$msg" 2
    fi


}

# booting count
booting_count() {
[ -d $MOUNTPOINT ] || /bin/mkdir $MOUNTPOINT
/bin/mount $DEV_NAS_CONFIG -t ext2 $MOUNTPOINT > /dev/null 2>&1
if [ $? = 0 ]; then
	[ -f $BOOTCOUNT ] || /bin/echo -n "0" > $BOOTCOUNT
	bcount=`/bin/cat $BOOTCOUNT`
	if [ ! -f $EXPR ]; then
		[ -f /bin/expr ] || /bin/ln -sf busybox /bin/expr
		EXPR="/bin/expr"
	fi
	bcount=`$EXPR ${bcount} + 1`
	[ ${bcount} -lt 10000 ] || bcount=0
	/bin/echo -n $bcount > $BOOTCOUNT
	/bin/sync
	/bin/umount $DEV_NAS_CONFIG
	[ $? = 0 ] || /bin/echo "umount $DEV_NAS_CONFIG failed."
	/bin/echo "The system had been started ${bcount} time(s)."
	return 0
fi
}

booting_cur_count() {
[ -d $MOUNTPOINT ] || /bin/mkdir $MOUNTPOINT
/bin/mount $DEV_NAS_CONFIG -t ext2 $MOUNTPOINT > /dev/null 2>&1
if [ $? = 0 ]; then
	[ -f $BOOTCOUNT ] || /bin/echo -n "0" > $BOOTCOUNT
	bcount=`/bin/cat $BOOTCOUNT`
	/bin/sync
	/bin/umount $DEV_NAS_CONFIG
	[ $? = 0 ] || /bin/echo "umount $DEV_NAS_CONFIG failed."
	/bin/echo "The system had been started ${bcount} time(s)."
	return 0
fi
}

reset_booting_count() {
[ -d $MOUNTPOINT ] || /bin/mkdir $MOUNTPOINT
/bin/mount $DEV_NAS_CONFIG -t ext2 $MOUNTPOINT > /dev/null 2>&1
if [ $? = 0 ]; then
	/bin/echo -n "0" > $BOOTCOUNT
	/bin/umount $DEV_NAS_CONFIG
	[ $? = 0 ] || /bin/echo "umount $DEV_NAS_CONFIG failed."
	/bin/echo "The booting count is set to 0."
	return 0
fi
}

case "$1" in
  booting_count)
		booting_count
		exit 0
	;;
  booting_cur_count)
		booting_cur_count
		exit 0
	;;
  reset_booting_count)
		reset_booting_count
		exit 0
	;;
  *)
  echo
esac
# booting count end here
/sbin/daemon_mgr bcclient stop /sbin/bcclient
/bin/pidof bcclient >/dev/null
[ $? != 0 ] || /bin/kill -9 `pidof bcclient`
/sbin/daemon_mgr bcclient start /sbin/bcclient
/sbin/setcfg System Booting 1

# add for Buzzer of warning
BUZZER="/root/.buzzer_warnning.conf"
echo "[Misc]" >> $BUZZER
if [ `/sbin/getcfg "Misc" "Buzzer Warning Enable" -u -d TRUE` = TRUE ]; then
        /sbin/setcfg "Misc" "Buzzer Warning Enable" TRUE -f $BUZZER
else
        /sbin/setcfg "Misc" "Buzzer Warning Enable" FALSE -f $BUZZER
fi

[ -f /sbin/picd ] && /sbin/daemon_mgr picd start "/sbin/picd  1>/dev/null 2>/dev/null &" &
[ -f /sbin/gpiod ] && /sbin/daemon_mgr gpiod start "/sbin/gpiod &" &

CPU_MODEL=""
[ ! -f /proc/tsinfo/cpu_model ] || CPU_MODEL=`/bin/cat /proc/tsinfo/cpu_model 2>/dev/null`
MODEL_NAME=`/sbin/getcfg "System" "Model"`
if [ "x${CPU_MODEL}" = "x88F6281 A0" ] && [ "x${MODEL_NAME}" = "xTS-119" ];then
        /bin/rm /sbin/hwmond
fi

[ -f /sbin/hwmond ] && /sbin/daemon_mgr hwmond start "/sbin/hwmond &" &
[ -f /sbin/acpid ] && /sbin/daemon_mgr acpid start "/sbin/acpid" &
[ -f /usr/sbin/upsutil ] && /sbin/daemon_mgr upsutil start "/usr/sbin/upsutil &" &

[ -f /sbin/gpiod ] && /bin/kill -USR1 `pidof gpiod`

if [ `/sbin/getcfg "" "User Enable" -u -d FALSE -f /etc/config/usr_quota.conf` = TRUE ]; then
	/sbin/quotaon -au > /dev/null 2>&1
	/sbin/setquota -u -F vfsv0 0 0 0 0 0 -a
	/sbin/setquota -u -F vfsv0 99 0 0 0 0 -a 2>>/dev/null
fi

rsync_startup

#start syslog server
[ -f /etc/init.d/rsyslog.sh ] && /etc/init.d/rsyslog.sh start &

# for customer
touch /tmp/quick_tmp.conf
/bin/mount $DEV_NAS_CONFIG -t ext2 /tmp/config
if [ "$?" = "0" ]; then
	echo -n "Automatically start autorun.sh: "
	if [ -f /tmp/config/autorun.sh ]; then
		echo "SUCCESS!"
		/tmp/config/autorun.sh
	else
		echo "Not found."
	fi
	for i in /tmp/config/* ;do
		case "$i" in
			/tmp/config/K01*.sh)
				echo $i start
				. $i start &
				cp $i /etc/rcK_init.d
				;;
			*)
				;;
		esac
	done
	if [ -f /tmp/config/.write_cache ]; then
		/sbin/setcfg Misc "Enable Write Cache" 1
		/sbin/setcfg Misc "Enable Write Cache" 1 -f /etc/default_config/uLinux.conf
	fi
	if [ -f /tmp/config/.case_sensitive ]; then
		/sbin/setcfg Samba "Case Sensitive" yes
		/sbin/setcfg Samba "Case Sensitive" yes -f /etc/default_config/uLinux.conf
	fi
	CNT=0
	while [ $CNT -lt 5 ]; do
		/bin/umount $DEV_NAS_CONFIG 2>/dev/null
		if [ $? = 0 ]; then
			break;
		fi
		sleep 1
		CNT=`expr $CNT + 1`
	done
	[ $? = 0 ] || /bin/echo "init_nas.sh: umount $DEV_NAS_CONFIG failed." 
fi

# For Badblocks Scan
if [ -f /etc/config/badblocks.conf ]; then
	/sbin/setcfg -f /etc/config/badblocks.conf "Bad Block" Status 0
fi

check_def_share
/etc/init.d/update_def_share.sh &
[ -f /sbin/get_hwsn ] && /sbin/get_hwsn

if [ x`/sbin/getcfg Misc "Buzzer Quiet Enable"` != "xTRUE" ]; then
	if [ -x /sbin/hal_app ]; then
		/sbin/hal_app --se_buzzer enc_id=0,mode=1
	else
		/sbin/pic_raw 81
	fi	    
fi

# For NASPPC TS-129 & TS-119P+ LOGO LED Mode
if [ "x${BOOT_CONF}" = "xTS-NASARM" ] || [ "x${BOOT_CONF}" = "xTS-NASPPC" ]; then
        CPU_MODEL=""
	[ ! -f /proc/tsinfo/cpu_model ] || CPU_MODEL=`/bin/cat /proc/tsinfo/cpu_model | cut -d ' ' -f 1 2>/dev/null`
        if [ "x${CPU_MODEL}" = "x88F6282" ];then
                LOGO_Model=`/sbin/getcfg System "QNAP_LOGO_Mode" -d 1`
                case "x${LOGO_Model}" in
                "x1")
                    /sbin/pic_raw 192
                ;;
                "x2")
                    /sbin/pic_raw 193
                ;;
                "x3")
                    /sbin/pic_raw 194
                ;;
                *)
                ;;
                esac
        fi
fi

Internal_Model=`/sbin/getcfg System "Internal Model"`
/bin/echo "init_nas.sh: check_last_degrade_error storage_boot_init 5"
if [ -f /sbin/storage_boot_init ]; then
	/sbin/storage_boot_init 5
fi
/bin/echo "init_nas.sh: set LED with storage_boot_init 4"
if [ -f /sbin/storage_boot_init ]; then
	/sbin/storage_boot_init 4
fi

[ -f /sbin/hd_util ] && /sbin/daemon_mgr hd_util start "/sbin/hd_util &" &

[ -f /sbin/.tune ] && /sbin/.tune
[ -f /sbin/gen_bandwidth ] && /sbin/daemon_mgr gen_bandwidth start "/sbin/gen_bandwidth -r -i 5 &" &

# For utelnetd
/etc/init.d/runone.sh
/etc/init.d/gen_issue.sh
/etc/init.d/ddns_update.sh
/usr/bin/readlink /etc/inittab | grep enable > /dev/null
/sbin/ip_filter

#add for online users
/sbin/initsem 2>/dev/null 1>/dev/null

# add Wake On Lan
NIC_NUM=`/sbin/getcfg "Network" "Interface Number"`
if [ `/sbin/getcfg "Misc" "Wake On Lan" -u -d TRUE` = TRUE ]; then
	echo "WOL: enabled."
	for (( i=0; i<$NIC_NUM; i=i+1 ))
  do
  	/usr/sbin/ethtool -s eth$i wol g
  done
else
	echo "WOL: disabled."
	for (( i=0; i<$NIC_NUM; i=i+1 ))
  do
  	/usr/sbin/ethtool -s eth$i wol d
  done
fi

if [ "x$Internal_Model" = "xTS-119" ] || [ "x$Internal_Model" = "xTS-219" ] || [ "x$Internal_Model" = "xTS-419" ] ;then
    if [ -x /sbin/hal_app ]; then
        if [ `/sbin/getcfg System "EUP Status" -d 0` = 0 ]; then
            echo "EUP: disable."
            /sbin/hal_app --se_sys_disable_eup enc_sys_id=root
        else
            echo "EUP: enable."
            /sbin/hal_app --se_sys_enable_eup enc_sys_id=root
        fi
    else
        CPU_MODEL=""
        [ ! -f /proc/tsinfo/cpu_model ] || CPU_MODEL=`/bin/cat /proc/tsinfo/cpu_model | cut -d ' ' -f 1 2>/dev/null`
        if [ "x${CPU_MODEL}" = "x88F6282" ];then
                if [ `/sbin/getcfg System "EUP Status" -d 0` = 0 ]; then
                        echo "EUP: disable."
                        /sbin/pic_raw 244
                else
                        echo "EUP: enable."
                        /sbin/pic_raw 245
                fi
        fi
    fi        
fi


# For localtime
if [ -f /etc/config/localtime ]; then
	/bin/echo "/etc/config/localtime is exist"
	/bin/rm /etc/localtime
	/bin/cp /etc/config/localtime /etc/localtime
else
	/bin/echo "/etc/config/localtime is not exist"
fi

# Only allow read in GUI 2.0
if [ -f /home/httpd/cgi-bin/authLogin.cgi ]; then
	/bin/chmod 444 /home/httpd/cgi-bin/quick/quick.cgi
	/bin/chmod 444 /home/httpd/cgi-bin/quick/quick_info.cgi
fi

/sbin/setcfg System Booting 0
/sbin/setcfg "UPS" "AC Power" "OK"

#Look usb copy function in Booting QNAS
/sbin/setcfg "usb copy" "lock flag" 0 -f "/etc/config/uLinux.conf"

#Set power LED on
if [ -x /sbin/hal_app ]; then
        /sbin/hal_app --power_led enc_id=0,enable=1
else
    if [ "x$Internal_Model" = "xTS-609" ] || [ "x$Internal_Model" = "xTS-239" ] || [ "x$Internal_Model" = "xTS-439" ] || [ "x$Internal_Model" = "xTS-639" ] || [ "x$Internal_Model" = "xTS-839" ] || [ "x$Internal_Model" = "xTS-259" ] || [ "x$Internal_Model" = "xTS-459" ] || [ "x$Internal_Model" = "xTS-559" ]|| [ "x$Internal_Model" = "xTS-659" ] || [ "x$Internal_Model" = "xTS-859" ] || [ "x$Internal_Model" = "xTS-1279" ] || [ "x$Internal_Model" = "xTS-879" ] || [ "x$Internal_Model" = "xTS-1079" ] || [ "x$Internal_Model" = "xTS-469" ] || [ "x$Internal_Model" = "xTS-569" ] || [ "x$Internal_Model" = "xTS-669" ] || [ "x$Internal_Model" = "xTS-869" ]; then
            /sbin/pic_raw 77
    fi
fi    

#set web file manager link
if [ `/sbin/getcfg "WebFS" "Enable" -u -d TRUE` = TRUE ]; then
	[ -d /home/httpd/cgi-bin/filemanager/share ] || /bin/ln -sf /share /home/httpd/cgi-bin/filemanager/share
else
	[ -d /home/httpd/cgi-bin/filemanager/share ] && /bin/rm -f /home/httpd/cgi-bin/filemanager/share
fi

#start Virtual Disk in TS-x10 TS-x19 TS-x12 This machine slower connection not occur before the boot process.
if [ "x$Internal_Model" = "xTS-119" ] || [ "x$Internal_Model" = "xTS-219" ] || [ "x$Internal_Model" = "xTS-419" ] || [ "x$Internal_Model" = "xTS-110" ] || [ "x$Internal_Model" = "xTS-210" ] || [ "x$Internal_Model" = "xTS-410" ] || [ "x$Internal_Model" = "xTS-112" ] || [ "x$Internal_Model" = "xTS-212" ] || [ "x$Internal_Model" = "xTS-412" ];then
	VDD_ENABLED_LAST=yes
else
	VDD_ENABLED_LAST=no
fi

if [ "${VDD_ENABLED_LAST}" = "no" ];then
	[ -f /etc/init.d/iscsiinit.sh ] && /etc/init.d/iscsiinit.sh start &
fi

#start lcdmond
if [ -f /sbin/lcd_hwtest ] ;then
    /sbin/lcd_hwtest -c
    [ $? = 0 ] && /sbin/daemon_mgr lcdmond start /sbin/lcdmond
fi
[ -f /sbin/qLogEngined ] && /sbin/daemon_mgr qLogEngined start /sbin/qLogEngined &
[ "x${CMS_ENABLE}" != xTRUE ] && [ -f /sbin/qsyslogd ] && /sbin/daemon_mgr qsyslogd start /sbin/qsyslogd &
[ -f /sbin/qShield ] && /sbin/daemon_mgr qShield start /sbin/qShield &
[ -f /etc/config/update_fw_ok ] && /bin/rm /etc/config/update_fw_ok &
[ -f /sbin/update_krb5_ticket ] && /sbin/update_krb5_ticket &
[ -f /etc/init.d/klogd.sh ] && /sbin/daemon_mgr klogd.sh start "/etc/init.d/klogd.sh start &" &

#start Radius
[ -f /etc/init.d/radius.sh ] && /etc/init.d/radius.sh start &

#mount ISO share
[ -f /etc/init.d/iso_mount.sh ] && /etc/init.d/iso_mount.sh start &

#check wfm2 config file
[ ! -f /etc/config/wfm2.conf ] && /bin/cp -rf /etc/default_config/wfm2.conf /etc/config/

#init acl
[ -f /sbin/init_acl ] && /etc/init.d/init_acl.sh &

#check ldap
if [ `/sbin/getcfg LDAP Enable -u -d FALSE` = TRUE ]
then
	/sbin/check_ldap & 1>/dev/null 2>/dev/null
fi

#start vpn pptp
[ -f /etc/init.d/vpn_pptp.sh ] && /etc/init.d/vpn_pptp.sh start &

#start vpn openvpn
[ -f /etc/init.d/vpn_openvpn.sh ] && /etc/init.d/vpn_openvpn.sh start &

#start ldap server
if [ -f /etc/init.d/ldap_server.sh ];then
	/etc/init.d/ldap_server.sh init
	/etc/init.d/ldap_server.sh start &
fi

if [ "${VDD_ENABLED_LAST}" = "yes" ];then
	[ -f /etc/init.d/iscsiinit.sh ] && /etc/init.d/iscsiinit.sh start &
fi

if [ -x "/sbin/appriv" ]; then
        if [ ! -f /etc/config/nas_priv.db  -o ! -s /etc/config/nas_priv.db ]; then
                /sbin/appriv -i
        else
                /sbin/appriv -g &
        fi
fi
