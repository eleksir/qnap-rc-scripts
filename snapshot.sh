#!/bin/sh
#
# chkconfig: 345 13 89
# description: Starts and stops the snapshot service
#

# Source function library.

DDSNAP_KO=/usr/local/lib/dm-ddsnap.ko

start()
{
	echo $"Starting snapshot service: "
	/sbin/insmod $DDSNAP_KO
}

stop()
{
	echo $"Stopping snapshot service: "
	/sbin/rmmod dm_ddsnap
}

restart()
{
	stop
	sleep 1
	start

}

[ -f $DDSNAP_KO ] || exit 0

case "$1" in
	start)
			start
			;;
	stop)
			stop
			;;
	restart)
			stop
			start
			;;
esac

exit $RETVAL
