#!/bin/sh
TMP_MP=/tmp/mp6
# check if the HAL subsystem exist
BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf 2>/dev/null`
if [ -x /sbin/hal_app ]; then
    BOOT_DEV=$(/sbin/hal_app --get_boot_pd port_id=0)
elif [ "x${BOOT_CONF}" = "xTS-NASARM" ]; then
	BOOT_DEV="/dev/mtdblock"
else
    BOOT_DEV="/dev/sdx"
fi

if [ "x${BOOT_CONF}" = "xTS-NASARM" ]; then
	NAS_CONFIG=${BOOT_DEV}5
	NAS_CONFIG_SIZE=1024
else    
    NAS_CONFIG=${BOOT_DEV}6
    NAS_CONFIG_SIZE=4096
fi
NAS_CONFIG_BAK=/tmp/nas_config.tgz
NAS_CONFIG_IMG=/tmp/nas_config.img


[ -d $TMP_MP ] || mkdir $TMP_MP
mount $NAS_CONFIG $TMP_MP
CFG_SIZE=`/bin/df -k | /bin/grep "$NAS_CONFIG" | /bin/awk '{print $2}'`
if [ $CFG_SIZE -ge $NAS_CONFIG_SIZE ]; then
	#Backup /tmp/mp6
	cd $TMP_MP
	/bin/tar czf $NAS_CONFIG_BAK *
	RET=$?
	cd /
	umount $TMP_MP
	if [ $RET = 0 ]; then
		/bin/dd if=/dev/zero of=$NAS_CONFIG_IMG bs=1k count=$NAS_CONFIG_SIZE
		if [ $? = 0 ]; then
			/sbin/mke2fs -F -v -m0 -b 1024 -I 128 $NAS_CONFIG_IMG
			/bin/dd if=$NAS_CONFIG_IMG of=$NAS_CONFIG bs=1k count=$NAS_CONFIG_SIZE
			mount $NAS_CONFIG $TMP_MP
			/bin/tar xzf $NAS_CONFIG_BAK -C $TMP_MP
			umount $TMP_MP
			rm $NAS_CONFIG_IMG
		fi
	fi
	rm $NAS_CONFIG_BAK
fi
umount $TMP_MP
