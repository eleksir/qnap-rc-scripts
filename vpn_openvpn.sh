#!/bin/sh

OPENVPN=/usr/sbin/openvpn
PIDFILE="/var/run/openvpn.server.pid"
OPENVPN_CONF="/etc/openvpn/server.conf"
OPENVPN_CONF_PATH="/etc/openvpn"
OPENVPN_CA_FILE="/etc/openvpn/easy-rsa/keys/ca.crt"
VPN_CONF="/etc/config/vpn.conf"
VPN_OPENVPN_CONF_PATH="/etc/config/openvpn"
VPN_ONLINE_LOG="/var/log/openvpn_online_user.log"
NIC_NUM=`/sbin/getcfg "Network" "Interface Number" -d "2"`

use_source_route_for_openvpn()
{
	TUN_DEV="tun0"
	TUN_IP_ADDR="$(/sbin/ifconfig ${TUN_DEV} 2> /dev/null | /bin/sed -n '/inet /{s/.*addr://;s/ .*//;p}')"
	TUN_SUBNET=`/bin/grep "^server" ${OPENVPN_CONF} | /bin/cut -d' ' -f 2`

	for LAN_PREFIX in eth bond
	do
		for (( i=0; i<$NIC_NUM; i=i+1 ))
		do
			case "$LAN_PREFIX" in
				eth)
				let ROUTE_TBL=$i+1
				;;
				bond)
				let ROUTE_TBL=$i+11
				;;
			esac
			LAN=$LAN_PREFIX$i
			IP_ADDR="$(/sbin/ifconfig $LAN 2> /dev/null | /bin/sed -n '/inet /{s/.*addr://;s/ .*//;p}')"
			if [ x$IP_ADDR != "x" ]; then
				/bin/ip route add $TUN_SUBNET/24 via $TUN_IP_ADDR dev ${TUN_DEV} tab $ROUTE_TBL &> /dev/null
			fi
		done
	done
	/bin/ip route flush cache
}

init_modules()
{
	/bin/echo "init insmod for openvpn"
	/bin/echo "1" > /proc/sys/net/ipv4/ip_forward
	/sbin/insmod /lib/modules/others/tun.ko 1>>/dev/null 2>&1
	if [ ! -L /usr/sbin/vpn_check_account ]; then
		/bin/ln -sf /sbin/nasutil /usr/sbin/vpn_check_account
	fi
	if [ ! -L /usr/sbin/vpn_openvpn_download ]; then
		/bin/ln -sf /sbin/nasutil /usr/sbin/vpn_openvpn_download
	fi	
}

init_iptable_modules()
{
	[ -L /usr/lib/libxtables.so.7 ] || /bin/ln -sf /usr/lib/libxtables.so.7.0.0 /usr/lib/libxtables.so.7
	[ -L /usr/lib/libxtables.so ] || /bin/ln -sf /usr/lib/libxtables.so.7.0.0 /usr/lib/libxtables.so
	[ -L /usr/lib/libip4tc.so.0 ] || /bin/ln -sf /usr/lib/libip4tc.so.0.0.0 /usr/lib/libip4tc.so.0
	[ -L /usr/lib/libip4tc.so ] || /bin/ln -sf /usr/lib/libip4tc.so.0.0.0 /usr/lib/libip4tc.so
	[ -L /usr/lib/libip6tc.so.0 ] || /bin/ln -sf /usr/lib/libip6tc.so.0.0.0 /usr/lib/libip6tc.so.0
	[ -L /usr/lib/libip6tc.so ] || /bin/ln -sf /usr/lib/libip6tc.so.0.0.0 /usr/lib/libip6tc.so
	/sbin/lsmod | /bin/grep x_tables 1>>/dev/null 2>>/dev/null
	if [ $? != 0 ]; then
		/bin/echo "insmod iptable modules"
		for i in x_tables.ko nf_conntrack.ko xt_tcpudp.ko nf_defrag_ipv4.ko nf_conntrack_ipv4.ko nf_nat.ko \
			ip_tables.ko iptable_nat.ko iptable_filter.ko ipt_MASQUERADE.ko ipt_REDIRECT.ko 
		do
			[ ! -f /lib/modules/others/vpn/$i ] || /sbin/insmod /lib/modules/others/vpn/$i
		done
	fi
}


remove_iptable_modules()
{
	for i in ipt_REDIRECT ipt_MASQUERADE iptable_filter iptable_nat ip_tables nf_nat nf_conntrack_ipv4 nf_defrag_ipv4 \
		xt_tcpudp nf_conntrack x_tables
	do
		/sbin/rmmod $i
	done
}

init_vars()
{
	/bin/rm -rf ${OPENVPN_CONF_PATH}/easy-rsa/vars
	/bin/touch ${OPENVPN_CONF_PATH}/easy-rsa/vars
cat >${OPENVPN_CONF_PATH}/easy-rsa/vars <<__EOF__
export EASY_RSA="\`/bin/pwd\`"
export OPENSSL="openssl"
export PKCS11TOOL="pkcs11-tool"
export GREP="/bin/grep"
export KEY_CONFIG=\`\$EASY_RSA/whichopensslcnf \$EASY_RSA\`
export KEY_DIR="\$EASY_RSA/keys"
export PKCS11_MODULE_PATH="dummy"
export PKCS11_PIN="dummy"
export KEY_SIZE=1024
export CA_EXPIRE=3650
export KEY_EXPIRE=3650
export KEY_COUNTRY="TW"
export KEY_PROVINCE="Taiwan"
export KEY_CITY="Taipei"
export KEY_ORG="QNAP Systems Inc."
export KEY_EMAIL="admin@qnap.com"
export KEY_CN="TS Series NAS"
export KEY_NAME="NAS"
export KEY_OU="NAS"
export PKCS11_MODULE_PATH=changeme
export PKCS11_PIN=1234
__EOF__

/bin/chmod 755 ${OPENVPN_CONF_PATH}/easy-rsa/vars

}


init()
{
	/bin/echo "openvpn init."
	if [ ! -L /etc/rcK.d/K48vpn_openvpn ]; then
		/bin/ln -sf ../init.d/vpn_openvpn.sh /etc/rcK.d/K48vpn_openvpn
	fi
	/sbin/vpn_util init_openvpn
	sleep 1
	/sbin/vpn_util init_client
	sleep 1
	/sbin/vpn_util init_readme
	sleep 1

	/bin/rm ${OPENVPN_CONF_PATH}/connect.sh
	/bin/ln -sf /sbin/nasutil ${OPENVPN_CONF_PATH}/connect.sh
	/bin/rm ${OPENVPN_CONF_PATH}/disconnect.sh
	/bin/ln -sf /sbin/nasutil ${OPENVPN_CONF_PATH}/disconnect.sh
	/bin/rm ${OPENVPN_CONF_PATH}/kick.sh
	/bin/ln -sf /sbin/nasutil ${OPENVPN_CONF_PATH}/kick.sh

	/bin/rm -rf /etc/openvpn/keys

	if [ -d ${VPN_OPENVPN_CONF_PATH} ]; then
		ADMIN_NAME=`/bin/grep  "Subject: C=TW" ${VPN_OPENVPN_CONF_PATH}/keys/myserver.crt | /bin/cut -d= -f 9`
		if [ "${ADMIN_NAME}" == "peterlai@qnap.com" ]; then 
			/bin/rm -rf ${VPN_OPENVPN_CONF_PATH}
			sleep 1
		fi
	fi

	if [ ! -d ${VPN_OPENVPN_CONF_PATH} ]; then
		/bin/mkdir -p /etc/config/openvpn/keys
		init_vars
		sleep 1
		cd /etc/openvpn/easy-rsa
		source /etc/openvpn/easy-rsa/vars 1>>/dev/null 2>&1
		/etc/openvpn/easy-rsa/clean-all 1>>/dev/null 2>&1
		sleep 1
		/etc/openvpn/easy-rsa/build-dh 1>>/dev/null 2>&1
		sleep 80
		/etc/openvpn/easy-rsa/pkitool --initca 1>>/dev/null 2>&1
		sleep 1
		/etc/openvpn/easy-rsa/pkitool --server myserver 1>>/dev/null 2>&1
		sleep 1

		if [ ! -f ${OPENVPN_CA_FILE} ]; then
			sleep 80
			if [ -f ${OPENVPN_CA_FILE} ]; then
				/bin/cp -rf /etc/openvpn/easy-rsa/keys /etc/config/openvpn
				/bin/ln -sf /etc/config/openvpn/keys /etc/openvpn/keys
			else
				sleep 100
				if [ -f ${OPENVPN_CA_FILE} ]; then
					/bin/cp -rf /etc/openvpn/easy-rsa/keys /etc/config/openvpn
					/bin/ln -sf /etc/config/openvpn/keys /etc/openvpn/keys
				else
					/bin/echo "openvpn init ${OPENVPN_CA_FILE} ERROR !." >/var/log/err_openvpn_ca.txt
				fi
			fi
		else
			/bin/cp -rf /etc/openvpn/easy-rsa/keys /etc/config/openvpn
			/bin/ln -sf /etc/config/openvpn/keys /etc/openvpn/keys
		fi
	else
		if [ -f /etc/config/openvpn/keys/ca.crt ]; then
			/bin/ln -sf /etc/config/openvpn/keys /etc/openvpn/keys
		else
			init_vars
			sleep 1
			cd /etc/openvpn/easy-rsa
			source /etc/openvpn/easy-rsa/vars 1>>/dev/null 2>&1
			/etc/openvpn/easy-rsa/clean-all 1>>/dev/null 2>&1
			sleep 1
			/etc/openvpn/easy-rsa/build-dh 1>>/dev/null 2>&1
			sleep 60
			/etc/openvpn/easy-rsa/pkitool --initca 1>>/dev/null 2>&1
			sleep 1
			/etc/openvpn/easy-rsa/pkitool --server myserver 1>>/dev/null 2>&1
			sleep 1

			if [ ! -f ${OPENVPN_CA_FILE} ]; then
				sleep 80
				if [ -f ${OPENVPN_CA_FILE} ]; then
					/bin/cp -rf /etc/openvpn/easy-rsa/keys /etc/config/openvpn
					/bin/ln -sf /etc/config/openvpn/keys /etc/openvpn/keys
				else
					sleep 100
					if [ -f ${OPENVPN_CA_FILE} ]; then
						/bin/cp -rf /etc/openvpn/easy-rsa/keys /etc/config/openvpn
						/bin/ln -sf /etc/config/openvpn/keys /etc/openvpn/keys
					else
						/bin/echo "openvpn init ${OPENVPN_CA_FILE} ERROR !." >/var/log/err_openvpn_ca.txt
					fi
				fi
			else
				/bin/cp -rf /etc/openvpn/easy-rsa/keys /etc/config/openvpn
				/bin/ln -sf /etc/config/openvpn/keys /etc/openvpn/keys
			fi
		fi
	fi

	/bin/echo "openvpn init OK !."
}

start()
{
	if [ ! -f ${OPENVPN} ]; then
		/bin/echo "openvpn is not exit."
		exit 1
	fi

	if [ -f ${PIDFILE} ]; then
		/bin/echo "openvpn is aleady run."
		exit 1
	fi

	sleep 1

	init

	if [ `/sbin/getcfg "OPENVPN" "Enable" -u -d "FALSE" -f "${VPN_CONF}"` = "TRUE" ]; then
		/bin/echo "openvpn start."

		init_modules
		sleep 1
		init_iptable_modules

		/usr/bin/killall openvpn 1>>/dev/null 2>&1

		if [ -f ${VPN_ONLINE_LOG} ]; then
			/bin/rm ${VPN_ONLINE_LOG}
		fi
		sleep 1

		/sbin/daemon_mgr openvpn start "${OPENVPN} --writepid ${PIDFILE} --daemon ovpn-server --cd ${OPENVPN_CONF_PATH} --config ${OPENVPN_CONF} --script-security 3"

		LOCALIP=`/bin/grep "^server" ${OPENVPN_CONF} | /bin/cut -d' ' -f 2`
		INTERFACE=`/sbin/getcfg "OPENVPN" "interface" -d "eth0" -f "${VPN_CONF}"`
		/bin/echo "LOCALIP = ${LOCALIP}    INTERFACE = ${INTERFACE}"
		/sbin/iptables -t nat -A POSTROUTING -s ${LOCALIP}/24 -o ${INTERFACE} -j MASQUERADE
		use_source_route_for_openvpn
	else
		/bin/echo "openvpn is disabled." 
	fi

}

stop()
{
	/bin/echo "openvpn stop."
	LOCALIP=`/bin/grep "^server" ${OPENVPN_CONF} | /bin/cut -d' ' -f 2`
	INTERFACE=`/sbin/getcfg "OPENVPN" "interface" -d "eth0" -f "${VPN_CONF}"`
	/bin/echo "LOCALIP = ${LOCALIP}    INTERFACE = ${INTERFACE}"
	/sbin/iptables -t nat -D POSTROUTING -s ${LOCALIP}/24 -o ${INTERFACE} -j MASQUERADE


	if [ -f ${PIDFILE} ]; then
		/sbin/daemon_mgr openvpn stop "{OPENVPN}"
		/bin/kill -9 `/bin/cat ${PIDFILE}` 1>>/dev/null 2>&1
	fi

	if [ -f ${PIDFILE} ]; then
		/bin/rm -f ${PIDFILE} 1>>/dev/null 2>&1
	fi

	if [ -f ${VPN_ONLINE_LOG} ]; then
		/bin/rm -f ${VPN_ONLINE_LOG} 1>>/dev/null 2>&1
		/bin/touch ${VPN_ONLINE_LOG}
	fi
	
	sleep 2
	/sbin/rmmod tun 1>>/dev/null 2>&1
}

reload()
{
	/bin/echo "openvpn reload."
	LOCALIP=`/bin/grep "^server" ${OPENVPN_CONF} | /bin/cut -d' ' -f 2`
	INTERFACE=`/sbin/getcfg "OPENVPN" "interface" -d "eth0" -f "${VPN_CONF}"`
	/bin/echo "LOCALIP = ${LOCALIP}    INTERFACE = ${INTERFACE}"
	/sbin/iptables -t nat -D POSTROUTING -s ${LOCALIP}/24 -o ${INTERFACE} -j MASQUERADE
	if [ -f ${PIDFILE} ]; then
		/bin/kill -s HUP `/bin/cat ${PIDFILE}` 1>>/dev/null 2>&1
	fi

	LOCALIP=`/bin/grep "^server" ${OPENVPN_CONF} | /bin/cut -d' ' -f 2`
	INTERFACE=`/sbin/getcfg "OPENVPN" "interface" -d "eth0" -f "${VPN_CONF}"`
	/bin/echo "LOCALIP = ${LOCALIP}    INTERFACE = ${INTERFACE}"
	/sbin/iptables -t nat -A POSTROUTING -s ${LOCALIP}/24 -o ${INTERFACE} -j MASQUERADE
	use_source_route_for_openvpn
}

case "$1" in
	start)
		start
		;;
	stop)
		stop
		;;
	restart)
		stop
		/bin/sleep 2
		start
		;;
	init)
		init
		;;
	reload)
		reload
		;;
	*)
		echo "Usage: /etc/init.d/$0 {start|stop|restart|reload}"
		exit 1
esac

exit 0
