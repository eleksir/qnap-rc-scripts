#!/bin/sh
PATH=/bin:/sbin:/usr/bin:/usr/sbin
MNT_POINT="/mnt/ext"
FLASH_RFS1="/dev/sdx2"
FLASH_RFS2="/dev/sdx3"
FLASH_RFS5="/dev/sdx5"
FLASH_RFS6="/dev/sdx6"
FLASH_TMP="/flashfs_tmp"
CONFIG_Dev="/dev/md9"
MNT_HD_ROOT_TMP="/hd_root_tmp"
TS_LOG_DIR=/etc/logs
ISCSI_CONFIG="/etc/config/iscsi.conf"
ISCSI_DEFAULT_CONFIG="/etc/default_config/iscsi.conf"
DEV_NAS_CONFIG="/dev/sdx6"

function run_burntest()
{
	mnt_burntest="/mnt/burntest"
    
	[ ! -d $mnt_burntest ] && mkdir $mnt_burntest
    
	mount -t ext3 /dev/sda1 $mnt_burntest
    
	if [ $? -eq 0 ]; then
		# Check the special symbol  
		SYM="`cat /etc/burntest.sym 2> /dev/null`"
		BURNSYM="$mnt_burntest/burntest/$SYM/QNAP-burning-test"
		if [ -f $BURNSYM ]; then
			[ "x$1" = "xstart" ] && [ -x $mnt_burntest/burntest/burntest-init ] && {
				$mnt_burntest/burntest/burntest-init
				[ $? -eq 0 ] && return 0
			}

			[ "x$1" = "xstop" ] && [ -x $mnt_burntest/burntest/burntest-fini ] && {
				$mnt_burntest/burntest/burntest-fini
				[ $? -eq 0 ] && return 0
			}
		fi
		umount $mnt_burntest > /dev/null 2>&1
		rm -rf $mnt_burntest > /dev/null 2>&1
		return 1
	fi

	return 1
}

create_iscsi_conf()
{
/bin/echo '[iSCSISetting]' > $ISCSI_CONFIG
/bin/echo 'Enable = FALSE' >> $ISCSI_CONFIG
/bin/echo 'iSCSIPort = 3260' >> $ISCSI_CONFIG
/bin/echo '[iSCSISetting]' > $ISCSI_DEFAULT_CONFIG
/bin/echo 'Enable = FALSE' >> $ISCSI_DEFAULT_CONFIG
/bin/echo 'iSCSIPort = 3260' >> $ISCSI_DEFAULT_CONFIG
}

remove_S30_S45()
{
	/bin/rm /etc/rcS.d/S[3-4]* 1>/dev/null 2>&1
}

mount -t proc /proc /proc
# Mount local file systems in /etc/fstab
mount -o remount,rw /
mount -a
mkdir /sys
mount -t sysfs sysfs /sys
[ -d /tmp ] || mkdir /tmp
mount -t tmpfs tmpfs /tmp -o size=32M
if [ ! -z $_memsize ] && [ $_memsize -gt 1500000 ]; then
	mount -t tmpfs tmpfs /tmp -o remount,size=64M
	echo 131072 > /proc/sys/fs/inotify/max_user_watches
elif [ ! -z $_memsize ] && [ $_memsize -gt 1000000 ]; then
	mount -t tmpfs tmpfs /tmp -o remount,size=64M
	echo 65536 > /proc/sys/fs/inotify/max_user_watches
else
	echo 32768 > /proc/sys/fs/inotify/max_user_watches
fi
chmod 777 /tmp
[ -d /tmp/config ] || mkdir /tmp/config
[ -d /tmp/qget ] || mkdir /tmp/qget
#set HDD LED
for i in 0 1 2 3 4 5; do
	[ ! -f /sys/class/scsi_host/host$i/em_message ] || /bin/echo 0x80000 > /sys/class/scsi_host/host$i/em_message && sync
done
#insmod
[ ! -f /lib/modules/misc/jbd.ko ] || /sbin/insmod /lib/modules/misc/jbd.ko
[ ! -f /lib/modules/misc/ext3.ko ] || /sbin/insmod /lib/modules/misc/ext3.ko
[ ! -f /lib/modules/misc/hid.ko ] || /sbin/insmod /lib/modules/misc/hid.ko &
[ ! -f /lib/modules/misc/gcu.ko ] || /sbin/insmod /lib/modules/misc/gcu.ko &
[ ! -f /lib/modules/misc/iegbe.ko ] || /sbin/insmod /lib/modules/misc/iegbe.ko &
[ ! -f /lib/modules/misc/mv61xx.ko ] || /sbin/insmod /lib/modules/misc/mv61xx.ko 
[ ! -f /lib/modules/misc/mv_sata.ko ] || /sbin/insmod /lib/modules/misc/mv_sata.ko
if [ -f /lib/modules/misc/mv_sata.ko ] || [ -f /lib/modules/misc/mv61xx.ko ]; then
	/bin/sleep 10
fi
[ ! -f /lib/modules/misc/libata.ko ] || /sbin/insmod /lib/modules/misc/libata.ko &
[ ! -f /lib/modules/misc/usb-common.ko ] || /sbin/insmod /lib/modules/misc/usb-common.ko 
[ ! -f /lib/modules/misc/usbcore.ko ] || /sbin/insmod /lib/modules/misc/usbcore.ko 
[ ! -f /lib/modules/misc/ehci-hcd.ko ] || /sbin/insmod /lib/modules/misc/ehci-hcd.ko 
[ ! -f /lib/modules/misc/uhci-hcd.ko ] || /sbin/insmod /lib/modules/misc/uhci-hcd.ko 
if [ -f /lib/modules/misc/usb-storage.ko ]; then
	/sbin/insmod /lib/modules/misc/usb-storage.ko
	sleep 10
fi
/bin/mount -t usbfs none /proc/bus/usb
[ $? = 0 ] || /bin/echo "mount usbfs failed." 

#insmod hal module
[ ! -f /lib/modules/misc/hal_netlink.ko ] || /sbin/insmod /lib/modules/misc/hal_netlink.ko 2>>/dev/null

Model_Name=`cat /etc/default_config/Model_Name.conf 2>/dev/null`
[ "x$Model_Name" != "x" ] || Model_Name="TS-509"
if [ "x${Model_Name}" = "xTS-101" ]; then
	/bin/echo -n "Recover device node...."
	/bin/mv /dev/sdareal4 /dev/sda4
	[ $? = 0 ] && /bin/echo "OK."
	/bin/echo "Wait for 1 seconds."
	/bin/sleep 1
fi
/bin/echo

if [ -f /etc/lcd_disable_install ]; then
	export COMPRT4LCD=yes
	[ -x /sbin/lcd_tool ] || COMPRT4LCD=no
	# Turn on sysklog ... because I will be blind
	[ "x$COMPRT4LCD" = "xyes" ] && {
#		/sbin/syslogd -O /tmp/messages -s 1000
#		/sbin/klogd -c 7
#Check if generic version
		if [ -f /etc/IS_G ]; then
			lcd_tool -1 " NAS" -2 "    Welcome! "
		else
			lcd_tool -1 " QNAP Turbo NAS" -2 "    Welcome! "
		fi
	}
fi

	/bin/echo "Start config_util..."
	/sbin/config_util 1
	/sbin/config_util 4
	[ $? != 0 ] && /bin/echo "Some errors occurred while running config_util." 
	/bin/echo "config_util finished."
	/bin/echo 500 > /proc/sys/vm/dirty_expire_centisecs &
	/bin/echo 100 > /proc/sys/vm/dirty_writeback_centisecs &
	/bin/echo 5 > /proc/sys/vm/dirty_ratio &
	/bin/echo 10 > /proc/sys/vm/dirty_background_ratio &
	/bin/echo 0x4000000 > /proc/sys/kernel/shmmax &
	/bin/echo 16384 > /proc/sys/vm/min_free_kbytes &

echo "mount HDD root device"
/bin/mkdir $MNT_HD_ROOT_TMP
/bin/mount $CONFIG_Dev $MNT_HD_ROOT_TMP 1>>/dev/null 2>>/dev/null
if [ $? != 0 ]; then
	/bin/echo "mount $CONFIG_Dev failed."
else
	/bin/echo "mount $CONFIG_Dev successful."
	[ -f $MNT_HD_ROOT_TMP/.config/Model_Name.conf ] || echo "$Model_Name" > $MNT_HD_ROOT_TMP/.config/Model_Name.conf
	HD_Model_Name=`cat $MNT_HD_ROOT_TMP/.config/Model_Name.conf 2>/dev/null`
#try to fix model name here.
	if [ "x$HD_Model_Name" != "x$Model_Name" ]; then
		/bin/ln -sf config_util /sbin/ch_model
		/sbin/ch_model "$Model_Name"
		. /etc/init.d/functions
		install_PKGs "${MNT_HD_ROOT_TMP}/update_pkg"
	fi
	HD_Model_Name=`cat $MNT_HD_ROOT_TMP/.config/Model_Name.conf 2>/dev/null`	
	echo $HD_Model_Name > /tmp/Internal_Model
	HD_BOOT_CONF=`cat ${MNT_HD_ROOT_TMP}/.config/BOOT.conf`
	/bin/echo "$HD_BOOT_CONF" > /tmp/BOOT_CONF
	/bin/umount $CONFIG_Dev
	[ $? = 0 ] || /bin/echo "umount $CONFIG_Dev failed."
	if [ "x$HDD_INTERNAL_MODEL" != "x$SYS_INTERNAL_MODEL" ]; then
		/sbin/write_log "HDD cannot be mounted because the Internal Model is not matched" 2
	fi
fi

[ -f /etc/init.d/check_bootcmd.sh ] && /etc/init.d/check_bootcmd.sh
[ -f /bin/md5sum ] || /bin/ln -sf busybox /bin/md5sum

BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf 2>/dev/null`
/sbin/create_hwtype


if [ "x${HD_BOOT_CONF}" = "xTS-NASX86" -a "x$HD_Model_Name" = "x$Model_Name" ] || \
	[ "x${HD_BOOT_CONF}" = "xTS-NASARM" -a "x$HD_Model_Name" = "x$Model_Name" ] || \
	[ "x${HD_BOOT_CONF}" = "xTS-NASPPC" -a "x$HD_Model_Name" = "x$Model_Name" ]; then
	/bin/echo "Update Extended rcX_init.d...(1)"
    BOOT_NORMAL=1
	remove_S30_S45
#	/bin/echo "------ /etc/init.d/mountall --------"
	/etc/init.d/mountall
#	/bin/echo "------ /etc/init.d/init_hardware.sh --------"
	/etc/init.d/init_hardware.sh start
#	/bin/echo "------ /etc/init.d/init_mac_addr.sh --------"
	/etc/init.d/init_mac_addr.sh start
#	/bin/echo "------ /etc/init.d/sysinit.sh --------"
	/etc/init.d/sysinit.sh start
#	/bin/echo "------ /etc/init.d/shutdown_check.sh --------"
	/etc/init.d/shutdown_check.sh start
#	/bin/echo "------ /etc/init.d/init_disk.sh --------"
	/etc/init.d/init_disk.sh start
#	/bin/echo "------ /etc/init.d/init_network.sh --------"
	/etc/init.d/init_network.sh start 
#	/bin/echo "------ /etc/init.d/hostname.sh --------"
	/etc/init.d/hostname.sh start
#	/bin/echo "------ /etc/init.d/network.sh --------"
	CNT=0
        while [ $CNT -lt 10 ]; do
                if [ -f /tmp/init.1 ]; then
                        break
                else
                        sleep 1
                        CNT=`expr $CNT + 1`
                fi
        done
	/etc/init.d/network.sh start
	/etc/init.d/rcS_normal_fast
	if [ -f /mnt/HDA_ROOT/update_pkg/.Squid_required ]; then
		[ -x /mnt/HDA_ROOT/update_pkg/Squid.bin ] && ( /mnt/HDA_ROOT/update_pkg/Squid.bin 2>/dev/null 1>/dev/null & ) && sleep 3
		/bin/rm -f /mnt/HDA_ROOT/update_pkg/.Squid_required 2>/dev/null
	fi
	#/bin/mv /etc/rcS.d/* /etc/rcS_init.d/
	#/bin/mv /etc/rcK.d/* /etc/rcK_init.d/
else
	/bin/echo "Update Extended rcX_init.d...(2)"
    BOOT_NORMAL=0
	/etc/rcS.d/S30mountall start
	/sbin/setcfg System Booting 1
	/sbin/daemon_mgr picd start "/sbin/picd &"
	/sbin/daemon_mgr gpiod start "/sbin/gpiod &"
	
	CPU_MODEL=""
        [ ! -f /proc/tsinfo/cpu_model ] || CPU_MODEL=`/bin/cat /proc/tsinfo/cpu_model 2>/dev/null`
        MODEL_NAME=`/sbin/getcfg "System" "Model"`
        if [ "x${CPU_MODEL}" = "x88F6281 A0" ] && [ "x${MODEL_NAME}" = "xTS-119" ];then
                /bin/rm /sbin/hwmond
        fi

	[ -f /sbin/hwmond ] && /sbin/daemon_mgr hwmond start "/sbin/hwmond &"
	[ -f /sbin/acpid ] && /sbin/daemon_mgr acpid start "/sbin/acpid"
	[ -f /usr/sbin/upsutil ] && /sbin/daemon_mgr upsutil start "/usr/sbin/upsutil &"
	/etc/rcS.d/S31init_hardware start
	#/etc/init.d/check_lan_port.sh
	/etc/rcS.d/S32init_mac_addr start
	#/etc/rcS.d/S33sysinit start
	/sbin/hwclock --hctosys
	/sbin/daemon_mgr bcclient start "/sbin/bcclient"
	run_burntest start
	[ $? -eq 0 ] && exit 0

	/bin/echo log hidden config directory not exist, initial log file...
		/bin/mkdir $TS_LOG_DIR
		/sbin/log_tool -c -v
		/sbin/log_tool -b 50 -v
		/sbin/conn_log_tool -c -v
		/sbin/conn_log_tool -b 50 -v
	/etc/rcS.d/S34shutdown_check start
	/sbin/storage_boot_init 3
	/sbin/storage_boot_init 4
	#/etc/rcS.d/S35init_disk start
	/etc/rcS.d/S42hostname start
	/etc/rcS.d/S45network start
	/etc/rcS.d/S60thttpd start
	#/usr/local/sbin/Qthttp -p 80 -nor -nos -u admin -d /home/httpd -c '**.*'
	/etc/rcS.d/S61Qthttpd start
	# create default index html file (replace "SERVER_NAME" string)
	#/bin/sed s/SERVER_NAME/`/sbin/getcfg system "server name"`/ /root/index_default.html > /home/httpd/index.html
	#/bin/mv /etc/rcK.d/K24thttpd /etc/rcK_init.d/
	#/bin/mv /etc/rcK.d/K40network /etc/rcK_init.d/
	#/bin/mv /etc/rcK.d/K66shutdown_check /etc/rcK_init.d/
	#/bin/mv /etc/rcK.d/K80init_disk /etc/rcK_init.d/
	if [ "x${BOOT_CONF}" = "xTS-NASARM" ] || [ "x${BOOT_CONF}" = "xTS-NASPPC" ]; then
		DEV_NAS_CONFIG=/dev/mtdblock5
	fi
	/bin/mount $DEV_NAS_CONFIG -t ext2 /tmp/config -o ro 2>/dev/null 1>/dev/null
	if [ -f /tmp/config/.write_cache ]; then
		/sbin/setcfg Misc "Enable Write Cache" 1
		/sbin/setcfg Misc "Enable Write Cache" 1 -f /etc/default_config/uLinux.conf
	fi
	if [ -f /tmp/config/.case_sensitive ]; then
		/sbin/setcfg Samba "Case Sensitive" yes
		/sbin/setcfg Samba "Case Sensitive" yes -f /etc/default_config/uLinux.conf
	fi
	/bin/umount /tmp/config 2>/dev/null 1>/dev/null
	/bin/touch /tmp/format.status
	Internal_Model=`/sbin/getcfg System "Internal Model"`
		/sbin/config_util 0
		if [ $? != 0 ]; then
			/bin/echo "init_check.sh: No HD existed."
			/sbin/set_status_led 1 1
			# /sbin/burning &
		fi

	#Set power LED on
	if [ "x$Internal_Model" = "xTS-609" ] || [ "x$Internal_Model" = "xTS-239" ] || [ "x$Internal_Model" = "xTS-439" ] || [ "x$Internal_Model" = "xTS-639" ] || [ "x$Internal_Model" = "xTS-839" ] || [ "x$Internal_Model" = "xTS-259" ] || [ "x$Internal_Model" = "xTS-459" ] || [ "x$Internal_Model" = "xTS-559" ]|| [ "x$Internal_Model" = "xTS-659" ] || [ "x$Internal_Model" = "xTS-859" ] || [ "x$Internal_Model" = "xTS-1279" ] || [ "x$Internal_Model" = "xTS-879" ] || [ "x$Internal_Model" = "xTS-1079" ] || [ "x$Internal_Model" = "xTS-469" ] || [ "x$Internal_Model" = "xTS-569" ] || [ "x$Internal_Model" = "xTS-669" ] || [ "x$Internal_Model" = "xTS-869" ]; then
        	/sbin/pic_raw 77
	fi

	[ -f /sbin/qLogEngined ] && /sbin/daemon_mgr qLogEngined start /sbin/qLogEngined
	[ -f /sbin/qsyslogd ] && /sbin/daemon_mgr qsyslogd start /sbin/qsyslogd
	[ -f /sbin/qShield ] && /sbin/daemon_mgr qShield start /sbin/qShield

	# For utelnetd
	cd /bin; ln -sf busybox readlink;
	/bin/readlink /etc/inittab | /bin/grep enable > /dev/null
	/etc/init.d/login.sh start
	/etc/init.d/stunnel.sh start
	/etc/init.d/bonjour.sh restart
	# For localtime
	readlink /usr/share/zoneinfo 1>>/dev/null 2>>/dev/null
	if [ $? != 0 ]; then
		[ -d /usr/share ] || /bin/mkdir /usr/share -p
		[ -d /usr/share/zoneinfo ] || /bin/ln -sf /etc/zoneinfo /usr/share/zoneinfo
	fi

	# For passwd
	if [ ! -d /usr/bin ]; then
		echo "create /usr/bin/passwd"
		/bin/mkdir /usr/bin -p
		/bin/ln -sf /bin/busybox /usr/bin/passwd
	fi

	/sbin/pidof upnpd
	[ $? = 0 ] || /etc/init.d/upnpd.sh restart
	[ -f /sbin/lcdmond ] && /sbin/daemon_mgr lcdmond start /sbin/lcdmond
	if [ x`/sbin/getcfg Misc "Buzzer Quiet Enable"` != "xTRUE" ]; then	
		/sbin/pic_raw 81
	fi
	/etc/init.d/update_def_share.sh
	/bin/touch /etc/config/usb_share_mapping
	/bin/touch /etc/default_config/usb_share_mapping
	/bin/touch /var/lock/add_ext_dev
	/bin/touch /etc/config/usb_share_setting
	/bin/touch /etc/default_config/usb_share_setting
	/bin/touch /etc/config/esata_share_mapping
	/bin/touch /etc/default_config/esata_share_mapping
	/bin/touch /etc/config/smart.conf

	[ ! -f /home/httpd/cgi-bin/inc/lang_es-mex.js ] || /bin/rm /home/httpd/cgi-bin/inc/lang_es-mex.js
	[ ! -f /home/httpd/cgi-bin/inc/s_lang_es-mex.js ] || /bin/rm /home/httpd/cgi-bin/inc/s_lang_es-mex.js
	[ ! -f /etc/init.d/genLanglist.sh ] || /etc/init.d/genLanglist.sh
fi
# scan network card on PCIE bus
#/etc/init.d/check_lan_port.sh

[ -f /bin/ts_init ] && /bin/ts_init
/sbin/set_home_directory
/sbin/sss_convert
/etc/init.d/network.sh write_hosts
/bin/umount $FLASH_RFS1 1>>/dev/null 2>>/dev/null
/bin/umount $FLASH_RFS2 1>>/dev/null 2>>/dev/null
/bin/e2fsck -pf $FLASH_RFS5 1>>/dev/null 2>>/dev/null
/bin/e2fsck -pf $FLASH_RFS6 1>>/dev/null 2>>/dev/null
[ ! -d $FLASH_TMP ] || /bin/rmdir $FLASH_TMP 1>>/dev/null 2>>/dev/null
[ -f /etc/init.d/upnpd.sh ] && /bin/ln -sf ../init.d/upnpd.sh /etc/rcK.d/K15upnp
[ -f /etc/init.d/iscsiinit.sh ] && /bin/ln -sf ../init.d/iscsiinit.sh /etc/rcK.d/K59iscsiinit
#[ -f /etc/init.d/upnpd.sh ] && /etc/init.d/upnpd.sh restart
[ -f $ISCSI_CONFIG ] || create_iscsi_conf
if [ -f /sbin/upnpcd ]; then
	[ -f /etc/config/upnpc.conf ] || /bin/touch /etc/config/upnpc.conf
	/sbin/daemon_mgr upnpcd start "/sbin/upnpcd -i 300 &"
fi
# Send alert mail
[ -f /tmp/send_alert_mail ] && \
/sbin/send_alert_mail "The system was not shut down properly last time." && \
/bin/rm -f /tmp/send_alert_mail

[ $BOOT_NORMAL == 1 ] && /etc/init.d/boot_done.sh &
/etc/init.d/usb_device_check.sh

/bin/grep "${MNT_HD_ROOT_TMP}" "/proc/mounts" 1>>/dev/null 2>>/dev/null
[ $? != 0 ] || /bin/umount "${MNT_HD_ROOT_TMP}" 1>>/dev/null 2>>/dev/null
[ ! -d $MNT_HD_ROOT_TMP ] || /bin/rmdir $MNT_HD_ROOT_TMP 1>>/dev/null 2>>/dev/null
