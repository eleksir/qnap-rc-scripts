#!/bin/sh

SSH=/usr/sbin/sshd
TELNET=/bin/utelnetd
SSHD_CONF=/etc/ssh/sshd_config

/sbin/test -f $SSHD || exit 0
/sbin/test -f $TELNET || exit 0

[ -f "/bin/cmp" ] || ln -sf /bin/busybox /bin/cmp

SSH_PORT=`/sbin/getcfg LOGIN "SSH Port" -d 22`
DEAFULT_TELNET_PORT=`/sbin/getcfg -f /var/default LOGIN "TELNET Port" -d 13131`
TELNET_PORT=`/sbin/getcfg LOGIN "TELNET Port" -d $DEAFULT_TELNET_PORT`
SSHKEY_CONFIG_DIR=/etc/config/ssh
BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf`
generte_ssh_key()
{
	[ -d $SSHKEY_CONFIG_DIR ] || /bin/mkdir $SSHKEY_CONFIG_DIR
	if [ -f /usr/bin/ssh-keygen ]; then
		if [ ! -f ${SSHKEY_CONFIG_DIR}/ssh_host_rsa_key ] || [ ! -f ${SSHKEY_CONFIG_DIR}/ssh_host_rsa_key.pub ]; then
			/bin/rm -f ${SSHKEY_CONFIG_DIR}/ssh_host_rsa_key*
			/usr/bin/ssh-keygen -t rsa -f ${SSHKEY_CONFIG_DIR}/ssh_host_rsa_key -N ""
			/bin/touch /etc/config/ssh_key.fla
			/bin/sync
		fi
		if [ ! -f ${SSHKEY_CONFIG_DIR}/ssh_host_dsa_key ] || [ ! -f ${SSHKEY_CONFIG_DIR}/ssh_host_dsa_key.pub ]; then
			/bin/rm -f ${SSHKEY_CONFIG_DIR}/ssh_host_dsa_key*
			/usr/bin/ssh-keygen -t dsa -f ${SSHKEY_CONFIG_DIR}/ssh_host_dsa_key -N ""
			/bin/touch /etc/config/ssh_key.fla
			/bin/sync
		fi
	
		/bin/cmp /etc/ssh/ssh_host_rsa_key ${SSHKEY_CONFIG_DIR}/ssh_host_rsa_key 1>>/dev/null 2>>/dev/null
		retrsa1=$?
		/bin/cmp /etc/ssh/ssh_host_dsa_key.pub ${SSHKEY_CONFIG_DIR}/ssh_host_dsa_key.pub 1>>/dev/null 2>>/dev/null
		retrsa2=$?
		/bin/cmp /etc/ssh/ssh_host_dsa_key ${SSHKEY_CONFIG_DIR}/ssh_host_dsa_key 1>>/dev/null 2>>/dev/null
		retdsa1=$?
		/bin/cmp /etc/ssh/ssh_host_dsa_key.pub ${SSHKEY_CONFIG_DIR}/ssh_host_dsa_key.pub 1>>/dev/null 2>>/dev/null
		retdsa2=$?

		[ $retrsa1 -eq 0 ] || /bin/cp -a ${SSHKEY_CONFIG_DIR}/ssh_host_rsa_key /etc/ssh/
		[ $retrsa2 -eq 0 ] || /bin/cp -a ${SSHKEY_CONFIG_DIR}/ssh_host_rsa_key.pub /etc/ssh/
		[ $retdsa1 -eq 0 ] || /bin/cp -a ${SSHKEY_CONFIG_DIR}/ssh_host_dsa_key /etc/ssh/
		[ $retdsa2 -eq 0 ] || /bin/cp -a ${SSHKEY_CONFIG_DIR}/ssh_host_dsa_key.pub /etc/ssh/

		if [ -d /etc/config/ssh ]; then
			/bin/rm -rf /root/.ssh 1>>/dev/null 2>>/dev/null
			/bin/ln -sf /etc/config/ssh /root/.ssh
			[ -f /etc/config/ssh/id_rsa ] || /bin/ln -sf ssh_host_rsa_key /etc/config/ssh/id_rsa
			[ -f /etc/config/ssh/id_rsa.pub ] || /bin/ln -sf ssh_host_rsa_key.pub /etc/config/ssh/id_rsa.pub
		fi
		
		if [ "x${BOOT_CONF}" = "xTS-NASX86" ] && [ ! -f /etc/config/ssh_key.fla ]; then
			/bin/rm -f ${SSHKEY_CONFIG_DIR}/ssh_host_rsa_key*	2>>/dev/null
			/bin/rm -f ${SSHKEY_CONFIG_DIR}/ssh_host_dsa_key* 2>>/dev/null
			/usr/bin/ssh-keygen -t rsa -f ${SSHKEY_CONFIG_DIR}/ssh_host_rsa_key -N ""
			/usr/bin/ssh-keygen -t dsa -f ${SSHKEY_CONFIG_DIR}/ssh_host_dsa_key -N ""
			/bin/touch /etc/config/ssh_key.fla
		fi
	fi

}

update_sshd_config()
{
	ENABLED_SFTP=`/sbin/getcfg LOGIN "SFTP Enable" -u -d TRUE`

	if [ "x${ENABLED_SFTP}" = "xTRUE" ]; then
		/bin/grep "/usr/libexec/sftp-server" ${SSHD_CONF} > /dev/null
		if [ $? != 0 ]; then
			/bin/sed '107i\Subsystem sftp \/usr\/libexec\/sftp-server' ${SSHD_CONF} > ${SSHD_CONF}.tmp
			/bin/cp -f ${SSHD_CONF}.tmp ${SSHD_CONF}
		fi
	else
		/bin/grep "/usr/libexec/sftp-server" ${SSHD_CONF} > /dev/null
		if [ $? = 0 ]; then
			/bin/sed "/\/usr\/libexec\/sftp-server/d" ${SSHD_CONF} > ${SSHD_CONF}.tmp
			/bin/cp -f ${SSHD_CONF}.tmp ${SSHD_CONF}
		fi
	fi
}

case "$1" in
    start)
	if [ `/sbin/getcfg LOGIN "SSH Enable" -u -d TRUE` = FALSE ]; then
		echo "Starting sshd services: disabled."
	else
		echo -n "Starting sshd services: "
		generte_ssh_key
		update_sshd_config
		/sbin/daemon_mgr sshd start "$SSH -f ${SSHD_CONF} -p $SSH_PORT"
		echo "sshd."
		touch /var/lock/subsys/sshd
	fi

	if [ `/sbin/getcfg LOGIN "TELNET Enable" -u -d FALSE` = FALSE ]; then
		echo "Starting telnet services: disabled."
	else
		echo -n "Starting telnet services: "
		/sbin/daemon_mgr utelnetd start "$TELNET -p $TELNET_PORT &"
		echo "utelnetd."
		touch /var/lock/subsys/utelnetd
	fi

	if [ `/sbin/getcfg "TFTP Server" "Enable" -u -d FALSE` = FALSE ]; then
		echo "Starting opentftpd services: disabled."
	else
		/etc/init.d/opentftp.sh start
	fi

	;;
    stop)
	echo -n "Shutting down sshd services:" 
	/sbin/daemon_mgr sshd stop $SSH
	/usr/bin/killall sshd
	rm -f /var/lock/subsys/sshd
	echo "sshd"

	echo -n "Shutting down telnet services:"
	/sbin/daemon_mgr utelnetd stop $TELNET
	rm -f /var/lock/subsys/utelnetd
	echo "utelnetd"
	;;

    restart)
	$0 stop
	$0 start
	;;	
    *)
        echo "Usage: /etc/init.d/login.sh {start|stop|restart}"
        exit 1
esac

exit 0
