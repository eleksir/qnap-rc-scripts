#/bin/sh

GETCFG="/sbin/getcfg"
WPA_SUPPLICANT="/usr/local/bin/wpa_supplicant"
WPA_SUPPLICANT_CONF="/etc/config/wpa_supplicant.conf"
WIRELESS_PROFILE="/etc/config/wireless.conf"
WIRELESS_STATUS="/tmp/wireless_status"
WIRELESS_TEMP="/tmp/wireless_temp"

net=""
IP_FORWARD=`/bin/cat /proc/sys/net/ipv4/ip_forward`

check_ip_forward()
{
	#check ip_forward is 0 or not
	if [ "$IP_FORWARD" != "0" ]; then
		/bin/echo "0" > /proc/sys/net/ipv4/ip_forward
	fi
}

do_exit()
{
	/bin/echo "$IP_FORWARD" > /proc/sys/net/ipv4/ip_forward
	exit $1
}

get_net()
{
	ip1=`/bin/echo $1|cut -d '.' -f 1`
	ip2=`/bin/echo $1|cut -d '.' -f 2`
	ip3=`/bin/echo $1|cut -d '.' -f 3`
	ip4=`/bin/echo $1|cut -d '.' -f 4`
	netmask1=`/bin/echo $2|cut -d '.' -f 1`
	netmask2=`/bin/echo $2|cut -d '.' -f 2`
	netmask3=`/bin/echo $2|cut -d '.' -f 3`
	netmask4=`/bin/echo $2|cut -d '.' -f 4`
	if [ $netmask2 -ne 255 ]; then
		left=`expr 255 - $(($netmask2)) + 1`
		remainder=`expr $(($ip2)) % $left`
		mask=`expr $(($ip2)) - $remainder`
		net="$ip1.$mask.0.0"
	elif [ $netmask3 -ne 255 ]; then
		left=`expr 255 - $(($netmask3)) + 1`
		remainder=`expr $(($ip3)) % $left`
		mask=`expr $(($ip3)) - $remainder`
		net="$ip1.$ip2.$mask.0"
	elif [ $netmask4 -ne 255 ]; then
		left=`expr 255 - $(($netmask4)) + 1`
		remainder=`expr $(($ip4)) % $left`
		mask=`expr $(($ip4)) - $remainder`
		net="$ip1.$ip2.$ip3.$mask"
	else
		net="$ip1.$ip2.$ip3.$ip4"
	fi
}

reset_status()
{
	while read LINE
	do
		ssid=`/bin/echo $LINE |/bin/grep '\['|/bin/cut -d'[' -f2|/bin/cut -d']' -f1`
		[ "x$ssid" = "x" ] && continue
		status=`/sbin/getcfg "$ssid" status -f $WIRELESS_PROFILE`
		[ $status -ge 5 ] && /sbin/setcfg "$ssid" status 0 -f $WIRELESS_PROFILE
	done < $WIRELESS_PROFILE
}

auto_connect()
{
	/bin/rm -f $WIRELESS_STATUS 2>>/dev/null
	/bin/touch $WIRELESS_STATUS
	ifname=`/bin/cat /proc/net/wireless |/bin/grep -v Inter|/bin/grep -v face|cut -d':' -f 1|sed 's/\ //g'`
	if [ "x$ifname" = "x" ]; then
		/sbin/setcfg "" status "1" -f $WIRELESS_STATUS
		do_exit 1
	fi
	
	stop
	/sbin/ifconfig $ifname up
	local check=0
        while [ 1 ]; do
		if [ ! -f $WIRELESS_PROFILE ]; then
			check=$(($check+1))
                	sleep 5
		else
			break
		fi
		[ $check -gt 10 ] && break
        done

	while read LINE
	do
		essid=`/bin/echo $LINE |/bin/grep '\['|/bin/cut -d'[' -f2|/bin/cut -d']' -f1`
		[ "x$essid" = "x" ] && continue
		iwlist=`/bin/ps|/bin/grep iwlist|/bin/grep scan`
		while [ "$iwlist" != "" ]; do
			iwlist=`/bin/ps|/bin/grep iwlist|/bin/grep scan`
			[ "x$iwlist" = "x" ] && break
			sleep 1
		done

        check=0
        while [ 1 ];do
            in_range=`/usr/local/bin/iwlist $ifname scanning | /bin/grep "$essid"`
            [ "x$in_range" != "x" ] && break
            check=$(($check+1))
            [ $check -gt 10 ] && break
            sleep 1
            /sbin/ifconfig $ifname up
        done
		[ "x$in_range" = "x" ] && continue
		autoconnect=`/sbin/getcfg "$essid" autoconnect -f $WIRELESS_PROFILE`
		if [ "$autoconnect" = "1" ]; then
			/bin/rm -f $WPA_SUPPLICANT_CONF	
			/bin/touch $WPA_SUPPLICANT_CONF
			mode=`/sbin/getcfg "$essid" mode -f $WIRELESS_PROFILE`
			if [ "$mode" = "Ad-Hoc" ]; then
				/bin/echo "ap_scan=2" >> $WPA_SUPPLICANT_CONF
			fi

			/bin/echo "network={" >> $WPA_SUPPLICANT_CONF
			/bin/echo "	ssid=\"$essid\"" >> $WPA_SUPPLICANT_CONF
			if [ "$mode" = "Ad-Hoc" ]; then
				/bin/echo "	mode=1" >> $WPA_SUPPLICANT_CONF
				/bin/echo "	frequency=2412" >> $WPA_SUPPLICANT_CONF
			fi

			auth=`/sbin/getcfg "$essid" auth -f $WIRELESS_PROFILE`
			if [ "$auth" = "OPEN" ]; then
				/bin/echo "	key_mgmt=NONE" >> $WPA_SUPPLICANT_CONF
			elif [ "$auth" = "WEP" ]; then
				/bin/echo "	key_mgmt=NONE" >> $WPA_SUPPLICANT_CONF
				wep_tx_keyidx=`/sbin/getcfg "$essid" wep_tx_keyidx -f $WIRELESS_PROFILE`
				wep="wep_key$wep_tx_keyidx"
				wep_key=`/sbin/getcfg "$essid" $wep -f $WIRELESS_PROFILE`
				if [ ${#wep_key} = 5 -o ${#wep_key} = 13 ]; then
					/bin/echo "	$wep=\"$wep_key\"" >> $WPA_SUPPLICANT_CONF
				elif [ ${#wep_key} = 10 -o ${#wep_key} = 26 ]; then
					/bin/echo "	$wep=$wep_key" >> $WPA_SUPPLICANT_CONF
				fi

				/bin/echo "	wep_tx_keyidx=$wep_tx_keyidx" >> $WPA_SUPPLICANT_CONF

				if [ "$mode" != "Ad-Hoc" ]; then
					/bin/echo "	auth_alg=OPEN" >> $WPA_SUPPLICANT_CONF

				fi
			else
				if [ "$mode" = "Ad-Hoc" ]; then
					/bin/echo "	key_mgmt=WPA-NONE" >> $WPA_SUPPLICANT_CONF
				else
					/bin/echo "	key_mgmt=WPA-PSK" >> $WPA_SUPPLICANT_CONF
				fi
				cipher=`/sbin/getcfg "$essid" cipher -f $WIRELESS_PROFILE`
				psk=`/sbin/getcfg "$essid" psk -f $WIRELESS_PROFILE`
				if [ ${#psk} -ge 8 -o ${#psk} -le 63 ]; then
					/bin/echo "	psk=\"$psk\"" >> $WPA_SUPPLICANT_CONF
				elif [ ${#psk} -eq 64 ]; then
					/bin/echo "	psk=$psk" >> $WPA_SUPPLICANT_CONF
				fi
			fi
			/bin/echo "}" >> $WPA_SUPPLICANT_CONF

			start
			/sbin/ifconfig $ifname|/bin/grep "inet addr:"
			if [ $? = 0 ]; then
				mac_addr=`/sbin/getcfg "$essid" mac_addr -f $WIRELESS_PROFILE`
				channel=`/sbin/getcfg "$essid" channel -f $WIRELESS_PROFILE`
				freq=`/sbin/getcfg "$essid" freq -f $WIRELESS_PROFILE`
				quality=`/sbin/getcfg "$essid" quality -f $WIRELESS_PROFILE`
				auth=`/sbin/getcfg "$essid" auth -f $WIRELESS_PROFILE`
				cipher=`/sbin/getcfg "$essid" cipher -f $WIRELESS_PROFILE`
				enc=`/sbin/getcfg "$essid" enc -f $WIRELESS_PROFILE`
				wep_key0=`/sbin/getcfg "$essid" wep_key0 -f $WIRELESS_PROFILE`
				wep_key1=`/sbin/getcfg "$essid" wep_key1 -f $WIRELESS_PROFILE`
				wep_key2=`/sbin/getcfg "$essid" wep_key2 -f $WIRELESS_PROFILE`
				wep_key3=`/sbin/getcfg "$essid" wep_key3 -f $WIRELESS_PROFILE`
				wep_tx_keyidx=`/sbin/getcfg "$essid" wep_tx_keyidx -f $WIRELESS_PROFILE`
				mode=`/sbin/getcfg "$essid" mode -f $WIRELESS_PROFILE`
				psk=`/sbin/getcfg "$essid" psk -f $WIRELESS_PROFILE`
				autoconnect=`/sbin/getcfg "$essid" autoconnect -f $WIRELESS_PROFILE`
				status=`/sbin/getcfg "$essid" status -f $WIRELESS_PROFILE`
				protocol=`/sbin/getcfg "$essid" protocol -f $WIRELESS_PROFILE`
				/bin/rm -f $WIRELESS_TEMP
				/bin/touch $WIRELESS_TEMP
				/sbin/setcfg "$essid" mac_addr "$mac_addr" -f $WIRELESS_TEMP
				/sbin/setcfg "$essid" channel "$channel" -f $WIRELESS_TEMP
				/sbin/setcfg "$essid" freq "$freq" -f $WIRELESS_TEMP
				/sbin/setcfg "$essid" quality "$quality" -f $WIRELESS_TEMP
				/sbin/setcfg "$essid" auth "$auth" -f $WIRELESS_TEMP
				/sbin/setcfg "$essid" cipher "$cipher" -f $WIRELESS_TEMP
				/sbin/setcfg "$essid" enc "$enc" -f $WIRELESS_TEMP
				/sbin/setcfg "$essid" mode "$mode" -f $WIRELESS_TEMP
				/sbin/setcfg "$essid" wep_key0 "$wep_key0" -f $WIRELESS_TEMP
				/sbin/setcfg "$essid" wep_key1 "$wep_key1" -f $WIRELESS_TEMP
				/sbin/setcfg "$essid" wep_key2 "$wep_key2" -f $WIRELESS_TEMP
				/sbin/setcfg "$essid" wep_key3 "$wep_key3" -f $WIRELESS_TEMP
				/sbin/setcfg "$essid" autoconnect "$autoconnect" -f $WIRELESS_TEMP
				/sbin/setcfg "$essid" protocol "$protocol" -f $WIRELESS_TEMP
				/sbin/setcfg "$essid" status "$status" -f $WIRELESS_TEMP
				/sbin/setcfg "$essid" wep_tx_keyidx "$wep_tx_keyidx" -f $WIRELESS_TEMP
				/sbin/setcfg "$essid" psk "$psk" -f $WIRELESS_TEMP

				/sbin/rmcfg "$essid" -f $WIRELESS_PROFILE
				/bin/cat $WIRELESS_PROFILE >> $WIRELESS_TEMP
				/bin/mv $WIRELESS_TEMP $WIRELESS_PROFILE
				break
			fi
			#stop #not to let wlan0 down
		fi
	done < $WIRELESS_PROFILE
}

stop()
{
	local ssid=`/sbin/getcfg "" ssid -f $WPA_SUPPLICANT_CONF |/bin/cut -d '"' -f 2`
	local status=`/sbin/getcfg "$ssid" status -f $WIRELESS_PROFILE`
	[ $(($status)) -le 4 ] && /sbin/setcfg "$ssid" status 0 -f $WIRELESS_PROFILE

	# init log file
	/sbin/setcfg "" status "" -f $WIRELESS_STATUS 2> /dev/null

	# disconnect AP
	/usr/bin/killall wpa_supplicant 2> /dev/null

	if [ -f /etc/dhcpc/dhcpcd-$ifname.pid ]; then
                local pid=`/bin/cat /etc/dhcpc/dhcpcd-$ifname.pid`
                /bin/kill -9 $pid 2>>/dev/null
                /bin/rm -f /etc/dhcpc/dhcpcd-$ifname.pid &> /dev/null
                /bin/rm -f /etc/dhcpc/dhcpcd-$ifname.info &> /dev/null
	fi
	pid=$(/bin/ps | /bin/grep "dhcpcd" | /bin/grep "wlan0" | /bin/awk '{print $1}')
	/bin/kill $pid 2>>/dev/null

	[ "$status" = "1" -o "$status" = "2" ] && [ ! -z "$ssid" ] && /sbin/write_log "[TCP/IP] You have successfully disconnected from the wireless network [$ssid]." 4

	ifname=`/bin/cat /proc/net/wireless |/bin/grep -v Inter|/bin/grep -v face|cut -d':' -f 1|sed 's/\ //g'`
	if [ "x$ifname" = "x" ]; then
		#/sbin/setcfg "" status "1" -f $WIRELESS_STATUS
		do_exit 1
	fi

	# release ip
	/sbin/ifconfig $ifname 0.0.0.0 2> /dev/null

	sleep 1
	# free interface
	/sbin/ifconfig $ifname down

	/bin/ps | /bin/grep "wireless.sh" | /bin/grep start | /bin/cut -d 'a' -f 1| sed 's/\ //g'|xargs kill 2> /dev/null
}

start()
{
	#check interface plugin
	[ ! -d /sys/class/net/wlan0 ] && do_exit 0

	# init log file
	/bin/rm -f $WIRELESS_STATUS
	/bin/touch $WIRELESS_STATUS

	# set interface up
	ifname=`/bin/cat /proc/net/wireless |/bin/grep -v Inter|/bin/grep -v face|cut -d':' -f 1|sed 's/\ //g'`
	if [ "x$ifname" = "x" ]; then
		/sbin/setcfg "" status "1" -f $WIRELESS_STATUS
		do_exit 1
	fi
	/sbin/ifconfig $ifname up
	
	# pre-set static ip
	iptype=`/sbin/getcfg wireless iptype`
	if [ "$iptype" = "static" ]; then
		# set ip 
		ip=`/sbin/getcfg wireless ip -d ""`
		[ "x$ip" != "x" ] || do_exit 1
		netmask=`/sbin/getcfg wireless netmask`
		gateway=`/sbin/getcfg wireless gateway`
		/sbin/ifconfig $ifname $ip netmask $netmask 
		if [ $? != 0 ]; then
			#Error : set static ip fail.
			/sbin/setcfg "" status "4" -f $WIRELESS_STATUS
			/sbin/setcfg "$ssid" status 6 -f $WIRELESS_PROFILE
			stop
			return
			#exit 1
		fi
	fi
	
	[ ! -f $WPA_SUPPLICANT ] && do_exit 0
	[ ! -f $WPA_SUPPLICANT_CONF ] && do_exit 0

	# set status "connecting"
	ssid=`/sbin/getcfg "" ssid -f $WPA_SUPPLICANT_CONF |/bin/cut -d '"' -f 2`
	/sbin/setcfg "$ssid" status 2 -f $WIRELESS_PROFILE
	# connect to AP
	$WPA_SUPPLICANT -d -D wext -i $ifname -c $WPA_SUPPLICANT_CONF &> /dev/null &
	if [ $? != 0 ]; then
		#Error : can't start wireless tool.
		/sbin/setcfg "" status "2" -f $WIRELESS_STATUS
		stop
		do_exit 1
	fi
	/usr/local/bin/iwconfig "$ifname" essid "$ssid"
	# check ap link
	local count=0
	local mode=`/sbin/getcfg "$ssid" mode -f $WIRELESS_PROFILE`
	while [ 1 ]; do
		# authentication fail (key incorrect)
		status=`/sbin/getcfg "" status -f $WIRELESS_STATUS`
		if [ "$status" = "9" ]; then
			/sbin/setcfg "$ssid" status 7 -f $WIRELESS_PROFILE
			#stop
			#return
			#exit 1
		fi

		# check if AP linked
		/usr/local/bin/iwconfig $ifname 2> /dev/null |/bin/grep "Not-Associated" 
		if [ $? = 0 ]; then
			count=$(($count + 2))
			[ $count = 60 ] && /sbin/setcfg "$ssid" status 6 -f $WIRELESS_PROFILE && stop && return
		fi

		local mac=`/sbin/getcfg "$ssid" mac_addr -d "use_profile" -f $WIRELESS_PROFILE`
		if [ "$mac" = "use_profile" ]; then
			/usr/local/bin/iwconfig $ifname 2> /dev/null|/bin/grep "ESSID:"
                else
			if [ "$mode" = "Ad-Hoc" ]; then
				/usr/local/bin/iwconfig $ifname 2> /dev/null|/bin/grep "Cell:"|/bin/grep $mac
			else
				/usr/local/bin/iwconfig $ifname 2> /dev/null|/bin/grep "Access Point:"|/bin/grep $mac
			fi
		fi
		[ $? = 0 ] && break
		sleep 2
	done

	# set ip and route
	iptype=`/sbin/getcfg wireless iptype`
	if [ "$iptype" = "static" ]; then
		# set ip 
		ip=`/sbin/getcfg wireless ip`
		netmask=`/sbin/getcfg wireless netmask`
		gateway=`/sbin/getcfg wireless gateway`
		/sbin/ifconfig $ifname $ip netmask $netmask 
		if [ $? != 0 ]; then
			#Error : set static ip fail.
			/sbin/setcfg "" status "4" -f $WIRELESS_STATUS
			/sbin/setcfg "$ssid" status 6 -f $WIRELESS_PROFILE
			stop
			return
			#exit 1
		fi

		#/sbin/route del -net $net netmask $netmask
		if [ "x$gateway" != "x" -a ${#gateway} -gt 3 ]; then
			if [ "$mode" != "Ad-Hoc" ]; then
				keyerr_ssid=""
				while [ 1 ]; do
					# authentication fail (key incorrect)
					status=`/sbin/getcfg "" status -f $WIRELESS_STATUS`
					if [ "$status" = "9" ]; then
						/sbin/setcfg "$ssid" status 7 -f $WIRELESS_PROFILE
						keyerr_ssid="$ssid"
						#stop
						#return
						#exit 1
					fi
					
					count=`expr $count + 1`
					if [ $count = 60 ]; then
						auth=`/sbin/getcfg "$ssid" auth -f $WIRELESS_PROFILE`
						if [ "$auth" = "WEP" ]; then
							/usr/local/bin/iwconfig $ifname 2> /dev/null |/bin/grep "Encryption key" |/bin/grep "off"
							if [ $? != 0 ]; then
								/sbin/setcfg "$ssid" status 7 -f $WIRELESS_PROFILE
								stop
								return
							fi
						fi
						#Error : key incorrect.
						if [ -z "$keyerr_ssid" ] || [ "x$keyerr_ssid" != "x$ssid" ]; then
							/bin/ping -c 1 $gateway 2> /dev/null
							if [ $? = 0 ]; then
								break
							fi
							/sbin/setcfg "" status "8" -f $WIRELESS_STATUS
							/sbin/setcfg "$ssid" status 6 -f $WIRELESS_PROFILE
						fi
						#stop #not to stop wlan0 even without correct key
						return
						#exit 1
					fi
					sleep 1
				done
			fi
			#get_net $ip $netmask

                        # check and add default gateway
			local dfgw=`$GETCFG "Network" "Default GW Device" -d "eth0"`
			if [ "$dfgw" = "wlan0" ]; then
					#/sbin/route add -net $net netmask $netmask gw $gateway
				/sbin/route add default gw $gateway wlan0
				if [ $? != 0 ]; then
					#Error : set routing fail.
					/sbin/setcfg "" status "5" -f $WIRELESS_STATUS
					/sbin/setcfg "$ssid" status 6 -f $WIRELESS_PROFILE
					stop
					return
					#exit 1
				fi
			fi

		fi

		#/sbin/setcfg "" status "Message : wireless is working." -f $WIRELESS_STATUS
		/sbin/setcfg "$ssid" status 1 -f $WIRELESS_PROFILE
		/sbin/write_log "[TCP/IP] You have successfully connected to the wireless network [$ssid]." 4
		reset_status
	else # include dhcp
		#/sbin/setcfg "" status "Message : geting wireless ip ..." -f $WIRELESS_STATUS

		# starting dhcp
		local dfgw=`$GETCFG "Network" "Default GW Device" -d "eth0"`
		if [ "$dfgw" = "wlan0" ]; then
			/sbin/dhcpcd -h "`/bin/hostname`" -t 30 $ifname & 
		else
			/sbin/dhcpcd -h "`/bin/hostname`" -t 30 -G $ifname &
		fi
		if [ $? != 0 ]; then
			#Error : can't start dhcp client.
			/sbin/setcfg "" status "6" -f $WIRELESS_STATUS
			/sbin/setcfg "$ssid" status 5 -f $WIRELESS_PROFILE
			stop
			return
			#exit 1
		fi

		#/sbin/setcfg "" status "Message : dhcp ok." -f $WIRELESS_STATUS

		# check ip was set on interface
		keyerr_ssid=""
		while [ 1 ]; do
			# authentication fail (key incorrect)
			status=`/sbin/getcfg "" status -f $WIRELESS_STATUS`
			if [ "$status" = "9" ]; then
				/sbin/setcfg "$ssid" status 7 -f $WIRELESS_PROFILE
				keyerr_ssid="$ssid"
				#stop
				#return
				#exit 1
			fi

			/sbin/ifconfig $ifname|/bin/grep "inet addr:"
			if [ $? = 0 ]; then
				ip=`/sbin/ifconfig $ifname|/bin/grep inet|/bin/cut -d : -f 2|/bin/cut -d ' ' -f 1`
				netmask=`/sbin/ifconfig $ifname|/bin/grep Mask|/bin/cut -d : -f 4|/bin/cut -d ' ' -f 1`
				gateway=`/sbin/route -e|/bin/grep $ifname|/bin/grep default|/bin/cut -d' ' -f 10`
				get_net $ip $netmask
				#/sbin/route del default gw $gateway
				#/sbin/route add -net $net netmask $netmask gw $gateway
				if [ $? != 0 ]; then
					#Error : set routing fail.
					/sbin/setcfg "" status "5" -f $WIRELESS_STATUS
					/sbin/setcfg "$ssid" status 5 -f $WIRELESS_PROFILE
					stop
					return
					#exit 1
				fi
				#/sbin/setcfg "" status "Message : wireless is working." -f $WIRELESS_STATUS
				/sbin/setcfg "$ssid" status 1 -f $WIRELESS_PROFILE
				/sbin/write_log "[TCP/IP] You have successfully connected to the wireless network [$ssid]." 4
				reset_status
				return
				#exit 0
			else
				count=`expr $count + 1`
				if [ $count = 60 ]; then
					auth=`/sbin/getcfg "$ssid" auth -f $WIRELESS_PROFILE`
					if [ "$auth" = "WEP" ]; then
						/usr/local/bin/iwconfig $ifname 2> /dev/null |/bin/grep "Encryption key" |/bin/grep "off"
						if [ $? != 0 ]; then
							/sbin/setcfg "$ssid" status 7 -f $WIRELESS_PROFILE
							stop
							return
						fi
					fi
					#Error : dhcp can't get ip information.
					/sbin/setcfg "" status "7" -f $WIRELESS_STATUS
					if [ -z "$keyerr_ssid" ] || [ "x$keyerr_ssid" != "x$ssid" ]; then
						/sbin/setcfg "$ssid" status 5 -f $WIRELESS_PROFILE
					fi
					#stop #not to let wlan0 down even without ip
					return
					#exit 1
				fi
			fi
			sleep 1
		done

	fi
}

case "$1" in
start)
	check_ip_forward
	start
	;;
stop)
	stop
	;;
restart)
	stop
	check_ip_forward
	start
	;;
auto_connect)
	check_ip_forward
	auto_connect
	;;
*)
	/bin/echo $"Usage: $0 {start|stop|restart|auto_connect}"
	exit 1
esac

# Service binding
/sbin/getbindaddr -refresh > /dev/null 2>/dev/null

do_exit 0
