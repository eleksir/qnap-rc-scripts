#/bin/sh

CUPS_CONF="/etc/config/cups/cupsd.conf"
TEMP="/tmp/cupsd.conf"
LPADMIN="/usr/local/sbin/lpadmin"
BACKEND_USB="/usr/lib/cups/backend/usb"
#LPADMIN="./systemv/lpadmin"
#BACKEND_USB="./backend/usb"

set_config()
{
	[ ! -d /var/spool/cups ] && /bin/mkdir -p /var/spool/cups
	[ ! -d /var/run/cups ] && /bin/mkdir -p /var/run/cups

	if [ -f $CUPS_CONF ]; then
		/bin/cat $CUPS_CONF|/bin/grep "BrowseRemoteProtocols" &> /dev/null
		[ $? = 0 ] && return
	fi

	/bin/mkdir -p /etc/config/cups 
	echo "#" > /etc/config/cups/cupsd.conf 
	echo "# \"$Id: cupsd.sh,v 1.24 2011-11-23 04:52:13 aixchou Exp $\"" >> /etc/config/cups/cupsd.conf
	echo "#" >> /etc/config/cups/cupsd.conf
	echo "# Sample configuration file for the CUPS scheduler.  See \"man cupsd.conf\" for a" >> /etc/config/cups/cupsd.conf
	echo "# complete description of this file." >> /etc/config/cups/cupsd.conf
	echo "#" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "# Log general information in error_log - change \"warn\" to \"debug\"" >> /etc/config/cups/cupsd.conf
	echo "# for troubleshooting..." >> /etc/config/cups/cupsd.conf
	echo "LogLevel warn" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "# Administrator user group..." >> /etc/config/cups/cupsd.conf
	echo "SystemGroup administrators" >> /etc/config/cups/cupsd.conf
	echo "ServerAlias *" >> /etc/config/cups/cupsd.conf
	echo "PreserveJobHistory Yes" >> /etc/config/cups/cupsd.conf
	echo "Include /etc/config/cups/cupsd.conf.maxjobs" >> /etc/config/cups/cupsd.conf
	echo "Include /etc/config/cups/cupsd.conf.allowip" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "# Only listen for connections from the local machine." >> /etc/config/cups/cupsd.conf
	echo "Listen *:631" >> /etc/config/cups/cupsd.conf
	echo "Listen /var/run/cups/cups.sock" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "# Show shared printers on the local network." >> /etc/config/cups/cupsd.conf
	echo "Browsing On" >> /etc/config/cups/cupsd.conf
	echo "BrowseOrder allow,deny" >> /etc/config/cups/cupsd.conf
	echo "BrowseAllow all" >> /etc/config/cups/cupsd.conf
	echo "BrowseLocalProtocols CUPS" >> /etc/config/cups/cupsd.conf
	echo "BrowseRemoteProtocols none" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "# Default authentication type, when authentication is required..." >> /etc/config/cups/cupsd.conf
	echo "DefaultAuthType Basic" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "# Restrict access to the server..." >> /etc/config/cups/cupsd.conf
	echo "#<Location />" >> /etc/config/cups/cupsd.conf
	echo "#  Order allow,deny" >> /etc/config/cups/cupsd.conf
	echo "#  allow all" >> /etc/config/cups/cupsd.conf
	echo "#</Location>" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "# Restrict access to the admin pages..." >> /etc/config/cups/cupsd.conf
	echo "<Location /admin>" >> /etc/config/cups/cupsd.conf
	echo "  Order allow,deny" >> /etc/config/cups/cupsd.conf
	echo "</Location>" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "# Restrict access to configuration files..." >> /etc/config/cups/cupsd.conf
	echo "<Location /admin/conf>" >> /etc/config/cups/cupsd.conf
	echo "  AuthType Default" >> /etc/config/cups/cupsd.conf
	echo "  Require user @SYSTEM" >> /etc/config/cups/cupsd.conf
	echo "  Order allow,deny" >> /etc/config/cups/cupsd.conf
	echo "</Location>" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "# Set the default printer/job policies..." >> /etc/config/cups/cupsd.conf
	echo "<Policy default>" >> /etc/config/cups/cupsd.conf
	echo "  # Job-related operations must be done by the owner or an administrator..." >> /etc/config/cups/cupsd.conf
	echo "  <Limit Send-Document Send-URI Hold-Job Release-Job Restart-Job Purge-Jobs Set-Job-Attributes Create-Job-Subscription Renew-Subscription Cancel-Subscription Get-Notifications Reprocess-Job Cancel-Current-Job Suspend-Current-Job Resume-Job CUPS-Move-Job CUPS-Get-Document>" >> /etc/config/cups/cupsd.conf
	echo "    Require user @OWNER @SYSTEM" >> /etc/config/cups/cupsd.conf
	echo "    Order deny,allow" >> /etc/config/cups/cupsd.conf
	echo "  </Limit>" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "  # All administration operations require an administrator to authenticate..." >> /etc/config/cups/cupsd.conf
	echo "  <Limit CUPS-Add-Modify-Printer CUPS-Delete-Printer CUPS-Add-Modify-Class CUPS-Delete-Class CUPS-Set-Default CUPS-Get-Devices>" >> /etc/config/cups/cupsd.conf
	echo "    AuthType Default" >> /etc/config/cups/cupsd.conf
	echo "    Require user @SYSTEM" >> /etc/config/cups/cupsd.conf
	echo "    Order deny,allow" >> /etc/config/cups/cupsd.conf
	echo "  </Limit>" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "  # All printer operations require a printer operator to authenticate..." >> /etc/config/cups/cupsd.conf
	echo "  <Limit Pause-Printer Resume-Printer Enable-Printer Disable-Printer Pause-Printer-After-Current-Job Hold-New-Jobs Release-Held-New-Jobs Deactivate-Printer Activate-Printer Restart-Printer Shutdown-Printer Startup-Printer Promote-Job Schedule-Job-After CUPS-Accept-Jobs CUPS-Reject-Jobs>" >> /etc/config/cups/cupsd.conf
	echo "    AuthType Default" >> /etc/config/cups/cupsd.conf
	echo "    Require user @SYSTEM" >> /etc/config/cups/cupsd.conf
	echo "    Order deny,allow" >> /etc/config/cups/cupsd.conf
	echo "  </Limit>" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "  # Only the owner or an administrator can cancel or authenticate a job..." >> /etc/config/cups/cupsd.conf
	echo "  <Limit Cancel-Job CUPS-Authenticate-Job>" >> /etc/config/cups/cupsd.conf
	echo "    Require user @OWNER @SYSTEM" >> /etc/config/cups/cupsd.conf
	echo "    Order deny,allow" >> /etc/config/cups/cupsd.conf
	echo "  </Limit>" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "  <Limit All>" >> /etc/config/cups/cupsd.conf
	echo "    Order deny,allow" >> /etc/config/cups/cupsd.conf
	echo "  </Limit>" >> /etc/config/cups/cupsd.conf
	echo "</Policy>" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "# Set the authenticated printer/job policies..." >> /etc/config/cups/cupsd.conf
	echo "<Policy authenticated>" >> /etc/config/cups/cupsd.conf
	echo "  # Job-related operations must be done by the owner or an administrator..." >> /etc/config/cups/cupsd.conf
	echo "  <Limit Create-Job Print-Job Print-URI>" >> /etc/config/cups/cupsd.conf
	echo "    AuthType Default" >> /etc/config/cups/cupsd.conf
	echo "    Order deny,allow" >> /etc/config/cups/cupsd.conf
	echo "  </Limit>" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "  <Limit Send-Document Send-URI Hold-Job Release-Job Restart-Job Purge-Jobs Set-Job-Attributes Create-Job-Subscription Renew-Subscription Cancel-Subscription Get-Notifications Reprocess-Job Cancel-Current-Job Suspend-Current-Job Resume-Job CUPS-Move-Job CUPS-Get-Document>" >> /etc/config/cups/cupsd.conf
	echo "    AuthType Default" >> /etc/config/cups/cupsd.conf
	echo "    Require user @OWNER @SYSTEM" >> /etc/config/cups/cupsd.conf
	echo "    Order deny,allow" >> /etc/config/cups/cupsd.conf
	echo "  </Limit>" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "  # All administration operations require an administrator to authenticate..." >> /etc/config/cups/cupsd.conf
	echo "  <Limit CUPS-Add-Modify-Printer CUPS-Delete-Printer CUPS-Add-Modify-Class CUPS-Delete-Class CUPS-Set-Default>" >> /etc/config/cups/cupsd.conf
	echo "    AuthType Default" >> /etc/config/cups/cupsd.conf
	echo "    Require user @SYSTEM" >> /etc/config/cups/cupsd.conf
	echo "    Order deny,allow" >> /etc/config/cups/cupsd.conf
	echo "  </Limit>" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "  # All printer operations require a printer operator to authenticate..." >> /etc/config/cups/cupsd.conf
	echo "  <Limit Pause-Printer Resume-Printer Enable-Printer Disable-Printer Pause-Printer-After-Current-Job Hold-New-Jobs Release-Held-New-Jobs Deactivate-Printer Activate-Printer Restart-Printer Shutdown-Printer Startup-Printer Promote-Job Schedule-Job-After CUPS-Accept-Jobs CUPS-Reject-Jobs>" >> /etc/config/cups/cupsd.conf
	echo "    AuthType Default" >> /etc/config/cups/cupsd.conf
	echo "    Require user @SYSTEM" >> /etc/config/cups/cupsd.conf
	echo "    Order deny,allow" >> /etc/config/cups/cupsd.conf
	echo "  </Limit>" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "  # Only the owner or an administrator can cancel or authenticate a job..." >> /etc/config/cups/cupsd.conf
	echo "  <Limit Cancel-Job CUPS-Authenticate-Job>" >> /etc/config/cups/cupsd.conf
	echo "    AuthType Default" >> /etc/config/cups/cupsd.conf
	echo "    Require user @OWNER @SYSTEM" >> /etc/config/cups/cupsd.conf
	echo "    Order deny,allow" >> /etc/config/cups/cupsd.conf
	echo "  </Limit>" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "  <Limit All>" >> /etc/config/cups/cupsd.conf
	echo "    Order deny,allow" >> /etc/config/cups/cupsd.conf
	echo "  </Limit>" >> /etc/config/cups/cupsd.conf
	echo "</Policy>" >> /etc/config/cups/cupsd.conf
	echo "" >> /etc/config/cups/cupsd.conf
	echo "#" >> /etc/config/cups/cupsd.conf
	echo "# End of \"$Id: cupsd.sh,v 1.24 2011-11-23 04:52:13 aixchou Exp $\"." >> /etc/config/cups/cupsd.conf
	echo "#" >> /etc/config/cups/cupsd.conf

	echo "MaxJobsPerPrinter 500" > /etc/config/cups/cupsd.conf.maxjobs

	echo "<Location />" > /etc/config/cups/cupsd.conf.allowip
  	echo "  Order allow,deny" >> /etc/config/cups/cupsd.conf.allowip
  	echo "  allow all" >> /etc/config/cups/cupsd.conf.allowip
	echo "</Location>" >> /etc/config/cups/cupsd.conf.allowip
}

cups_del_all_printers()
{
	for i in  1 2 3 4 5 6 7; do
		/usr/local/sbin/lpstat -t|/bin/grep printer|/bin/cut -d' ' -f2|xargs /usr/local/sbin/lpadmin -x > /dev/null 2>&1
	done
	/bin/rm -f /var/log/cups/* 2> /dev/null
}

cups_del_printer()
{
	index="$1"
	[ "$index" = "1" ] && index=""

	local printer=`/sbin/getcfg "Printers" "Name$index"`
	$LPADMIN -x $printer 2> /dev/null
}

add_bonjour_printer()
{
	index="$1"
	[ "$index" = "1" ] && index=""

	/sbin/getcfg "Bonjour Service" "PRINTER$index" > /dev/null 2>&1
	if [ $? != 0 ]; then
		/sbin/setcfg "Bonjour Service" "PRINTER$index" "1"
		local name=`/sbin/getcfg "Printers" "Name$index" -d "printer$index"`
		/sbin/setcfg "Bonjour Service" "PRINTER Service Name$index" "$name"
	fi
}

cups_add_printer()
{
	index="$1"
	[ "$index" = "1" ] && index=""

	local phy_exist=`/sbin/getcfg "Printers" "Phy exist$index"`
	#echo "index = $index, phy_exit = $phy_exist"
	[ "x$phy_exist" = "x" ] || [ "$phy_exist" = "0" ] && return

	local printer=`/sbin/getcfg "Printers" "Name$index" -d "printer$index"`
	local printer_model=`/sbin/getcfg "Printers" "Model$index"`
	local printer_sn=`/sbin/getcfg "Printers" "SN$index" -d "unknow"`
	if [ "$printer_sn" != "unknow" ]; then
	    local printer_uri=`$BACKEND_USB 2> /dev/null|/bin/grep "$printer_model"|/bin/grep "$printer_sn"|/bin/grep -v FAX|/bin/cut -d ' ' -f 2`
	else
	    local printer_uri=`$BACKEND_USB 2> /dev/null|/bin/grep "$printer_model"|/bin/grep -v FAX|/bin/cut -d ' ' -f 2`
	fi
	if [ "$printer_uri" = "" ]; then
		sleep 2
		if [ "$printer_sn" != "unknow" ]; then
			printer_uri=`$BACKEND_USB 2> /dev/null|/bin/grep "$printer_model"|/bin/grep "$printer_sn"|/bin/cut -d ' ' -f 2`
		else
			printer_uri=`$BACKEND_USB 2> /dev/null|/bin/grep "$printer_model"|/bin/cut -d ' ' -f 2`
		fi
		[ "$printer_uri" = "" ] && echo "can't get printer uri!" && exit 1
	fi
	#echo "printer = $printer  uri = $printer_uri"
	$LPADMIN -p $printer -v $printer_uri -o printer-error-policy=retry-job -E
	add_bonjour_printer $index
}

start()
{
	set_config
	/usr/sbin/cupsd -c /etc/config/cups/cupsd.conf
	#add or delete printers
	for i in  1 2 3 4 5 6 7; do
		index="$i"
		[ "$index" = "1" ] && index=""
		local phy_exist=`/sbin/getcfg "Printers" "Phy exist$index"`
		if [ "x$phy_exist" = "x" ] || [ "$phy_exist" = "0" ]; then
			cups_del_printer $i
		elif [ "$phy_exist" = "1" ]; then
			cups_add_printer $i
		fi
	done
}

stop()
{
	/usr/bin/killall cupsd 2> /dev/null
}

case "$1" in
start)
	start
	;;
stop)
	stop
	;;
restart)
	stop
	start
	;;
add_printer)
	cups_add_printer $2
	;;
del_printer)
	cups_del_printer $2
	;;
del_all_printers)
	cups_del_all_printers
	;;
*)
	/bin/echo $"Usage: $0 {start|stop|restart|add_printer|del_printer|del_all_printers}"
	exit 1
esac

exit 0
