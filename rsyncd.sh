#!/bin/sh

start()
{
	[ -f /etc/config/rsyncd.conf ] || /etc/init.d/rsyncd_srv.sh
	[ -f /etc/config/rsync_schedule.conf ] || echo "" > /etc/config/rsync_schedule.conf
	
	if [ `/sbin/getcfg System "Rsync Support" -u -d FALSE` = TRUE ]; then
		[ -f /var/run/rsyncd.pid ] && /bin/rm -f /var/run/rsyncd.pid
		echo "Starting rsync services: rsync."
		# Always launching rsyncd in server mode 1.
		#if [ `/sbin/getcfg "System" "Rsync Model" -u -d QNAP` = NORMAL ]; then
			#/sbin/daemon_mgr rsync start "/usr/bin/rsyncd --daemon --sever-mode=0"
			#/usr/bin/rsyncd --daemon --sever-mode=0 &
		#else
			#/sbin/daemon_mgr rsync start "/usr/bin/rsyncd --daemon --sever-mode=1"
			/usr/bin/rsyncd --daemon --sever-mode=1 --qnap-bwlimit &
		#fi
	fi
}

stop()
{
	echo "Shutting down rsync services: rsync."
	#/sbin/daemon_mgr rsync stop "/sbin/rsync --daemon"
#	/bin/kill -9 `/bin/cat /var/run/rsyncd.pid`
	/bin/kill -9 `/bin/pidof rsyncd`
	/bin/kill -9 `/bin/pidof rsync`
}

case "$1" in

start)
	start
	;;

stop)
	stop
	;;

restart)
	stop
	sleep 1
	start
	;;

*)
	echo "Usage: /etc/init.d/rsyncd.sh {start|stop|restart}"
	exit 1
esac

exit 0
