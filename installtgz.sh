#!/bin/sh
MNT_POINT="/mnt/ext"
MNT_HD_ROOT_TMP="/hd_root_tmp"
UPDATEPKG_DIR="${MNT_HD_ROOT_TMP}/update_pkg"
_PKG=$2
_LCK_FLAG=$1
if [ ! -z "${_PKG}" ] && [ -f "${_PKG}" ] && [ ! -z "${_LCK_FLAG}" ]; then
	[ -f "/tmp/${_LCK_FLAG}.lck" ] && exit 1
	/bin/touch "/tmp/${_LCK_FLAG}.lck"
fi

case "$1" in
    ffmpeg)
	_PKG=$2
	if [ -f ${_PKG} ]; then
		[ -d ${MNT_POINT}/opt/ffmpeg ] || /bin/mkdir -p ${MNT_POINT}/opt/ffmpeg
		/bin/chmod 777 ${MNT_POINT}/opt/ffmpeg
		/bin/tar zxf "${_PKG}" -C ${MNT_POINT}/opt/ffmpeg 2>/dev/null
		/bin/ln -sf ${MNT_POINT}/opt/ffmpeg/usr/bin/ffmpeg /usr/bin/ffmpeg
		[ ! -f ${MNT_POINT}/opt/ffmpeg/usr/bin/ffserver ] || /bin/ln -sf ${MNT_POINT}/opt/ffmpeg/usr/bin/ffserver /usr/bin/
		for i in `/bin/ls ${MNT_POINT}/opt/ffmpeg/usr/lib/`
		do
			/bin/ln -sf "${MNT_POINT}/opt/ffmpeg/usr/lib/${i}" /usr/lib/
		done
		/bin/sync
	fi
	[ ! -d /tmp/install ] || /bin/touch /tmp/install/ffmpeg
    ;;
    ImageMagick)
	_PKG=$2
	if [ -f ${_PKG} ]; then
		[ -d ${MNT_POINT}/opt/ImageMagick ] || /bin/mkdir -p ${MNT_POINT}/opt/ImageMagick
		/bin/chmod 777 ${MNT_POINT}/opt/ImageMagick
		/bin/tar zxf "${_PKG}" -C ${MNT_POINT}/opt/ImageMagick 2>/dev/null
		/bin/ln -sf ${MNT_POINT}/opt/ImageMagick/usr/local/sbin/convert /usr/local/sbin/convert
		/bin/ln -sf ${MNT_POINT}/opt/ImageMagick/usr/local/sbin/identify /usr/local/sbin/identify
		/bin/ln -sf ${MNT_POINT}/opt/ImageMagick/usr/lib/libMagickWand.so.3.0.0 /usr/lib/libMagickWand.so.3.0.0
		/bin/ln -sf libMagickWand.so.3.0.0 /usr/lib/libMagickWand.so.3
		/bin/ln -sf ${MNT_POINT}/opt/ImageMagick/usr/lib/libMagickCore.so.3.0.0 /usr/lib/libMagickCore.so.3.0.0
		/bin/ln -sf libMagickCore.so.3.0.0 /usr/lib/libMagickCore.so.3
		/bin/sync
	fi
	[ ! -d /tmp/install ] || /bin/touch /tmp/install/ImageMagick
    ;;
    radius)
	_PKG=$2
	if [ -f ${_PKG} ]; then
		[ -d ${MNT_POINT}/opt/radius ] || /bin/mkdir -p ${MNT_POINT}/opt/radius
		/bin/chmod 777 ${MNT_POINT}/opt/radius
		/bin/tar zxf "${_PKG}" -C ${MNT_POINT}/opt/radius 2>/dev/null
		/bin/ln -sf ${MNT_POINT}/opt/radius/etc/raddb /etc/raddb
		/bin/ln -sf ${MNT_POINT}/opt/radius/usr/local/sbin/radclient /usr/local/sbin/radclient
		/bin/ln -sf ${MNT_POINT}/opt/radius/usr/local/sbin/radiusd /usr/local/sbin/radiusd
		/bin/ln -sf ${MNT_POINT}/opt/radius/usr/local/sbin/radtest /usr/local/sbin/radtest
		/bin/sync
	fi
	[ ! -d /tmp/install ] || /bin/touch /tmp/install/radius
    ;;
    vim)
	_PKG=$2
	if [ -f ${_PKG} ]; then
		echo "install ${_PKG}"
		[ -d ${MNT_POINT}/opt ] || /bin/mkdir -p ${MNT_POINT}/opt
		/bin/tar zxf ${_PKG} -C ${MNT_POINT}/opt 2>/dev/null
		/bin/rm -f /bin/vi
		/bin/ln -sf ${MNT_POINT}/opt/vim/vim /bin/vi
		/bin/ln -sf ${MNT_POINT}/opt/vim/vim /bin/vim
		/bin/cp -f ${MNT_POINT}/opt/vim/.vimrc /root/
	else
		echo "${_PKG} is not exist!"
	fi
	[ ! -d /tmp/install ] || /bin/touch /tmp/install/vim
    ;;
    samba)
	_PKG=$2
	if [ -f ${_PKG} ]; then
		echo "install ${_PKG}"
		[ -d ${MNT_POINT}/opt ] || /bin/mkdir -p ${MNT_POINT}/opt
		/bin/tar zxf ${_PKG} -C ${MNT_POINT}/opt 2>/dev/null
		/bin/rm -rf /usr/local/samba
		/bin/ln -sf ${MNT_POINT}/opt/samba /usr/local/samba
	else
		echo "${_PKG} is not exist!"
	fi
	[ ! -d /tmp/install ] || /bin/touch /tmp/install/samba
    ;;
    libtorrent)   
        _PKG=$2
        if [ -f ${_PKG} ]; then
                echo "install ${_PKG}"
                [ -d ${MNT_POINT}/opt/libtorrent ] || /bin/mkdir -p ${MNT_POINT}/opt/libtorrent
                /bin/tar zxf ${_PKG} -C ${MNT_POINT}/opt/libtorrent 2>/dev/null
                if [ -f ${MNT_POINT}/opt/libtorrent/libtorrent-0.15.9.so.enc ]; then
                        /bin/ln -sf ${MNT_POINT}/opt/libtorrent/libtorrent-0.15.9.so.enc /usr/lib/libtorrent-0.15.9.so.enc
                fi
                if [ -f ${MNT_POINT}/opt/libtorrent/libtorrent-0.15.9.so.nonenc ]; then
                        /bin/ln -sf ${MNT_POINT}/opt/libtorrent/libtorrent-0.15.9.so.nonenc /usr/lib/libtorrent-0.15.9.so.nonenc
                fi
                if [ -f ${MNT_POINT}/opt/libtorrent/libtorrent-0.13.so.enc ]; then
                        /bin/ln -sf ${MNT_POINT}/opt/libtorrent/libtorrent-0.13.so.enc /usr/lib/libtorrent-0.13.so.enc
                fi
                if [ -f ${MNT_POINT}/opt/libtorrent/libtorrent-0.13.so.nonenc ]; then
                        /bin/ln -sf ${MNT_POINT}/opt/libtorrent/libtorrent-0.13.so.nonenc /usr/lib/libtorrent-0.13.so.nonenc
                fi
                if [ -f ${MNT_POINT}/opt/libtorrent/libboost_date_time.so.1.42.0 ]; then
                        /bin/ln -sf ${MNT_POINT}/opt/libtorrent/libboost_date_time.so.1.42.0 /usr/lib/libboost_date_time.so.1.42.0
                        /bin/ln -sf /usr/lib/libboost_date_time.so.1.42.0 /usr/lib/libboost_date_time.so
                fi
                if [ -f ${MNT_POINT}/opt/libtorrent/libboost_filesystem.so.1.42.0 ]; then
                        /bin/ln -sf ${MNT_POINT}/opt/libtorrent/libboost_filesystem.so.1.42.0 /usr/lib/libboost_filesystem.so.1.42.0
                        /bin/ln -sf /usr/lib/libboost_filesystem.so.1.42.0 /usr/lib/libboost_filesystem.so
                fi
                if [ -f ${MNT_POINT}/opt/libtorrent/libboost_program_options.so.1.42.0 ]; then
                        /bin/ln -sf ${MNT_POINT}/opt/libtorrent/libboost_program_options.so.1.42.0 /usr/lib/libboost_program_options.so.1.42.0
                        /bin/ln -sf /usr/lib/libboost_program_options.so.1.42.0 /usr/lib/libboost_program_options.so
                fi
                if [ -f ${MNT_POINT}/opt/libtorrent/libboost_regex.so.1.42.0 ]; then
                        /bin/ln -sf ${MNT_POINT}/opt/libtorrent/libboost_regex.so.1.42.0 /usr/lib/libboost_regex.so.1.42.0
                        /bin/ln -sf /usr/lib/libboost_regex.so.1.42.0 /usr/lib/libboost_regex.so
                fi
                if [ -f ${MNT_POINT}/opt/libtorrent/libboost_system.so.1.42.0 ]; then
                        /bin/ln -sf ${MNT_POINT}/opt/libtorrent/libboost_system.so.1.42.0 /usr/lib/libboost_system.so.1.42.0
                        /bin/ln -sf /usr/lib/libboost_system.so.1.42.0 /usr/lib/libboost_system.so
                fi
                if [ -f ${MNT_POINT}/opt/libtorrent/libboost_thread.so.1.42.0 ]; then
                        /bin/ln -sf ${MNT_POINT}/opt/libtorrent/libboost_thread.so.1.42.0 /usr/lib/libboost_thread.so.1.42.0
                        /bin/ln -sf /usr/lib/libboost_thread.so.1.42.0 /usr/lib/libboost_thread.so
                fi
        else
                echo "${_PKG} is not exist!"
        fi
	[ ! -d /tmp/install ] || /bin/touch /tmp/install/libtorrent
    ;;
    printer)
	_PKG=$2
	_MNT_POINT="/mnt/HDA_ROOT"
	if [ -f ${_PKG} ]; then
		echo "install ${_PKG}"
		[ -d ${_MNT_POINT}/.driver ] || /bin/mkdir -p ${_MNT_POINT}/.driver
		/bin/tar zxf ${_PKG} -C ${_MNT_POINT}/.driver 2>/dev/null
	else
		echo "${_PKG} is not exist!"
	fi
	[ ! -d /tmp/install ] || /bin/touch /tmp/install/printer
    ;;
    vaultServices)
	_PKG=$2
	if [ -f ${_PKG} ]; then
		echo "install ${_PKG}"
		[ -d ${MNT_POINT}/opt ] || /bin/mkdir -p ${MNT_POINT}/opt
		/bin/tar zxf ${_PKG} -C ${MNT_POINT}/opt 2>/dev/null
		[ -d /etc/config/vaultServices ] || /usr/bin/install -d /etc/config/vaultServices
		[ -f /etc/config/vaultServices/vaultServices.config ] || /bin/cp -p /etc/default_config/vaultServices/vaultServices.config /etc/config/vaultServices/vaultServices.config
		/bin/ln -sf ${MNT_POINT}/opt/vaultServices/vaultServices /usr/sbin/vaultServices
		/bin/ln -sf ${MNT_POINT}/opt/vaultServices/vaultServicesDaemon /usr/sbin/vaultServicesDaemon
	else
		echo "${_PKG} is not exist!"
	fi
	[ ! -d /tmp/install ] || /bin/touch /tmp/install/vaultServices
    ;;
    language)
	_PKG=$2
	BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf`
	/usr/bin/readlink /home/httpd/cgi-bin/inc 1>>/dev/null 2>>/dev/null
	if [ $? != 0 ] && [ "x${BOOT_CONF}" = "xTS-NASX86" ]; then
		[ -d ${MNT_POINT}/opt/lang/inc ] || /bin/mkdir -p ${MNT_POINT}/opt/lang/inc
		/bin/mv /home/httpd/cgi-bin/inc/* ${MNT_POINT}/opt/lang/inc/
		/bin/rm -rf /home/httpd/cgi-bin/inc
		/bin/ln -sf ${MNT_POINT}/opt/lang/inc /home/httpd/cgi-bin/
	fi
	if [ -f ${_PKG} ]; then
		echo "install ${_PKG}"
		/bin/tar zxf ${_PKG} -C /
	else
		echo "${_PKG} is not exist!"
	fi
	[ ! -f /home/httpd/cgi-bin/inc/lang_es-mex.js ] || /bin/rm -rf /home/httpd/cgi-bin/inc/lang_es-mex.js
	[ ! -f /home/httpd/cgi-bin/inc/s_lang_es-mex.js ] || /bin/rm -rf /home/httpd/cgi-bin/inc/s_lang_es-mex.js
	[ ! -f /etc/init.d/genLanglist.sh ] || /etc/init.d/genLanglist.sh
	[ ! -d /tmp/install ] || /bin/touch /tmp/install/language
    ;;
    jsLib)
	_PKG=$2
	if [ -f ${_PKG} ]; then
		BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf`
		/usr/bin/readlink /home/httpd/ajax_obj 1>>/dev/null 2>>/dev/null
		if [ $? != 0 ] && [ "x${BOOT_CONF}" = "xTS-NASX86" ]; then
			/bin/rm -rf ${MNT_POINT}/opt/ajax_obj 1>/dev/null 2>/dev/null
			[ ! -d /home/httpd/ajax_obj ] || /bin/mv /home/httpd/ajax_obj ${MNT_POINT}/opt/
			[ ! -d ${MNT_POINT}/opt/ajax_obj ] || /bin/mkdir ${MNT_POINT}/opt/ajax_obj
			/bin/ln -sf ${MNT_POINT}/opt/ajax_obj /home/httpd/
		else
			[ -d /home/httpd/ajax_obj ] || /bin/mkdir -p /home/httpd/ajax_obj
		fi
		/bin/tar xf ${_PKG} -C /home/httpd/
		[ ! -f /home/httpd/cgi-bin/user-settings.json.bg6 ] || /bin/mv /home/httpd/cgi-bin/user-settings.json.bg6 /home/httpd/cgi-bin/user-settings.json
	fi
    ;;
    DS)
	BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf`
	_PKG=$2
	if [ -f ${_PKG} ]; then
		echo "install ${_PKG}"
		if [ ! -d /home/httpd/cgi-bin/Qdownload ]; then
			if [ "x${BOOT_CONF}" = "xTS-NASX86" ]; then
				[ -d ${MNT_POINT}/opt ] && /bin/tar zxf ${_PKG} -C ${MNT_POINT}/opt
				/bin/ln -sf ${MNT_POINT}/opt/Qdownload /home/httpd/cgi-bin/
			else
				/bin/tar zxf ${_PKG} -C /home/httpd/cgi-bin/
			fi
		fi
	else
		echo "${_PKG} is not exist!"
	fi
    ;;
    vpnpptp)
	_PKG=$2
	if [ -f ${_PKG} ]; then
		echo "install ${_PKG}!"
		[ -d ${MNT_POINT}/opt/vpnpptp ] || /bin/mkdir -p ${MNT_POINT}/opt/vpnpptp
		/bin/chmod 777 ${MNT_POINT}/opt/vpnpptp
		/bin/tar zxf ${_PKG} -C ${MNT_POINT}/opt/vpnpptp 2>/dev/null
		/bin/ln -sf ${MNT_POINT}/opt/vpnpptp/usr/lib/libsmbpw.1.3.so /usr/lib/libsmbpw.1.3.so
		/bin/ln -sf ${MNT_POINT}/opt/vpnpptp/usr/sbin/pppd /usr/sbin/pppd
		/bin/ln -sf ${MNT_POINT}/opt/vpnpptp/usr/sbin/pptpd /usr/sbin/pptpd
		/bin/ln -sf ${MNT_POINT}/opt/vpnpptp/usr/sbin/pptpctrl /usr/sbin/pptpctrl
		/bin/ln -sf ${MNT_POINT}/opt/vpnpptp/usr/sbin/bcrelay /usr/sbin/bcrelay
		/bin/ln -sf ${MNT_POINT}/opt/vpnpptp/sbin/xtables-multi /sbin/xtables-multi
		/bin/ln -sf ${MNT_POINT}/opt/vpnpptp/usr/lib/libip4tc.so.0.0.0 /usr/lib/libip4tc.so.0.0.0
		/bin/ln -sf ${MNT_POINT}/opt/vpnpptp/usr/lib/libip6tc.so.0.0.0 /usr/lib/libip6tc.so.0.0.0
		/bin/ln -sf ${MNT_POINT}/opt/vpnpptp/usr/lib/libxtables.so.7.0.0 /usr/lib/libxtables.so.7.0.0
		[ -L /usr/local/modules/vpn ] || /bin/ln -sf ${MNT_POINT}/opt/vpnpptp/usr/local/modules/vpn /usr/local/modules/vpn
		[ -L /usr/lib/xtables ] || /bin/ln -sf ${MNT_POINT}/opt/vpnpptp/usr/lib/xtables /usr/lib/xtables
		/bin/sync
		[ ! -d /tmp/install ] || /bin/touch /tmp/install/vpnpptp
	else
		echo "${_PKG} is not exist!"
	fi
    ;;
	vpnopenvpn)
	_PKG=$2
	if [ -f ${_PKG} ]; then
		echo "install ${_PKG}!"
		[ -d ${MNT_POINT}/opt/vpnopenvpn ] || /bin/mkdir -p ${MNT_POINT}/opt/vpnopenvpn
		/bin/chmod 777 ${MNT_POINT}/opt/vpnopenvpn
		/bin/tar zxf ${_PKG} -C ${MNT_POINT}/opt/vpnopenvpn 2>/dev/null
		/bin/ln -sf ${MNT_POINT}/opt/vpnopenvpn/usr/lib/liblzo2.so.2.0.0 /usr/lib/liblzo2.so.2.0.0
		/bin/ln -sf ${MNT_POINT}/opt/vpnopenvpn/usr/lib/liblzo2.so.2.0.0 /usr/lib/liblzo2.so.2
		/bin/ln -sf ${MNT_POINT}/opt/vpnopenvpn/usr/lib/liblzo2.so.2.0.0 /usr/lib/liblzo2.so
		/bin/ln -sf ${MNT_POINT}/opt/vpnopenvpn/usr/sbin/openvpn /usr/sbin/openvpn
		/bin/ln -sf ${MNT_POINT}/opt/vpnopenvpn/etc/openvpn /etc/openvpn
		/bin/sync
		[ ! -d /tmp/install ] || /bin/touch /tmp/install/vpnopenvpn
	else
		echo "${_PKG} is not exist!"
	fi
	;;
	avahi0630)
	_PKG=$2
	if [ -f ${_PKG} ]; then
		echo "install ${_PKG}!"
		[ -d ${MNT_POINT}/opt/avahi0630 ] || /bin/mkdir -p ${MNT_POINT}/opt/avahi0630
		/bin/chmod 777 ${MNT_POINT}/opt/avahi0630
		/bin/tar zxf ${_PKG} -C ${MNT_POINT}/opt/avahi0630 2>/dev/null
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/lib/libdaemon.so.0.5.0 /usr/lib/libdaemon.so
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/lib/libdaemon.so.0.5.0 /usr/lib/libdaemon.so.0
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/sbin/dbus-daemon /usr/sbin/dbus-daemon
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/sbin/dbus-daemon-launch-helper /usr/sbin/dbus-daemon-launch-helper
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/sbin/dbus-send /usr/sbin/dbus-send
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/sbin/dbus-monitor /usr/sbin/dbus-monitor
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/sbin/dbus-uuidgen /usr/sbin/dbus-uuidgen
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/lib/libdbus-1.so.3.5.8 /usr/lib/libdbus-1.so
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/lib/libdbus-1.so.3.5.8 /usr/lib/libdbus-1.so.3
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/etc/dbus-1 /etc/dbus-1
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/sbin/avahi-autoipd /usr/sbin/avahi-autoipd
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/lib/libavahi-client.so.3.2.9 /usr/lib/libavahi-client.so
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/lib/libavahi-client.so.3.2.9 /usr/lib/libavahi-client.so.3
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/lib/libavahi-common.so.3.5.3 /usr/lib/libavahi-common.so
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/lib/libavahi-common.so.3.5.3 /usr/lib/libavahi-common.so.3
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/lib/libavahi-core.so.7.0.2 /usr/lib/libavahi-core.so
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/lib/libavahi-core.so.7.0.2 /usr/lib/libavahi-core.so.7
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/sbin/avahi-daemon /usr/sbin/avahi-daemon
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/sbin/avahi-dnsconfd /usr/sbin/avahi-dnsconfd
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/lib/libavahi-glib.so.1.0.2 /usr/lib/libavahi-glib.so
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/lib/libavahi-glib.so.1.0.2 /usr/lib/libavahi-glib.so.1
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/lib/libavahi-gobject.so.0.0.4 /usr/lib/libavahi-gobject.so
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/lib/libavahi-gobject.so.0.0.4 /usr/lib/libavahi-gobject.so.0
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/sbin/avahi-browse /usr/sbin/avahi-browse
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/sbin/avahi-publish /usr/sbin/avahi-publish
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/sbin/avahi-resolve /usr/sbin/avahi-resolve
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/sbin/avahi-set-host-name /usr/sbin/avahi-set-host-name
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/sbin/avahi-resolve /usr/sbin/avahi-resolve-host-name
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/sbin/avahi-resolve /usr/sbin/avahi-resolve-address
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/sbin/avahi-browse /usr/sbin/avahi-browse-domains
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/sbin/avahi-publish /usr/sbin/avahi-publish-address
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/usr/sbin/avahi-publish /usr/sbin/avahi-publish-service
		/bin/ln -sf ${MNT_POINT}/opt/avahi0630/etc/avahi /etc/avahi
		if [ -f /etc/init.d/avahi.sh ]; then
			/etc/init.d/bonjour.sh stop 1>>/dev/null 2>>/dev/null
			/bin/ln -sf avahi.sh /etc/init.d/bonjour.sh
		fi
		/bin/sync
		[ ! -d /tmp/install ] || /bin/touch /tmp/install/avahi0630
	else
		echo "${_PKG} is not exist!"
	fi
	;;
	ldap_server)
	_PKG=$2
	if [ -f ${_PKG} ]; then
		[ -d ${MNT_POINT}/opt/ldap_server ] || /bin/mkdir -p ${MNT_POINT}/opt/ldap_server
		/bin/chmod 777 ${MNT_POINT}/opt/ldap_server
		/bin/tar zxf ${_PKG} -C ${MNT_POINT}/opt/ldap_server 2>/dev/null
		[ -d /etc/openldap ] || /bin/mkdir -p /etc/openldap
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/etc/openldap/schema /etc/openldap/schema	
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/usr/sbin/slapd /usr/sbin/slapd
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/usr/sbin/slapd /usr/sbin/slapacl
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/usr/sbin/slapd /usr/sbin/slapadd
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/usr/sbin/slapd /usr/sbin/slapauth
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/usr/sbin/slapd /usr/sbin/slapcat
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/usr/sbin/slapd /usr/sbin/slapdn
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/usr/sbin/slapd /usr/sbin/slapindex
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/usr/sbin/slapd /usr/sbin/slappasswd
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/usr/sbin/slapd /usr/sbin/slapschema
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/usr/sbin/slapd /usr/sbin/slaptest
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/usr/bin/ldapdelete /usr/bin/ldapdelete
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/usr/bin/ldapmodify /usr/bin/ldapadd
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/usr/bin/ldapmodify /usr/bin/ldapmodify
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/usr/bin/ldappasswd /usr/bin/ldappasswd
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/usr/bin/mkntpwd /usr/bin/mkntpwd
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/sbin/ntpd /sbin/ntpd
		/bin/ln -sf ${MNT_POINT}/opt/ldap_server/sbin/ntpq /sbin/ntpq
		echo "install ${_PKG}!"
	else
		echo "${_PKG} is not exist!"
	fi
	;;
    WFM2)
	_PKG=$2
	if [ -f ${_PKG} ]; then
		echo "install ${_PKG}"
		/bin/tar zxf ${_PKG} -C ${MNT_POINT}/opt 2>/dev/null
		/bin/rm -f /home/httpd/cgi-bin/filemanager 2>/dev/null
		/bin/ln -sf ${MNT_POINT}/opt/filemanager /home/httpd/cgi-bin/
	else
		echo "${_PKG} is not exist!"
	fi
	[ ! -d /tmp/install ] || /bin/touch /tmp/install/WFM2
    ;;
    WFM3)
	_PKG=$2
	if [ -f ${_PKG} ]; then
		echo "install ${_PKG}"
		[ -d ${MNT_POINT}/opt/WFM ] || /bin/mkdir -p ${MNT_POINT}/opt/WFM
		/bin/tar zxf ${_PKG} -C ${MNT_POINT}/opt/WFM 2>/dev/null
		[ -d /home/httpd/cgi-bin/apps ] || /bin/mkdir -p /home/httpd/cgi-bin/apps
		/bin/rm -f /home/httpd/cgi-bin/apps/fileExplorer 2>/dev/null
		/bin/rm -f /home/httpd/cgi-bin/css 2>/dev/null
		/bin/rm -f /home/httpd/cgi-bin/js 2>/dev/null
		[ -d /home/httpd/cgi-bin/apps/fileExplorer ] || /bin/ln -sf ${MNT_POINT}/opt/WFM/cgi-bin/apps/fileExplorer /home/httpd/cgi-bin/apps/
		[ -d /home/httpd/cgi-bin/css ] || /bin/ln -sf ${MNT_POINT}/opt/WFM/cgi-bin/css /home/httpd/cgi-bin/
		[ -d /home/httpd/cgi-bin/js ] || /bin/ln -sf ${MNT_POINT}/opt/WFM/cgi-bin/js /home/httpd/cgi-bin/
		[ -d /home/httpd/libs ] || /bin/ln -sf ${MNT_POINT}/opt/WFM/libs /home/httpd/
		/bin/ln -sf ${MNT_POINT}/opt/WFM/cgi-bin/filemanager.html /home/httpd/cgi-bin/
	else
		echo "${_PKG} is not exist!"
	fi
	[ ! -d /tmp/install ] || /bin/touch /tmp/install/WFM2
    ;;
    DSv3)
	_PKG=$2
	if [ -f ${_PKG} ]; then
		#remove addons
		_HG=`getcfg HappyGet Install_Path -f /etc/config/qpkg.conf`
		if [ "x$_HG" != x ]; then
			[ ! -d "$_HG" ] || /bin/rm -rf "$_HG"
			/bin/rm -f /share/`/sbin/getcfg SHARE_DEF defWeb -d Qweb -f /etc/config/def_share.info`/gt 2>/dev/null
			/sbin/rmcfg HappyGet -f /etc/config/qpkg.conf 2>/dev/null
		fi
		echo "install ${_PKG}"
		[ -d ${MNT_POINT}/opt/DSv3 ] || /bin/mkdir -p ${MNT_POINT}/opt/DSv3
		/bin/tar zxf ${_PKG} -C ${MNT_POINT}/opt/DSv3 2>/dev/null
		if [ -f ${MNT_POINT}/opt/DSv3/usr/lib/libtorrent-0.15.9.so.enc ]; then
			/bin/ln -sf ${MNT_POINT}/opt/DSv3/usr/lib/libtorrent-0.15.9.so.enc /usr/lib/libtorrent-0.15.9.so.enc
		fi
		if [ -f ${MNT_POINT}/opt/DSv3/usr/lib/libtorrent-0.15.9.so.nonenc ]; then
			/bin/ln -sf ${MNT_POINT}/opt/DSv3/usr/lib/libtorrent-0.15.9.so.nonenc /usr/lib/libtorrent-0.15.9.so.nonenc
		fi
		if [ -f ${MNT_POINT}/opt/DSv3/usr/lib/libboost_date_time.so.1.42.0 ]; then
			/bin/ln -sf ${MNT_POINT}/opt/DSv3/usr/lib/libboost_date_time.so.1.42.0 /usr/lib/libboost_date_time.so.1.42.0
			/bin/ln -sf /usr/lib/libboost_date_time.so.1.42.0 /usr/lib/libboost_date_time.so
		fi
		if [ -f ${MNT_POINT}/opt/DSv3/usr/lib/libboost_filesystem.so.1.42.0 ]; then
			/bin/ln -sf ${MNT_POINT}/opt/DSv3/usr/lib/libboost_filesystem.so.1.42.0 /usr/lib/libboost_filesystem.so.1.42.0
			/bin/ln -sf /usr/lib/libboost_filesystem.so.1.42.0 /usr/lib/libboost_filesystem.so
		fi
		if [ -f ${MNT_POINT}/opt/DSv3/usr/lib/libboost_program_options.so.1.42.0 ]; then
			/bin/ln -sf ${MNT_POINT}/opt/DSv3/usr/lib/libboost_program_options.so.1.42.0 /usr/lib/libboost_program_options.so.1.42.0
			/bin/ln -sf /usr/lib/libboost_program_options.so.1.42.0 /usr/lib/libboost_program_options.so
		fi
		if [ -f ${MNT_POINT}/opt/DSv3/usr/lib/libboost_regex.so.1.42.0 ]; then
			/bin/ln -sf ${MNT_POINT}/opt/DSv3/usr/lib/libboost_regex.so.1.42.0 /usr/lib/libboost_regex.so.1.42.0
			/bin/ln -sf /usr/lib/libboost_regex.so.1.42.0 /usr/lib/libboost_regex.so
		fi
		if [ -f ${MNT_POINT}/opt/DSv3/usr/lib/libboost_system.so.1.42.0 ]; then
			/bin/ln -sf ${MNT_POINT}/opt/DSv3/usr/lib/libboost_system.so.1.42.0 /usr/lib/libboost_system.so.1.42.0
			/bin/ln -sf /usr/lib/libboost_system.so.1.42.0 /usr/lib/libboost_system.so
		fi
		if [ -f ${MNT_POINT}/opt/DSv3/usr/lib/libboost_thread.so.1.42.0 ]; then
			/bin/ln -sf ${MNT_POINT}/opt/DSv3/usr/lib/libboost_thread.so.1.42.0 /usr/lib/libboost_thread.so.1.42.0
			/bin/ln -sf /usr/lib/libboost_thread.so.1.42.0 /usr/lib/libboost_thread.so
		fi
		[ ! -d /home/httpd/cgi-bin/Qdownload ] || /bin/rm -rf /home/httpd/cgi-bin/Qdownload
		/bin/ln -sf ${MNT_POINT}/opt/DSv3/home/httpd/cgi-bin/Qdownload /home/httpd/cgi-bin/
		if [ -f ${MNT_POINT}/opt/DSv3/usr/sbin/bt_tool ]; then
			/bin/ln -sf ${MNT_POINT}/opt/DSv3/usr/sbin/bt_tool /usr/sbin/bt_tool
		fi
		if [ -f ${MNT_POINT}/opt/DSv3/usr/sbin/btd ]; then
			/bin/ln -sf ${MNT_POINT}/opt/DSv3/usr/sbin/btd /usr/sbin/btd
		fi
		if [ -f ${MNT_POINT}/opt/DSv3/usr/sbin/bt_scheduler ]; then
			/bin/ln -sf ${MNT_POINT}/opt/DSv3/usr/sbin/bt_scheduler /usr/sbin/bt_scheduler
		fi
		if [ -f ${MNT_POINT}/opt/DSv3/usr/sbin/torrent_util ]; then
			/bin/ln -sf ${MNT_POINT}/opt/DSv3/usr/sbin/torrent_util /usr/sbin/torrent_util
		fi
		if [ -f ${MNT_POINT}/opt/DSv3/usr/sbin/curl_download ]; then
			/bin/ln -sf ${MNT_POINT}/opt/DSv3/usr/sbin/curl_download /usr/sbin/curl_download
		fi
		if [ -d ${MNT_POINT}/opt/DSv3/opt/gt ]; then
			/bin/mv -f ${MNT_POINT}/opt/DSv3/opt/gt ${MNT_POINT}/opt/DSv3/
			/bin/rm -r ${MNT_POINT}/opt/DSv3/opt
		fi
		if [ -f ${MNT_POINT}/opt/DSv3/gt/gt_run.sh ]; then
			/bin/ln -sf ${MNT_POINT}/opt/DSv3/gt/gt_run.sh /etc/init.d/gt_run.sh
			/bin/chmod 755 ${MNT_POINT}/opt/DSv3/gt/gt_run.sh
		fi
		if [ -f ${UPDATEPKG_DIR}/gt.tgz ]; then
			/bin/tar zxf ${UPDATEPKG_DIR}/gt.tgz -C ${MNT_POINT}/opt/DSv3/ 2>/dev/null
		fi
		_UPDATEPKG_DIR="/mnt/HDA_ROOT/update_pkg"
		if [ -f ${_UPDATEPKG_DIR}/gt.tgz ]; then
			/bin/tar zxf ${_UPDATEPKG_DIR}/gt.tgz -C ${MNT_POINT}/opt/DSv3/ 2>/dev/null
		fi
	else
		echo "${_PKG} is not exist!"
	fi
	;;
	aMule)
	_PKG=$2
	if [ -f ${_PKG} ]; then
		[ -d ${MNT_POINT}/opt/aMule ] || /bin/mkdir -p ${MNT_POINT}/opt/aMule
		/bin/chmod 777 ${MNT_POINT}/opt/aMule
		/bin/tar zxf ${_PKG} -C ${MNT_POINT}/opt/aMule 2>/dev/null
		/bin/ln -sf ${MNT_POINT}/opt/aMule /usr/local/aMule
		echo "install ${_PKG}!"
	else
		echo "${_PKG} is not exist!"
	fi
    ;;
	medialibrary)
	_PKG=$2
	if [ -f ${_PKG} ]; then
		echo "install ${_PKG}"
		/bin/tar zxf ${_PKG} -C ${MNT_POINT}/opt 2>/dev/null
		/bin/ln -sf ${MNT_POINT}/opt/medialibrary /usr/local/medialibrary
	else
		echo "${_PKG} is not exist!"
	fi
	[ ! -d /tmp/install ] || /bin/touch /tmp/install/medialibrary
    ;;
	*)
	exit 0
esac

/bin/rm "/tmp/${_LCK_FLAG}.lck" 2>/dev/null
exit 0
