#!/bin/sh

#. /etc/init.d/check_nss2
NSS_ENABLED=`/sbin/getcfg NVR "NSS Enabled" -d no`

restart_other_services()
{
# restart daemon_mgr
#/usr/bin/killall daemon_mgr 2>/dev/null 1>/dev/null
#/sbin/daemon_mgr & 2>/dev/null 1>/dev/null
#/bin/sleep 1
# restart daemon_mgr.nvr
/usr/bin/killall daemon_mgr.nvr 2>/dev/null 1>/dev/null
/sbin/daemon_mgr.nvr & 2>/dev/null 1>/dev/null
/bin/sleep 1

# restart qLogEngined
/bin/kill -INT `pidof qLogEngined`
/sbin/daemon_mgr qLogEngined stop /sbin/qLogEngined
[ -f /sbin/qLogEngined ] && /sbin/daemon_mgr qLogEngined start /sbin/qLogEngined

# restart crond
/etc/init.d/crond.sh restart
/sbin/gen_next_alarm

[ -f /sbin/hd_util ] && /sbin/daemon_mgr hd_util stop "/sbin/hd_util -d"
[ -f /sbin/hd_util ] && /sbin/daemon_mgr hd_util start "/sbin/hd_util &"
}

case "$1" in
    before)
	echo "timechange_notify.sh before"
        if [ "x${NSS_ENABLED}" = "xyes" ]; then
             if [ "`/etc/init.d/nvrd.sh started`" == "1" ]; then
		[ -f /etc/init.d/nvrd.sh ] && /etc/init.d/nvrd.sh _stop
             fi
	else
		[ -f /etc/init.d/nvrd.sh ] && /etc/init.d/nvrd.sh stop
        fi

        ;;
    after)
	echo "timechange_notify.sh after"
	restart_other_services
	if [ `/sbin/getcfg NVR Enable -u -d FALSE` = TRUE ]; then
		/usr/bin/qcmd rmcache
	fi
        if [ "x${NSS_ENABLED}" = "xyes" ]; then
             if [ "`/etc/init.d/nvrd.sh started`" == "1" ]; then
		[ -f /etc/init.d/nvrd.sh ] && /etc/init.d/nvrd.sh _start
             fi
	else
		[ -f /etc/init.d/nvrd.sh ] && /etc/init.d/nvrd.sh start
        fi
        ;;
    *)
	echo "Usage: /etc/init.d/timechange_notify.sh {before|after}"
	exit 1
esac

exit 0
