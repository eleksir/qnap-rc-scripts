#!/bin/sh

GETCFG="/sbin/getcfg"

/bin/touch /etc/hosts
[ $? = 0 ] || echo "/etc/hosts cannot be touched."
echo "127.0.0.1 localhost       localhost" > /etc/hosts

write_hosts()
{
	local ip
	local hostname
	local domainname
	ip=`/sbin/ifconfig $1 | grep "inet addr" | cut -f 2 -d ':' | cut -f 1 -d ' '`
	domainname=`/sbin/getcfg global realm -f /etc/config/smb.conf`
	if [ "x$ip" != "x" ]; then
		hostname=`/bin/hostname`
		if [ "x$domainname" != "x" ]; then
			echo "$ip	${hostname}.${domainname}	$hostname" >> /etc/hosts
		else
			echo "$ip	$hostname	$hostname" >> /etc/hosts
		fi
	fi
}
name=`/sbin/getcfg System "Server Name"`
if /sbin/test $? != 0
then
	/sbin/gen_hostname
	/bin/sync
	name=`/sbin/getcfg System "Server Name"`
fi
hostname $name
/bin/echo $name > /etc/hostname 
NIC_NUM=`/sbin/getcfg "Network" "Interface Number" -d 2`
BOND_SUP=`/sbin/getcfg "Network" "BONDING Support" -d "FALSE"`
ADV_BOND_SUPPORT=`$GETCFG "Network" "Advanced Bonding Support" -d "FALSE"`
ULINUX_UPGRADE=`$GETCFG "Network" "uLinux Upgrade" -d "FALSE"`
if [ "x$ADV_BOND_SUPPORT" = "xTRUE" ] && [ "x$ULINUX_UPGRADE" = "xTRUE" ]; then
  GROUP_NUM=`$GETCFG "Network Group" "Group Count" -d $NIC_NUM`
  for (( i=0; i<$GROUP_NUM; i=i+1 ))
  do
    net_interface=`$GETCFG "Network Group" "Group $i" -d ""`
    if [ "x${net_interface}" != "x" ]; then
      write_hosts ${net_interface}
    fi
  done
else
if [ "$BOND_SUP" = "TRUE" ]; then
	write_hosts bond0
else
	if [ "x${NIC_NUM}" = "x1" ] ;then
		write_hosts eth0
	else
		write_hosts eth0
		write_hosts eth1
	fi
fi
fi
/etc/init.d/smb.sh nmbdrestart &

SNMPD_CONF_TMP=`/bin/mktemp /tmp/snmpd.conf.XXXXXX`
echo "sysName $name" > $SNMPD_CONF_TMP
/bin/sed -e '/^sysName/d' -e '/^sysDescr/d' /etc/config/snmpd.conf >> $SNMPD_CONF_TMP
/bin/mv $SNMPD_CONF_TMP /etc/config/snmpd.conf
if [ "x$1" != "xreset" ]; then
	/usr/bin/killall -HUP snmpd
fi
