#!/bin/sh

/sbin/getcfg "QRAID1" "device name" > /dev/null

result=$?

if [ $result -eq 0 ]
then
	[ -f /sbin/gpiod ] && /bin/kill -USR1 `pidof gpiod`
fi
