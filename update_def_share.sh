#!/bin/sh

DEF_SHARE="/etc/config/def_share.info"
GETCFG="/sbin/getcfg"
SED="/bin/sed"

DOWNLOAD=`${GETCFG} SHARE_DEF defDownload -d Download -f ${DEF_SHARE}`
MULTIMEDIA=`${GETCFG} SHARE_DEF defMultimedia -d Multimedia -f ${DEF_SHARE}`
RECORDINGS=`${GETCFG} SHARE_DEF defRecordings -d Recordings -f ${DEF_SHARE}`
USB=`${GETCFG} SHARE_DEF defUsb -d Usb -f ${DEF_SHARE}`
WEB=`${GETCFG} SHARE_DEF defWeb -d Web -f ${DEF_SHARE}`

${SED} -i "s/@Qrecordings@/${RECORDINGS}/g" /home/httpd/cgi-bin/Qrecordings/help/*.html
${SED} -i "s/@Qmultimedia@/${MULTIMEDIA}/g" /home/httpd/help/inc/app/*.js
${SED} -i "s/@Qmultimedia@/${MULTIMEDIA}/g" /home/httpd/cgi-bin/inc/*.js
[ ! -d /home/httpd/cgi-bin/langs ] || ${SED} -i "s/@Qweb@/${WEB}/g" /home/httpd/cgi-bin/langs/lang_*.js
${SED} -i "s/@Qweb@/${WEB}/g" /home/httpd/cgi-bin/inc/*.js

