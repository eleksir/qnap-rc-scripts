#!/bin/sh
. /etc/init.d/functions
CONFIG=/etc/config/smb.conf
SMBD_LOCKS="/usr/local/samba/var/locks"
GROUP_FILE="/etc/group"
SAMBA_NET="/usr/local/samba/bin/net"
WBINFO="/usr/bin/wbinfo"
# Check that smb.conf exists.
[ -f $CONFIG ] || exit 0
. /etc/init.d/functions
DATA_VOL_PATH=

make_vol_base(){
	FindDefVol
	_ret=$?
	if [ $_ret = 0 ]; then
		DATA_VOL_PATH="${DEF_VOLMP}"
	fi
}
conv_old_msdfs()
{
	if [ -d /etc/config/msdfs_root ]; then
		echo conver MSDFS
		make_vol_base
		if [ "x$DATA_VOL_PATH" != "x" ]; then
			mkdir ${DATA_VOL_PATH}/.@msdfs_root
			cd /etc/config/msdfs_root
			tar cf - * | tar xf - -C ${DATA_VOL_PATH}/.@msdfs_root
			if [ $? = 0 ]; then
				sed -i "s:/etc/config/msdfs_root:${DATA_VOL_PATH}/.@msdfs_root:g" $CONFIG
				if [ $? = 0 ]; then
					cd ~
					rm -rf /etc/config/msdfs_root
				fi
			fi
		fi
	fi
}

_init_smbconf()
{
    if [ -x /sbin/hal_app ]; then
	    /sbin/setcfg global "socket options" "TCP_NODELAY SO_KEEPALIVE" -f ${CONFIG}
    else        
	    SO_SNDBUF=`/sbin/getcfg Samba SO_SNDBUF -d 65536  -f /etc/default_config/uLinux.conf`
	    SO_RCVBUF=`/sbin/getcfg Samba SO_RCVBUF -d 65536  -f /etc/default_config/uLinux.conf`
	    /sbin/setcfg global "socket options" "TCP_NODELAY SO_KEEPALIVE SO_SNDBUF=${SO_SNDBUF} SO_RCVBUF=${SO_RCVBUF}" -f ${CONFIG}
    fi
	#/sbin/setcfg global "passdb backend" smbpasswd -f ${CONFIG}
	/sbin/setcfg global "null passwords" yes -f ${CONFIG}
	/sbin/setcfg global "use sendfile" yes -f ${CONFIG}
	/sbin/setcfg global "oplocks" yes -f ${CONFIG}
	DEAD_TIME=`/sbin/getcfg Samba deadtime -d 10 -f /etc/config/uLinux.conf`
	/sbin/setcfg global "deadtime" "${DEAD_TIME}" -f ${CONFIG}
	/sbin/setcfg global "username level" 0 -f ${CONFIG}
	/sbin/setcfg global "display charset" "UTF8" -f ${CONFIG}
	/sbin/setcfg global "unix extensions" no -f ${CONFIG}
	/sbin/setcfg global "store dos attributes" yes -f ${CONFIG}
	/sbin/setcfg global "client ntlmv2 auth" yes -f ${CONFIG}
	/sbin/setcfg global "dos filetime resolution" no -f ${CONFIG}
	/sbin/setcfg global "inherit acls" yes -f ${CONFIG}
	/sbin/setcfg global "wide links" yes -f ${CONFIG}
	/sbin/setcfg global "force unknown acl user" yes -f ${CONFIG}
	/sbin/setcfg global "template homedir" "/share/homes/DOMAIN=%D/%U" -f ${CONFIG}
	/sbin/setcfg global "domain logons" "no" -f ${CONFIG}
	/sbin/setcfg -e global "config file" -f ${CONFIG}
	/sbin/setcfg -e global "template shell" -f ${CONFIG}
	/sbin/setcfg -e global 'obey pam restrictions' -f ${CONFIG}
	_dos_charset=`/sbin/getcfg global "dos charset" -d none -f ${CONFIG}`
	if [ "x${_dos_charset}" = "xUTF8" ]; then
		/sbin/setcfg -e global 'dos charset' -f ${CONFIG}
	fi
    if [ -x /sbin/hal_app ]; then
        _splice=0
    else
	    _splice=`/sbin/getcfg Samba "SPLICE" -d 1`
    fi
	if [ "x${_splice}" = "x1" ]; then
		/sbin/setcfg global "min receivefile size" 4096 -f ${CONFIG}
	else
		/sbin/setcfg -e global "min receivefile size" -f ${CONFIG}
	fi
	_case_sensitive=`/sbin/getcfg Samba "Case Sensitive" -d auto`
	/sbin/setcfg global "case sensitive" ${_case_sensitive} -f ${CONFIG}
	OS_LEVEL=`/sbin/getcfg global "os level" -d none -f ${CONFIG}`
	if [ "x${OS_LEVEL}" != "x20" ]; then
		/sbin/setcfg global "preferred master" no -f ${CONFIG}
		/sbin/setcfg global "domain master" auto -f ${CONFIG}
		/sbin/setcfg global "local master" no -f ${CONFIG}
		/sbin/setcfg global "os level" 20 -f ${CONFIG}
	fi
	map_dos=`/sbin/getcfg global "map archive" -d yes -f ${CONFIG}`
	if [ "x${map_dos}" = "xyes" ]; then
		/sbin/setcfg global "map archive" no -f ${CONFIG}
		/sbin/setcfg global "map system" no -f ${CONFIG}
		/sbin/setcfg global "map hidden" no -f ${CONFIG}
		/sbin/setcfg global "map read only" no -f ${CONFIG}
	fi

	mode=`/sbin/getcfg global "force directory security mode" -f ${CONFIG}`
	if [ "x${mode}" = "x0777" ]; then
		/sbin/setcfg global "force directory security mode" 0000 -f ${CONFIG}
	fi

	# for Mac OS 10.5 smb catnot upload the file to the empty share
	/sbin/setcfg global "veto files" "/.AppleDB/.AppleDouble/.AppleDesktop/:2eDS_Store/Network Trash Folder/Temporary Items/TheVolumeSettingsFolder/.@__thumb/.@__desc/:2e*/" -f ${CONFIG}

	/usr/bin/readlink /usr/local/samba 2>/dev/null 1>/dev/null
	if [ $? != 0 ] && [ -f /mnt/HDA_ROOT/update_pkg/samba.tgz ]; then
		/etc/init.d/installtgz.sh samba /mnt/HDA_ROOT/update_pkg/samba.tgz
	fi

	if [ `/sbin/getcfg LDAP Enable -u -d FALSE` = TRUE ]; then
		_passwd=`/sbin/getcfg global "passdb backend" -d smbpasswd -f ${CONFIG}`
		if [ "x$_passwd" != "xsmbpasswd" ]; then
			_ldap_host=`/sbin/getcfg LDAP host -d "127.0.0.1"`
			_ssl=`/sbin/getcfg LDAP ssl -d 0`
			if [ "x$_ssl" = "x1" ]; then    # ssl
				/sbin/setcfg global "passdb backend" "ldapsam:ldaps://$_ldap_host" -f ${CONFIG}
				/sbin/setcfg global "ldap ssl" "off" -f ${CONFIG}
			elif [ "x$_ssl" = "x2" ]; then  # tls
				/sbin/setcfg global "passdb backend" "ldapsam:ldap://$_ldap_host" -f ${CONFIG}
				/sbin/setcfg global "ldap ssl" "start tls" -f ${CONFIG}
			else
				/sbin/setcfg global "passdb backend" "ldapsam:ldap://$_ldap_host" -f ${CONFIG}
				/sbin/setcfg global "ldap ssl" "off" -f ${CONFIG}
			fi
			# set WORKGROUP if backend LDAP is QNAP NAS
			# LDAP Server Type = 0 (Remote QNAP NAS)
			# LDAP Server Type = 1 (Local QNAP NAS)
			# LDAP Server Type = 2 (Remote LDAP Server)
			if [ `/sbin/getcfg LDAP "LDAP Server Type" -u -d 2` != "2" ]; then
				base_dn=`/sbin/getcfg global "ldap suffix" -f ${CONFIG}`
				dc=${base_dn#*dc=}
				workgroup=${dc%%,*}
				/sbin/setcfg global workgroup "$workgroup" -f ${CONFIG}
			fi
			/sbin/setcfg global "domain logons" "yes" -f ${CONFIG}
		fi
	else
		/sbin/setcfg global "passdb backend" smbpasswd -f ${CONFIG}
	fi

	## QNAP private flag for enhance ACL. please notice switch this flag will break samba acl hash.
	if [ x`/sbin/getcfg Samba "Win ACL" -u -d FALSE` = "xFALSE" ]; then
		enhance_acl_v1=`/sbin/getcfg global "enhance acl v1" -d null -f ${CONFIG}`
		if [ "x$enhance_acl_v1" != "xno" ]; then
			/sbin/setcfg global "enhance acl v1" yes -f ${CONFIG}
		fi
	fi

	## QNAP private flag for replacing Everyone to everyone or remove it if need.
	if [ x`/sbin/getcfg System "ACL Enable" -u -d FALSE` = "xFALSE" ]; then
		/sbin/setcfg global "remove everyone" no -f ${CONFIG}
	else
		/sbin/setcfg global "remove everyone" yes -f ${CONFIG}
	fi

	offline_files=`/sbin/getcfg Samba "offline_files" -d TRUE`
	if [ "x${offline_files}" == "xTRUE" ]; then
		/sbin/setcfg global 'kernel oplocks' no -f ${CONFIG}
	else
		/sbin/setcfg global 'kernel oplocks' yes -f ${CONFIG}
	fi

	mangled_names=`/sbin/getcfg Samba "mangled_names" -d FALSE`
	if [ "x${mangled_names}" == "xFALSE" ]; then
		/sbin/setcfg global 'mangled names' no -f ${CONFIG}
	else
		/sbin/setcfg global 'mangled names' yes -f ${CONFIG}
	fi
	[ `/sbin/getcfg global "printcap cache time" -d "1" -f ${CONFIG}` == "0" ] || /sbin/setcfg global "printcap cache time" "0" -f ${CONFIG}

}


_init_group()
{
	
	_passwd=`/sbin/getcfg global "passdb backend" -d smbpasswd -f ${CONFIG}`
	if [ "x$_passwd" != "xsmbpasswd" ]; then
		return
	fi
	HIDDEN_GROUP="dovecot guest xmail"
	/bin/cat $GROUP_FILE | while read line 
	do
		group_name=${line%%:*}
		hidden_flag=0
		for hidden in $HIDDEN_GROUP
		do
			if [ "$group_name" = "$hidden" ]; then
				hidden_flag=1
				break
			fi
		done
		if [ "$hidden_flag" = 0 -a -x ${SAMBA_NET} ]; then
			${SAMBA_NET} groupmap add unixgroup=$group_name > /dev/null 2>&1
		fi
	done
}

_del_group()
{
	/bin/cat $GROUP_FILE | while read line 
	do
		group_name=${line%%:*}
		if [ -x ${SAMBA_NET} ]; then
			${SAMBA_NET} groupmap delete ntgroup=$group_name > /dev/null 2>&1
		fi
	done
}

_init_home()
{
	home_func=`/sbin/getcfg Samba HomeLink -u -d FALSE`
	local acl_list="\"%u\""
	winbind_separator=`/sbin/getcfg global "winbind separator" -d '\\' -f ${CONFIG}`
	homes_path=`/sbin/getcfg homes path -d FALSE -f ${CONFIG}`

	if [ $home_func == "FALSE" -o $homes_path == "FALSE" ]; then
		#echo "home folder is disabled"
		return
	fi

	if [ ! -e "$homes_path" ]; then
		#echo "homes is lost"
		return
	fi

	/sbin/setcfg home "comment" Home -f ${CONFIG}
	/sbin/setcfg home "path" %H -f ${CONFIG}
	/sbin/setcfg home "browsable" yes -f ${CONFIG}
	/sbin/setcfg home "oplocks" yes -f ${CONFIG}
	/sbin/setcfg home "ftp write only" no -f ${CONFIG}
	/sbin/setcfg home "inherit permissions" yes -f ${CONFIG}
	/sbin/setcfg home "writable" yes -f ${CONFIG}
	/sbin/setcfg home "read list" $acl_list -f ${CONFIG}
	/sbin/setcfg home "write list" $acl_list -f ${CONFIG}
	/sbin/setcfg home "valid users" $acl_list -f ${CONFIG}
	/sbin/setcfg home "root preexec" "/sbin/create_home -u '%q'" -f ${CONFIG}
}

_del_home()
{
	home_path=`/sbin/getcfg home "path" -d home -f ${CONFIG}`
	if [ "$home_path" == "%H" ]; then
		/sbin/rmcfg home -f ${CONFIG}
	fi
}

# move samba locks directory to /mnt/HDA_ROOT/.locks
_locks()
{
	if [ -d /mnt/HDA_ROOT/.locks ]; then
		/bin/rm -rf /mnt/HDA_ROOT/.locks
		/bin/rm $SMBD_LOCKS
	fi
	volume_test=`/sbin/getcfg Public path -f /etc/smb.conf | cut -d '/' -f 3`
	[ "x${volume_test}" = "x" ] || volume=${volume_test}
	_lock_real_path=/share/${volume}/.locks
	if [ ! -d ${_lock_real_path} ]; then
		echo "samba locks directory is not exist, create it"
		if [ ! -d /share/${volume} ]; then
			/bin/mkdir -p /share/${volume}
			/bin/chmod 777 /share/${volume}
		fi
		/bin/mkdir ${_lock_real_path}
		/bin/rm -rf $SMBD_LOCKS
		/bin/ln -sf ${_lock_real_path} $SMBD_LOCKS
	else
		/usr/bin/readlink $SMBD_LOCKS 2>/dev/null 1>/dev/null
		if [ $? != 0 ]; then
			# not symbolic link
			/bin/rm -rf $SMBD_LOCKS
			/bin/ln -sf ${_lock_real_path} $SMBD_LOCKS
		else
			# symbolic link
			ret=`/usr/bin/readlink $SMBD_LOCKS`
			if [ "x${ret}" != "x${_lock_real_path}" ]; then
				/bin/rm -f $SMBD_LOCKS
				/bin/ln -sf ${_lock_real_path} $SMBD_LOCKS
			fi
		fi
	fi
	/bin/mkdir ${_lock_real_path}/.test_locks 2>/dev/null 1>/dev/null
	if [ $? != 0 ]; then
		_lock_real_path=/mnt/HDA_ROOT/.locks
		/bin/mkdir ${_lock_real_path}
		/bin/rm -f $SMBD_LOCKS
		/bin/ln -sf ${_lock_real_path} $SMBD_LOCKS
		/bin/mkdir ${_lock_real_path}/.test_locks 2>/dev/null 1>/dev/null
		if [ $? != 0 ]; then
			_lock_real_path=/var/.locks
			/bin/mkdir ${_lock_real_path} 2>/dev/null 1>/dev/null
			/bin/rm -f $SMBD_LOCKS
			/bin/ln -sf ${_lock_real_path} $SMBD_LOCKS
		else
			/bin/rmdir ${_lock_real_path}/.test_locks
		fi
	else
		/bin/rmdir ${_lock_real_path}/.test_locks
	fi
	/bin/echo "locks path was set to ${_lock_real_path}"
}

# See how we were called.
case "$1" in
  start)
  conv_old_msdfs
  test -f /usr/local/samba/sbin/smbd || exit 0
	if [ `/sbin/getcfg Samba Enable -u -d TRUE` = FALSE ]
	then
	    _del_home
	    _init_home
	    echo "Starting SMB Services: disabled."
	    exit 0
	fi
	if [ x`/sbin/getcfg Samba "Win ACL" -u -d FALSE` = xTRUE ]; then
		[ ! -f /usr/local/samba/lib/acl_xattr.so.disable ] || /bin/mv /usr/local/samba/lib/acl_xattr.so.disable /usr/local/samba/lib/acl_xattr.so 2>>/dev/null
	else
		[ ! -f /usr/local/samba/lib/acl_xattr.so ] || /bin/mv /usr/local/samba/lib/acl_xattr.so /usr/local/samba/lib/acl_xattr.so.disable 2>>/dev/null
	fi
	_init_smbconf
	_locks
	_init_group
	_del_home
	/etc/init.d/winbind start
	_init_home
	echo -n "Starting SMB services:"
	/sbin/daemon_mgr smbd start "/usr/local/samba/sbin/smbd -l /var/log -D -s $CONFIG"
	if [ "$?" = "0" ]; then echo -n " smbd"; fi
	/sbin/daemon_mgr nmbd start "/usr/local/samba/sbin/nmbd -l /var/log -D -s $CONFIG"
	if [ "$?" = "0" ]; then echo -n " nmbd"; fi
	echo "."
	touch /var/lock/subsys/smb
	;;
  stop)
	echo -n "Shutting down SMB services:"

	## we have to get all the smbd process here instead of just the
	## main parent (i.e. killproc) because it can take a long time
	## for an individual process to process a TERM signal
	smbdpids=`pidof smbd`
	/sbin/daemon_mgr smbd stop /usr/local/samba/sbin/smbd
	for pid in $smbdpids; do
		/bin/kill -TERM $pid 2>/dev/null 1>/dev/null
	done
	echo -n " smbd"
	nmbdpids=`pidof nmbd`
	/sbin/daemon_mgr nmbd stop /usr/local/samba/sbin/nmbd
	for pid in $nmbdpids; do
		/bin/kill -TERM $pid 2>/dev/null 1>/dev/null
	done
	echo " nmbd."
	/bin/rm -f /var/lock/subsys/smb
	/bin/rm -f $SMBD_LOCKS/smbd.pid $SMBD_LOCKS/nmbd.pid
	/etc/init.d/winbind stop
	_del_group
	;;
  nmbdrestart)
	test -f /usr/local/samba/sbin/smbd || exit 0
	if [ `/sbin/getcfg Samba Enable -u -d TRUE` = FALSE ]
	then
		echo "Starting nmbd Services: disabled."
		exit 0
	fi
        echo -n "Restarting nmbd services:"
	nmbdpids=`pidof nmbd`
	/sbin/daemon_mgr nmbd stop /usr/local/samba/sbin/nmbd
	for pid in $nmbdpids; do
		/bin/kill -TERM $pid 2>/dev/null 1>/dev/null
	done
	/bin/rm -f $SMBD_LOCKS/nmbd.pid
        /sbin/daemon_mgr nmbd start "/usr/local/samba/sbin/nmbd -l /var/log -D -s $CONFIG"
        if [ "$?" = "0" ]; then echo -n " nmbd"; fi
        echo "."

	echo "done."
	;;
  restart)
	echo "Restarting SMB services:"
	$0 stop
	$0 start
	echo "done."
	;;
  *)
	echo "Usage: smb {start|stop|restart}"
	exit 1
esac

