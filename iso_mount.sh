#!/bin/sh
MOUNT_DVD="/bin/mount -t iso9660 -o ro,loop"
MOUNT_BD="/bin/mount -t udf -o ro,loop"
UMOUNT="/bin/umount"
LOSETUP="/usr/local/sbin/losetup"
ISO_CONF="/etc/config/iso_share_mapping"
MAX_NUM=256

mount_iso()
{
	for (( i=1; i<=$MAX_NUM; i=i+1 ))
	do
		/bin/grep "\[ISOShare$i\]" /etc/config/iso_share_mapping 1>/dev/null 2>/dev/null
		[ $? = 0 ] || continue
		#echo "Mount ISOShare$i"
		auto_mount=`/sbin/getcfg ISOShare$i Auto_mount -d TRUE -f $ISO_CONF`
		if [ "$auto_mount" == "TRUE" ]; then
			source=`/sbin/getcfg ISOShare$i Source -d none -f $ISO_CONF`
			if [ "$source" != "none" ]; then
				dest=`/sbin/getcfg ISOShare$i Dest -d none -f $ISO_CONF`
				type=`/sbin/getcfg ISOShare$i Type -d none -f $ISO_CONF`
				if [ "$type" == "iso9660" ] && [ "$dest" != "none" ]; then
					/bin/df | /bin/grep  "$dest" 1>/dev/null 2>/dev/null
					if [ $? != 0 ]; then
						$MOUNT_DVD "$source" "$dest" 2>/dev/null
					fi
#					if [ $? != 0 ]; then
#						/sbin/iso_check -d "$dest" 1>/dev/null 2>/dev/null
#					fi
				elif [ "$type" == "udf" ] && [ "$dest" != "none" ]; then
					/bin/df | /bin/grep  "$dest" 1>/dev/null 2>/dev/null
					if [ $? != 0 ]; then
						$MOUNT_BD "$source" "$dest" 2>/dev/null
					fi
#					if [ $? != 0 ]; then
#						/sbin/iso_check -d "$dest" 1>/dev/null 2>/dev/null
#					fi
				fi
			fi
		fi
	done
}

unmount_iso()
{
	for ((  i=1; i<=$MAX_NUM; i=i+1 ))
	do
		/bin/grep "\[ISOShare$i\]" /etc/config/iso_share_mapping 1>/dev/null 2>/dev/null
		[ $? = 0 ] || continue
		#echo "Unmount ISOShare$i"
		source=`/sbin/getcfg ISOShare$i Source -d none -f $ISO_CONF`
		if [ "$source" != "none" ]; then
			dest=`/sbin/getcfg ISOShare$i Dest -d none -f $ISO_CONF`
			loop_dev=`/bin/cat /proc/mounts | /bin/grep loop | /bin/grep "${dest}" | /bin/awk '{print $1}'`
			$UMOUNT "$dest" 2>/dev/null
			$LOSETUP -d "$loop_dev" 2>/dev/null
		fi

	done
}

case "$1" in
	start)
		mount_iso
		;;
	stop)
		/bin/grep "/dev/loop" /proc/mounts 1>/dev/null 2>&1
		[ $? != 0 ] || unmount_iso
		;;
	restart)
		$0 stop
		$0 start
		;;
	*)
		/bin/echo 	"Usage: /etc/init.d/iso_mount.sh {start|stop|restart}"
		exit 1
esac
