#!/bin/sh

RETVAL=0
case "$1" in
  start)
	mypid=`/bin/pidof ntpd`
	if [ ! -z $mypid ]; then
		echo "NTP server has been started."
		exit 1
	else
		#check if it is necessary to startup NTP server : ntpd
		if [ `/sbin/getcfg NTP "Use NTP Server" -u -d FALSE` = TRUE ]; then
			if [ `/sbin/getcfg NTP "Enable NTP Server" -u -d FALSE` = TRUE ]; then
				if [ -f /etc/config/ntp.conf ]; then
					echo "Remove the old ntp.conf"
					rm -f /etc/config/ntp.conf
				fi
				echo "Create the new ntp.conf"
				echo > /etc/config/ntp.conf
				srvip1=`/sbin/getcfg NTP "Server1 IP" -d ""`
				if [ "$srvip1" != "" ]; then
					echo "server $srvip1" >> /etc/config/ntp.conf
					/usr/sbin/ntpdate -q $srvip1 2>/dev/null 1>/dev/null
					srvip1ok=$?
				fi
				srvip2=`/sbin/getcfg NTP "Server2 IP" -d ""`
				if [ "$srvip2" != "" ]; then
					echo "server $srvip2" >> /etc/config/ntp.conf
					/usr/sbin/ntpdate -q $srvip2 2>/dev/null 1>/dev/null
					srvip2ok=$?
				fi
				srvip3=`/sbin/getcfg NTP "Server3 IP" -d ""`
				if [ "$srvip3" != "" ]; then
					echo "server $srvip3" >> /etc/config/ntp.conf
					/usr/sbin/ntpdate -q $srvip3 2>/dev/null 1>/dev/null
					srvip3ok=$?
				fi
				if [ "$srvip1" == "" -a "$srvip2" == "" -a "$srvip3" == "" ] || 
				   [ "$srvip1ok" != "0" -a "$srvip2ok" != "0" -a "$srvip3ok" != "0" ]; then
					echo > /etc/config/ntp.conf
					echo "server 127.127.1.0" >> /etc/config/ntp.conf
					echo "Error: No available IP address for time synchronization. Use local system clock as reference clock."
					/sbin/write_log "There is no available IP address for time synchronization. System will use the local system clock as the reference clock." 2
				fi
				echo "driftfile /etc/ntp.drift" >> /etc/config/ntp.conf
			        echo "Starting NTP server: "
				/sbin/daemon_mgr ntpd start "/usr/sbin/ntpd -c /etc/config/ntp.conf"
			        RETVAL=$?
			fi
		fi
	fi 
        ;;
  stop)
        echo "Shutting down NTP server: "
	/sbin/daemon_mgr ntpd stop "/usr/sbin/ntpd"
	RETVAL=$?
        ;;
  restart)
        $0 stop
        $0 start
        RETVAL=$?
        ;;
  *)
        echo "Usage: $0 {start|stop|restart}"
        exit 1
esac

exit $RETVAL
