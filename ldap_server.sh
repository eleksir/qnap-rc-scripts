#!/bin/sh
RETVAL=0
STUNNEL_KEY=/etc/config/stunnel/backup.key
STUNNEL_CERT=/etc/config/stunnel/backup.cert
STUNNEL_KEY_DEFAULT=/etc/default_config/stunnel/backup.key
STUNNEL_CERT_DEFAULT=/etc/default_config/stunnel/backup.cert
QPKG_LDAP_DIR=/mnt/ext/opt/ldap_server
LDAP_DB_DIR=""
. /etc/init.d/functions

prepare_cert_key()
{
	if [ ! -f $STUNNEL_CERT ] && [ -f $STUNNEL_CERT_DEFAULT ]; then
		/bin/cp $STUNNEL_CERT_DEFAULT $STUNNEL_CERT 2>>/dev/null
	fi
	
	if [ ! -f $STUNNEL_KEY ] && [ -f $STUNNEL_KEY_DEFAULT ]; then
		/bin/cp $STUNNEL_KEY_DEFAULT $STUNNEL_KEY 2>>/dev/null
	fi
}

find_db_base(){
	FindDefVol
	_ret=$?
	if [ $_ret != 0 ]; then
		exit 1
	fi
	LDAP_DB_DIR="${DEF_VOLMP}/.ldapdb"
}

init_ldap_conf()
{
        if [ -f ${QPKG_LDAP_DIR}/etc/openldap/slapd.conf ];then
                [ -d /etc/config/openldap ] || /bin/mkdir -p /etc/config/openldap
                [ -f /etc/config/openldap/slapd.conf ] || /bin/cp ${QPKG_LDAP_DIR}/etc/openldap/slapd.conf /etc/config/openldap
                /bin/cp ${QPKG_LDAP_DIR}/etc/openldap/slapdacl.conf /etc/config/openldap
                [ -L /etc/openldap/slapd.conf ] || /bin/ln -sf /etc/config/openldap/slapd.conf /etc/openldap/slapd.conf
                [ -L /etc/openldap/slapdacl.conf ] || /bin/ln -sf /etc/config/openldap/slapdacl.conf /etc/openldap/slapdacl.conf
        fi
}

init_ntp_conf()
{
	if [ -f ${QPKG_LDAP_DIR}/etc/ntp/ntp.conf ];then
		[ -d /etc/config/ntp ] || /bin/mkdir -p /etc/config/ntp
		[ -f /etc/config/ntp/ntp.conf ] || /bin/cp ${QPKG_LDAP_DIR}/etc/ntp/ntp.conf /etc/config/ntp
		[ -L /etc/ntp.conf ] || /bin/ln -sf /etc/config/ntp/ntp.conf /etc/ntp.conf
	fi
}

init_db_dir()
{
	[ -d ${LDAP_DB_DIR} ] || /bin/mkdir -p $LDAP_DB_DIR
	cp /mnt/ext/opt/ldap_server/var/openldap-data/DB_CONFIG $LDAP_DB_DIR
}


case "$1" in
  start)
        echo "Starting LDAP server service: "
	find_db_base
	[ -f ${LDAP_DB_DIR}/DB_CONFIG ] || init_db_dir
	if [ -L /var/openldap-data ]; then
		if [ ! -e /var/openldap-data ]; then
			/bin/rm -f /var/openldap-data
			/bin/ln -sf ${LDAP_DB_DIR} /var/openldap-data
		fi
	else
		/bin/ln -sf ${LDAP_DB_DIR} /var/openldap-data
	fi
	prepare_cert_key

	if [ `/sbin/getcfg "LDAP Server" "Enable" -u -d FALSE` = TRUE ]; then
		if [ -f /usr/sbin/slapd ]; then
			/sbin/daemon_mgr slapd start '/usr/sbin/slapd -4 -h "ldap:/// ldaps:///"'
		fi
	        RETVAL=$?	
		echo "slapd."
        	
		echo "Starting NTP server service: "
		if [ -f /sbin/ntpd ]; then
			if [ ! -f /etc/ntp.drift ]; then
				echo "0.000" > /etc/ntp.drift
			fi
			/sbin/daemon_mgr ntpd start "/sbin/ntpd"
		fi
		echo "ntpd."
	else
		echo "slapd is disabled."
	fi
        ;;
  stop)
        echo "Shutting down LDAP service: "
	/sbin/daemon_mgr slapd stop "/usr/sbin/slapd"

	/bin/kill -INT `pidof slapd` 1>/dev/null 2>/dev/null
	echo "slapd."
	
        echo "Shutting down NTP service: "
	/sbin/daemon_mgr ntpd stop "/sbin/ntpd"

	/bin/kill -INT `pidof ntpd` 1>/dev/null 2>/dev/null
	echo "ntpd."
        ;;
  init)
	echo "Initailize ldap server config file"
	init_ldap_conf
	echo "Initailize ntp config file"
	init_ntp_conf
	;;
  restart)
        $0 stop
        $0 start
        RETVAL=$?
        ;;
  *)
        echo "Usage: $0 {start|stop|restart}"
        exit 1
esac

exit $RETVAL

