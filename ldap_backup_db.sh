#!/bin/sh

EXPORT_DB_PRFIX="LDAP_Backup"
TAR_CMD=/bin/tar
LDAP_BK_CONF_FILE="/var/openldap-data/.ldaps_conf"
EXPORT_DEST=
BACKUP_DB_FILE="DB_CONFIG __db.* *.bdb alock log.*"
BACKUP_DEST="`/sbin/getcfg "LDAP Server" "Backup destination" -d ""`"
APPEND_DATE_STR="`date --date="now" +%Y_%m_%d`"
PWD=`pwd`

LDAP_ENABLE="`/sbin/getcfg "LDAP Server" "Enable" -u -d FALSE`"
LDAP_FQDN="`/sbin/getcfg "LDAP Server" "fqdn" -d ""`"
LDAP_ROOTDN="`/sbin/getcfg "LDAP Server" "rootdn" -d ""`"
LDAP_BASDN="`/sbin/getcfg "LDAP Server" "base" -d ""`"
LDAP_ROOTPW="`/sbin/getcfg "LDAP Server" "rootpw" -d ""`"

set_ldap_bk_conf_file()
{
	LDAP_ENABLE="`/sbin/getcfg "LDAP Server" "Enable" -u -d FALSE`"
	LDAP_FQDN="`/sbin/getcfg "LDAP Server" "fqdn" -d ""`"
	LDAP_ROOTDN="`/sbin/getcfg "LDAP Server" "rootdn" -d ""`"
	LDAP_BASEDN="`/sbin/getcfg "LDAP Server" "base" -d ""`"
	LDAP_ROOTPW="`/sbin/getcfg "LDAP Server" "rootpw" -d ""`"
	/bin/touch ${LDAP_BK_CONF_FILE}
	/sbin/setcfg "LDAP Server" "Enable" "${LDAP_ENABLE}" -f ${LDAP_BK_CONF_FILE}
	/sbin/setcfg "LDAP Server" "fqdn" "${LDAP_FQDN}" -f ${LDAP_BK_CONF_FILE}
	/sbin/setcfg "LDAP Server" "base" "${LDAP_BASEDN}" -f ${LDAP_BK_CONF_FILE}
	/sbin/setcfg "LDAP Server" "rootdn" "${LDAP_ROOTDN}" -f ${LDAP_BK_CONF_FILE}
	/sbin/setcfg "LDAP Server" "rootpw" "${LDAP_ROOTPW}" -f ${LDAP_BK_CONF_FILE}
}

if [ `/sbin/getcfg "LDAP Server" "Export DB" -u -d FALSE` = TRUE ]; then
	set_ldap_bk_conf_file		
	if [ `/sbin/getcfg "LDAP Server" "Overwrite" -u -d FALSE` = TRUE ]; then
		cd /var/openldap-data
		echo "overwrite backup db file, Backup destination=>${BACKUP_DEST}"
		$TAR_CMD -czvf ${EXPORT_DB_PRFIX}.exp .ldaps_conf $BACKUP_DB_FILE
		/bin/mv ${EXPORT_DB_PRFIX}.exp ${BACKUP_DEST}
		cd $PWD
	elif [ `/sbin/getcfg "LDAP Server" "Create new file" -u -d FALSE` = TRUE ]; then
		cd /var/openldap-data
		echo "create new backup db file, Backup destination=>${BACKUP_DEST}"
		$TAR_CMD -czvf ${EXPORT_DB_PRFIX}_${APPEND_DATE_STR}.exp $BACKUP_DB_FILE .ldaps_conf 
		/bin/mv ${EXPORT_DB_PRFIX}_${APPEND_DATE_STR}.exp ${BACKUP_DEST}
		cd $PWD
	fi
	/bin/rm ${LDAP_BK_CONF_FILE}
fi
