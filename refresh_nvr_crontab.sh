#!/bin/sh

LOG_CHK_NOTIFY_FILE=/tmp/.log_chk_notify

if [ "`/sbin/getcfg -f /etc/default_config/uLinux.conf NVR Support -d FALSE`" == "TRUE" ]; then
    SUPPORT_NVR=yes
else
    SUPPORT_NVR=no
fi
if [ "`/sbin/getcfg -f /etc/config/uLinux.conf NVR Enable -d FALSE`" == "TRUE" ]; then
    NVR_ENABLED=yes
else
    NVR_ENABLED=
fi

if [ "$SUPPORT_NVR" == "yes" -a "$NVR_ENABLED" == "yes" -a "`/usr/bin/qcmd recfg`" == "1" ]; then
	if [ x"`/bin/cat /etc/config/crontab | /bin/grep "logChecker"`" = x ]; then
		echo "*/60 * * * * /usr/bin/logChecker" >> /etc/config/crontab
	fi
	if [ x"`/bin/cat /etc/config/crontab | /bin/grep "DUSG"`" = x ]; then
		echo "0-59/15 * * * * /usr/bin/qcmd DUSG" >> /etc/config/crontab
	fi
	if [ x"`/bin/cat /etc/config/crontab | /bin/grep rec_report`" != x"5 * * * * /usr/bin/qcmd rec_report" ]; then
		/bin/sed -i '/qcmd rec_report/d' /etc/config/crontab
		echo "5 * * * * /usr/bin/qcmd rec_report" >> /etc/config/crontab
	fi
	/bin/touch $LOG_CHK_NOTIFY_FILE
else
	/bin/sed -i '/qcmd/d' /etc/config/crontab
	/bin/sed -i '/logChecker/d' /etc/config/crontab
	/bin/rm -rf $LOG_CHK_NOTIFY_FILE
fi
/usr/bin/crontab /etc/config/crontab
