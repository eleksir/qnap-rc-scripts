#!/bin/sh
#
# MediaServer Control File written by Itzchak Rehberg
# Modified for fedora/redhat by Landon Bradshaw <phazeforward@gmail.com>
# Adapted to TwonkyMedia 3.0 by TwonkyVision GmbH
# Adapted to TwonkyMedia 4.0 by TwonkyVision GmbH
#
# This script is intended for SuSE and Fedora systems. Please report
# problems and suggestions at http://www.twonkyvision.de/mantis/
#
#
###############################################################################
#
### BEGIN INIT INFO
# Provides:       twonkymedia
# Required-Start: $network $remote_fs
# Default-Start:  3 5
# Default-Stop:   0 1 2 6
# Description:    TwonkyVision UPnP server
### END INIT INFO
#
# Comments to support chkconfig on RedHat/Fedora Linux
# chkconfig: 345 71 29
# description: TwonkyVision UPnP server
#
#==================================================================[ Setup ]===

WORKDIR1="/mnt/HDA_ROOT/twonkymedia"
WORKDIR2="`dirname $0`"
DAEMON=twonkymedia
PIDFILE=/var/run/mediaserver.pid
MNT_POINT="/mnt/ext"
TWONKY_SOURCE="${MNT_POINT}/opt/source/twonkymedia.tar.gz"
ROOT_PART="/mnt/HDA_ROOT"
UPDATEPKG_DIR="${ROOT_PART}/update_pkg"
MULTIMEDIA_SHARE=`/sbin/getcfg SHARE_DEF defMultimedia -d Qmultimedia -f /etc/config/def_share.info`
BOND_SUP=`/sbin/getcfg "Network" "BONDING Support" -d "FALSE"`
NIC_NUM=`/sbin/getcfg "Network" "Interface Number" -d "FALSE"`
APP_RUNTIME_CONF="/var/.application.conf"
TwonkyMedia_Enable=`/sbin/getcfg TwonkyMedia Enable -u -d FALSE`
RETVAL=0

#=================================================================[ Script ]===
clean_twonky_cache()
{
	if [ -d /mnt/HDA_ROOT/twonkymedia/twonkymedia.db/cache ]; then
		/bin/rm -rf /mnt/HDA_ROOT/twonkymedia/twonkymedia.db/cache/*
		[ $? = 0 ] && echo "TwonkyMedia cache have been cleaned."
	fi
        if [ x"`/bin/cat /etc/config/crontab | /bin/grep "twonkymedia"`" = x ]; then
                echo "0 3 * * * /bin/rm -rf /mnt/HDA_ROOT/twonkymedia/twonkymedia.db/cache/*" >> /etc/config/crontab
                /usr/bin/crontab /etc/config/crontab
        fi
}

move_db_path_to_hdd()
{
	volume_test=`/sbin/getcfg ${MULTIMEDIA_SHARE} path -f /etc/smb.conf | cut -d '/' -f 3`
	[ "x${volume_test}" = "x" ] || volume=${volume_test}
	if [ ! -d "/share/${volume}/${MULTIMEDIA_SHARE}" ]; then
		/bin/echo "db can't be moved to HDD"
		/bin/echo "Default share ${MULTIMEDIA_SHARE} is not found."
		#/sbin/write_log "Default share ${MULTIMEDIA_SHARE} is not found. TwonkyMedia start failed." 2
		exit 1
	fi
	[ -d /share/${volume}/.@twonkymedia.db ] || /bin/mkdir /share/${volume}/.@twonkymedia.db
	twonkydb=`/usr/bin/readlink "/mnt/HDA_ROOT/twonkymedia/twonkymedia.db" 2>>/dev/null`
	if [ -z $twonkydb ] || [ "$twonkydb" != "/share/${volume}/.@twonkymedia.db" ]; then
		/bin/rm -rf /mnt/HDA_ROOT/twonkymedia/twonkymedia.db
		/bin/ln -sf /share/${volume}/.@twonkymedia.db /mnt/HDA_ROOT/twonkymedia/twonkymedia.db
	fi
	[ ! -f "${WORKDIR}/twonkymediaserver6-log.txt" ] || /bin/rm -f "${WORKDIR}/twonkymediaserver6-log.txt"
	[ -f /share/${volume}/.@twonkymedia.db/twonkymediaserver-log.txt ] || /bin/touch /share/${volume}/.@twonkymedia.db/twonkymediaserver-log.txt
	/bin/ln -sf /share/${volume}/.@twonkymedia.db/twonkymediaserver-log.txt "${WORKDIR}/twonkymediaserver6-log.txt"
	logtxt=`/usr/bin/readlink "/mnt/HDA_ROOT/twonkymedia/twonkymediaserver-log.txt" 2>>/dev/null`
	if [ "x$logtxt" != "x/share/${volume}/.@twonkymedia.db/twonkymediaserver-log.txt" ]; then
		/bin/rm -rf /mnt/HDA_ROOT/twonkymedia/twonkymediaserver-log.txt
		/bin/ln -sf /share/${volume}/.@twonkymedia.db/twonkymediaserver-log.txt /mnt/HDA_ROOT/twonkymedia/twonkymediaserver-log.txt
	fi

	# twonky 5.0 default config is /var/twonkymedia
	if [ -d "/var/twonkymedia" ]; then
		/bin/rm -rf /var/twonkymedia
	fi
	/usr/bin/readlink /var/twonkymedia 2>>/dev/null
	if [ $? = 0 ]; then
		/bin/rm /var/twonkymedia
	fi
	/bin/ln -sf /share/${volume}/.@twonkymedia.db /var/twonkymedia
	/bin/rm -rf /share/twonkymedia 2>>/dev/null
	/bin/ln -sf /share/${volume}/.@twonkymedia.db /share/twonkymedia
}

check_twonky_qpkg()
{
	QPKG_Twonky_Ver=`/sbin/getcfg TwonkyMedia Version -d "0.0" -f /etc/config/qpkg.conf`
	QPKG_Twonky_Enable=`/sbin/getcfg TwonkyMedia ENABLE -u -d "FALSE" -f /etc/config/qpkg.conf`
	QPKG_Twonky_Shell=`/sbin/getcfg TwonkyMedia Shell -d "null" -f /etc/config/qpkg.conf`
	if [ "x$QPKG_Twonky_Enable" = "xTRUE" ] && [ -x "${QPKG_Twonky_Shell}" ]; then
		${QPKG_Twonky_Shell} stop 1>>/dev/null 2>>/dev/null
		/sbin/setcfg TwonkyMedia ENABLE FALSE -f /etc/config/qpkg.conf
	fi
}

# Source function library.
if [ -f /etc/rc.status ]; then
  # SUSE
  . /etc/rc.status
  rc_reset
else
  # Reset commands if not available
  rc_status() {
    case "$1" in
	-v)
	    true
	    ;;
	*)
	    false
	    ;;
    esac
    echo
  }
  alias rc_exit=exit
fi

#QNAP
	if [ -f ${UPDATEPKG_DIR}/twonkymedia.tar.gz ]; then
		[ ! -f $TWONKY_SOURCE ] || /bin/rm -f $TWONKY_SOURCE
		TWONKY_SOURCE="${UPDATEPKG_DIR}/twonkymedia.tar.gz"
	fi			
	if [ -f $TWONKY_SOURCE ] && [ ! -d ${MNT_POINT}/opt/twonkymedia ]; then
		/bin/mkdir ${MNT_POINT}/opt/twonkymedia
		/bin/rm -f ${WORKDIR1} 1>>/dev/null 2>>/dev/null
		/bin/ln -sf ${MNT_POINT}/opt/twonkymedia ${WORKDIR1}
		#/bin/cp $TWONKY_SOURCE $WORKDIR1
		#/bin/gunzip $WORKDIR1/twonkymedia.tar.gz
		/bin/tar -zxf $TWONKY_SOURCE -C $WORKDIR1
		#/bin/rm $WORKDIR1/twonkymedia.tar
		/bin/sync
	fi
	[ -d /rpc ] || /bin/mkdir /rpc
#END

if [ -x "$WORKDIR1" ]; then
WORKDIR="$WORKDIR1"
else
WORKDIR="$WORKDIR2"
fi

if [ ! -f "${WORKDIR}/${DAEMON}" ]; then
    DAEMON=twonkymusic
fi
TWONKYSRV="${WORKDIR}/${DAEMON}"
INIFILE="/etc/config/twonkyvision-mediaserver6.ini"
DEFAULT_INIFILE="${WORKDIR}/twonkymedia6-default.ini"
DEFAULT_SERVER_INIFILE="${WORKDIR}/twonkymedia-server-default.ini"
LOG_FILE="${WORKDIR}/twonkymediaserver6-log.txt"
TwonkyMedia_Hide=`/sbin/getcfg DISABLE upnp -d 0 -f $APP_RUNTIME_CONF`
if [ "x$TwonkyMedia_Hide" = "x1" ]; then
	/sbin/setcfg TwonkyMedia Enable FALSE
	TwonkyMedia_Enable="FALSE"
fi

cd $WORKDIR

case "$1" in
  start)
	if [ "x$TwonkyMedia_Enable" != "xTRUE" ]; then
		echo "UPnP MediaServer is disabled. If the process still existed, try to run \"${0} stop\""
		exit 0
	fi
	check_twonky_qpkg
	clean_twonky_cache
	move_db_path_to_hdd
    if [ -e $PIDFILE ]; then
      PID=`cat $PIDFILE`
      echo "Twonky server seems already be running under PID $PID"
      echo "(PID file $PIDFILE already exists). Checking for process..."
      running=`ps --no-headers -o "%c" -p $PID`
      if ( [ "${DAEMON}"=="${running}" ] ); then
        echo "Process IS running. Not started again."
      else
        echo "Looks like the daemon crashed: the PID does not match the daemon."
        echo "Removing flag file..."
        rm $PIDFILE
        $0 start
        exit $?
      fi
      exit 0
    else
      if [ ! -x "${TWONKYSRV}" ]; then
	  echo "Twonky servers not found".
	  rc_status -u
	  exit $?
      fi
	
	echo -n "Starting UPnP MediaServer: ($TWONKYSRV) "
	
	_contentdir=`/sbin/getcfg "" contentdir -d "/Qmultimedia" -f ${DEFAULT_INIFILE}`
	[ "+A|/${MULTIMEDIA_SHARE}" = "${_contentdir}" ] || /bin/sed -i 's/^contentdir.*$/'"contentdir=+A|\/${MULTIMEDIA_SHARE}"'/' ${DEFAULT_INIFILE}
	_contentdir=`/sbin/getcfg "" contentdir -d "/Qmultimedia" -f ${DEFAULT_SERVER_INIFILE}`
	[ "+A|/${MULTIMEDIA_SHARE}" = "${_contentdir}" ] || /bin/sed -i 's/^contentdir.*$/'"contentdir=+A|\/${MULTIMEDIA_SHARE}"'/' ${DEFAULT_SERVER_INIFILE}
	
	if [ ! -f ${INIFILE} ]; then
		/bin/cp ${DEFAULT_INIFILE} ${INIFILE}
	fi

	# end
	if [ `/sbin/getcfg TwonkyMedia "Web Enable" -u -d FALSE` = FALSE ]; then
		$TWONKYSRV -inifile "${INIFILE}" -enableweb 1 -powersavemode 1 -logfile ${LOG_FILE} -suppressmenu mediafeeds 2>/dev/null 1>/dev/null &
	else
		enableweb=`/sbin/getcfg main enableweb -d 9 -f "${INIFILE}"` # 9 should not exist
		if [ $enableweb = 1 ] || [ $enableweb = 0 ]; then
			/sbin/setcfg main enableweb 2 -f "${INIFILE}"
		fi
		$TWONKYSRV -inifile "${INIFILE}" -enableweb 2 -powersavemode 1 -logfile ${LOG_FILE} -suppressmenu mediafeeds 2>/dev/null 1>/dev/null &
	fi
	rc_status -v
	/bin/sleep 2

    fi
  ;;
  stop)
    if [ ! -e $PIDFILE ]; then
      echo "Shutting down UPnP MediaServer services: twonkymedia."
      echo "PID file $PIDFILE not found, stopping server anyway..."
      killall twonkymedia twonkymusic 2>/dev/null 1>/dev/null
      rc_status -u
#      exit 3
    else
      echo -n "Shutting down UPnP MediaServer services: twonkymedia."
      PID=`cat $PIDFILE`
      kill -s TERM $PID
      kill -9 `pidof twonkymediaserver`
      rm -f $PIDFILE
	# end
      rc_status -v
    fi
	
	_pid=`/bin/pidof twonkymediaserver`
	[ -z "$_pid" ] || kill $_pid 2>/dev/null 1>/dev/null
	_pid=`/bin/pidof twonkymedia`
	[ -z "$_pid" ] || kill $_pid 2>/dev/null 1>/dev/null

      if [ "x${NIC_NUM}" = "x1" ]; then
        /sbin/route del -net 224.0.0.0 netmask 240.0.0.0 dev eth0 2> /dev/null 1>/dev/null
      else
        /sbin/route del -net 224.0.0.0 netmask 240.0.0.0 dev bond0 2> /dev/null 1>/dev/null
        /sbin/route del -net 224.0.0.0 netmask 240.0.0.0 dev eth0 2> /dev/null 1>/dev/null
        /sbin/route del -net 224.0.0.0 netmask 240.0.0.0 dev eth1 2> /dev/null 1>/dev/null
      fi
  ;;
  reload)
	[ -f ${INIFILE} ] || /bin/cp ${DEFAULT_INIFILE} ${INIFILE}
    if [ ! -e $PIDFILE ]; then
      echo "PID file $PIDFILE not found, stopping server anyway..."
      killall twonkymedia twonkymusic 2>/dev/null 1>/dev/null
      rc_status -u
      exit 3
    else
      echo -n "Reloading Twonky server ... "
      PID=`cat $PIDFILE`
      kill -s HUP $PID
      rc_status -v
    fi
  ;;
  restart)
    $0 stop
    /bin/sleep 2
    $0 start
  ;;
  status)
    if [ ! -e $PIDFILE ]; then
      running="`ps ax --no-headers | grep -e twonkymedia -e twonkymusic | grep -v grep | grep -v twonkymedia.sh | cut -d ' ' -f 1`"
      if [ "${running}" == "" ]; then
        echo "No twonky server is running"
      else
        echo "A twonky server seems to be running (PID: "${running}"), but no PID file exists."
        echo "Probably no write permission for ${PIDFILE}."
      fi
      exit 0
    fi
    PID=`cat $PIDFILE`
    running=`ps --no-headers -o "%c" -p $PID`
    if ( [ "${DAEMON}"=="${running}" ] ); then
      echo "Twonky server IS running."
    else
      echo "Looks like the daemon crashed: the PID does not match the daemon."
    fi
  ;;
  *)
    echo ""
    echo "Twonky server"
    echo "-------------"
    echo "Syntax:"
    echo "  $0 {start|stop|restart|reload|status}"
    echo ""
    exit 3
  ;;
esac

rc_exit
