#!/bin/sh
# Copyright @ 2009 QNAP Systems Inc.

GETCFG="/sbin/getcfg"
SETCFG="/sbin/setcfg"
DAEMON_MGR="/sbin/daemon_mgr"

IPV6=`$GETCFG "Network" "IPV6" -u -d "FALSE"`
BOND_SUP=`$GETCFG "Network" "BONDING Support" -u -d "FALSE"`
RA_INTERFACE=`$GETCFG "Network" "RA_INTERFACE" -d "eth0"`
BOND_NIC=`$GETCFG "Network" "BONDING NICs" -d "0"`
NIC_NUM=`$GETCFG "Network" "Interface Number" -d "1"`
ADV_BOND_SUPPORT=`$GETCFG "Network" "Advanced Bonding Support" -d "FALSE"`
GROUP_NUM=`$GETCFG "Network Group" "Group Count" -d $NIC_NUM`
BOND_NUM=`/usr/bin/expr ${NIC_NUM} / 2`

if [ "x$ADV_BOND_SUPPORT" = "xTRUE" ]; then
	for (( i=0; i<$GROUP_NUM; i=i+1 ))
	do
		net_interface=`$GETCFG "Network Group" "Group $i" -d ""`
		if [ "x${net_interface}" != "x" ]; then
			/bin/echo "${net_interface}" | /bin/grep "^eth" &>/dev/null
			if [ $? = 0 ]; then #eth
				/bin/echo "${net_interface}" | /bin/grep "^eth0" &>/dev/null
				if [ $? = 0 ]; then
					RA_INTERFACE="${net_interface}"
					break;
				fi
			else #bond
				bond_member=`$GETCFG "${net_interface}" "Bonding Member" -d ""`
				/bin/echo "${bond_member}" | /bin/grep "0" &>/dev/null
				if [ $? = 0 ]; then
					RA_INTERFACE="${net_interface}"
					break;
				fi
			fi
      	fi
	done
else
if [ "$BOND_SUP" = "TRUE" ]; then
	RA_INTERFACE=bond0
fi
if [ "$BOND_NIC" = 1 ]; then
        RA_INTERFACE=eth0
fi
fi
PREFIX=`$GETCFG $RA_INTERFACE "RA_PREFIX" -d "2002:509:1F00:168::/64"`
RADVD_CONF="/etc/radvd.conf"
GATEWAY=`$GETCFG $RA_INTERFACE "IPV6 Gateway" -d "::/0"`

gen_radvd_conf()
{
	
	/bin/echo "interface $RA_INTERFACE" > "$RADVD_CONF"
	/bin/echo "{" >> "$RADVD_CONF"
	/bin/echo " AdvSendAdvert on;" >> "$RADVD_CONF"
	/bin/echo " MaxRtrAdvInterval 300;" >> "$RADVD_CONF"
	/bin/echo " prefix $PREFIX" >> "$RADVD_CONF"
	/bin/echo " {" >> "$RADVD_CONF"
	/bin/echo "  AdvOnLink on;" >> "$RADVD_CONF"
	/bin/echo "  AdvAutonomous on;" >> "$RADVD_CONF"
	/bin/echo "  AdvRouterAddr on;" >> "$RADVD_CONF"
	/bin/echo " };" >> "$RADVD_CONF"
	/bin/echo " route $GATEWAY" >> "$RADVD_CONF"
	/bin/echo " {" >> "$RADVD_CONF"
	/bin/echo " };" >> "$RADVD_CONF"
	/bin/echo "};" >> "$RADVD_CONF"
}

start()
{
	[ "$IPV6" = "TRUE" ] || exit 1
	[ `/bin/cat /sys/class/net/$RA_INTERFACE/operstate 2>>/dev/null` = "up" ] || return 1;
	if [ `$GETCFG $RA_INTERFACE "Use RADVD" -u -d "FALSE"` = "TRUE" ]; then
		[ -f /proc/sys/net/ipv6/conf/all/forwarding ] && /bin/echo "1" > /proc/sys/net/ipv6/conf/all/forwarding
#		[ -f /proc/sys/net/ipv6/conf/bond0/forwarding ] && /bin/echo "1" > /proc/sys/net/ipv6/conf/bond0/forwarding
#		[ -f /proc/sys/net/ipv6/conf/eth0/forwarding ] && /bin/echo "1" > /proc/sys/net/ipv6/conf/eth0/forwarding
#		[ -f /proc/sys/net/ipv6/conf/eth1/forwarding ] && /bin/echo "0" > /proc/sys/net/ipv6/conf/eth1/forwarding
		for (( i=0; i<$NIC_NUM; i=i+1 ))
		do
			if [ -f /proc/sys/net/ipv6/conf/eth$i/forwarding ]; then
				if [ "eth$i" != "$RA_INTERFACE" ]; then
					/bin/echo "0" > /proc/sys/net/ipv6/conf/eth$i/forwarding
				fi
			fi
		done
		for (( i=0; i<$BOND_NUM; i=i+1 ))
		do
			if [ -f /proc/sys/net/ipv6/conf/bond$i/forwarding ]; then
				if [ "bond$i" != "$RA_INTERFACE" ]; then
					/bin/echo "0" > /proc/sys/net/ipv6/conf/bond$i/forwarding
				fi
	      		fi
		done
		[ -f /proc/sys/net/ipv6/conf/$RA_INTERFACE/forwarding ] && /bin/echo "1" > /proc/sys/net/ipv6/conf/$RA_INTERFACE/forwarding
		gen_radvd_conf
		$DAEMON_MGR radvd start "/usr/sbin/radvd -d 1 -C $RADVD_CONF &"
	fi
}

stop()
{
	local pid
	$DAEMON_MGR radvd stop /usr/sbin/radvd
	/bin/sleep 2
	pid=`/bin/pidof radvd`
	if [ ! -z "$pid" ]; then 
		/bin/kill $pid
		/bin/sleep 2
	fi
	pid=`/bin/pidof radvd`
	[ ! -z "$pid" ] && /bin/kill -9 $pid
	[ -f /proc/sys/net/ipv6/conf/all/forwarding ] && /bin/echo "0" > /proc/sys/net/ipv6/conf/all/forwarding
#	[ -f /proc/sys/net/ipv6/conf/bond0/forwarding ] && /bin/echo "0" > /proc/sys/net/ipv6/conf/bond0/forwarding
#	[ -f /proc/sys/net/ipv6/conf/eth0/forwarding ] && /bin/echo "0" > /proc/sys/net/ipv6/conf/eth0/forwarding
	[ -f /proc/sys/net/ipv6/conf/$RA_INTERFACE/forwarding ] && /bin/echo "0" > /proc/sys/net/ipv6/conf/$RA_INTERFACE/forwarding
}

restart() 
{
	stop
	/bin/sleep 1
	start
	/bin/sleep 1
}

case "$1" in
start)
	stop 
	/bin/sleep 1
	start
	;;

stop)
	stop
	;;

restart)
	restart
	;;

*)
	/bin/echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit 0

