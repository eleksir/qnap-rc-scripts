#!/bin/sh
#
# Copyright @ 2012 QNAP Systems Inc.
#
# network  This script start/stop/restart network.
#
# Version  2.0.0
#

DBG=0

dbg()
{
  if [ -f /tmp/dbg_network ]; then
    echo "[# DBG #] $@ " >> /tmp/dbg_network
  fi
  if [ "x$DBG" = "x1" ]; then
    echo ""
    echo -n "[# DBG #] $@ "
  fi
}

dbg "============== network.sh =============="

GETCFG="/sbin/getcfg"
SETCFG="/sbin/setcfg"
DEFAULT_GW_DEV=`$GETCFG "Network" "Default GW Device" -d eth0`
BOND_SUP=`$GETCFG "Network" "BONDING Support" -u -d FALSE`
NIC_NUM=`$GETCFG "Network" "Interface Number" -d "1"`
DEFAULT_NIC_NUM=`$GETCFG "Network" "Interface Number" -d 1 -f /etc/default_config/uLinux.conf` #reset by check_lan_port
if [ x$NIC_NUM != x$DEFAULT_NIC_NUM ]; then
	$SETCFG "Network" "Interface Number" "$DEFAULT_NIC_NUM"
	NIC_NUM=$DEFAULT_NIC_NUM
fi
BOND_NUM=`/usr/bin/expr ${DEFAULT_NIC_NUM} / 2`
GROUP_NUM=`$GETCFG "Network Group" "Group Count" -d $DEFAULT_NIC_NUM`
ADV_BOND_SUPPORT=`$GETCFG "Network" "Advanced Bonding Support" -u -d FALSE -f /etc/default_config/uLinux.conf`
ULINUX_UPGRADE=`$GETCFG "Network" "uLinux Upgrade" -u -d FALSE`
TwonkyMedia_Enable=`$GETCFG "TwonkyMedia" "Enable" -u -d FALSE`
QPKG_TwonkyMedia_Enable=`$GETCFG "TwonkyMedia" "Enable" -u -d FALSE -f "/etc/config/qpkg.conf"`
BOOT_CONF=`/bin/cat /etc/default_config/BOOT.conf 2>/dev/null`
HAVECHANGE=0

use_default()
{
  dbg "Entering use_default()"
  DEFAULT_IP=`$GETCFG "Network" "Default IP" -d "169.254.100.100" -f "/var/default"`
  dbg "cmd:/sbin/ifconfig $1 $DEFAULT_IP up"
  /sbin/ifconfig $1 $DEFAULT_IP up &>/dev/null
  #wait NIC up
  /bin/sleep 3
  dbg "cmd:/sbin/route add -host 255.255.255.255 $1"
  /sbin/route add -host 255.255.255.255 $1 &>/dev/null
  DEFAULT_ROUTE=`$GETCFG "Network" "Default Route" -d "169.254.0.0" -f "/var/default"`
  dbg "cmd:/sbin/route del -net $DEFAULT_ROUTE netmask 255.255.0.0 dev $1"
  /sbin/route del -net $DEFAULT_ROUTE netmask 255.255.0.0 dev $1 &>/dev/null
  dbg "cmd:/sbin/route add -net $DEFAULT_ROUTE netmask 255.255.0.0 dev $1"
  /sbin/route add -net $DEFAULT_ROUTE netmask 255.255.0.0 dev $1 &>/dev/null
}

check_and_reset_default_gw()
{
  if [ $HAVECHANGE = 1 ]; then
    return 0;
  fi
  HAVECHANGE=1
  dbg "Entering check_and_reset_default_gate() $1"
  local status=`/bin/cat /sys/class/net/${DEFAULT_GW_DEV}/operstate`
  dbg "${DEFAULT_GW_DEV} status $status"
  if [ "$status" = "up" ]; then
    return 0;
  fi
  local updev="$1"
  if [ "x$updev" = "xall" ]; then
    for (( i=0; i<$GROUP_NUM; i=i+1 ))
    do
      local net_interface=`$GETCFG "Network Group" "Group $i" -d ""`
      if [ "x${net_interface}" != "x" ]; then
        local status=`/bin/cat /sys/class/net/${net_interface}/operstate`
        dbg "${net_interface} status $status"
        if [ "$status" = "up" ]; then
      	  DEFAULT_GW_DEV=${net_interface}
          dbg "cmd:$SETCFG "Network" "Default GW Device" "$DEFAULT_GW_DEV""
          $SETCFG "Network" "Default GW Device" "$DEFAULT_GW_DEV"
          return 1;
        fi
      fi
    done
    return 0;
  fi
  
  for (( i=0; i<$GROUP_NUM; i=i+1 ))
  do
    local net_interface=`$GETCFG "Network Group" "Group $i" -d ""`
    if [ "x${net_interface}" != "x" ]; then
      local status=`/bin/cat /sys/class/net/${net_interface}/operstate`
      /bin/echo "${net_interface}" | /bin/grep "^eth" &>/dev/null
      if [ $? = 0 ]; then #eth
        if [ "${net_interface}" = "$updev" ]; then
          if [ "$status" = "up" ]; then
            DEFAULT_GW_DEV=${net_interface}
            dbg "cmd:$SETCFG "Network" "Default GW Device" "$DEFAULT_GW_DEV""
            $SETCFG "Network" "Default GW Device" "$DEFAULT_GW_DEV"
            return 1;
          fi
        fi
      else #bond
        if [ "$status" = "up" ]; then
          local bond_member=`$GETCFG "${net_interface}" "Bonding Member" -d ""`
          for eth_id in `echo ${bond_member//,/ }`;
          do
            if [ "eth${eth_id}" = "$updev" ]; then
              DEFAULT_GW_DEV=${net_interface}
              dbg "cmd:$SETCFG "Network" "Default GW Device" "$DEFAULT_GW_DEV""
              $SETCFG "Network" "Default GW Device" "$DEFAULT_GW_DEV"
              return 1;
            fi
          done
        fi
      fi
    fi
  done
  return 0;
}

add_dns()
{
  dbg "Entering add_dns()"
  local nas_dns
  local myflag=0
  local i
  
  if [ "$1" = "STATIC" ]; then
    /bin/rm -f /etc/resolv.conf &>/dev/null
  fi
  
  for i in 2 1
  do
    nas_dns=`$GETCFG "Network" "Domain Name Server $i" -d 0.0.0.0`
    /bin/grep $nas_dns /etc/resolv.conf &>/dev/null
    if [ $? != 0 ]; then
      if [ "$nas_dns" != "0.0.0.0" ]; then
        [ -f /etc/resolv.conf ] && /bin/mv /etc/resolv.conf /etc/resolv.conf.bak
        dbg "cmd:/bin/echo "nameserver $nas_dns" > /etc/resolv.conf"
        /bin/echo "nameserver $nas_dns" > /etc/resolv.conf
        if [ -f /etc/resolv.conf.bak ]; then
          /bin/cat /etc/resolv.conf.bak >> /etc/resolv.conf
          /bin/rm -f /etc/resolv.conf.bak &>/dev/null
        fi
        myflag=1
      fi
    else
      myflag=1
    fi
  done
  if [ $myflag = 0 ]; then
    /bin/echo "nameserver 0.0.0.0" >> /etc/resolv.conf
  fi
}

set_mtu()
{
  /bin/rm -f /tmp/NowSpeed &>/dev/null
  for (( i=0; i<$GROUP_NUM; i=i+1 ))
  do
    local net_interface=`$GETCFG "Network Group" "Group $i" -d ""`
    if [ "x${net_interface}" != "x" ]; then
      check_mtu ${net_interface}
    fi
  done
}

check_mtu()
{
  dbg "Entering check_mtu()"
  local MTU=`$GETCFG $1 MTU -d 1500`
  local SPEED="1000"

  /bin/echo "$1" | /bin/grep "^eth" &>/dev/null
  if [ $? = 0 ]; then #eth
    local STATUS=`/bin/cat /sys/class/net/$1/operstate`
    if [ "$STATUS" = "up" ]; then
      /bin/echo "[$1]" >> /tmp/NowSpeed
      /sbin/eth_util $1 >> /tmp/NowSpeed
      SPEED=`$GETCFG $1 "Speed of $1" -f /tmp/NowSpeed`
      if [ "$SPEED" = "100" ] && [ ! "$MTU" = "1500" ]; then
        $SETCFG $1 MTU 1500
      fi
    fi
  else #bond
    local bond_member=`$GETCFG $1 "Bonding Member" -d ""`
    local member_up="0"
    for eth_id in `echo ${bond_member//,/ }`;
    do
      local STATUS=`/bin/cat /sys/class/net/eth$eth_id/operstate`
      if [ "$STATUS" = "up" ]; then
        member_up="1"
        /bin/echo "[eth$eth_id]" >> /tmp/NowSpeed
        /sbin/eth_util eth$eth_id >> /tmp/NowSpeed
        local _SPEED=`$GETCFG eth${eth_id} "Speed of eth${eth_id}" -f /tmp/NowSpeed`
        if [ "$_SPEED" = "100" ]; then
          SPEED="100"
        fi
      fi
    done
    if [ "$member_up" = "0" ]; then
      SPEED=`$GETCFG $1 Speed -d "auto"`
      if [ "$SPEED" = "auto" ]; then
        SPEED="1000"
      fi
    fi
    if [ "$SPEED" = "100" ] && [ ! "$MTU" = "1500" ]; then
      dbg "cmd:$SETCFG $1 MTU 1500"
      $SETCFG $1 MTU 1500
      /sbin/write_log "[$1] Jumbo Frame is disabled because the connection speed is not Gigabit." 1
    fi
  fi
  if [ "$SPEED" = "100" ]; then
    dbg "cmd:/sbin/ifconfig $1 mtu 1500"
    /sbin/ifconfig $1 mtu 1500
  else
    dbg "cmd:/sbin/ifconfig $1 mtu $MTU"
    /sbin/ifconfig $1 mtu $MTU
  fi
}

set_speed()
{
  for (( i=0; i<$GROUP_NUM; i=i+1 ))
  do
    local net_interface=`$GETCFG "Network Group" "Group $i" -d ""`
    if [ "x${net_interface}" != "x" ]; then
      check_speed ${net_interface}
    fi
  done
}

check_speed()
{
  dbg "Entering check_speed()"
  local SPEED=`$GETCFG $1 Speed -d "auto"`
  if [ "$SPEED" = "10000" ] || [ "$SPEED" = "1000" ] || [ "$SPEED" = "auto" ]; then
    local SPEEDX=0x02f
  elif [ "$SPEED" = "100" ]; then
    local SPEEDX=0x008
  fi

  /bin/echo "$1" | /bin/grep "^eth" &>/dev/null
  if [ $? = 0 ]; then #eth
    local S0=`/bin/cat /sys/class/net/$1/speed`
    /usr/sbin/ethtool $1 | /bin/grep 10000baseT &>/dev/null
    if [ $? = 0 ] || [ "$SPEED" = "auto" ] ; then # Fixed 10G
      /usr/sbin/ethtool -s $1 autoneg on &>/dev/null
      dbg "cmd:/usr/sbin/ethtool -s $1 autoneg on"
    elif [ "$SPEED" != "$S0" ]; then
      /usr/sbin/ethtool -s $1 speed $SPEED advertise $SPEEDX autoneg on &>/dev/null
      dbg "cmd:/usr/sbin/ethtool -s $1 speed $SPEED advertise $SPEEDX autoneg on"
    fi
  else #bond
    local bond_member=`$GETCFG $1 "Bonding Member" -d ""`
    for eth_id in `echo ${bond_member//,/ }`;
    do
      local S0=`/bin/cat /sys/class/net/eth${eth_id}/speed`
      /usr/sbin/ethtool eth${eth_id} | /bin/grep 10000baseT &>/dev/null
      if [ $? = 0 ] || [ "$SPEED" = "auto" ]; then # Fixed 10G
        /usr/sbin/ethtool -s eth${eth_id} autoneg on &>/dev/null
        dbg "cmd:/usr/sbin/ethtool -s eth${eth_id} autoneg on"
      elif [ "$SPEED" != "$S0" ]; then
        /usr/sbin/ethtool -s eth${eth_id} speed $SPEED advertise $SPEEDX autoneg on &>/dev/null
        dbg "cmd:/usr/sbin/ethtool -s eth${eth_id} speed $SPEED advertise $SPEEDX autoneg on"
      fi
    done
  fi
}

reorder_routeing_table_up()
{
  dbg "Entering reorder_routeing_table_up() $1"
  /bin/cat /proc/net/route | /bin/sed '/^Iface/d' > /tmp/nasroute.org
  /bin/sed '/lo/d' -i /tmp/nasroute.org
  
  [ ! -f /tmp/.boot_done ] || check_and_reset_default_gw $1
  
  for (( i=0; i<$GROUP_NUM; i=i+1 ))
  do
    local net_interface=`$GETCFG "Network Group" "Group $i" -d ""`
  
    if [ "x${net_interface}" != "x" ]; then
      local status=`/bin/cat /sys/class/net/${net_interface}/operstate`
      if [ "$status" = "up" ]; then
        /bin/grep "^${net_interface}" /tmp/nasroute.org &>/dev/null
        if [ $? != 0 ]; then
          dbg "cmd:/sbin/ifconfig ${net_interface} down"
          /sbin/ifconfig ${net_interface} down
          /bin/sleep 1
          NIC_start ${net_interface}
        fi
      fi
    fi
  done
  [ -f /tmp/nasroute.org ] && /bin/rm -f /tmp/nasroute.org &>/dev/null
  [ ! -f /etc/init.d/ipv6.sh ] || /etc/init.d/ipv6.sh start
}

routeing_table_delete()
{
  dbg "Entering routeing_table_delete()"
  pid=$(/bin/ps | /bin/grep "dhcpcd" | /bin/grep "$1" | /bin/awk '{print $1}')
  if [ "x$pid" != "x" ]; then
    dbg "cmd:/bin/kill -9 $pid"
    /bin/kill -9 $pid &>/dev/null
  fi
  /etc/init.d/vlan.sh revert_vid $1
  while read LINE
  do
    dbg "LINE=$LINE"
    ip_dest=`/bin/echo $LINE | /bin/cut -f 1 -d " "`
    if [ "$ip_dest" = "default" ]; then
      if [ "x${DEFAULT_GW_DEV}" != "x$1" ]; then
        dbg "cmd:/bin/ip route del default via $ip_dest dev $1"
        /bin/ip route del default via $ip_dest dev $1 &>/dev/null
        dbg "cmd:/sbin/route del -net 224.0.0.0 netmask 240.0.0.0 dev $1"
        /sbin/route del -net 224.0.0.0 netmask 240.0.0.0 dev $1 &>/dev/null
      fi
    else
      dbg "cmd:/bin/ip route del $ip_dest dev $1"
      /bin/ip route del $ip_dest dev $1 &>/dev/null
    fi
  done < /tmp/$1.route
  dbg "cmd:/sbin/ifconfig $1 0.0.0.0"
  /sbin/ifconfig $1 0.0.0.0
}

reorder_routeing_table_down()
{
  if [ "x${BOOT_CONF}" = "xTS-NASARM" ]; then
    sleep 5
  fi
  dbg "Entering reorder_routeing_table_down()"
  for (( i=0; i<$NIC_NUM; i=i+1 ))
  do
    /bin/ip route | /bin/grep "eth$i" > /tmp/eth$i.route
  done

  for (( i=0; i<$BOND_NUM; i=i+1 ))
  do
    /bin/ip route | /bin/grep "bond$i" > /tmp/bond$i.route
  done

  for (( i=0; i<$GROUP_NUM; i=i+1 ))
  do
    local net_interface=`$GETCFG "Network Group" "Group $i" -d ""`
    
    if [ "x${net_interface}" != "x" ]; then
      local status=`/bin/cat /sys/class/net/${net_interface}/operstate`
      /bin/echo "${net_interface}" | /bin/grep "^eth" &>/dev/null
      if [ $? = 0 ]; then #eth
        if [ "$status" = "down" ]; then
          routeing_table_delete ${net_interface}
        fi
      else #bond
        if [ "$status" = "down" ]; then
          routeing_table_delete ${net_interface}
        else # Fixed, 802.3ad can not get bond down or up message
          local bond_down=1
          local bond_member=`$GETCFG "${net_interface}" "Bonding Member" -d ""`
          for eth_id in `echo ${bond_member//,/ }`;
          do
            local status=`/bin/cat /sys/class/net/${net_interface}/slave_eth${eth_id}/operstate`
            if [ "$status" = "up" ]; then
              bond_down=0
            fi
          done
          if [ ${bond_down} -eq 1 ]; then
            routeing_table_delete ${net_interface}
          fi
        fi
      fi
    fi
  done
  
  for (( i=0; i<$NIC_NUM; i=i+1 ))
  do
    [ -f /tmp/eth${i}.route ] && /bin/rm -f /tmp/eth${i}.route &>/dev/null
    /bin/ip route | /bin/grep "default" | /bin/grep eth${i}
    if [ $? = 0 ]; then
      if [ "x${DEFAULT_GW_DEV}" != "xeth${i}" ]; then
        dbg "cmd:/bin/ip route del default via default dev eth${i}"
        /bin/ip route del default via default dev eth${i} &>/dev/null
      fi
    fi
  done

  for (( i=0; i<$BOND_NUM; i=i+1 ))
  do
    [ -f /tmp/bond${i}.route ] && /bin/rm -f /tmp/bond${i}.route &>/dev/null
    /bin/ip route | /bin/grep "default" | /bin/grep bond${i}
    if [ $? = 0 ]; then
      if [ "x${DEFAULT_GW_DEV}" != "xbond${i}" ]; then
        dbg "cmd:/bin/ip route del default via default dev bond${i}"
        /bin/ip route del default via default dev bond${i} &>/dev/null
      fi
    fi
  done
}

NIC_start()
{
  dbg "Entering NIC_start()"
  if [ `$GETCFG "$1" "Use DHCP" -u -d "TRUE"` = "TRUE" ]; then
    /bin/echo "$1 DHCP"
    use_default $1
    [ -f /etc/dhcpc/dhcpcd-$1.info ] && /bin/rm -f /etc/dhcpc/dhcpcd-$1.info &>/dev/null
    [ -f /etc/dhcpc/dhcpcd-$1.pid ] && /bin/rm -f /etc/dhcpc/dhcpcd-$1.pid &>/dev/null
    local vlan_enable=`$GETCFG $1 vlan_enable -u -d "FALSE"`
    if [ "$vlan_enable" = "TRUE" ]; then
      /etc/init.d/vlan.sh set_vid $1
      dbg "cmd:/sbin/ifconfig $1 0.0.0.0"
      /sbin/ifconfig $1 0.0.0.0 &>/dev/null
      [ -f /etc/dhcpc/dhcpcd-$1.*.info ] && /bin/rm -f /etc/dhcpc/dhcpcd-$1.*.info &>/dev/null
      [ -f /etc/dhcpc/dhcpcd-$1.*.pid ] && /bin/rm -f /etc/dhcpc/dhcpcd-$1.*.pid &>/dev/null
      local vlan_id=`$GETCFG $1 vlan_id -d ""`
      use_default $1.$vlan_id
      if [ "x${DEFAULT_GW_DEV}" = "x$1" ]; then
        dbg "cmd:/sbin/dhcpcd -h "`/bin/hostname`" -t 30 $1.$vlan_id &"
        /sbin/dhcpcd -h "`/bin/hostname`" -t 30 $1.$vlan_id &>/dev/null &
      else
        dbg "cmd:/sbin/dhcpcd -h "`/bin/hostname`" -t 30 -G $1.$vlan_id &"
        /sbin/dhcpcd -h "`/bin/hostname`" -t 30 -G $1.$vlan_id &>/dev/null &
      fi
    else
      dbg "cmd:/sbin/dhcpcd -h "`/bin/hostname`" -t 30 $1 &"
      /sbin/dhcpcd -h "`/bin/hostname`" -t 30 $1 &>/dev/null &
    fi
    if [ $? != 0 ]; then
      dbg "$1 run dhcpcd failed"
      /bin/echo "Run dhcpcd failed."
    fi
  else
    /bin/echo "$1 STATIC"
    local ipaddress=`$GETCFG "$1" "IP Address" -d "169.254.100.100"`
    local netmask=`$GETCFG "$1" "Subnet Mask" -d "255.255.0.0"`
    local broadcast=`$GETCFG "$1" "Broadcast" -d "169.254.255.255"`
    local gateway=`$GETCFG "$1" "Gateway" -d "0.0.0.0"`
    if [ "$gateway" = "0.0.0.0" ]; then
      gateway=$ipaddress
    fi
    dbg "cmd:/sbin/ifconfig $1 $ipaddress netmask $netmask broadcast $broadcast up"
    /sbin/ifconfig $1 $ipaddress netmask $netmask broadcast $broadcast up &>/dev/null
    dbg "cmd:/sbin/route add -host 255.255.255.255 $1"
    /sbin/route add -host 255.255.255.255 $1 &>/dev/null
    dbg "cmd:/sbin/route add -host $ipaddress $1"
    /sbin/route add -host $ipaddress $1 &>/dev/null
    if [ "x${DEFAULT_GW_DEV}" = "x$1" ]; then
      dbg "cmd:/sbin/route add default gw $gateway metric 1 $1"
      /sbin/route add default gw $gateway metric 1 $1 &>/dev/null
    fi
    local vlan_enable=`$GETCFG $1 vlan_enable -u -d "FALSE"`
    if [ "$vlan_enable" = "TRUE" ]; then
      /etc/init.d/vlan.sh set_vid $1
      dbg "cmd:/sbin/ifconfig $1 0.0.0.0"
      /sbin/ifconfig $1 0.0.0.0 &>/dev/null
    fi
    set_dns_static=1
  fi
  if [ "x$TwonkyMedia_Enable" = "xTRUE" ] || [ "x$QPKG_TwonkyMedia_Enable" = "xTRUE" ]; then
    dbg "cmd:/sbin/route add -net 224.0.0.0 netmask 240.0.0.0 dev $1"
    /sbin/route add -net 224.0.0.0 netmask 240.0.0.0 dev $1 &>/dev/null
  fi
}

start() 
{
  dbg "Entering start()"
  /bin/echo -n "Starting network..."
  [ -d /etc/dhcpc ] || /bin/mkdir /etc/dhcpc
  set_dns_static=0
  DEFAULT_GW_DEV=`$GETCFG "Network" "Default GW Device" -d "NULL"`
  if [ "$DEFAULT_GW_DEV" = "NULL" ]; then
    if [ "$BOND_SUP" = "TRUE" ] && [ "$NIC_NUM" != "1" ]; then
      DEFAULT_GW_DEV=bond0
    else
      DEFAULT_GW_DEV=eth0
    fi
    dbg "cmd:$SETCFG "Network" "Default GW Device" "$DEFAULT_GW_DEV""
    $SETCFG "Network" "Default GW Device" "$DEFAULT_GW_DEV"
  fi
  
  [ ! -f /etc/init.d/ipv6.sh ] || /etc/init.d/ipv6.sh init
  
  if [ "$BOND_SUP" = "TRUE" ]; then
    /sbin/lsmod | /bin/grep bonding &>/dev/null      
    if [ $? != 0 ]; then
      if [ -f /lib/modules/misc/bonding.ko ]; then
        local bond_mod=`$GETCFG "bond0" "Bonding Mode" -d "1"`
        if [ "x${BOOT_CONF}" = "xTS-NASARM" ] && [ "x$bond_mod" = "x5" -o "x$bond_mod" = "x6" ]; then
          dbg "cmd:/sbin/insmod /lib/modules/misc/bonding.ko mode=$bond_mod miimon=100 downdelay=8000 max_bonds=$BOND_NUM"
          /sbin/insmod /lib/modules/misc/bonding.ko mode=$bond_mod miimon=100 downdelay=8000 max_bonds=$BOND_NUM &>/dev/null
        else
          dbg "cmd:/sbin/insmod /lib/modules/misc/bonding.ko mode=1 miimon=100 max_bonds=$BOND_NUM"
          /sbin/insmod /lib/modules/misc/bonding.ko mode=1 miimon=100 max_bonds=$BOND_NUM &>/dev/null
        fi
      fi
    fi

    for (( i=0; i<$GROUP_NUM; i=i+1 ))
    do
      local net_interface=`$GETCFG "Network Group" "Group $i" -d ""`
      if [ "x${net_interface}" != "x" ]; then
        /bin/echo "${net_interface}" | /bin/grep "^bond" &>/dev/null
        if [ $? = 0 ]; then #bond
          local bond_mod=`$GETCFG "${net_interface}" "Bonding Mode" -d "1"`
          dbg "cmd:/bin/echo $bond_mod > /sys/class/net/${net_interface}/bonding/mode"
          /bin/echo $bond_mod > /sys/class/net/${net_interface}/bonding/mode
          dbg "cmd:/sbin/ifconfig ${net_interface} up"
          /sbin/ifconfig ${net_interface} up &>/dev/null
          local bond_member=`$GETCFG "${net_interface}" "Bonding Member" -d ""`
          for eth_id in `echo ${bond_member//,/ }`;
          do
            dbg "cmd:/sbin/ifenslave ${net_interface} eth${eth_id}"
            /sbin/ifenslave ${net_interface} eth${eth_id} &>/dev/null
          done
        fi
      fi
    done
    
    /bin/sleep 1

    vlan_enable=`$GETCFG bond0 vlan_enable -u -d "FALSE"`
    if [ "$vlan_enable" = "TRUE" ]; then
      #avoid the devide down 
      /bin/sleep 10
    fi
  elif [ "x$GROUP_NUM" != "x$NIC_NUM" ]; then
    $SETCFG "Network Group" "Group Count" ${NIC_NUM}
    for (( i=0; i<$NIC_NUM; i=i+1 ))
    do
      $SETCFG "Network Group" "Group ${i}" "eth${i}"
      $SETCFG "eth${i}" "Configure" "TRUE"
    done
    GROUP_NUM=$NIC_NUM
  fi

  for (( i=0; i<$GROUP_NUM; i=i+1 ))
  do
    local net_interface=`$GETCFG "Network Group" "Group $i" -d ""`
    if [ "x${net_interface}" != "x" ]; then
      NIC_start ${net_interface}
    fi
  done

  /etc/init.d/wireless.sh start
  if [ "x${DEFAULT_GW_DEV}" = "xwlan0" ]; then
    if [ `$GETCFG "wireless" "iptype" -d "dhcp"` = "static" ]; then
      set_dns_static=1
    fi
  fi
  if [  ${set_dns_static} -eq 1 ]; then
    add_dns "STATIC"
    /bin/sleep 1
    restart_dhcpd
  fi
  /etc/init.d/ipchange_notify.sh 
  [ ! -f /etc/init.d/ipv6.sh ] || /etc/init.d/ipv6.sh start 
}

stop() 
{
  dbg "Entering stop()"
  /bin/echo "Stopping network..."
  [ -f /etc/init.d/ipv6.sh ] && /etc/init.d/ipv6.sh stop
  [ -f /etc/init.d/upnpd.sh ] && /etc/init.d/upnpd.sh stop
  
  /bin/rm -f /etc/resolv.conf &>/dev/null
  
  for (( i=0; i<$NIC_NUM; i=i+1 ))
  do
    dbg "cmd:/sbin/ifconfig eth$i down"
    /sbin/ifconfig eth$i down &>/dev/null
  done

  /etc/init.d/vlan.sh stop
  
  for (( i=0; i<$BOND_NUM; i=i+1 ))
  do
    /sbin/ifconfig | /bin/grep bond$i &>/dev/null
    [ $? = 0  ] && release_bond bond$i
  done
  dbg "cmd:/sbin/rmmod bonding"
  /sbin/rmmod bonding &>/dev/null
  
  local pid
  pid=`/bin/pidof dhcpcd`
  if [ ! -z "$pid" ]; then
    dbg "cmd:/bin/kill -9 $pid"
    /bin/kill -9 $pid &>/dev/null
  fi
  /etc/init.d/wireless.sh stop
  /bin/echo "Stopped."
}

release_bond()
{
  dbg "Entering release_bond()"
  local Max_NIC_NUM=`/usr/bin/expr ${NIC_NUM} - 1`
  for (( i=$Max_NIC_NUM; i>=0; i=i-1 ))
  do
    /bin/cat /proc/net/bonding/$1 | /bin/grep "Slave Interface" | /bin/grep eth$i &>/dev/null
    if [ $? = 0 ]; then
      dbg "cmd:/sbin/ifenslave -d $1 eth$i"
      /sbin/ifenslave -d $1 eth$i &>/dev/null
    fi
  done
  dbg "cmd:/sbin/ifconfig $1 0.0.0.0"
  /sbin/ifconfig $1 0.0.0.0 &>/dev/null
  dbg "cmd:/sbin/ifconfig $1 down"
  /sbin/ifconfig $1 down &>/dev/null
}

restart_dhcpd()
{
  dbg "Entering restart_dhcpd()"
  /sbin/test -f /usr/local/sbin/dhcpd || return 1
  if [ `$GETCFG "DHCP Server" Enable -u -d FALSE` = "TRUE" ]; then
    /etc/init.d/dhcpd.sh restart &>/dev/null
    if [ $? != 0 ]; then
      dbg "Run dhcpd.sh failed"
      echo "Run dhcpd.sh failed."
    fi
    return 0
  fi
}

reload()
{
  dbg "Entering reload($1)"
  
  # Patch for one lan port model (Bug #27514)
	if [ "x${NIC_NUM}" = "x1" ]; then
		if [ `$GETCFG "eth0" "Use DHCP" -u -d "TRUE"` = "TRUE" ]; then
			/bin/echo "DHCP"
			use_default eth0
			/bin/kill -9 `pidof dhcpcd`
			/bin/rm -f /etc/dhcpc/dhcpcd-eth0.*
			/sbin/dhcpcd -h "`/bin/hostname`" -t 30 eth0 &
		else
			echo "STATIC"
			local ipaddress=`$GETCFG "eth0" "IP Address" -d "169.254.100.100"`
      local netmask=`$GETCFG "eth0" "Subnet Mask" -d "255.255.0.0"`
      local broadcast=`$GETCFG "eth0" "Broadcast" -d "169.254.255.255"`
      local gateway=`$GETCFG "eth0" "Gateway" -d "0.0.0.0"`
			if [ "$gateway" = "0.0.0.0" ]; then
				gateway=$ipaddress
			fi
			/sbin/ifconfig eth0 $ipaddress netmask $netmask broadcast $broadcast up
			/sbin/route add -host 255.255.255.255 eth0
			/sbin/route add -host $ipaddress eth0
			/sbin/route add default gw $gateway metric 1 eth0
			add_dns "STATIC"
			restart_dhcpd
			return $?
		fi
	fi
	
	# Reload network settings for bond.
  /bin/echo "$1" | /bin/grep "^bond" &>/dev/null
  if [ $? != 0 ]; then
    dbg "network.sh reload $1 fail. (network reload is bond only)"
    /bin/echo "network.sh reload $1 fail. (network reload is bond only)"
    return 1
  fi
  /sbin/ifconfig | /bin/grep $1 &>/dev/null
  [ $? = 0  ] || return 1
  
  /bin/echo "reload $1..."
  if [ `$GETCFG "$1" "Use DHCP" -u -d "TRUE"` = "TRUE" ]; then
    /bin/echo "$1 DHCP"
    local vlan_enable=`$GETCFG "$1" vlan_enable -u -d "FALSE"`
    if [ "$vlan_enable" = "TRUE" ]; then
      local vlan_id=`$GETCFG "$1" vlan_id -d ""`
      /etc/init.d/vlan.sh revert_vid $1
      /etc/init.d/vlan.sh set_vid $1
      dbg "cmd:/sbin/ifconfig $1 0.0.0.0"
      /sbin/ifconfig $1 0.0.0.0 &>/dev/null
      use_default $1.$vlan_id
      pid=$(/bin/ps | /bin/grep "dhcpcd" | /bin/grep "$1" | /bin/awk '{print $1}')
      if [ "x$pid" != "x" ]; then
        dbg "cmd:/bin/kill -9 $pid"
        /bin/kill -9 $pid &>/dev/null
      fi
      [ -f /etc/dhcpc/dhcpcd-$1.info ] && /bin/rm -f /etc/dhcpc/dhcpcd-$1.info
      [ -f /etc/dhcpc/dhcpcd-$1.pid ] && /bin/rm -f /etc/dhcpc/dhcpcd-$1.pid
      if [ "x${DEFAULT_GW_DEV}" = "x$1" ]; then
        /sbin/dhcpcd -h "`/bin/hostname`" -t 30 $1.$vlan_id & 
      else
        /sbin/dhcpcd -h "`/bin/hostname`" -t 30 -G $1.$vlan_id & 
      fi
    else
      use_default $1
      pid=$(/bin/ps | /bin/grep "dhcpcd" | /bin/grep "$1" | /bin/awk '{print $1}')
      if [ "x$pid" != "x" ]; then
        dbg "cmd:/bin/kill -9 $pid"
        /bin/kill -9 $pid &>/dev/null
      fi
      [ -f /etc/dhcpc/dhcpcd-$1.info ] && /bin/rm -f /etc/dhcpc/dhcpcd-$1.info
      [ -f /etc/dhcpc/dhcpcd-$1.pid ] && /bin/rm -f /etc/dhcpc/dhcpcd-$1.pid
      dbg "cmd:/sbin/dhcpcd -h "`/bin/hostname`" -t 30 $1 &"
      /sbin/dhcpcd -h "`/bin/hostname`" -t 30 $1 &>/dev/null &
    fi
  else
    /bin/echo "$1 STATIC"
    local ipaddress=`$GETCFG "$1" "IP Address" -d "169.254.100.100"`
    local netmask=`$GETCFG "$1" "Subnet Mask" -d "255.255.0.0"`
    local broadcast=`$GETCFG "$1" "Broadcast" -d "169.254.255.255"`
    local gateway=`$GETCFG "$1" "Gateway" -d "0.0.0.0"`
    if [ "$gateway" = "0.0.0.0" ]; then
      gateway=$ipaddress
    fi
    dbg "cmd:/sbin/ifconfig $1 $ipaddress netmask $netmask broadcast $broadcast"
    /sbin/ifconfig $1 $ipaddress netmask $netmask broadcast $broadcast &>/dev/null
    dbg "cmd:/sbin/route add -host 255.255.255.255 $1"
    /sbin/route add -host 255.255.255.255 $1 &>/dev/null
    dbg "cmd:/sbin/route add -host $ipaddress $1"
    /sbin/route add -host $ipaddress $1 &>/dev/null
    if [ "x${DEFAULT_GW_DEV}" = "x$1" ]; then
      dbg "cmd:/sbin/route add default gw $gateway metric 1 $1"
      /sbin/route add default gw $gateway metric 1 $1 &>/dev/null
    fi
    set_dns_static=1
    local vlan_enable=`$GETCFG "$1" vlan_enable -u -d "FALSE"`
    if [ "$vlan_enable" = "TRUE" ]; then
      /etc/init.d/vlan.sh revert_vid $1
      /etc/init.d/vlan.sh set_vid $1
      dbg "cmd:/sbin/ifconfig $1 0.0.0.0"
      /sbin/ifconfig $1 0.0.0.0 &>/dev/null
    fi
  fi
}

upgrade_ulinux()
{
  dbg "upgrade_ulinux() start"
  
	$SETCFG "Network Group" "LAN Count" ${NIC_NUM}
  for (( i=0; i<$NIC_NUM; i=i+1 ))
  do
    local enable=`$GETCFG "eth$i" "Configure" -d "NULL"`
    if [ "x$enable" != "xNULL" ]; then
      $SETCFG "eth${i}" "Configure" "False"
    fi
  done
  for (( i=0; i<$BOND_NUM; i=i+1 ))
  do
    local enable=`$GETCFG "bond$i" "Configure" -d "NULL"`
    if [ "x$enable" != "xNULL" ]; then
      $SETCFG "bond${i}" "Configure" "False"
    fi
  done
  
  if [ "x${BOND_SUP}" != "xTRUE" ]; then #standalone
    $SETCFG "Network Group" "Group Count" ${NIC_NUM}
    for (( i=0; i<$NIC_NUM; i=i+1 ))
    do
      $SETCFG "Network Group" "Group ${i}" "eth${i}"
      $SETCFG "eth${i}" "Configure" "TRUE"
    done
  else #bonding mix
    local bonding_type=`$GETCFG "bond0" "Bonding Mode" -d 1`
    if [ $NIC_NUM -gt 2 ]; then # NAS has more than 2 NICs.
      local legacy_bond_mode=`$GETCFG "Network" "BONDING NICs" -d 0`
      if [ "x${legacy_bond_mode}" = "x0" ]; then
        TMP_GROUP=3
        $SETCFG "Network Group" "Group 0" "bond0"
        $SETCFG "bond0" "Configure" "TRUE"
        $SETCFG "Network Group" "Group 1" "eth2"
        $SETCFG "eth2" "Configure" "TRUE"
        $SETCFG "Network Group" "Group 2" "eth3"
        $SETCFG "eth3" "Configure" "TRUE"
        $SETCFG "bond0" "Bonding Mode" "$bonding_type"
        $SETCFG "bond0" "Bonding Member" "0,1"
        $SETCFG "bond0" "Bonding Index" "1"
      elif [ "x${legacy_bond_mode}" = "x1" ]; then
        TMP_GROUP=3
        $SETCFG "Network Group" "Group 0" "bond0"
        $SETCFG "bond0" "Configure" "TRUE"
        $SETCFG "Network Group" "Group 1" "eth0"
        $SETCFG "eth0" "Configure" "TRUE"
        $SETCFG "Network Group" "Group 2" "eth1"
        $SETCFG "eth1" "Configure" "TRUE"
        $SETCFG "bond0" "Bonding Mode" "$bonding_type"
        $SETCFG "bond0" "Bonding Member" "2,3"
        $SETCFG "bond0" "Bonding Index" "1"
      elif [ "x${legacy_bond_mode}" = "x2" ]; then
        TMP_GROUP=2
        $SETCFG "Network Group" "Group 0" "bond0"
        $SETCFG "bond0" "Configure" "TRUE"
        $SETCFG "Network Group" "Group 1" "bond1"
        $SETCFG "bond1" "Configure" "TRUE"
        $SETCFG "bond0" "Bonding Mode" "$bonding_type"
        $SETCFG "bond1" "Bonding Mode" "$bonding_type"
        $SETCFG "bond0" "Bonding Member" "0,1"
        $SETCFG "bond1" "Bonding Member" "2,3"
        $SETCFG "bond0" "Bonding Index" "1"
        $SETCFG "bond1" "Bonding Index" "2"
      elif [ "x${legacy_bond_mode}" = "x3" ]; then
        TMP_GROUP=1
        $SETCFG "Network Group" "Group 0" "bond0"
        $SETCFG "bond0" "Configure" "TRUE"
        $SETCFG "bond0" "Bonding Mode" "$bonding_type"
        $SETCFG "bond0" "Bonding Member" "0,1,2,3"
        $SETCFG "bond0" "Bonding Index" "1"
      fi
      if [ $NIC_NUM -gt 4 ]; then # Patch for legacy nas with more than 4 NICs.
      	for (( i=4; i<$NIC_NUM; i=i+1 ))
      	do
      		$SETCFG "Network Group" "Group ${TMP_GROUP}" "eth${i}"
      		$SETCFG "eth${i}" "Configure" "TRUE"
      		TMP_GROUP=$((TMP_GROUP+1))
      	done
      fi
      $SETCFG "Network Group" "Group Count" "${TMP_GROUP}"
    else # NAS has 2 NICs.
      $SETCFG "Network Group" "Group Count" "1"
      $SETCFG "Network Group" "Group 0" "bond0"
      $SETCFG "bond0" "Configure" "TRUE"
      $SETCFG "bond0" "Bonding Mode" "$bonding_type"
      $SETCFG "bond0" "Bonding Member" "0,1"
      $SETCFG "bond0" "Bonding Index" "1"
    fi
  fi
  
  for (( i=0; i<$NIC_NUM; i=i+1 ))
  do
    MAC_ADDR=`$GETCFG "MAC" "eth$i" -d "00:00:00:00:00:00" -f "/var/MacAddress"`
    $SETCFG "eth${i}" "Mac Addresss" "$MAC_ADDR"
  done
  
  $SETCFG "Network" "Advanced Bonding Support" "TRUE"
  $SETCFG "Network" "uLinux Upgrade" "TRUE"
  dbg "upgrade_ulinux() finished"
}

resetgw()
{
  dbg "reset default gateway"
  check_and_reset_default_gw all
  if [ $? = 1 ]; then
    if [ `$GETCFG "${DEFAULT_GW_DEV}" "Use DHCP" -u -d "TRUE"` = "TRUE" ]; then
      pid=$(/bin/ps | /bin/grep "dhcpcd" | /bin/grep "${DEFAULT_GW_DEV}" | /bin/awk '{print $1}')
      if [ "x$pid" != "x" ]; then
        dbg "cmd:/bin/kill -9 $pid"
        /bin/kill -9 $pid &>/dev/null
      fi
    fi
  	/sbin/ifconfig ${DEFAULT_GW_DEV} down
    /bin/sleep 1
    NIC_start ${DEFAULT_GW_DEV}
  fi
}

_lock()
{
  [ -f /tmp/touch_network ] && exit 0
  /bin/touch /tmp/touch_network
}

_wait_lock()
{
	count=60
	while [ -f /tmp/touch_network ] && [ $count -gt 0 ]; do
		count=$(($count-1));
		/bin/sleep 1
	done
	[ -f /tmp/touch_network ] && exit 0
}

_unlock()
{
  /bin/rm -f /tmp/touch_network &>/dev/null
}

###################
#  Main Function  #
###################

if [ "x$ADV_BOND_SUPPORT" = "xTRUE" ] && [ "x$ULINUX_UPGRADE" != "xTRUE" ]; then
  upgrade_ulinux
fi
#if [ x"$1" = xrouteup ] || [ x"$1" = xstart ] || [ x"$1" = xrestart ]; then
#	/etc/init.d/bonjour.sh stop 1>>/dev/null 2>>/dev/null
#fi
case "$1" in
start)
  _lock
  stop
  /bin/sleep 1
  start
  /bin/sleep 1
  resetgw
  /bin/sleep 1
  reorder_routeing_table_down
  /bin/sleep 1
  set_speed
  /bin/sleep 1
  set_mtu
  /bin/touch /etc/config/ipsec_deny.conf
  /bin/touch /etc/config/ipsec.conf
  /bin/touch /etc/config/ipsec_allow.conf
  [ "x`/bin/pidof bcclient`" = "x" ] || /bin/kill -USR1 `/bin/pidof bcclient`
  /etc/init.d/ads_register_dns.sh
  _unlock
  ;;
  
stop)
  stop
  ;;

restart)
  _lock
  stop
  /bin/sleep 1
  start
  /bin/sleep 1
  resetgw
  /bin/sleep 1
  reorder_routeing_table_down
  /bin/sleep 1
  set_speed
  /bin/sleep 1
  set_mtu
  _unlock
  ;;

setdns)
  add_dns
  ;;

write_hosts)
  /etc/init.d/hostname.sh
  ;;

mtu)
  set_mtu
  exit 0
  ;;

speed)
  set_speed
  /bin/sleep 1
  set_mtu
  exit 0
  ;;

routedown)
  reorder_routeing_table_down
  ;;

routeup)
  _wait_lock
  /bin/sleep 3
  reorder_routeing_table_up $2
  ;;
  
reload)
  reload $2
  ;;
cleanip)
  /sbin/ifconfig bond0 0.0.0.0
  ;;

*)
  /bin/echo $"Usage: $0 {start|stop|restart|setdns|write_hosts|mtu|speed|routedown|routeup|reload bond[i]}"
  exit 1
esac

if [ x"$1" = xrouteup ] || [ x"$1" = xstart ] || [ x"$1" = xrestart ]; then
	# Service binding
	/sbin/getbindaddr -refresh &>/dev/null
	dbg "/etc/init.d/bonjour.sh reload"
	/etc/init.d/bonjour.sh reload &>/dev/null
fi
exit 0
