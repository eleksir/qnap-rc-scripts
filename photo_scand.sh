#!/bin/sh

PHOTO_SCAND=photo_scand
PHOTO_SCAND_CMD="/sbin/photo_scand"
INOTIFYWAIT=/usr/sbin/inotifywait
lock_time=0
MultimediaShare=`/sbin/getcfg SHARE_DEF defMultimedia -d Qmultimedia -f /etc/config/def_share.info`

_lock()
{
    if [ -f /var/lock/NAS/photo_scan.record.lck ]; then
	if [ "$lock_time" != "0" ]; then
	    now=`date +%s`
	    wait=`expr $now - $lock_time`
	    if [ "$wait" -gt 10 ]; then
		lock_time=0
		/bin/rm -f /var/lock/NAS/photo_scan.record.lck
		echo "unlock"
		return 0
	    fi
	fi
	lock_time=`date +%s`
	return 1
    else
	lock_time=0
	return 0
    fi
}

# start photo scand
/bin/touch /mnt/HDA_ROOT/photo_scan.record
/bin/chown httpdusr /mnt/HDA_ROOT/photo_scan.record
/bin/chmod 666 /mnt/HDA_ROOT/photo_scan.record
/sbin/daemon_mgr $PHOTO_SCAND stop "/sbin/photo_scand"
sleep 1
/sbin/daemon_mgr $PHOTO_SCAND start "/sbin/photo_scand&"

$INOTIFYWAIT -m "/share/${MultimediaShare}" -r -e moved_to -e close_write --format "%w%f" --exclude "Network Trash Folder|.@__thumb|Temporary Items" | while read line
do
    folder=`dirname "${line}"`
    type=${line##*.}
    found_img=1
    if [ "$folder" == "/share/${MultimediaShare}" ]; then
	##echo "folder= $folder ignore"
	continue
    fi

    case "${type}" in
	"PNG" | "png" | "GIF" | "gif" )
	;;
	"jpg" | "JPG" | "bmp" | "BMP" )
	;;
	*)
	found_img=0
	;;
    esac
    if [ $found_img == 1 ]; then
	_lock
	if [ $? == 1 ]; then
	    continue
	fi
	##echo "found img folder=$folder type=$type"
	echo "${folder}" >> /mnt/HDA_ROOT/photo_scan.record
	kill -s USR1 `pidof $PHOTO_SCAND`
    fi
done
